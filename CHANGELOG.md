# Changelog

All notable changes to [ifml-js](https://gitlab.com/fehrlich/ifml-js) are documented here. We use [semantic versioning](http://semver.org/) for releases.
