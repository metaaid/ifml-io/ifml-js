import {
  bootstrapModeler,
  collectTranslations,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import translateModule from 'diagram-js/lib/i18n/translate';
import customTranslateModule from './custom-translate';
import modelingModule from 'lib/features/modeling';
import paletteModule from 'lib/features/palette';
import contextPadModule from 'lib/features/context-pad';
import IfmlAutoPlace from 'lib/features/auto-place';

var diagramXML = require('test/fixtures/ifml/simple.ifml');


// skipping this file during translation extraction

collectTranslations ? describe.skip : describe('i18n - translate', function () {

  beforeEach(bootstrapModeler(diagramXML, {
    modules: [
      coreModule,
      modelingModule,
      paletteModule,
      IfmlAutoPlace,
      contextPadModule,
      translateModule,
      customTranslateModule
    ]
  }));


  it('should translate palette', inject(function (palette) {

    // when
    var handToolEntry = palette.getEntries()['space-tool'];

    // then
    expect(handToolEntry.title).to.equal('Aktiviere das Abstandstool');
  }));


  it('should translate context pad', inject(function (contextPad) {

    // given
    contextPad.open('ViewContainer1');

    // when 
    var deleteEntry = contextPad._current.entries.delete;

    // then
    expect(deleteEntry.title).to.equal('Löschen');
  }));

});