import translate from 'diagram-js/lib/i18n/translate/translate';

export default function customTranslate(template, replacements) {
  if (template === 'Remove') {
    template = 'Löschen';
  }

  if (template === 'Activate the create/remove space tool') {
    template = 'Aktiviere das Abstandstool';
  }

  return translate(template, replacements);
}