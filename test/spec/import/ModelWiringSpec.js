import {
  bootstrapViewer,
  inject
} from 'test/TestHelper';

import {
  is,
  getDi
} from 'lib/util/ModelUtil';


describe('import - model wiring', function () {


  describe('basics', function () {

    var xml = require('../../fixtures/ifml/import/simple.ifml');

    beforeEach(bootstrapViewer(xml));


    it('should wire root element', inject(function (elementRegistry, canvas) {

      // when
      var model = elementRegistry.get('_myModel');
      var vContainer = elementRegistry.get('ViewContainer1');
      var vComponent = elementRegistry.get('List1');

      // then
      expect(vComponent.parent).to.eql(vContainer);
      expect(vContainer.parent).to.eql(model);
      expect(canvas.getRootElement()).to.eql(model);

      expect(is(vContainer, 'ifml:ViewContainer')).to.be.true;
      expect(is(vComponent, 'ifml:ViewComponent')).to.be.true;
    }));


    it('should wire parent child relationship container/component', inject(function (elementRegistry) {

      // when
      var vContainer = elementRegistry.get('ViewContainer1');
      var vComponent = elementRegistry.get('List1');

      // then
      expect(vComponent.parent).to.eql(vContainer);
      expect(vContainer.children).to.contain(vComponent);
    }));

    it('should wire parent child relationship - form', inject(function (elementRegistry) {

      // when
      var form = elementRegistry.get('Form1');
      var field = elementRegistry.get('Form1_SimpleField');


      // then
      expect(field.parent).to.eql(form);
      expect(form.children).to.contain(field);
    }));

    it('should wire all child-relationships', inject(function (elementRegistry) {

      // when
      var vContainer = elementRegistry.get('ViewContainer1');
      expect(vContainer.children.length).to.equal(5);
    }));

    it('should wire label relationship', inject(function (elementRegistry) {

      // when
      var deleteEventShape = elementRegistry.get('Event_delete');
      var label = deleteEventShape.label;

      // then
      expect(label).to.exist;
      expect(label.id).to.equal(deleteEventShape.id + '_label');

      expect(label.labelTarget).to.eql(deleteEventShape);
    }));


    it('should wire businessObject', inject(function (elementRegistry) {

      // when
      var viewContainerShape = elementRegistry.get('ViewContainer1');
      var deleteEventShape = elementRegistry.get('Event_delete');

      var subProcess = viewContainerShape.businessObject,
        startEvent = deleteEventShape.businessObject;

      // then
      expect(subProcess).to.exist;
      expect(is(subProcess, 'ifml:ViewContainer')).to.be.true;

      expect(startEvent).to.exist;
      expect(is(startEvent, 'ifml:Event')).to.be.true;
    }));


    it('should wire shape di', inject(function (elementRegistry) {

      // when
      var viewContainerShape = elementRegistry.get('ViewContainer1');
      var viewContainer = viewContainerShape.businessObject;
      var viewContainerDi = getDi(viewContainerShape);

      // then
      expect(viewContainerDi).to.exist;
      expect(viewContainerDi.modelElement).to.eql(viewContainer);
    }));


    it('should wire connection di', inject(function (elementRegistry) {

      // when
      var flowElement = elementRegistry.get('NavigationFlow_1');

      var sequenceFlow = flowElement.businessObject;
      var sequenceFlowDi = getDi(flowElement);

      // then
      expect(sequenceFlowDi).to.exist;
      expect(sequenceFlowDi.modelElement).to.eql(sequenceFlow);
    }));


    it('should wire label di', inject(function (elementRegistry) {

      // when
      var eventShape = elementRegistry.get('Event_delete');
      var eventLabel = elementRegistry.get('Event_delete_label');

      // assume
      expect(eventShape).to.exist;
      expect(eventLabel).to.exist;

      // label relationship wired
      expect(eventShape.label).to.eql(eventLabel);
      expect(eventLabel.labelTarget).to.eql(eventShape);

      // moddle relationships wired
      expect(eventShape.di).to.exist;
      expect(eventShape.businessObject).to.exist;

      expect(eventShape.di).to.eql(eventLabel.di);
      expect(eventShape.businessObject).to.eql(eventLabel.businessObject);
    }));

  });
});