import IfmlTreeWalker from 'lib/import/IfmlTreeWalker';

import IfmlModdle from 'ifml-moddle';

import {
  find
} from 'min-dash';

import simpleXML from 'test/fixtures/ifml/simple.ifml';
// import collaboration from 'test/fixtures/bpmn/collaboration.bpmn';


describe('import - IfmlTreeWalker', function () {

  it('should expose functions', function () {

    // when
    var walker = createWalker();

    // then
    expect(walker.handleDeferred).to.exist;
    expect(walker.handleDefinitions).to.exist;
    expect(walker.registerDi).to.exist;
  });


  it('should walk elements', function () {

    // given
    var elementSpy = sinon.spy(),
      rootSpy = sinon.spy(),
      errorSpy = sinon.spy();

    var walker = createWalker({
      element: elementSpy,
      root: rootSpy,
      error: errorSpy
    });

    return createModdle(simpleXML).then(function (result) {

      var definitions = result.rootElement;

      // when
      walker.handleDefinitions(definitions);
      // then
      expect(elementSpy.callCount).to.equal(18);
      expect(rootSpy).to.be.calledOnce;
      expect(errorSpy).not.to.be.called;
    });
  });

  it('define multiple di elements for single modelelement should throw error', function () {


    // given
    var elementSpy = sinon.spy(),
      rootSpy = sinon.spy(),
      errorSpy = sinon.spy();

    var walker = createWalker({
      element: elementSpy,
      root: rootSpy,
      error: errorSpy
    });

    return createModdle(simpleXML).then(function (result) {

      var definitions = result.rootElement;
      var modelElements = definitions.ownedElement[0].interactionFlowModel.interactionFlowModelElements;
      var diagramElements = definitions.ownedElement[1].ownedElement;

      var modelElement = findElementWithId(modelElements, 'ViewContainer1');


      // will error
      diagramElements.push({
        modelElement: modelElement
      });

      // when
      walker.handleDefinitions(definitions);

      // then
      expect(elementSpy.callCount).to.equal(18);
      expect(rootSpy.calledOnce).to.be.true;
      expect(errorSpy.calledOnce).to.be.true;
      expect(errorSpy.callCount).to.equal(1);
    });
    // });
  });
});


// helpers //////////

function createModdle(xml) {
  var moddle = new IfmlModdle();

  return moddle.fromXML(xml);
}

function createWalker(listeners) {

  listeners = listeners || {};

  var visitor = {
    element: function (element, parent) {
      return listeners.element && listeners.element(element, parent);
    },
    root: function (root) {
      return listeners.root && listeners.root(root);
    },
    error: function (message, context) {
      return listeners.error && listeners.error(message, context);
    }
  };

  return new IfmlTreeWalker(visitor, function () {});
}

function findElementWithId(definitions, id) {

  function findElement(element) {
    if (element.$id === id) {
      return element;
    }

    if (element.viewElements) {
      return find(element.viewElements, function (element) {
        var foundElement = findElement(element);

        return foundElement && foundElement.$id === id;
      });
    }
  }

  return definitions.reduce(function (foundElement, rootElement) {
    if (rootElement.$id === id) {
      return rootElement;
    } else {
      return findElement(rootElement) || foundElement;
    }
  }, null);
}