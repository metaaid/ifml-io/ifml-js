import TestContainer from 'mocha-test-container-support';

import Diagram from 'diagram-js/lib/Diagram';
import IfmlModdle from 'ifml-moddle';

import {
  ImportIFMLDiagram
} from 'lib/import/Importer';

import CoreModule from 'lib/core';

import {
  find
} from 'min-dash';

import { is } from 'lib/util/ModelUtil';


describe('import - Importer', function() {

  function createDiagram(container, modules) {
    return new Diagram({
      canvas: { container: container },
      modules: modules
    });
  }

  var diagram;

  beforeEach(function() {
    diagram = createDiagram(TestContainer.get(this), [CoreModule]);
  });


  function runImport(diagram, xml, diagramId) {

    var moddle = new IfmlModdle();

    return moddle.fromXML(xml).then(function(result) {

      var definitions = result.rootElement;

      var selectedDiagram = find(definitions.ownedElement, function(element) {
        return diagramId && element.$id === diagramId;
      });

      return ImportIFMLDiagram(diagram, definitions, selectedDiagram);
    }).then(function(result) {

      return result;
    });
  }


  describe('events', function() {

    it('should fire <import.render.start> and <import.render.complete>', function() {

      // given
      var xml = require('../../fixtures/ifml/simple.ifml');

      var eventCount = 0;

      var eventBus = diagram.get('eventBus');

      // log events
      eventBus.on('import.render.start', function(event) {
        expect(event.definitions).to.exist;

        eventCount++;
      });

      eventBus.on('import.render.complete', function(event) {
        expect(event).to.have.property('error');
        expect(event).to.have.property('warnings');

        eventCount++;
      });

      // when
      return runImport(diagram, xml).then(function() {

        // then
        expect(eventCount).to.equal(2);
      });
    });


    it('should fire <modelElement.added> during import', function() {

      // given
      var xml = require('../../fixtures/ifml/allElements.ifml');

      var eventCount = 0;

      // log events
      diagram.get('eventBus').on('modelElement.added', function(e) {
        eventCount++;
      });

      // when
      return runImport(diagram, xml).then(function() {

        // then
        expect(eventCount).to.equal(28);
      });
    });

  });



  describe('position', function() {

    var xml = require('../../fixtures/ifml/import/position-testcase.ifml');

    it('should round shape coordinates', function() {

      // given
      var events = {};

      // log events
      diagram.get('eventBus').on('modelElement.added', function(e) {

        events[e.element.id] = e.element;
      });

      return runImport(diagram, xml).then(function() {

        // round up
        expect(events.vc2.x).to.equal(Math.round(541));
        expect(events.vc2.y).to.equal(Math.round(211));

        // round down
        expect(events.vc1.x).to.equal(Math.round(270));
        expect(events.vc1.y).to.equal(Math.round(210));
      });
    });


    it('should round shape dimensions', function() {

      // given
      var events = {};

      // log events
      diagram.get('eventBus').on('modelElement.added', function(e) {

        events[e.element.id] = e.element;
      });

      return runImport(diagram, xml).then(function() {

        // round down
        expect(events.vc1.height).to.equal(Math.round(80.4));
        expect(events.vc1.width).to.equal(Math.round(180.4));

      });
    });

  });


  describe('order', function() {

    it('should import flows behind other interaction elements', function() {

      var xml = require('../../fixtures/ifml/import/position-testcase.ifml');

      // given
      var elementRegistry = diagram.get('elementRegistry');
      var canvas = diagram.get('canvas');


      return runImport(diagram, xml).then(function() {

        // when
        var processShape = canvas.getRootElement();

        var children = processShape.children;

        // lanes 
        // connections
        // other elements
        var correctlyOrdered = [].concat(
          children.filter(function(c) { return is(c, 'ifml:ViewComponent'); }),
          children.filter(function(c) { return !is(c, 'ifml:ViewComponent')}),
        );

        // then
        expectChildren(diagram, processShape, correctlyOrdered);

      });
    });
  });


  describe('elements', function() {

    function wrapElement(id, parent = "_myModel"){
      return { type: 'add', semantic: id, di: id + "_di", diagramElement: id, parent: parent }
    }

    function checkForElements(xml, expectedElements, diagramId = undefined, rootElement = null) {

      var elements = [];

      // log
      diagram.get('eventBus').on('modelElement.added', function(e) {
        if(!is(e.element, 'ifml:InteractionFlowModel')){

          elements.push({
            type: 'add',
            semantic: e.element.businessObject.$id,
            di: e.element.di.$id,
            diagramElement: e.element && e.element.id,
            parent: e.element.parent.id,
          });
        }
      });

      // when
      return runImport(diagram, xml, diagramId).then(function() {
        // then
        expect(elements).to.eql(expectedElements);
        if(rootElement){
          expect(diagram.get('elementRegistry').get(rootElement)).to.equal(diagram.get('canvas').getRootElement());
        }
      });
    }

    it('should import action', function() {

      // given
      var xml = require('../../fixtures/ifml/simple/action.ifml');
      return checkForElements(xml, [
        wrapElement('Action1' ),
      ]);
    });
    it('should import viewcomponents', function() {

      // given
      var xml = require('../../fixtures/ifml/simple/component.ifml');
      return checkForElements(xml, [
      wrapElement('ViewComponent1'),
      ]);
    });
    it('should import viewcontainer', function() {
      var xml = require('../../fixtures/ifml/simple/container.ifml');
      return checkForElements(xml, [
      wrapElement('ViewContainer1'),
      ]);
    });
    it('should import databinding', function() {
      var xml = require('../../fixtures/ifml/simple/databinding.ifml');
      return checkForElements(xml, [
      wrapElement('ViewComponent1'),
      wrapElement('DataBinding1', 'ViewComponent1'),
      wrapElement('DataBinding2', 'ViewComponent1'),
      ]);
    });
    it('should import dataFlow', function() {
      var xml = require('../../fixtures/ifml/simple/dataFlow.ifml');
      return checkForElements(xml, [
        wrapElement('ViewComponent1'),
        wrapElement('ViewComponent2'),
        wrapElement('Flow1'),
      ]);
    });
    it('should import form-field', function() {
      var xml = require('../../fixtures/ifml/simple/form-field.ifml');
      return checkForElements(xml, [
        wrapElement('myFormId'),
        wrapElement('SimpleField1', 'myFormId'),
      ]);
    });
    it('should import ParameterBinding', function() {
      var xml = require('../../fixtures/ifml/simple/form-action-binded.ifml');
      return checkForElements(xml, [
        wrapElement('myFormId'),
        wrapElement('SimpleField1', 'myFormId'),
        wrapElement('Event1', 'myFormId'),
        wrapElement('Action1'),
        wrapElement('Flow1'),
        wrapElement('myParameterbindingGroup', 'Flow1'),
        wrapElement('myParameterbinding', 'myParameterbindingGroup'),
      ]);
    });
    it('should import navigationFlow', function() {
      var xml = require('../../fixtures/ifml/simple/navigationFlow.ifml');
      return checkForElements(xml, [
        wrapElement('ViewComponent1'),
        wrapElement('Event', 'ViewComponent1'),
        wrapElement('ViewComponent2'),
        wrapElement('NavigationFlow'),
      ]);
    });
    it('should import navigationFlow', function() {
      var xml = require('../../fixtures/ifml/simple/module.ifml');
      return checkForElements(xml, [
        wrapElement('ModuleDefinition1'),
      ]);
    });
    it('should import navigationFlow', function() {
      var xml = require('../../fixtures/ifml/simple/port.ifml');
      return checkForElements(xml, [
        wrapElement('ModuleDefinition1'),
        wrapElement('PortDefinition1', 'ModuleDefinition1'),
      ]);
    });



    it('should import single diagram from multiple diagrams 2', function() {

      var xml = require('../../fixtures/ifml/multiple-diagrams.ifml');
      return checkForElements(xml, [
        wrapElement('ViewContainer2', '_myModel2')
      ], 'dia2', '_myModel2');
    });

    it('should import single diagram from multiple diagrams 1', function() {

      var xml = require('../../fixtures/ifml/multiple-diagrams.ifml');
      return checkForElements(xml, [
        wrapElement('ViewContainer1', '_myModel'),
        wrapElement('ViewComponent1', 'ViewContainer1'),
        wrapElement('Event1', 'ViewComponent1'),
        wrapElement('ViewComponent2', 'ViewContainer1'),
        wrapElement('Flow1', '_myModel'),
        wrapElement('Flow2', '_myModel'),
      ], 'dia1', '_myModel');
    });

  });


  describe('forgiveness', function() {

    it('should import multiple DIs', function() {

      // given
      var xml = require('../../fixtures/ifml/error/multiple-dis.ifml');

      // when
      return runImport(diagram, xml).then(function(result) {

        var warnings = result.warnings;

        var expectedMessage = 'multiple DI elements defined for <ifml:ViewComponent id="ViewComponent1" />';

        expect(warnings).to.have.length(1);
        expect(warnings[0].message).to.equal(expectedMessage);

      });
    });


    it('should import sequence flow without waypoints', function() {

      // given
      var xml = require('../../fixtures/ifml/error/dataFlow-nowaypoints.ifml');

      // when
      return runImport(diagram, xml).then(function(result) {

        var warnings = result.warnings;

        // then
        expect(warnings).to.be.empty;

      });
    });
  });


});



// helpers //////////////////////

function expectChildren(diagram, parent, children) {

  return diagram.invoke(function(elementRegistry) {

    // verify model is consistent
    expect(parent.children).to.eql(children);

    // verify SVG is consistent
    var parentGfx = elementRegistry.getGraphics(parent);

    var expectedChildrenGfx = children.map(function(c) {
      return elementRegistry.getGraphics(c);
    });

    var existingChildrenGfx = Array.prototype.map.call(parentGfx.childNodes, function(c) {
      return c.querySelector('.djs-element');
    });

    expect(existingChildrenGfx).to.eql(expectedChildrenGfx);
  });

}
