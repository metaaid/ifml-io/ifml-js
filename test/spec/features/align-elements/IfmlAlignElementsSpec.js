import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';



import alignElementsModule from 'diagram-js/lib/features/align-elements';
import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';
import IfmlAutoPlace from 'lib/features/auto-place';


describe('features/align-elements', function () {

  var testModules = [
    IfmlAutoPlace, alignElementsModule, modelingModule, coreModule
  ];

  var basicXML = require('../../../fixtures/ifml/simple.ifml');

  beforeEach(bootstrapModeler(basicXML, {
    modules: testModules
  }));

  describe('integration', function () {

    it('should align to the left', inject(function (elementRegistry, alignElements) {

      // given
      var taskBoundEvt = elementRegistry.get('List1'),
        task = elementRegistry.get('Form1'),
        subProcess = elementRegistry.get('Action_Save'),
        endEvent = elementRegistry.get('Action_Done'),
        vc = elementRegistry.get('ViewContainer1'),
        elements = [taskBoundEvt, task, subProcess, endEvent, vc];

      // when
      alignElements.trigger(elements, 'left');

      let leftX = 190;

      // then
      expect(taskBoundEvt.x).to.equal(leftX);
      expect(task.x).to.equal(leftX);
      expect(subProcess.x).to.equal(leftX);
      expect(endEvent.x).to.equal(leftX);
      expect(vc.x).to.equal(leftX);
    }));


    it('should align to the right', inject(function (elementRegistry, alignElements) {

      // given
      var taskBoundEvt = elementRegistry.get('List1'),
        task = elementRegistry.get('Form1'),
        subProcess = elementRegistry.get('Action_Save'),
        endEvent = elementRegistry.get('Action_Done'),
        vc = elementRegistry.get('ViewContainer1'),
        elements = [taskBoundEvt, task, subProcess, endEvent, vc];

      // when
      alignElements.trigger(elements, 'right');

      // then
      expect(taskBoundEvt.x).to.equal(1090);
      expect(task.x).to.equal(1090);
      expect(subProcess.x).to.equal(1390);
      expect(endEvent.x).to.equal(1390);
      expect(vc.x).to.equal(190);
    }));


    it('should align to the center', inject(function (elementRegistry, alignElements) {

      // given
      var taskBoundEvt = elementRegistry.get('List1'),
        task = elementRegistry.get('Form1'),
        subProcess = elementRegistry.get('Action_Save'),
        endEvent = elementRegistry.get('Action_Done'),
        vc = elementRegistry.get('ViewContainer1'),
        elements = [taskBoundEvt, task, subProcess, endEvent, vc];

      // when
      alignElements.trigger(elements, 'center');

      // then
      expect(taskBoundEvt.x).to.equal(640);
      expect(task.x).to.equal(640);
      expect(subProcess.x).to.equal(790);
      expect(endEvent.x).to.equal(790);
      expect(vc.x).to.equal(190);
    }));


    it('should align to the top', inject(function (elementRegistry, alignElements) {

      // given
      var taskBoundEvt = elementRegistry.get('List1'),
        task = elementRegistry.get('Form1'),
        subProcess = elementRegistry.get('Action_Save'),
        endEvent = elementRegistry.get('Action_Done'),
        vc = elementRegistry.get('ViewContainer1'),
        elements = [taskBoundEvt, task, subProcess, endEvent, vc];

      // when
      alignElements.trigger(elements, 'top');

      // then
      expect(taskBoundEvt.y).to.equal(140);
      expect(task.y).to.equal(140);
      expect(subProcess.y).to.equal(140);
      expect(endEvent.y).to.equal(140);
      expect(vc.y).to.equal(140);
    }));


    it('should align to the bottom', inject(function (elementRegistry, alignElements) {

      // given
      var taskBoundEvt = elementRegistry.get('List1'),
        task = elementRegistry.get('Form1'),
        subProcess = elementRegistry.get('Action_Save'),
        endEvent = elementRegistry.get('Action_Done'),
        vc = elementRegistry.get('ViewContainer1'),
        elements = [taskBoundEvt, task, subProcess, endEvent, vc];

      // when
      alignElements.trigger(elements, 'bottom');

      // then
      expect(taskBoundEvt.y).to.equal(610);
      expect(task.y).to.equal(610);
      expect(subProcess.y).to.equal(730);
      expect(endEvent.y).to.equal(730);
      expect(vc.y).to.equal(140);
    }));


    it('should align to the middle', inject(function (elementRegistry, alignElements) {

      // given
      var taskBoundEvt = elementRegistry.get('List1'),
        task = elementRegistry.get('Form1'),
        subProcess = elementRegistry.get('Action_Save'),
        endEvent = elementRegistry.get('Action_Done'),
        vc = elementRegistry.get('ViewContainer1'),
        elements = [taskBoundEvt, task, subProcess, endEvent, vc];

      // when
      alignElements.trigger(elements, 'middle');

      // then
      expect(taskBoundEvt.y).to.equal(290);
      expect(task.y).to.equal(290);
      expect(subProcess.y).to.equal(350);
      expect(endEvent.y).to.equal(350);
      expect(vc.y).to.equal(55);
    }));

  });

});