import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

var pick = require('min-dash').pick;

var getBBox = require('diagram-js/lib/util/Elements').getBBox;

var getParent = require('lib/features/modeling/util/ModelingUtil').getParent;


import editorActionsModule from 'lib/features/editor-actions';
import selectionModule from 'diagram-js/lib/features/selection';
import alignElementsModule from 'diagram-js/lib/features/align-elements';
import distributeElementsModule from 'diagram-js/lib/features/distribute-elements';
import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';

var basicXML = require('../../../fixtures/ifml/allElements.ifml');
var collaborationXML = require('../../../fixtures/ifml/allElements.ifml');


describe('features/editor-actions', function() {


  describe('#alignElements', function() {

    beforeEach(bootstrapModeler(basicXML, {
      modules: [
        selectionModule,
        alignElementsModule,
        editorActionsModule,
        modelingModule,
        coreModule
      ]
    }));


    it('should align items', inject(
      function(elementRegistry, selection, editorActions) {

        // given
        var elementIds = [ 'Menu1', 'List1', 'ViewComponent1' ];
        var elements = elementIds.map(function(id) {
          return elementRegistry.get(id);
        });

        // when
        selection.select(elements);
        editorActions.trigger('alignElements', { type: 'middle' });

        // then
        expect(elements.map(function(e) {
          return e.y + e.height / 2;
        })).to.eql([ 370, 370, 370 ]);
      }
    ));


    it('should not align if too few elements', inject(
      function(elementRegistry, eventBus, editorActions, selection) {

        // given
        var elementIds = [ 'List1' ];
        var elements = elementIds.map(function(id) {
          return elementRegistry.get(id);
        });

        var changedSpy = sinon.spy();

        // when
        eventBus.once('commandStack.changed', changedSpy);

        selection.select(elements);
        editorActions.trigger('alignElements', { type: 'center' });

        // then
        expect(changedSpy).not.to.have.been.called;
      }
    ));

  });


  describe('#distributeElements', function() {

    beforeEach(bootstrapModeler(basicXML, {
      modules: [
        selectionModule,
        distributeElementsModule,
        editorActionsModule,
        modelingModule,
        coreModule
      ]
    }));


    it('should distribute items', inject(
      function(elementRegistry, selection, editorActions) {

        // given
        var elementIds = [ 'Menu1', 'List1', 'ViewComponent1' ];
        var elements = elementIds.map(function(id) {
          return elementRegistry.get(id);
        });

        // when
        selection.select(elements);
        editorActions.trigger('distributeElements', { type: 'horizontal' });

        // then
        expect(elements.map(function(e) {
          return e.x + e.width / 2;
        })).to.eql([ 740, 540, 310 ]);
      }
    ));


    it('should not distribute if too few elements', inject(
      function(elementRegistry, eventBus, editorActions, selection) {

        // given
        var elementIds = [ 'StartEvent_1', 'UserTask_1' ];
        var elements = elementIds.map(function(id) {
          return elementRegistry.get(id);
        });

        var changedSpy = sinon.spy();

        // when
        eventBus.once('commandStack.changed', changedSpy);

        selection.select(elements);
        editorActions.trigger('distributeElements', { type: 'horizontal' });

        // then
        expect(changedSpy).not.to.have.been.called;
      }
    ));

  });

});
