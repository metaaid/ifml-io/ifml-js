import {
  bootstrapModeler,
  getIfmlJS,
  inject
} from 'test/TestHelper';

import {
  createCanvasEvent as canvasEvent
} from '../../../util/MockEvents';

import {
  pick
} from 'min-dash';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';

import { isMac } from 'diagram-js/lib/util/Platform';

var invertModifier = isMac() ? { metaKey: true } : { ctrlKey: true };


describe('features/modeling - create/remove space', function() {

  describe('create space', function() {

    var diagramXML = require("./SpaceTool.basic.ifml");

    var testModules = [ coreModule, modelingModule ];

    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


    it('should create space to the right', inject(function(elementRegistry, spaceTool) {

      // given
      var flowLeft = elementRegistry.get('FlowLeft'),
          viewcontainer = elementRegistry.get('ViewContainer1'),
          viewComponentRight = elementRegistry.get('ViewComponentRight');

      var viewcontainerPosition_old = getPosition(viewcontainer),
          viewComponentRightPosition_old = getPosition(viewComponentRight),
          flowLeftWaypoints = flowLeft.waypoints;

      // when
      makeSpace({ x: 285, y: 80 }, { dx: 50 }); 

      // then
      expect(viewcontainer).to.have.diPosition({
        x: viewcontainerPosition_old.x + 50,
        y: viewcontainerPosition_old.y
      });

      expect(viewComponentRight).to.have.diPosition({
        x: viewComponentRightPosition_old.x + 50,
        y: viewComponentRightPosition_old.y
      });

      expect(flowLeft).to.have.diWaypoints([
        flowLeftWaypoints[0],
        { x: flowLeftWaypoints[1].x + 50, y: flowLeftWaypoints[1].y}
      ]);
    }));


    it('should create space downwards', inject(function(elementRegistry, modeling, factory) {

      // given
      var ViewComponentLeft = elementRegistry.get('ViewComponentLeft'),
          flowLeft = elementRegistry.get('FlowLeft'),
          viewContainer = elementRegistry.get('ViewContainer1'),
          viewComponentRight = elementRegistry.get('ViewComponentRight');

      var ViewComponentLeftPosition_old = getPosition(ViewComponentLeft),
          viewContainerPosition_old = getPosition(viewContainer),
          viewComponentRightPosition_old = getPosition(viewComponentRight),
          flowLeftWaypoints = flowLeft.waypoints;

      // when
      makeSpace({ x: 330, y: 50 }, { dy: 50 });

      // then
      expect(ViewComponentLeft).to.have.diPosition({
        x: ViewComponentLeftPosition_old.x,
        y: ViewComponentLeftPosition_old.y + 50
      });

      expect(viewContainer).to.have.diPosition({
        x: viewContainerPosition_old.x,
        y: viewContainerPosition_old.y + 50
      });

      expect(viewComponentRight).to.have.diPosition({
        x: viewComponentRightPosition_old.x,
        y: viewComponentRightPosition_old.y + 50
      });

      expect(flowLeft).to.have.diWaypoints([
        { x: flowLeftWaypoints[0].x, y: flowLeftWaypoints[0].y + 50 },
        { x: flowLeftWaypoints[1].x, y: flowLeftWaypoints[1].y + 50 }
      ]);
    }));


    it('should remove space to the left', inject(function(elementRegistry, modeling, factory) {

      // given
      var flowLeft = elementRegistry.get('FlowLeft'),
          viewContainer = elementRegistry.get('ViewContainer1'),
          viewComponentRight = elementRegistry.get('ViewComponentRight');

      var viewContainerPosition_old = getPosition(viewContainer),
          viewComponentRightPosition_old = getPosition(viewComponentRight),
          flowLeftWaypoints = flowLeft.waypoints;

      // when
      makeSpace({ x: 280, y: 100 }, { dx: -50 });

      // then
      expect(viewContainer).to.have.diPosition({
        x: viewContainerPosition_old.x - 50,
        y: viewContainerPosition_old.y
      });

      expect(viewComponentRight).to.have.diPosition({
        x: viewComponentRightPosition_old.x - 50,
        y: viewComponentRightPosition_old.y
      });

      expect(flowLeft).to.have.diWaypoints([
            { x: flowLeftWaypoints[0].x, y: flowLeftWaypoints[0].y },
            { x: flowLeftWaypoints[1].x-50, y: flowLeftWaypoints[1].y }
      ]);
    }));


    it('should resize to the right', inject(function(elementRegistry, modeling) {

      // given
      var task = elementRegistry.get('ViewComponentInner'),
          viewContainer = elementRegistry.get('ViewContainer1'),
          viewComponentRight = elementRegistry.get('ViewComponentRight');

      var viewContainerBounds_old = getBounds(viewContainer),
          viewComponentRightPosition_old = getPosition(viewComponentRight),
          taskPosition_old = getPosition(task);

      // when
      makeSpace({ x: 450, y: 100 }, { dx: 50 });

      // then
      expect(viewContainer).to.have.diBounds({
        x: viewContainerBounds_old.x,
        y: viewContainerBounds_old.y,
        width: viewContainerBounds_old.width + 50,
        height: viewContainerBounds_old.height
      });

      expect(viewComponentRight).to.have.diPosition({
        x: viewComponentRightPosition_old.x + 50,
        y: viewComponentRightPosition_old.y
      });

      expect(task).to.have.diPosition(taskPosition_old);
    }));


    it('should create space to the left', inject(function(elementRegistry, modeling) {

      // given
      var ViewComponentLeft = elementRegistry.get('ViewComponentLeft');

      var ViewComponentLeftBounds_old = getBounds(ViewComponentLeft);
 
      // when
      makeSpace({ x: 80, y: 100 }, { dx: -50 }, true);

      // then
      expect(ViewComponentLeft).to.have.diBounds({
        x: ViewComponentLeftBounds_old.x - 50,
        y: ViewComponentLeftBounds_old.y,
        width: ViewComponentLeftBounds_old.width,
        height: ViewComponentLeftBounds_old.height
      });
    }));

  });

});



// helpers ////////////////

function makeSpace(start, delta, invert) {

  if (delta.dx && delta.dy) {
    throw new Error('must define either <dx> or <dy>');
  }

  var modifier = invert ? invertModifier : {};

  var end = {
    x: start.x + (delta.dx || 0),
    y: start.y + (delta.dy || 0)
  };

  return getIfmlJS().invoke(function(spaceTool, dragging) {
    spaceTool.activateMakeSpace(canvasEvent(start));

    dragging.move(canvasEvent(end, modifier));

    dragging.end();
  });

}

function getPosition(element) {
  return pick(element, [ 'x', 'y' ]);
}

function getBounds(element) {
  return pick(element, [ 'x', 'y', 'width', 'height' ]);
}