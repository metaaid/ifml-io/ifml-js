import {
  bootstrapModeler,
  getIfmlJS,
  inject
} from 'test/TestHelper';

import {
  createCanvasEvent as canvasEvent
} from 'test/util/MockEvents';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';


describe('features/modeling - ModelingFeedback', function() {

  var collaborationDiagramXML = require('../../../../fixtures/ifml/allElements.ifml');

  beforeEach(bootstrapModeler(collaborationDiagramXML, {
    modules: [
      coreModule,
      modelingModule
    ]
  }));


  it('should indicate', inject(function(create, canvas, elementFactory, dragging) {

    // given
    var task = elementFactory.createShape({ type: 'ifml:Event' });

    var model = canvas.getRootElement();
    var collaborationGfx = canvas.getGraphics(model);

    create.start(canvasEvent({ x: 100, y: 100 }), task);
    dragging.hover({ element: model, gfx: collaborationGfx });

    // when
    dragging.end();

    // then
    expectTooltip('error', 'events need to be attached to IFML-Elements');
  }));

});


function expectTooltip(cls, message) {

  return getIfmlJS().invoke(function(canvas) {

    var tooltipEl = document.querySelector('[data-tooltip-id]', canvas.getContainer());

    expect(tooltipEl.textContent).to.eql(message);
    expect(tooltipEl.classList.contains(cls));
  });
}