import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';
import { getMid } from 'diagram-js/lib/layout/LayoutUtil';
import { is } from '../../../../../lib/util/ModelUtil';


describe('features/modeling - CreateBehavior for Modules', function() {

  var allElementsDiagram = require('../../../../fixtures/ifml/allElements.ifml');

  beforeEach(bootstrapModeler(allElementsDiagram, {
    modules: [
      coreModule,
      modelingModule
    ]
  }));


  it('Modules should always have ModuleDefinition', inject(
    function(elementFactory, elementRegistry, modeling, canvas) {

      // given
      var root = canvas.getRootElement();

      var event = elementFactory.createShape({
        type: 'ifml:Module'
      });
      // var eventId = event.id;
      
      // when
      var shape = modeling.createShape(event, {x: 0, y:0}, root);
      
      // then
      expect(shape.businessObject.get('moduleDefinition')).to.exist;
      expect(is(shape.businessObject.get('moduleDefinition'), 'ifml:ModuleDefinition')).to.be.true;
    }
  ));

  it('Ports should always have PortDefinition', inject(
    function(elementFactory, elementRegistry, modeling, canvas) {

      // given
      var root = elementRegistry.get("ModuleDefinition1");

      var event = elementFactory.createShape({
        type: 'ifml:Port'
      });
      // var eventId = event.id;
      
      // when
      var shape = modeling.createShape(event, {x: 0, y:0}, root);
      
      // then
      expect(shape.businessObject.get('portDefinition')).to.exist;
      expect(is(shape.businessObject.get('portDefinition'), 'ifml:PortDefinition')).to.be.true;
    }
  ));

  it('should create ActionEvent on Action', inject(
    function(elementFactory, elementRegistry, modeling) {

      // given
      var viewcomponent = elementRegistry.get('Action1');

      var event = elementFactory.createShape({
        type: 'ifml:Event'
      });
      // var eventId = event.id;
      
      // when
      var shape = modeling.createShape(event, getMid(viewcomponent), viewcomponent);
      
      // then
      expect(is(shape, 'ifml:ActionEvent')).to.be.true;
    }
  ));

});