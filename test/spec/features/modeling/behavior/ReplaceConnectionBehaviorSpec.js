import { bootstrapModeler, inject } from 'test/TestHelper';

import { is } from 'lib/util/ModelUtil';

import { find } from 'min-dash';

import modelingModule from 'lib/features/modeling';
import moveModule from 'diagram-js/lib/features/move';
import coreModule from 'lib/core';

function getConnection(source, target, connectionOrType) {
  return find(source.businessObject.outInteractionFlows, function (c) {
    return (
      c.targetInteractionFlowElement === target.businessObject &&
      (typeof connectionOrType === 'string'
        ? is(c, connectionOrType)
        : c === connectionOrType)
    );
  });
}

function expectConnected(source, target, connectionOrType) {
  expect(getConnection(source, target, connectionOrType)).to.exist;
}

function expectNotConnected(source, target, connectionOrType) {
  expect(getConnection(source, target, connectionOrType)).not.to.exist;
}

describe('features/modeling - replace connection', function () {
  var testModules = [coreModule, moveModule, modelingModule];

  describe('should replace NavigationFlow <> DataFlow', function () {
    var allElementsDiagram = require('./ReplaceConnectionBehaviorSpec.ifml');

    beforeEach(
      bootstrapModeler(allElementsDiagram, {
        modules: testModules,
      })
    );

    var element;

    beforeEach(inject(function (elementRegistry) {
      element = function (id) {
        return elementRegistry.get(id);
      };
    }));

    describe('after reconnecting', function () {
      it('sequence flow from a participant', inject(function (modeling) {
        // given
        var viewcomponent = element('ViewComponent1'),
          viewComponent2 = element('ViewComponent2'),
          connection = element('NavigationFlow');

        var newWaypoints = [
          {
            x: viewcomponent.x,
            y: viewcomponent.y + viewcomponent.height / 2 + 30,
          },
          {
            x: viewComponent2.x + viewComponent2.width,
            y: viewComponent2.y + viewComponent2.height / 2,
          },
        ];

        // when
        modeling.reconnect(
          connection,
          viewcomponent,
          connection.targetInteractionFlowElement,
          newWaypoints
        );

        // then
        expectConnected(viewcomponent, viewComponent2, 'ifml:DataFlow');
      }));
    });

    describe('after reconnecting start', function () {
      it('sequence flow from a participant', inject(function (modeling) {
        // given
        var viewcomponent = element('ViewComponent1'),
          ViewComponent2 = element('ViewComponent2'),
          connection = element('NavigationFlow');

        var newWaypoints = [
          { x: viewcomponent.x + 200, y: viewcomponent.y },
          { x: ViewComponent2.x, y: ViewComponent2.y + 50 },
        ];

        // when
        modeling.reconnectStart(connection, viewcomponent, newWaypoints);

        // then
        expectConnected(viewcomponent, ViewComponent2, 'ifml:DataFlow');
      }));
    });

    describe('moving single shape', function () {
      it('moving source to a module', inject(function (modeling) {
        // given
        var taskShape = element('ViewComponent1'),
          targetShape = element('ModuleDefinition1');

        // when
        modeling.moveElements([taskShape], { x: 0, y: 330 }, targetShape);

        // then
        expect(taskShape.parent).to.eql(targetShape);

        expectNotConnected(
          element('Event'),
          element('ViewComponent2'),
          'ifml:NavigationFlow'
        );
      }));

      it('moving source target a module', inject(function (modeling) {
        // given
        var taskShape = element('ViewComponent2'),
          targetShape = element('ModuleDefinition1');

        // when
        modeling.moveElements([taskShape], { x: 0, y: 330 }, targetShape);

        // then
        expect(taskShape.parent).to.eql(targetShape);

        expectNotConnected(
          element('Event'),
          element('ViewComponent2'),
          'ifml:NavigationFlow'
        );
      }));

      it('moving source target a module', inject(function (
        modeling,
        commandStack
      ) {
        // given
        var taskShape = element('ViewComponent2'),
          targetShape = element('ModuleDefinition1');

        // when
        modeling.moveElements([taskShape], { x: 0, y: 330 }, targetShape);
        commandStack.undo();

        // // then
        expectConnected(
          element('Event'),
          element('ViewComponent2'),
          'ifml:NavigationFlow'
        );
      }));
    });

    describe('moving multiple shapes', function () {
      it('execute', inject(function (modeling, elementRegistry) {
        // given
        var elements = [
          element('ViewComponent1'),
          element('ViewComponent2'),
          element('Event'),
          element('NavigationFlow'),
        ];

        var targetShape = element('ModuleDefinition1');
        // var viewcomponent = element('ViewComponent1'),
        //     viewComponent2 = element('ViewComponent2'),
        //     connection = element('NavigationFlow');

        // when
        modeling.moveElements(elements, { x: 0, y: 330 }, targetShape);

        // then
        expectConnected(
          element('Event'),
          element('ViewComponent2'),
          'ifml:NavigationFlow'
        );
      }));
    });
  });
});
