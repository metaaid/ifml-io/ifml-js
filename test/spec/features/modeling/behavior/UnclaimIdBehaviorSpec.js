import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';


describe('features/modeling - unclaim id', function() {

  var testModules = [ coreModule, modelingModule ];

  var diagramXML = require('../../../../fixtures/ifml/allElements.ifml');

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  it('should unclaim ID of shape', inject(function(elementRegistry, moddle, modeling) {

    // given
    var startEvent = elementRegistry.get('Event1');

    // when
    modeling.removeElements([ startEvent ]);

    // then
    expect(moddle.ids.assigned('Event1')).to.be.false;
  }));


  it('should unclaim ID of childevent', inject(function(elementRegistry, moddle, modeling) {

    // given
    var participant = elementRegistry.get('ViewContainer1');

    // when
    modeling.removeElements([ participant ]);

    // then
    expect(moddle.ids.assigned('Event1')).to.be.false;
  }));


  it('should unclaim ID of connection', inject(function(elementRegistry, moddle, modeling) {

    // given
    var sequenceFlow = elementRegistry.get('ViewComponent1');

    // when
    modeling.removeElements([ sequenceFlow ]);

    // then
    expect(moddle.ids.assigned('Dataflow1')).to.be.false;
  }));
});