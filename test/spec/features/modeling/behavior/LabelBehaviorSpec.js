import { bootstrapModeler, inject } from 'test/TestHelper';

import { DEFAULT_LABEL_SIZE, getExternalLabelMid } from 'lib/util/LabelUtil';

import { getDi } from 'lib/util/ModelUtil';

import { assign, map, pick } from 'min-dash';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';
// import gridSnappingModule from 'lib/features/grid-snapping';

describe('behavior - LabelBehavior', function () {
  var diagramXML = require('../../../../fixtures/ifml/allElements.ifml');

  beforeEach(
    bootstrapModeler(diagramXML, {
      modules: [modelingModule, coreModule],
    })
  );

  describe('updating name property', function () {
    it('should update label', inject(function (
      elementRegistry,
      eventBus,
      modeling
    ) {
      // given
      var startEvent = elementRegistry.get('ViewComponent1'),
        spy = sinon.spy();

      eventBus.once('commandStack.element.updateLabel.execute', spy);

      // when
      modeling.updateProperties(startEvent, {
        name: 'bar',
      });

      // then
      expect(startEvent.businessObject.name).to.equal('bar');
      expect(spy).to.have.been.called;
    }));

    it('should create label', inject(function (
      elementRegistry,
      eventBus,
      modeling
    ) {
      // given
      var startEvent = elementRegistry.get('Event_submit_Form1'),
        spy = sinon.spy();

      eventBus.once('commandStack.element.updateLabel.execute', spy);

      // when
      modeling.updateProperties(startEvent, {
        name: 'foo',
      });

      // then
      var labelShape = startEvent.label;

      expect(labelShape).to.exist;
      expect(startEvent.businessObject.name).to.equal('foo');
      expect(spy).to.have.been.called;
    }));
  });

  describe('add label', function () {
    it('should NOT add to event without name', inject(function (
      elementFactory,
      elementRegistry,
      modeling
    ) {
      // given
      var parentShape = elementRegistry.get('ViewComponent1'),
        newShapeAttrs = {
          type: 'ifml:Event',
        };

      // when
      var newShape = modeling.createShape(
        newShapeAttrs,
        { x: 50, y: 50 },
        parentShape
      );

      // then
      expect(newShape.label).not.to.exist;
    }));

    it('should not add label if created shape is label', inject(function (
      elementFactory,
      elementRegistry,
      modeling,
      textRenderer,
      factory
    ) {
      // given
      var parentShape = elementRegistry.get('ViewContainer1');

      var createLabelSpy = sinon.spy(modeling, 'createLabel');

      var eventBo = factory.create('ifml:Event', {
        name: 'Foo',
      });

      var event = elementFactory.createShape({
        type: 'ifm:Event',
        businessObject: eventBo,
      });

      modeling.createElements([event], { x: 50, y: 50 }, parentShape, {
        createElementsBehavior: false,
      });

      var externalLabelMid = getExternalLabelMid(event);

      var externalLabelBounds = textRenderer.getExternalLabelBounds(
        DEFAULT_LABEL_SIZE,
        'Foo'
      );

      var label = elementFactory.createLabel({
        businessObject: eventBo,
        labelTarget: event,
        width: externalLabelBounds.width,
        height: externalLabelBounds.height,
      });

      // when
      modeling.createElements([label], externalLabelMid, parentShape);

      // then
      expect(createLabelSpy).not.to.have.been.called;
    }));

    it('should NOT add label if hint createElementsBehavior=false', inject(function (
      factory,
      elementFactory,
      elementRegistry,
      modeling
    ) {
      // given
      var parentShape = elementRegistry.get('ViewContainer1'),
        newShape = elementFactory.createShape({
          type: 'ifml:Event',
          businessObject: factory.create('ifml:Event', {
            name: 'foo',
          }),
        });

      // when
      newShape = modeling.createShape(newShape, { x: 50, y: 50 }, parentShape, {
        createElementsBehavior: false,
      });

      // then
      expect(newShape.label).not.to.exist;
    }));
  });

  describe('move label', function () {
    it('should move start event label', inject(function (
      elementRegistry,
      modeling
    ) {
      // given
      // label original =x="591" y="380"
      var eventShape = elementRegistry.get('Event_select_List'),
        eventDi = getDi(eventShape),
        labelShape = eventShape.label;

      // when
      modeling.moveElements([labelShape], { x: -10, y: -10 });

      // then
      expect(labelShape.x).to.equal(610);
      expect(labelShape.y).to.equal(370);
      expect(eventDi.bounds.x).to.equal(620);
      expect(eventDi.bounds.y).to.equal(360);
    }));
  });

  // });

  describe('delete label', function () {
    it('should remove name', inject(function (elementRegistry, modeling) {
      // given
      var startEventShape = elementRegistry.get('Event1'),
        startEvent = startEventShape.businessObject,
        labelShape = startEventShape.label;

      // when
      modeling.removeShape(labelShape);

      // then
      expect(startEventShape.label).not.to.exist;
      expect(startEvent.name).not.to.exist;
    }));
  });

  describe('update properties', function () {
    it('should resize after updating name property', inject(function (
      elementRegistry,
      modeling
    ) {
      // given
      var spy = sinon.spy(modeling, 'resizeShape');

      var startEventShape = elementRegistry.get('Event1');

      // when
      modeling.updateProperties(startEventShape, { name: 'bar' });

      // then
      expect(spy).to.have.been.called;
    }));

    it('should resize after updating text property', inject(function (
      elementRegistry,
      modeling
    ) {
      // given
      var spy = sinon.spy(modeling, 'resizeShape');

      var annotation = elementRegistry.get('Annotation1');

      // when
      modeling.updateProperties(annotation, {
        text: 'My Annotation foo lorem ipsum My Annotation foo lorem ipsum My Annotation foo lorem ipsum',
      });

      // then
      expect(spy).to.have.been.called;
    }));

    describe('behavior - LabelBehavior', function () {
      describe('copy/paste integration', function () {
        var diagramXML = require('./LabelBehavior.copyPaste.ifml');

        beforeEach(
          bootstrapModeler(diagramXML, {
            modules: [modelingModule, coreModule],
            // gridSnappingModule
          })
        );

        it('should skip adjustment during creation', inject(function (
          elementRegistry,
          copyPaste
        ) {
          // given
          var event = elementRegistry.get('Event');
          var vc = elementRegistry.get('ViewComponent');
          var elements = [
            event,
            event.label,
          ];

          copyPaste.copy(elements);

          // when
          var x = 56/2 + 270; // mid of labelwidth + eventposition
          var pastedElements = copyPaste.paste({
            element: vc,
            point: {
              x: x,
              y: 180,
            },
          });

          var label = pastedElements[1];

          // // then
          expect(label).to.exist;

          expect(label).to.have.position({ x: 270, y: 183 });
        }));
      });
    });
  });
});
