import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';
import { getMid } from 'diagram-js/lib/layout/LayoutUtil';
import { is } from '../../../../../lib/util/ModelUtil';


describe('features/modeling - CreateBehavior for Events', function() {

  var allElementsDiagram = require('../../../../fixtures/ifml/allElements.ifml');

  beforeEach(bootstrapModeler(allElementsDiagram, {
    modules: [
      coreModule,
      modelingModule
    ]
  }));


  it('should create ViewElementEvent on ViewElements', inject(
    function(elementFactory, elementRegistry, modeling) {

      // given
      var viewcomponent = elementRegistry.get('ViewComponent1');

      var event = elementFactory.createShape({
        type: 'ifml:Event'
      });
      // var eventId = event.id;
      
      // when
      var shape = modeling.createShape(event, getMid(viewcomponent), viewcomponent);
      
      // then
      expect(is(shape, 'ifml:ViewElementEvent')).to.be.true;
    }
  ));

  it('should create ActionEvent on Action', inject(
    function(elementFactory, elementRegistry, modeling) {

      // given
      var viewcomponent = elementRegistry.get('Action1');

      var event = elementFactory.createShape({
        type: 'ifml:Event'
      });
      // var eventId = event.id;
      
      // when
      var shape = modeling.createShape(event, getMid(viewcomponent), viewcomponent);
      
      // then
      expect(is(shape, 'ifml:ActionEvent')).to.be.true;
    }
  ));

});