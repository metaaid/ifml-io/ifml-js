import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';
import rulesModule from 'lib/features/rules';
import snappingModule from 'lib/features/snapping';
import spaceToolModule from 'diagram-js/lib/features/space-tool';

import {
  createCanvasEvent as canvasEvent
} from '../../../../util/MockEvents';

import {
  MODULE_MIN_DIMENSIONS,
  VIEWELEMENT_MIN_DIMENSIONS,
  ANNOTATION_MIN_DIMENSIONS
} from 'lib/features/modeling/behavior/ResizeBehavior';

var testModules = [
  coreModule,
  modelingModule,
  rulesModule,
  snappingModule,
  spaceToolModule
];


describe('features/modeling - space tool behavior', function() {

  describe('module', function() {

    describe('minimum dimensions', function() {

      var diagramXML = require('../../../../fixtures/ifml/simple/module.ifml');

      beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


      it('should ensure minimum dimensions', inject(
        function(dragging, elementRegistry, spaceTool) {

          // given
          var subProcess = elementRegistry.get('ModuleDefinition1');

          // when
          spaceTool.activateMakeSpace(canvasEvent({ x: subProcess.x+10, y: subProcess.y+10 }));

          dragging.move(canvasEvent({ x: -500, y: -500 }));

          dragging.end();

          // then
          expect(subProcess.width).to.equal(MODULE_MIN_DIMENSIONS.width);
        })
      );

    });

  });

  describe('viewElement', function() {

    describe('minimum dimensions', function() {

      var diagramXML = require('../../../../fixtures/ifml/viewcomponent.ifml');

      beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


      it('should ensure minimum dimensions', inject(
        function(dragging, elementRegistry, spaceTool) {

          // given
          var subProcess = elementRegistry.get('ViewComponent1');

          // when
          spaceTool.activateMakeSpace(canvasEvent({ x: subProcess.x+10, y: subProcess.y+10 }));

          dragging.move(canvasEvent({ x: -500, y: -500 }));

          dragging.end();

          // then
          expect(subProcess.width).to.equal(VIEWELEMENT_MIN_DIMENSIONS.width);
        })
      );

    });

  });

  describe('Annotation', function() {

    describe('minimum dimensions', function() {

      var diagramXML = require('../../../../fixtures/ifml/allElements.ifml');

      beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


      it('should ensure minimum dimensions', inject(
        function(dragging, elementRegistry, spaceTool) {

          // given
          var subProcess = elementRegistry.get('Annotation1');

          // when
          spaceTool.activateMakeSpace(canvasEvent({ x: subProcess.x+10, y: subProcess.y+10 }));

          dragging.move(canvasEvent({ x: -500, y: -500 }));

          dragging.end();

          // then
          expect(subProcess.width).to.equal(ANNOTATION_MIN_DIMENSIONS.width);
        })
      );

    });

  });

});