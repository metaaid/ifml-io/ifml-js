import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import replacePreviewModule from 'lib/features/replace-preview';
import modelingModule from 'lib/features/modeling';
import moveModule from 'diagram-js/lib/features/move';
import coreModule from 'lib/core';
import copyPasteModule from 'lib/features/copy-paste';

import {
  getBusinessObject,
  is
} from 'lib/util/ModelUtil';

import {
  createCanvasEvent as canvasEvent
} from '../../../../util/MockEvents';

import {
  query as domQuery
} from 'min-dom';

var ATTACH = { attach: true };


describe('features/modeling - replace element behavior', function() {

  describe('<shape.move>', function() {

    var testModules = [
      replacePreviewModule,
      modelingModule,
      coreModule,
      moveModule,
      copyPasteModule
    ];


    describe('Start Events', function() {

      var diagramXML = require('../../../../fixtures/ifml/allElements.ifml');

      beforeEach(bootstrapModeler(diagramXML, {
        modules: testModules
      }));

      var moveShape;

      beforeEach(inject(function(move, dragging, elementRegistry) {

        moveShape = function(shape, target, position) {
          var startPosition = { x: shape.x + 10 + shape.width / 2, y: shape.y + 30 + shape.height/2 };

          move.start(canvasEvent(startPosition), shape);

          dragging.hover({
            element: target,
            gfx: elementRegistry.getGraphics(target)
          });

          dragging.move(canvasEvent(position));
        };
      }));


      // it('should select the replacement after replacing the start event',
      //   inject(function(elementRegistry, canvas, dragging, move, selection) {

      //     // given
      //     var startEvent = elementRegistry.get('ViewComponent1'),
      //         rootElement = canvas.getRootElement();

      //     // when
      //     moveShape(startEvent, rootElement, { x: 140, y: 250 });

      //     dragging.end();

      //     var replacement = elementRegistry.get('ViewComponent1');

      //     // then
      //     expect(selection.get()).to.include(replacement);
      //     expect(selection.get()).not.to.include(startEvent);
      //   })
      // );
  });
  });
});
