import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';
import resizeModule from 'diagram-js/lib/features/resize';
import rulesModule from 'lib/features/rules';
import snappingModule from 'lib/features/snapping';

import {
  createCanvasEvent as canvasEvent
} from '../../../../util/MockEvents';

var testModules = [
  coreModule,
  modelingModule,
  resizeModule,
  rulesModule,
  snappingModule
];

const MIN_PADDING = 20;
var VIEWELEMENT_MIN_DIMENSIONS = {
  width: 150,
  height: 75
};
var MODULE_MIN_DIMENSIONS = {
  width: 300,
  height: 150
};


describe('features/modeling - resize behavior', function() {

  describe('modules', function() {

    describe('minimum dimensions', function() {

      var diagramXML = require('../../../../fixtures/ifml/nesting.ifml');

      beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));

      

      it('should set minimum dimensions', inject(function(dragging, elementRegistry, resize) {

        // given
        var textAnnotation = elementRegistry.get('ModuleDefinition2');

        // when
        resize.activate(canvasEvent({ x: 0, y: 0 }), textAnnotation, 'se');

        dragging.move(canvasEvent({ x: -400, y: -400 }));

        dragging.end();

        // then
        expect(textAnnotation.width).to.equal(MODULE_MIN_DIMENSIONS.width);
        expect(textAnnotation.height).to.equal(MODULE_MIN_DIMENSIONS.height);
      }));


      it('should snap to children from <se>', inject(function(dragging, elementRegistry, resize) {

        // given
        var resizedElement = elementRegistry.get('ModuleDefinition1'),
          restrictedElement = elementRegistry.get('Parent');

        // when
        resize.activate(canvasEvent({ x: 1000, y: 1000 }), resizedElement, 'se');

        dragging.move(canvasEvent({ x: 0, y: 0 }));

        dragging.end();

        // then
        var rightEnd = restrictedElement.x + restrictedElement.width + MIN_PADDING - resizedElement.x; // 1218
        var bottomEnd = restrictedElement.y + restrictedElement.height + MIN_PADDING - resizedElement.y; // 1218
        expect(resizedElement.height).to.equal(bottomEnd);
        expect(resizedElement.width).to.equal(rightEnd);
      }));

      it('should snap to children from <nw>', inject(function(dragging, elementRegistry, resize) {

        // given
        var resizedElement = elementRegistry.get('ModuleDefinition1'),
          restrictedElement = elementRegistry.get('Parent');

        // when
        resize.activate(canvasEvent({ x: 0, y: 0 }), resizedElement, 'nw');

        dragging.move(canvasEvent({ x: 1000, y: 1000 }));

        dragging.end();

        // then
        var westWidth = resizedElement.width + restrictedElement.x - MIN_PADDING - resizedElement.x;
        var northHeight = resizedElement.height + restrictedElement.y - MIN_PADDING - resizedElement.y;
        var eastWidth = westWidth + restrictedElement.x + restrictedElement.width + MIN_PADDING - resizedElement.x;
        var southHeight = restrictedElement.y + restrictedElement.height + MIN_PADDING - resizedElement.y;
        expect(resizedElement.height).to.equal(northHeight);
        expect(resizedElement.width).to.equal(westWidth);
      }));
      

      it('should snap to min dimensions from <se>', inject(function(dragging, elementRegistry, resize) {

          // given
          var participant = elementRegistry.get('ViewComponent1');

          // when
          resize.activate(canvasEvent({ x: 500, y: 500 }), participant, 'se');

          dragging.move(canvasEvent({ x: 0, y: 0 }));

          dragging.end();

          // then
          expect(participant.width).to.equal(VIEWELEMENT_MIN_DIMENSIONS.width);
          expect(participant.height).to.equal(VIEWELEMENT_MIN_DIMENSIONS.height);
      }));


      it('should snap to min dimensions from <nw>', inject(
        function(dragging, elementRegistry, resize) {
          // given
          var participant = elementRegistry.get('ViewComponent1');

          // when
          resize.activate(canvasEvent({ x: 0, y: 0 }), participant, 'nw');

          dragging.move(canvasEvent({ x: 1000, y: 1000 }));

          dragging.end();

          // then
          expect(participant.width).to.equal(VIEWELEMENT_MIN_DIMENSIONS.width);
          expect(participant.height).to.equal(VIEWELEMENT_MIN_DIMENSIONS.height);
        })
      );

    describe('viewcontainer', function(){

      it('should snap to viewcontainer from <se>', inject(function(dragging, elementRegistry, resize) {

        // given
        var resizedElement = elementRegistry.get('Child1'),
          restrictedElement = elementRegistry.get('Child1Child');

        // when
        resize.activate(canvasEvent({ x: 1000, y: 1000 }), resizedElement, 'se');

        dragging.move(canvasEvent({ x: 0, y: 0 }));

        dragging.end();

        // then
        var rightEnd = restrictedElement.x + restrictedElement.width + MIN_PADDING - resizedElement.x; // 1218
        var bottomEnd = restrictedElement.y + restrictedElement.height + MIN_PADDING - resizedElement.y; // 1218
        expect(resizedElement.height).to.equal(bottomEnd);
        expect(resizedElement.width).to.equal(rightEnd);
      }));

      it('should snap to viewcomponent from <se>', inject(function(dragging, elementRegistry, resize) {

        // given
        var resizedElement = elementRegistry.get('Child2'),
          restrictedElement = elementRegistry.get('ViewComponent1');

        // when
        resize.activate(canvasEvent({ x: 1000, y: 1000 }), resizedElement, 'se');

        dragging.move(canvasEvent({ x: 0, y: 0 }));

        dragging.end();

        // then
        var rightEnd = restrictedElement.x + restrictedElement.width + MIN_PADDING - resizedElement.x; // 1218
        var bottomEnd = restrictedElement.y + restrictedElement.height + MIN_PADDING - resizedElement.y; // 1218
        expect(resizedElement.height).to.equal(bottomEnd);
        expect(resizedElement.width).to.equal(rightEnd);
      }));
    });

    });

  });

  describe('text annotation', function() {

    var diagramXML = require('../../../../fixtures/ifml/allElements.ifml');

    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


    it('should set minimum dimensions', inject(function(dragging, elementRegistry, resize) {

      // given
      var textAnnotation = elementRegistry.get('Annotation1');

      // when
      resize.activate(canvasEvent({ x: 0, y: 0 }), textAnnotation, 'se');

      dragging.move(canvasEvent({ x: -400, y: -400 }));

      dragging.end();

      // then
      expect(textAnnotation.width).to.equal(50);
      expect(textAnnotation.height).to.equal(30);
    }));

  });


});