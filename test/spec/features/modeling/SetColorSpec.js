import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import { getDi } from 'lib/util/ModelUtil';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';

var FUCHSIA_HEX = '#ff00ff',
    YELLOW_HEX = '#ffff00';

describe('features/modeling - set color', function() {

  var diagramXML = require('../../../fixtures/ifml/simple.ifml');

  beforeEach(bootstrapModeler(diagramXML, {
    modules: [
      coreModule,
      modelingModule
    ]
  }));


  describe('execute', function() {

    it('setting fill color', inject(function(elementRegistry, modeling) {

      // given
      var taskShape = elementRegistry.get('List1'),
          taskDi = getDi(taskShape);

      // when
      modeling.setColor(taskShape, { fill: 'FUCHSIA' });

      // then
      expect(taskDi.get('fill')).to.equal(FUCHSIA_HEX);
    }));


    it('unsetting fill color', inject(function(elementRegistry, modeling) {

      // given
      var taskShape = elementRegistry.get('List1'),
          taskDi = getDi(taskShape);

      modeling.setColor(taskShape, { fill: 'FUCHSIA' });

      // when
      modeling.setColor(taskShape);

      // then
      expect(taskDi.get('fill')).not.to.exist;
    }));


    it('setting fill color without changing stroke color', inject(
      function(elementRegistry, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        modeling.setColor(taskShape, { fill: 'FUCHSIA', stroke: 'YELLOW' });

        // when
        modeling.setColor(taskShape, { fill: 'YELLOW' });

        // then
        expect(taskDi.get('fill')).to.equal(YELLOW_HEX);
        expect(taskDi.get('stroke')).to.equal(YELLOW_HEX);
      }
    ));


    it('unsetting fill color without changing stroke color', inject(
      function(elementRegistry, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        modeling.setColor(taskShape, { fill: 'FUCHSIA', stroke: 'YELLOW' });

        // when
        modeling.setColor(taskShape, { fill: undefined });

        // then
        expect(taskDi.get('fill')).not.to.exist;
        expect(taskDi.get('stroke')).to.equal(YELLOW_HEX);
      }
    ));


    it('unsetting both fill and stroke color', inject(
      function(elementRegistry, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        modeling.setColor(taskShape, { fill: 'FUCHSIA' });

        // when
        modeling.setColor(taskShape);

        // then
        expect(taskDi.get('fill')).not.to.exist;
        expect(taskDi.get('stroke')).not.to.exist;
      }
    ));


    it('setting stroke color', inject(function(elementRegistry, modeling) {

      // given
      var taskShape = elementRegistry.get('List1'),
          taskDi = getDi(taskShape);

      // when
      modeling.setColor(taskShape, { stroke: 'FUCHSIA' });

      // then
      expect(taskDi.get('stroke')).to.equal(FUCHSIA_HEX);
      expect(taskDi.get('fill')).not.to.exist;
    }));


    it('unsetting stroke color', inject(function(elementRegistry, modeling) {

      // given
      var taskShape = elementRegistry.get('List1'),
          taskDi = getDi(taskShape);

      modeling.setColor(taskShape, { stroke: 'FUCHSIA' });

      // when
      modeling.setColor(taskShape);

      // then
      expect(taskDi.get('stroke')).not.to.exist;
      expect(taskDi.get('fill')).not.to.exist;
    }));


    it('setting fill color (multiple elements)', inject(
      function(elementRegistry, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(startEventShape);

        // when
        modeling.setColor([ taskShape, startEventShape ], { fill: 'FUCHSIA' });

        // then
        expect(taskDi.get('fill')).to.equal(FUCHSIA_HEX);
        expect(startEventDi.get('fill')).to.equal(FUCHSIA_HEX);
        expect(taskDi.get('stroke')).not.to.exist;
        expect(startEventDi.get('stroke')).not.to.exist;
      }
    ));


    it('unsetting fill color (multiple elements)', inject(
      function(elementRegistry, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(startEventShape);

        modeling.setColor([ taskShape, startEventShape ], { fill: 'FUCHSIA' });

        // when
        modeling.setColor([ taskShape, startEventShape ]);

        // then
        expect(taskDi.get('fill')).not.to.exist;
        expect(startEventDi.get('fill')).not.to.exist;
      }
    ));


    it('setting stroke color (multiple elements)', inject(
      function(elementRegistry, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(startEventShape);

        // when
        modeling.setColor([
          taskShape,
          startEventShape
        ], { stroke: 'FUCHSIA' });

        // then
        expect(taskDi.get('stroke')).to.equal(FUCHSIA_HEX);
        expect(startEventDi.get('stroke')).to.equal(FUCHSIA_HEX);
        expect(taskDi.get('fill')).not.to.exist;
        expect(startEventDi.get('fill')).not.to.exist;
      }
    ));


    it('unsetting stroke color (multiple elements)', inject(
      function(elementRegistry, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(startEventShape);

        modeling.setColor([
          taskShape,
          startEventShape
        ], { stroke: 'FUCHSIA' });

        // when
        modeling.setColor([ taskShape, startEventShape ]);

        // then
        expect(taskDi.get('stroke')).not.to.exist;
        expect(startEventDi.get('stroke')).not.to.exist;
      }
    ));


    it('should not set fill on Edge', inject(function(elementRegistry, modeling) {

      // given
      var sequenceFlow = elementRegistry.get('NavigationFlow_1'),
          sequenceFlowDi = getDi(sequenceFlow);

      // when
      modeling.setColor(sequenceFlow, { fill: 'FUCHSIA' });

      // then
      expect(sequenceFlowDi.get('fill')).not.to.exist;
    }));


    it('should throw for an invalid color', inject(function(elementRegistry, modeling) {

      // given
      var sequenceFlow = elementRegistry.get('NavigationFlow_1');

      // when
      function setColor() {
        modeling.setColor(sequenceFlow, { fill: 'INVALID_COLOR' });
      }

      // then
      expect(setColor).to.throw(/^invalid color value/);
    }));


    it('should throw for a color with alpha', inject(function(elementRegistry, modeling) {

      // given
      var sequenceFlow = elementRegistry.get('NavigationFlow_1');

      // when
      function setColor() {
        modeling.setColor(sequenceFlow, { fill: 'rgba(0, 255, 0, 0.5)' });
      }

      // then
      expect(setColor).to.throw(/^invalid color value/);
    }));
  });


  describe('undo', function() {

    it('setting fill color', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        // when
        modeling.setColor(taskShape, { fill: 'FUCHSIA' });
        commandStack.undo();

        // then
        expect(taskDi.get('fill')).not.to.exist;
      }
    ));


    it('unsetting fill color', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        modeling.setColor(taskShape, { fill: 'FUCHSIA' });

        // when
        modeling.setColor(taskShape);
        commandStack.undo();

        // then
        expect(taskDi.get('fill')).to.equal(FUCHSIA_HEX);
      }
    ));


    it('setting stroke color', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        // when
        modeling.setColor(taskShape, { stroke: 'FUCHSIA' });
        commandStack.undo();

        // then
        expect(taskDi.get('stroke')).not.to.exist;
      }
    ));


    it('unsetting stroke color', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        modeling.setColor(taskShape, { stroke: 'FUCHSIA' });

        // when
        modeling.setColor(taskShape);
        commandStack.undo();

        // then
        expect(taskDi.get('stroke')).to.equal(FUCHSIA_HEX);
      }
    ));


    it('setting fill color (multiple elements)', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(taskShape);

        // when
        modeling.setColor([ taskShape, startEventShape ], { fill: 'FUCHSIA' });
        commandStack.undo();

        // then
        expect(taskDi.get('fill')).not.to.exist;
        expect(startEventDi.get('fill')).not.to.exist;
      }
    ));


    it('unsetting fill color (multiple elements)', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(taskShape);

        modeling.setColor([ taskShape, startEventShape ], { fill: 'FUCHSIA' });

        // when
        modeling.setColor([ taskShape, startEventShape ]);
        commandStack.undo();

        // then
        expect(taskDi.get('fill')).to.equal(FUCHSIA_HEX);
        expect(startEventDi.get('fill')).to.equal(FUCHSIA_HEX);
      }
    ));


    it('setting stroke color (multiple elements)', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(taskShape);

        // when
        modeling.setColor([
          taskShape,
          startEventShape
        ], { stroke: 'FUCHSIA' });
        commandStack.undo();

        // then
        expect(taskDi.get('stroke')).not.to.exist;
        expect(startEventDi.get('stroke')).not.to.exist;
      }
    ));


    it('unsetting stroke color (multiple elements)', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(taskShape);

        modeling.setColor([ taskShape, startEventShape ], { stroke: 'FUCHSIA' });

        // when
        modeling.setColor([ taskShape, startEventShape ]);
        commandStack.undo();

        // then
        expect(taskDi.get('stroke')).to.equal(FUCHSIA_HEX);
        expect(startEventDi.get('stroke')).to.equal(FUCHSIA_HEX);
      }
    ));

  });


  describe('redo', function() {

    it('setting fill color', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        // when
        modeling.setColor(taskShape, { fill: 'FUCHSIA' });
        commandStack.undo();
        commandStack.redo();

        // then
        expect(taskDi.get('fill')).to.equal(FUCHSIA_HEX);
      }
    ));


    it('unsetting fill color', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        modeling.setColor(taskShape, { fill: 'FUCHSIA' });

        // when
        modeling.setColor(taskShape);
        commandStack.undo();
        commandStack.redo();

        // then
        expect(taskDi.get('fill')).not.to.exist;
      }
    ));


    it('setting stroke color', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        // when
        modeling.setColor(taskShape, { stroke: 'FUCHSIA' });
        commandStack.undo();
        commandStack.redo();

        // then
        expect(taskDi.get('stroke')).to.equal(FUCHSIA_HEX);
      }
    ));


    it('unsetting stroke color', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape);

        modeling.setColor(taskShape, { stroke: 'FUCHSIA' });

        // when
        modeling.setColor(taskShape);
        commandStack.undo();
        commandStack.redo();

        // then
        expect(taskDi.get('stroke')).not.to.exist;
      }
    ));


    it('setting fill color (multiple elements)', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(taskShape);

        // when
        modeling.setColor([ taskShape, startEventShape ], { fill: 'FUCHSIA' });
        commandStack.undo();
        commandStack.redo();

        // then
        expect(taskDi.get('fill')).to.equal(FUCHSIA_HEX);
        expect(startEventDi.get('fill')).to.equal(FUCHSIA_HEX);
      }
    ));


    it('unsetting fill color (multiple elements)', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(taskShape);

        modeling.setColor([ taskShape, startEventShape ], { fill: 'FUCHSIA' });

        // when
        modeling.setColor([ taskShape, startEventShape ]);
        commandStack.undo();
        commandStack.redo();

        // then
        expect(taskDi.get('fill')).not.to.exist;
        expect(startEventDi.get('fill')).not.to.exist;
      }
    ));


    it('setting stroke color (multiple elements)', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(taskShape);

        // when
        modeling.setColor([ taskShape, startEventShape ], { stroke: 'FUCHSIA' });
        commandStack.undo();
        commandStack.redo();

        // then
        expect(taskDi.get('stroke')).to.equal(FUCHSIA_HEX);
        expect(startEventDi.get('stroke')).to.equal(FUCHSIA_HEX);
      }
    ));


    it('unsetting stroke color (multiple elements)', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var taskShape = elementRegistry.get('List1'),
            taskDi = getDi(taskShape),
            startEventShape = elementRegistry.get('Event_delete'),
            startEventDi = getDi(taskShape);

        modeling.setColor([
          taskShape,
          startEventShape
        ], { stroke: 'FUCHSIA' });

        // when
        modeling.setColor([ taskShape, startEventShape ]);
        commandStack.undo();
        commandStack.redo();

        // then
        expect(taskDi.get('stroke')).not.to.exist;
        expect(startEventDi.get('stroke')).not.to.exist;
      }
    ));

  });

});