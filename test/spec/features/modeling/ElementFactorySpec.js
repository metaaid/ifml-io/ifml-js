import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';

import {
  getBusinessObject,
  getDi,
  is
} from '../../../../lib/util/ModelUtil';


describe('features - element factory', function() {

  var diagramXML = require('../../../fixtures/ifml/simple.ifml');

  var testModules = [ modelingModule, coreModule ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  describe('create', function() {

    it('should error when accessing <di> via businessObject', inject(function(elementFactory) {

      // given
      var shape = elementFactory.createShape({
        type: 'ifml:Action',
      });

      // then
      expect(shape.di).to.exist;
      expect(function() {
        shape.businessObject.di;
      }).to.throw(/The di is available through the diagram element only./);
    }));


  //   describe('integration', function() {

      it('should add isExpanded to expanded Module', inject(function(elementFactory) {

        // when
        var module = elementFactory.createShape({
          type: 'ifml:Module',
          isExpanded: true
        });

        // then
        var di = getDi(module);

        expect(di.isExpanded).to.exist;
        expect(di.isExpanded).to.be.true;
      }));

  });

});
