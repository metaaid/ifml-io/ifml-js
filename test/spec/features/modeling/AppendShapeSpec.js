import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import {
  find
} from 'min-dash';

import {
  getDi,
  is,
  getBusinessObject
} from 'lib/util/ModelUtil';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features/modeling - append shape', function() {

  var diagramXML = require('../../../fixtures/ifml/allElements.ifml');

  var testModules = [ coreModule, modelingModule ];
  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  describe('shape handling', function() {

    it('should execute', inject(function(elementRegistry, modeling) {

      // given
      var viewComponentShape = elementRegistry.get('ViewComponent1');

      // when
      var targetShape = modeling.appendShape(viewComponentShape, { type: 'ifml:ViewComponent' }),
          target = targetShape.businessObject;

      // then
      expect(targetShape).to.exist;
      expect(target.$instanceOf('ifml:ViewComponent')).to.be.true;
    }));


    it('should create DI', inject(function(elementRegistry, modeling) {

      // given
      var viewComponentShape = elementRegistry.get('ViewComponent1');

      var startEventDi = getDi(viewComponentShape);

      // when
      var targetShape = modeling.appendShape(viewComponentShape, { type: 'ifml:ViewComponent' }),
          targetDi = getDi(targetShape);

      // then
      expect(targetDi).to.exist;
      expect(targetDi.$parent).to.eql(startEventDi.$parent);

      expect(targetDi).to.have.bounds(targetShape);
    }));

  });

});
