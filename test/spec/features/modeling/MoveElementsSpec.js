import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features/modeling - move elements', function() {

  describe('flow parent', function() {

    var diagramXML = require('./MoveElements.modules.ifml');

    beforeEach(bootstrapModeler(diagramXML, {
      modules: [
        coreModule,
        modelingModule
      ]
    }));

    it('should keep when moving shapes with flow', inject(function(elementRegistry, modeling, factory, canvas) {

      // given
      var flow1 = elementRegistry.get('Flow1'),
          shapeViewComponent1 = elementRegistry.get('ViewComponent1'),
          shapeViewComponent2 = elementRegistry.get('ViewComponent2'),
          shapeViewComponent3 = elementRegistry.get('ViewComponent3'),
          rootModel = canvas.getRootElement(),
          moduleDefinition = elementRegistry.get('ModuleDefinition1');

      // when
      modeling.moveElements(
        [ shapeViewComponent1, shapeViewComponent2, shapeViewComponent3 ],
        { x: 10, y: -50 },
        moduleDefinition,
        { primaryShape: shapeViewComponent3 }
      );

      // then
      expect(flow1.parent).to.eql(rootModel);
    }));

    it('should keep when moving shapes with flow', inject(function(elementRegistry, modeling, factory, canvas) {

      // given
      var flow1 = elementRegistry.get('Flow1'),
          shapeViewComponent1 = elementRegistry.get('ViewComponent1'),
          shapeViewComponent2 = elementRegistry.get('ViewComponent2'),
          shapeViewComponent3 = elementRegistry.get('ViewComponent3'),
          rootModel = canvas.getRootElement(),
          moduleDefinition = elementRegistry.get('ModuleDefinition1');

      // when
      modeling.moveElements(
        [ shapeViewComponent1, shapeViewComponent2, shapeViewComponent3, flow1 ],
        { x: 10, y: -50 },
        moduleDefinition,
        { primaryShape: shapeViewComponent3 }
      );

      // then
      expect(flow1.parent).to.eql(rootModel);
    }));

  });
});


// helpers //////////////////////

function moveWaypoint(p, delta) {
  return {
    x: p.x + delta.x || 0,
    y: p.y + delta.y || 0
  };
}

function moveWaypoints(waypoints, delta) {

  return waypoints.map(function(p) {

    var original = p.original;

    var moved = moveWaypoint(p, delta);

    if (original) {
      moved.original = moveWaypoint(original, delta);
    }

    return moved;
  });
}
