import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features - ifml-factory', function() {

  var diagramXML = require('../../../fixtures/ifml/simple.ifml');

  var testModules = [ modelingModule, coreModule ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  describe('create element', function() {

    it('should return instance', inject(function(factory) {

      var action = factory.create('ifml:Action');
      expect(action).to.exist;
      expect(action.$type).to.equal('ifml:Action');
    }));
    
    it('should assign id with generic semantic prefix starting with type (SimpleField)', inject(function(factory) {
      var event = factory.create('ifml:SimpleField');

      expect(event.$type).to.equal('ifml:SimpleField');
      expect(event.$id).to.match(/^SimpleField_/g);
    }));


    it('should assign id (with semantic prefix)', inject(function(factory) {
      var viewcomponent = factory.create('ifml:ViewComponent');

      expect(viewcomponent.$type).to.equal('ifml:ViewComponent');
      expect(viewcomponent.$id).to.match(/^ViewComponent/g);
    }));

    it('should assign id with semantic prefix (Form)', inject(function(factory) {
      var form = factory.create('ifml:Form');

      expect(form.$type).to.equal('ifml:Form');
      expect(form.$id).to.match(/^Form_/g);
    }));

    it('should assign id with semantic prefix (UMLDomainConcept)', inject(function(factory) {
      var form = factory.create('ifml:UMLDomainConcept');

      expect(form.$type).to.equal('ifml:UMLDomainConcept');
      expect(form.$id).to.match(/^Concept_/g);
    }));

    it('should assign id with semantic prefix (Event)', inject(function(factory) {
      var event = factory.create('ifml:Event');

      expect(event.$type).to.equal('ifml:Event');
      expect(event.$id).to.match(/^Event_/g);
    }));

    it('should assign id with semantic prefix (UMLBehavior)', inject(function(factory) {
      var event = factory.create('ifml:UMLBehavior');

      expect(event.$type).to.equal('ifml:UMLBehavior');
      expect(event.$id).to.match(/^UMLBehavior_/g);
    }));



    describe('generic id', function() {

      it('should assign id with generic semantic prefix (Activity)', inject(function(factory) {
        var menu = factory.create('ifml:Menu');

        expect(menu.$type).to.equal('ifml:Menu');
        expect(menu.$id).to.match(/^ViewContainer_/g);
      }));

      it('should assign id with generic semantic prefix (NavigationFlow)', inject(function(factory) {
        var event = factory.create('ifml:NavigationFlow');
  
        expect(event.$type).to.equal('ifml:NavigationFlow');
        expect(event.$id).to.match(/^Flow_/g);
      }));

      it('should assign id with generic semantic prefix (NavigationFlow)', inject(function(factory) {
        var event = factory.create('ifml:DataFlow');
  
        expect(event.$type).to.equal('ifml:DataFlow');
        expect(event.$id).to.match(/^Flow_/g);
      }));
    });


    it('should claim provided id', inject(function(factory, moddle) {
      var task = factory.create('ifml:Action', { id: 'foo' });

      expect(task).to.exist;
      expect(task.$id).to.eql('foo');
      expect(moddle.ids.assigned('foo')).to.exist;
    }));

  });


  describe('create di', function() {

    it('should create waypoints', inject(function(factory) {

      // given
      var waypoints = [
        { original: { x: 0, y: 0 }, x: 0, y: 0 },
        { original: { x: 0, y: 0 }, x: 0, y: 0 }
      ];

      // when
      var result = factory.createDiWaypoints(waypoints);

      // then
      expect(result).eql([
        factory.create('dc:Point', { x: 0, y: 0 }),
        factory.create('dc:Point', { x: 0, y: 0 })
      ]);

      // expect original not to have been accidently serialized
      expect(result[0].$attrs).to.eql({});
    }));
  });

});
