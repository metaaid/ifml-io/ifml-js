import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import { pick } from 'min-dash';

import {
  getDi
} from 'lib/util/ModelUtil';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features/modeling - resize shape', function() {

  var diagramXML = require('../../../fixtures/ifml/simple.ifml');

  var testModules = [ coreModule, modelingModule ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  describe('shape', function() {

    it('should resize', inject(function(elementRegistry, modeling) {

      // given
      var subProcessElement = elementRegistry.get('List1'),
          originalWidth = subProcessElement.width;

      // when
      modeling.resizeShape(subProcessElement, { x: 339, y: 142, width: 250, height: 200 });

      // then
      expect(subProcessElement.width).to.equal(250);
      expect(subProcessElement.width).to.not.equal(originalWidth);

    }));


    describe('businessObject', function() {

      it('should update bounds', inject(function(elementRegistry, modeling) {

        // given
        var subProcessElement = elementRegistry.get('List1');

        // when
        modeling.resizeShape(subProcessElement, { x: 339, y: 142, width: 250, height: 200 });

        // then
        var di = getDi(subProcessElement);
        expect(di.bounds.width).to.equal(250);
      }));

    });


    describe('connected flow', function() {

      it('should resize', inject(function(elementRegistry, modeling, factory) {

        // given
        var subProcessElement = elementRegistry.get('List1');

        var sequenceFlowElement = elementRegistry.get('NavigationFlow_1'),
            sequenceFlowDi = getDi(sequenceFlowElement);

        // when

        // Decreasing width by 100px
        modeling.resizeShape(subProcessElement, { x: 340, y: 290, width: 150, height: 180 });

        // then

        // expect flow layout
        var diWaypoints = factory.createDiWaypoints([
          { x: 630, y: 600 },
          { x: 430, y: 600 },
          { x: 430, y: 470 },
        ]);

        // expect(sequenceFlowDi.waypoint).eql(diWaypoints);
        expect(sequenceFlowElement).to.have.diWaypoints(diWaypoints);
      }));


      it('should move', inject(function(elementRegistry, modeling, factory) {

        // given
        var subProcessElement = elementRegistry.get('List1');

        var sequenceFlowElement = elementRegistry.get('NavigationFlow_1');
            // sequenceFlowDi = getDi(sequenceFlowElement);

        // when
        modeling.moveShape(subProcessElement, { x: -50, y: -10 });

        // then

        // expect flow layout
        var diWaypoints = factory.createDiWaypoints([
          { x: 630, y: 600 },
          { x: 380, y: 600 },
          { x: 380, y: 460 },
        ]);

        // assertWaypoints()

        expect(sequenceFlowElement).to.have.diWaypoints(diWaypoints);
      }));

    });

  });


  describe('integration', function() {

    it('should not move Boundary Event if unnecessary', inject(function(elementRegistry, modeling) {

      // given
      var boundaryEvent = elementRegistry.get('Event_delete'),
          originalPosition = getPosition(boundaryEvent),
          subProcessElement = elementRegistry.get('List1');

      // when
      modeling.resizeShape(subProcessElement, { x: 204, y: 28, width: 400, height: 339 });

      // then
      expect(getPosition(boundaryEvent)).to.jsonEqual(originalPosition);
    }));

  });

});

// helper /////
function getPosition(shape) {
  return pick(shape, [ 'x', 'y' ]);
}
