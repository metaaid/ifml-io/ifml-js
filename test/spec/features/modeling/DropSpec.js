import { bootstrapModeler, inject } from 'test/TestHelper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';

describe('features/move - drop', function () {
  var diagramXML = require('../../../fixtures/ifml/allElements.ifml');

  var testModules = [coreModule, modelingModule];

  describe('elements', function () {
    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));

    it('should update parent', inject(function (elementRegistry, modeling) {
      // given
      var task_1 = elementRegistry.get('ViewComponent1'),
        parent = elementRegistry.get('ViewContainer1');

      // when
      modeling.moveShape(task_1, { x: 0, y: 200 }, parent);

      // then
      expect(task_1.parent).to.eql(parent);
      expect(task_1.businessObject.$parent).to.eql(parent.businessObject);
    }));

    it('should update parents', inject(function (elementRegistry, modeling) {
      // given
      var task_1 = elementRegistry.get('ViewComponent1'),
        task_2 = elementRegistry.get('Form1'),
        parent = elementRegistry.get('ViewContainer1');

      // when
      modeling.moveElements([task_1, task_2], { x: 0, y: 200 }, parent);

      // then
      expect(task_1.parent).to.eql(parent);
      expect(task_1.businessObject.$parent).to.eql(parent.businessObject);
      expect(task_2.parent).to.eql(parent);
      expect(task_2.businessObject.$parent).to.eql(parent.businessObject);
    }));
  });

  describe('connection handling', function () {
    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));

    it('should remove flow if target and source have different module', inject(function (
      elementRegistry,
      modeling
    ) {
      // given
      var task_1 = elementRegistry.get('ViewContainer1'),
        parent = elementRegistry.get('ModuleDefinition1'),
        flow = elementRegistry.get('NavigationFlow1');

      // when
      modeling.moveElements([task_1], { x: 0, y: 200 }, parent);

      // then
      expect(flow.parent).to.be.null;
      expect(flow.businessObject.$parent).to.be.null;
    }));

    it('should update flow parent if target and source have same parents', inject(function (
      elementRegistry,
      modeling
    ) {
      // given
      var elements = [
        elementRegistry.get('ViewContainer1'),
        elementRegistry.get('Event1'),
        elementRegistry.get('Menu1'),
      ];
      var parent = elementRegistry.get('ModuleDefinition1');
      var flow = elementRegistry.get('NavigationFlow1');

      // when
      modeling.moveElements(elements, { x: 0, y: 250 }, parent);

      // then
      expect(flow.parent).to.eql(parent);
      expect(flow.businessObject.$parent).to.eql(parent.businessObject);
    }));
  });
});
