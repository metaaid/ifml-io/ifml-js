import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import { getDi } from 'lib/util/ModelUtil';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features/modeling - update properties', function() {

  var diagramXML = require('../../../fixtures/ifml/allElements.ifml');

  var testModules = [ coreModule, modelingModule ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  var updatedElements;

  beforeEach(inject(function(eventBus) {

    eventBus.on([ 'commandStack.execute', 'commandStack.revert' ], function() {
      updatedElements = [];
    });

    eventBus.on('element.changed', function(event) {
      updatedElements.push(event.element);
    });

  }));


  describe('should execute', function() {


    it('updating name', inject(function(elementRegistry, modeling) {

      // given
      var flowConnection = elementRegistry.get('Event1');

      // when
      modeling.updateProperties(flowConnection, { name: 'FOO BAR' });

      // then
      expect(flowConnection.businessObject.name).to.equal('FOO BAR');

      // flow label got updated, too
      expect(updatedElements).to.include(flowConnection.label);
    }));


    it('unsetting name', inject(function(elementRegistry, modeling) {

      // given
      var flowConnection = elementRegistry.get('Event1');

      // when
      modeling.updateProperties(flowConnection, { name: undefined });

      // then
      expect(flowConnection.businessObject.name).not.to.exist;
    }));


    it('updating id', inject(function(elementRegistry, modeling) {

      // given
      var flowConnection = elementRegistry.get('Event1'),
          flowBo = flowConnection.businessObject;

      var ids = flowBo.$model.ids;

      // when
      modeling.updateProperties(flowConnection, { id: 'FOO_BAR' });

      // then
      expect(ids.assigned('FOO_BAR')).to.eql(flowBo);
      expect(ids.assigned('Event1')).to.be.false;

      expect(flowBo.$id).to.equal('FOO_BAR');
      expect(flowConnection.id).to.equal('FOO_BAR');
    }));


    it('updating extension elements', inject(
      function(elementRegistry, modeling) {

        // given
        var flowConnection = elementRegistry.get('Event1'),
            flowBo = flowConnection.businessObject;

        // when
        modeling.updateProperties(flowConnection, {
          'xmlns:foo': 'http://foo',
          'foo:customAttr': 'FOO'
        });

        // then
        expect(flowBo.get('xmlns:foo')).to.equal('http://foo');
        expect(flowBo.get('foo:customAttr')).to.equal('FOO');
      }
    ));


    it('setting di properties', inject(function(elementRegistry, modeling) {

      // given
      var flowConnection = elementRegistry.get('Event1'),
          flowBo = flowConnection.businessObject,
          flowDi = getDi(flowConnection);

      // when
      modeling.updateProperties(flowConnection, {
        di: {
          fill: 'FUCHSIA'
        }
      });

      // then
      expect(flowDi.fill).to.equal('FUCHSIA');

      expect(flowBo.get('di')).not.to.exist;
    }));


    it('unsetting di properties', inject(function(elementRegistry, modeling) {

      // given
      var flowConnection = elementRegistry.get('Event1'),
          flowDi = getDi(flowConnection);

      modeling.updateProperties(flowConnection, { di: { fill: 'FUCHSIA' } });

      // when
      modeling.updateProperties(flowConnection, { di: { fill: undefined } });

      // then
      expect(flowDi.fill).not.to.exist;
    }));

  });


  describe('should undo', function() {

    it('updating name', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var flowConnection = elementRegistry.get('Event1');

        // when
        modeling.updateProperties(flowConnection, { name: 'FOO BAR' });
        commandStack.undo();

        // then
        expect(flowConnection.businessObject.name).to.equal('Test');

        // flow got updated, too
        expect(updatedElements).to.include(flowConnection.label);
      }
    ));


    it('unsetting name', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var flowConnection = elementRegistry.get('Event1');

        modeling.updateProperties(flowConnection, { name: undefined });

        // when
        commandStack.undo();

        // then
        expect(flowConnection.businessObject.name).to.equal('Test');
      }
    ));


    it('updating id', inject(function(elementRegistry, commandStack, modeling) {

      // given
      var flowConnection = elementRegistry.get('Event1'),
          flowBo = flowConnection.businessObject;

      var ids = flowBo.$model.ids;

      // when
      modeling.updateProperties(flowConnection, { id: 'FOO_BAR' });
      commandStack.undo();

      // then
      expect(ids.assigned('FOO_BAR')).to.be.false;
      expect(ids.assigned('Event1')).to.eql(flowBo);

      expect(flowConnection.id).to.equal('Event1');
      expect(flowBo.$id).to.equal('Event1');
    }));


    it('updating extension elements', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var flowConnection = elementRegistry.get('Event1'),
            flowBo = flowConnection.businessObject;

        modeling.updateProperties(flowConnection, {
          'xmlns:foo': 'http://foo',
          'foo:customAttr': 'FOO'
        });

        // when
        commandStack.undo();

        // then
        expect(flowBo.get('xmlns:foo')).not.to.exist;
        expect(flowBo.get('foo:customAttr')).not.to.exist;
      }
    ));

  });


  describe('should redo', function() {

    it('updating name', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var flowConnection = elementRegistry.get('Event1');

        // when
        modeling.updateProperties(flowConnection, { name: 'FOO BAR' });
        commandStack.undo();
        commandStack.redo();

        // then
        expect(flowConnection.businessObject.name).to.equal('FOO BAR');

        // flow got updated, too
        expect(updatedElements).to.include(flowConnection.label);
      }
    ));


    it('unsetting name', inject(
      function(elementRegistry, commandStack, modeling) {

        // given
        var flowConnection = elementRegistry.get('Event1');

        modeling.updateProperties(flowConnection, { name: undefined });

        // when
        commandStack.undo();
        commandStack.redo();

        // then
        expect(flowConnection.businessObject.name).not.to.exist;
      }
    ));

  });



  describe('error handling', function() {

    it('should ignore unchanged id', inject(
      function(elementRegistry, modeling) {

        // given
        var flowConnection = elementRegistry.get('Event1'),
            flowBo = flowConnection.businessObject;

        var ids = flowBo.$model.ids;

        // when
        modeling.updateProperties(flowConnection, { id: 'Event1' });

        // then
        expect(ids.assigned('Event1')).to.eql(flowBo);

        expect(flowBo.$id).to.equal('Event1');
      }
    ));


    it('should ignore setting color on elements without di', inject(
      function(modeling, factory) {

        // given
        var rootElement = factory.create('ifml:ViewComponent');

        // when
        modeling.updateProperties(rootElement, {
          di: {
            fill: 'fuchsia'
          }
        });

        // then
        expect(getDi(rootElement)).not.to.exist;
      }
    ));

  });

});
