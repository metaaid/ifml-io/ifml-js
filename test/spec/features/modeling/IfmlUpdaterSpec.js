import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import { getDi } from 'lib/util/ModelUtil';

import { pick } from 'min-dash';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';
import { is } from '../../../../lib/util/ModelUtil';

var testModules = [ coreModule, modelingModule ];


describe('features - ifml-updater', function() {

  var diagramXML = require('../../../fixtures/ifml/allElements.ifml');

  beforeEach(bootstrapModeler(diagramXML, {
    modules: testModules
  }));
  
  var element;
  beforeEach(inject(function (elementRegistry) {
    element = function (id) {
      var el = elementRegistry.get(id);
      if(el == null){
        throw Error('Element with ID "' + id +'" does not exist');
      }
      return el;
    };
  }));


  describe('connection cropping', function() {

    afterEach(sinon.restore);


    it('should crop connection only once per reconnect', inject(
      function(modeling, connectionDocking) {

        // given
        var sequenceFlow = element('NavigationFlow1'),
            target = element('Window1'),
            cropSpy = sinon.spy(connectionDocking, 'getCroppedWaypoints');

        // when
        modeling.reconnectEnd(sequenceFlow, target, { x: 418, y: 260 });

        // then
        expect(cropSpy).to.have.been.calledOnce;
        expect(cropSpy).to.have.been.calledWith(sequenceFlow);
      }
    ));


    it('should not crop connection after pasting', inject(
      function(canvas, copyPaste, connectionDocking) {

        // given
        var sequenceFlow = element('NavigationFlow1'),
            target = element('Window1'),
            cropSpy = sinon.spy(connectionDocking, 'getCroppedWaypoints');

        copyPaste.copy([
          target,
          sequenceFlow
        ]);

        // when
        copyPaste.paste({
          element: canvas.getRootElement(),
          point: {
            x: 500,
            y: 500
          }
        });

        // then
        expect(cropSpy).not.to.have.been.calledOnce;
      }
    ));

  });


  describe('incomplete DI', function() {

    var diagramXML = require('./IfmlUpdater.incompleteDi.ifml');

    beforeEach(bootstrapModeler(diagramXML, {
      modules: testModules
    }));
    

    it('should add missing label di:Bound', inject(
      function(modeling, elementRegistry) {

        // given
        var event = elementRegistry.get('Event1');
        var label = event.label;
        var di = getDi(event);

        // when
        modeling.moveElements([ label ], { x: 20, y: 20 });

        var labelBounds = di.ownedLabel.bounds;

        // then
        expect(labelBounds).to.exist;

        expect(labelBounds).to.include.keys(
          'x', 'y',
          'width', 'height'
        );
      }
    ));


    it('should add missing ifml:IFMLLabel', inject(
      function(modeling, elementRegistry) {

        // given
        var event = elementRegistry.get('Event2'),
            label = event.label,
            di = getDi(event);

        // when
        modeling.moveElements([ label ], { x: 20, y: 20 });

        var diLabel = di.ownedLabel;

        // then
        expect(diLabel).to.exist;

        expect(diLabel.bounds).to.exist;
      }
    ));

  });

  describe('should not update parents for external element', function() {  
    
    it('ActivationExpression', inject(function(modeling, elementRegistry) {
      // given
      var externalEvent = elementRegistry.get('ActivationExpression1');
      var vc = elementRegistry.get('ViewContainer1');
      var externalEventParent = externalEvent.parent;
      

      // when
      modeling.moveElements([ externalEvent ], { x: 20, y: 20 }, vc);

      // then
      expect(externalEvent.parent).to.equal(externalEventParent);
    }));
    it('ParameterBindingGroup', inject(function(modeling, elementRegistry) {
      // given
      var externalEvent = elementRegistry.get('ParameterBindingGroup1');
      var vc = elementRegistry.get('_myModel');
      var externalEventParent = externalEvent.parent;
      

      // when
      modeling.moveElements([ externalEvent ], { x: 20, y: 20 }, vc);

      // then
      expect(externalEvent.parent).to.equal(externalEventParent);
    }));
    it('Annotation1', inject(function(modeling, elementRegistry) {
      // given
      var externalEvent = elementRegistry.get('Annotation1');
      var vc = elementRegistry.get('Form1');
      var externalEventParent = externalEvent.parent;
      

      // when
      modeling.moveElements([ externalEvent ], { x: 20, y: 20 }, vc);

      // then
      expect(externalEvent.parent).to.equal(externalEventParent);
    }));
  });
  describe('should update parents on new elements', function() {  
    
    it('Event', inject(function(elementFactory, modeling, elementRegistry) {
      // given
      var vc = elementRegistry.get('ViewContainer1');
      var event = elementFactory.createShape({
        type: 'ifml:Event'
      });
      var shape = modeling.createShape(event, {
        x: vc.x,
        y: vc.y
      }, vc);
      
      // then
      expect(is(shape, 'ifml:ViewElementEvent')).to.be.true;
    }));
  });
});
