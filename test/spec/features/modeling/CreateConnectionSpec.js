import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import {
  getDi
} from 'lib/util/ModelUtil';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features/modeling - create connection', function() {

  var diagramXML = require('../../../fixtures/ifml/simple/navigationFlow.ifml');

  var testModules = [ coreModule, modelingModule ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  it('should connect', inject(function(elementRegistry, modeling, factory) {

    // given
    var taskShape = elementRegistry.get('ViewComponent1'),
        task = taskShape.businessObject,
        taskDi = getDi(taskShape),
        gatewayShape = elementRegistry.get('ViewComponent2'),
        gateway = gatewayShape.businessObject;

    // when
    var sequenceFlowConnection = modeling.createConnection(taskShape, gatewayShape, {
      type: 'ifml:DataFlow'
    }, taskShape.parent);

    var sequenceFlow = sequenceFlowConnection.businessObject,
        sequenceFlowDi = getDi(sequenceFlowConnection);

    // then
    expect(sequenceFlowConnection).to.exist;
    expect(sequenceFlow).to.exist;

    expect(sequenceFlow.sourceInteractionFlowElement).to.eql(task);
    expect(sequenceFlow.targetInteractionFlowElement).to.eql(gateway);

    expect(task.outInteractionFlows).to.include(sequenceFlow);
    expect(gateway.inInteractionFlows).to.include(sequenceFlow);

    expect(sequenceFlowDi.$parent).to.eql(taskDi.$parent);
    expect(sequenceFlowDi.$parent.ownedElement).to.include(sequenceFlowDi);

    // expect cropped connection
    expect(sequenceFlowConnection.waypoints).eql([
      { original: { x: 580, y: 250 }, x: 670, y: 250 },
      { original: { x: 910, y: 250 }, x: 820, y: 250 }
    ]);

    var diWaypoints = factory.createDiWaypoints([
      { x: 670, y: 250 },
      { x: 820, y: 250 },
    ]);

    // expect cropped waypoints in di
    expect(sequenceFlowDi.waypoint).eql(diWaypoints);
  }));


  it('should connect with custom start / end', inject(function(elementRegistry, modeling) {

    // given
    var sourceShape = elementRegistry.get('ViewComponent1'),
        sourcePosition = {
          x: 740,
          y: 400
        },
        targetShape = elementRegistry.get('ViewComponent2'),
        targetPosition = {
          x: 420,
          y: 130
        };

    // when
    var newConnection = modeling.connect(
      sourceShape, targetShape,
      null,
      {
        connectionStart: sourcePosition,
        connectionEnd: targetPosition
      }
    );

    // then
    // expect cropped connection with custom start/end
    expect(newConnection).to.have.waypoints([
      { x: 670, y: 400 },
      { x: 745, y: 400 },
      { x: 745, y: 130 },
      { x: 820, y: 130 }
    ]);
  }));


  it('should undo', inject(function(elementRegistry, commandStack, modeling) {

    // given
    var taskShape = elementRegistry.get('ViewComponent1'),
        task = taskShape.businessObject,
        gatewayShape = elementRegistry.get('ViewComponent2'),
        gateway = gatewayShape.businessObject;


    var sequenceFlowConnection = modeling.createConnection(taskShape, gatewayShape, {
      type: 'ifml:DataFlow'
    }, taskShape.parent);

    var sequenceFlow = sequenceFlowConnection.businessObject;

    // when
    commandStack.undo();

    // then
    expect(sequenceFlow.$parent).to.be.null;
    expect(sequenceFlow.sourceInteractionFlowElement).to.be.null;
    expect(sequenceFlow.targetInteractionFlowElement).to.be.null;

    expect(task.outInteractionFlows).not.to.include(sequenceFlow);
    expect(gateway.inInteractionFlows).not.to.include(sequenceFlow);
  }));


  it('should redo', inject(function(elementRegistry, commandStack, modeling) {

    // given
    var taskShape = elementRegistry.get('ViewComponent1'),
        task = taskShape.businessObject,
        taskDi = getDi(taskShape),
        gatewayShape = elementRegistry.get('ViewComponent2'),
        gateway = gatewayShape.businessObject;


    var sequenceFlowConnection = modeling.createConnection(taskShape, gatewayShape, {
      type: 'ifml:DataFlow'
    }, taskShape.parent);

    var sequenceFlow = sequenceFlowConnection.businessObject,
        sequenceFlowDi = getDi(sequenceFlowConnection);

    var newWaypoints = sequenceFlowConnection.waypoints,
        newDiWaypoints = sequenceFlowDi.waypoint;

    // when
    commandStack.undo();
    commandStack.redo();

    // then
    expect(sequenceFlow.sourceInteractionFlowElement).to.eql(task);
    expect(sequenceFlow.targetInteractionFlowElement).to.eql(gateway);

    expect(task.outInteractionFlows).to.include(sequenceFlow);
    expect(gateway.inInteractionFlows).to.include(sequenceFlow);

    expect(sequenceFlowDi.$parent).to.eql(taskDi.$parent);
    expect(sequenceFlowDi.$parent.ownedElement).to.include(sequenceFlowDi);

    // expect cropped connection
    expect(sequenceFlowConnection.waypoints).eql(newWaypoints);

    // expect cropped waypoints in di
    expect(sequenceFlowDi.waypoint).eql(newDiWaypoints);
  }));

});
