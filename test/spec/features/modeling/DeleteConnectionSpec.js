import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features/modeling - #removeConnection', function() {

  var diagramXML = require('../../../fixtures/ifml/simple/navigationFlow.ifml');

  var testModules = [ coreModule, modelingModule ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  describe('shape handling', function() {

    it('should execute', inject(function(elementRegistry, modeling) {

      // given
      var navigationFlowShape = elementRegistry.get('NavigationFlow'),
          navigationFlow = navigationFlowShape.businessObject;

      // when
      modeling.removeConnection(navigationFlowShape);

      // then
      expect(navigationFlow.$parent).to.be.null;
    }));
  });


  describe('undo support', function() {

    it('should undo', inject(function(elementRegistry, modeling, commandStack) {

      // given
      var navigationFlowShape = elementRegistry.get('NavigationFlow'),
          navigationFlow = navigationFlowShape.businessObject,
          parent = navigationFlow.$parent;

      // when
      modeling.removeConnection(navigationFlowShape);
      commandStack.undo();

      // then
      expect(navigationFlow.$parent).to.eql(parent);
    }));
  });


  describe('redo support', function() {

    it('redo', inject(function(elementRegistry, modeling, commandStack) {

      // given
      var navigationFlowShape = elementRegistry.get('NavigationFlow'),
          navigationFlow = navigationFlowShape.businessObject;

      // when
      modeling.removeConnection(navigationFlowShape);
      commandStack.undo();
      commandStack.redo();

      // then
      expect(navigationFlow.$parent).to.be.null;
    }));
  });

});
