import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';
import { getDi } from 'lib/util/ModelUtil';


describe('features/modeling - move connection', function() {

  describe('should move connection', function() {

    var diagramXML = require('../../../fixtures/ifml/simple/navigationFlow.ifml');

    beforeEach(bootstrapModeler(diagramXML, {
      modules: [
        coreModule,
        modelingModule
      ]
    }));


    it('execute', inject(function(elementRegistry, modeling, factory) {

      // given
      var navigationFlowConnection = elementRegistry.get('NavigationFlow'),
          navigationFlowDi = getDi(navigationFlowConnection);

      // when
      modeling.moveConnection(navigationFlowConnection, { x: 20, y: 10 });

      var expectedWaypoints = [
        { x: 700, y: 260 },
        { x: 840, y: 260 },
      ];

      // then

      // expect cropped connection
      expect(navigationFlowConnection).to.have.waypoints(expectedWaypoints);

      // expect cropped waypoints in di
      var diWaypoints = factory.createDiWaypoints(expectedWaypoints);

      expect(navigationFlowDi.waypoint).eql(diWaypoints);
    }));


    it('undo', inject(function(elementRegistry, commandStack, modeling) {

      // given
      var navigationFlowConnection = elementRegistry.get('NavigationFlow'),
          navigationFlowDi = getDi(navigationFlowConnection);

      var oldWaypoints = navigationFlowConnection.waypoints,
          oldDiWaypoints = navigationFlowDi.waypoint;

      modeling.moveConnection(navigationFlowConnection, { x: 20, y: 10 });

      // when
      commandStack.undo();

      // then
      expect(navigationFlowConnection.waypoints).eql(oldWaypoints);
      expect(navigationFlowDi.waypoint).eql(oldDiWaypoints);
    }));


    it('redo', inject(function(elementRegistry, commandStack, modeling) {

      // given
      var navigationFlowConnection = elementRegistry.get('NavigationFlow'),
          navigationFlowDi = getDi(navigationFlowConnection);

      modeling.moveConnection(navigationFlowConnection, { x: 20, y: 10 });

      var newWaypoints = navigationFlowConnection.waypoints,
          newDiWaypoints = navigationFlowDi.waypoint;

      // when
      commandStack.undo();
      commandStack.redo();

      // then
      expect(navigationFlowConnection.waypoints).eql(newWaypoints);
      expect(navigationFlowDi.waypoint).eql(newDiWaypoints);
    }));
  });
});
