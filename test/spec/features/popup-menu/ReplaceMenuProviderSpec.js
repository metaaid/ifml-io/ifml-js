import {
  bootstrapModeler,
  getIfmlJS,
  inject
} from 'test/TestHelper';

import {
  createEvent as globalEvent
} from '../../../util/MockEvents';

import coreModule from 'lib/core';
import customRulesModule from '../../../util/custom-rules';
import modelingModule from 'lib/features/modeling';
import replaceMenuProviderModule from 'lib/features/popup-menu';


import {
  query as domQuery,
  queryAll as domQueryAll,
  classes as domClasses
} from 'min-dom';
import { is } from '../../../../lib/util/ModelUtil';


describe('features/popup-menu - replace menu provider', function() {

  var testModules = [
    coreModule,
    modelingModule,
    replaceMenuProviderModule,
    customRulesModule
  ];



  var diagramXML = require('../../../fixtures/ifml/allElements.ifml');

  beforeEach(bootstrapModeler(diagramXML, {
    modules: testModules.concat([ customRulesModule ])
  }));

  function shouldREplaceInto(elementId, action, expectedType){
    return inject(function(elementRegistry, selection) {
      var element = elementRegistry.get(elementId);

      openPopup(element); 

      // when
      triggerAction(action);

      // then
      expect(selection.get()[0].businessObject.$type).to.eq(expectedType);
    });
  }

  describe('replace menu', function() {

    describe('viewcontainer', function() {
      it('should replace into menu', shouldREplaceInto('ViewContainer1', 'replace-with-menu', 'ifml:Menu'));
      it('should replace into window', shouldREplaceInto('ViewContainer1','replace-with-window', 'ifml:Window'));
    });

    describe('menu', function() {
      it('should replace into vc', () => {
        shouldREplaceInto('Menu1','replace-with-viewcontainer','ifml:ViewContainer');
      });
        it('should replace into window', () => {
          shouldREplaceInto('Menu1','replace-with-window','ifml:Window');
      });
    });

    describe('window', function() {
      it('should replace into viewcontainer', () => {
        shouldREplaceInto('Window1', 'replace-with-viewcontainer','ifml:ViewContainer');
      });
      it('should replace into menu', () => {
        shouldREplaceInto('Window1', 'replace-with-menu', 'ifml:Menu');
      });
    });
    
    describe('viewcomponent', function() {
      it('should replace into list', shouldREplaceInto('ViewComponent1','replace-with-list','ifml:List'));
      it('should replace into details', shouldREplaceInto('ViewComponent1','replace-with-details','ifml:Details'));
      it('should replace into form', shouldREplaceInto('ViewComponent1','replace-with-form','ifml:Form'));
    });

    describe('list', function() {
      it('should replace into viewcomponent', shouldREplaceInto('List1','replace-with-viewcomponent','ifml:ViewComponent'));
      it('should replace into details', shouldREplaceInto('List1','replace-with-details','ifml:Details'));
      it('should replace into form', shouldREplaceInto('List1','replace-with-form','ifml:Form'));
    });
  });
  

  describe('replace menu', function() {

    describe('list replacements', function() {
      it('should replace events field', inject(function(elementRegistry, selection) {
        var list = elementRegistry.get("List1");
        expect(is(list.businessObject.get('selectEvent')[0], "ifml:OnSelectEvent")).to.be.true ;
  
        openPopup(list); 
  
        // when
        triggerAction("replace-with-viewcomponent");
  

        var replacedElement = selection.get()[0].businessObject;
        // then
        expect(replacedElement.get('selectEvent')).to.not.exist ;
        expect(replacedElement.get('viewElementEvents')[0].$type).to.eq("ifml:OnSelectEvent")

        // and vice versa
        // when
        openPopup(selection.get()[0]); 
        triggerAction("replace-with-list");
  

        replacedElement = selection.get()[0].businessObject;
        // then
        expect(replacedElement.get('viewElementEvents')).to.have.lengthOf(0)
        expect(replacedElement.get('selectEvent')[0].$type).to.eq("ifml:OnSelectEvent")
      }));
    });
  });
});








// helpers ////////////

function openPopup(element, offset) {
  offset = offset || 100;

  getIfmlJS().invoke(function(popupMenu) {

    popupMenu.open(element, 'ifml-replace', {
      x: element.x + offset, y: element.y + offset
    });

  });
}

function queryEntry(id) {
  var container = getIfmlJS().get('canvas').getContainer();

  return domQuery('.djs-popup [data-id="' + id + '"]', container);
}

function queryEntries() {
  var container = getIfmlJS().get('canvas').getContainer();

  return domQueryAll('.djs-popup .entry', container);
}

function queryEntryLabel(id) {
  var entry = queryEntry(id);

  return domQuery('span', entry);
}

function triggerAction(id) {
  var entry = queryEntry(id);

  if (!entry) {
    throw new Error('entry "'+ id +'" not found in replace menu');
  }

  var popupMenu = getIfmlJS().get('popupMenu');

  return popupMenu.trigger(globalEvent(entry, { x: 0, y: 0 }));
}
