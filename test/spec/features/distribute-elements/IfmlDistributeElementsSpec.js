import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import distributeElements from 'lib/features/distribute-elements';
import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';

function last(arr) {
  return arr[arr.length - 1];
}


describe('features/distribute-elements', function() {

  var testModules = [ distributeElements, modelingModule, coreModule ];


  describe('basics', function() {

    var basicXML = require('../../../fixtures/ifml/distribute-align/3viewcomponents.ifml');

    beforeEach(bootstrapModeler(basicXML, { modules: testModules }));

    var elements;

    beforeEach(inject(function(elementRegistry, canvas) {
      elements = elementRegistry.filter(function(element) {
        return element.parent;
      });
    }));


    it('should align horizontally', inject(function(distributeElements) {

      // when
      var rangeGroups = distributeElements.trigger(elements, 'horizontal');
      var margin = rangeGroups[1].range.min - rangeGroups[0].range.max;

      // then
      expect(rangeGroups).to.have.length(3);

      expect(margin).to.equal(15);

      expect(rangeGroups[0].range).to.eql({
        min: 5, max: 175
      });

      expect(rangeGroups[1].range).to.eql({
        min: 190, max: 680
      });

      expect(last(rangeGroups).range).to.eql({
        min: 1005, max: 1175
      });
    }));


    it('should align vertically', inject(function(distributeElements) {

      // when
      var rangeGroups = distributeElements.trigger(elements, 'vertical'),
          margin = rangeGroups[1].range.min - rangeGroups[0].range.max;

      // then
      expect(rangeGroups).to.have.length(3);

      expect(margin).to.equal(100);

      expect(rangeGroups[0].range).to.eql({
        min: 0, max: 80
      });
      expect(rangeGroups[1].range).to.eql({
        min: 180, max: 308
      });

      expect(last(rangeGroups).range).to.eql({
        min: 455, max: 525
      });

    }));

    it('should not distribute boundary events', inject(function(distributeElements, elementRegistry) {
  
      // given
      var boundaryEvent = elementRegistry.get('Event1');
  
      // when
      var rangeGroups = distributeElements.trigger(elements, 'horizontal');
  
      // then
      expect(rangeGroups).to.have.length(3);
  
      expect(rangeGroups[1].elements).not.to.include(boundaryEvent);
  
    }));
  });
});
