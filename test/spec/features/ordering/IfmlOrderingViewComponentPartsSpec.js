import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import {
  move,
  attach,
  connect,
  expectZOrder
} from './Helper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';
import ifmlOrderingProvider from 'lib/features/ordering';
// import elementRegistry from  'lib/features/element-registry';


describe('features/modeling - ordering', function() {

  var testModules = [
    coreModule,
    modelingModule,
    ifmlOrderingProvider,
  ];
  const FIELD_DISTANCE = 35;

  describe('viewcomponentparts', function() {

    var diagramXML = require('../../../fixtures/ifml/ordering.ifml');
    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));

    describe('simple ordering', function() {

      describe('fields should be have the right target index', function() {

        it('field1', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field1');
          var parent =  elementRegistry.get('Form1');

          // when
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(0);
          expect(target.parent).to.equal(parent);
        }));

        it('field2', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field2');
          var parent =  elementRegistry.get('Form1');

          // when
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(1);
          expect(target.parent).to.equal(parent);
        }));

        it('field3', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field3');
          var parent =  elementRegistry.get('Form1');

          // when
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(2);
          expect(target.parent).to.equal(parent);
        }));
      });

      describe('move field forward', function() {

        it('position 0 to 1', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field1');
          var parent =  elementRegistry.get('Form1');

          // when
          move("Field1", { x: 0, y: FIELD_DISTANCE }, "Form1");
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(1);
          expect(target.parent).to.equal(parent);
        }));

        it('position 0 to 2', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field1');
          var parent =  elementRegistry.get('Form1');

          // when
          move("Field1", { x: 0, y: 2*FIELD_DISTANCE }, "Form1");
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(2);
          expect(target.parent).to.equal(parent);
        }));

        it('position 2 to 3', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field2');
          var parent =  elementRegistry.get('Form1');

          // when
          move("Field2", { x: 0, y: FIELD_DISTANCE }, "Form1");
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(2);
          expect(target.parent).to.equal(parent);
        }));

      });
        
      describe('move field backward', function() {

        it('position 1 to 0', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field2');
          var parent =  elementRegistry.get('Form1');

          // when
          move("Field2", { x: 0, y: -FIELD_DISTANCE - 1}, "Form1");
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(0);
          expect(target.parent).to.equal(parent);
        }));

        it('position 2 to 1', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field3');
          var parent =  elementRegistry.get('Form1');

          // when
          move("Field3", { x: 0, y: -FIELD_DISTANCE - 1 }, "Form1");
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(1);
          expect(target.parent).to.equal(parent);
        }));

        it('position 2 to 0', inject(function(ifmlOrderingProvider, elementRegistry) {

          // when
          var field =  elementRegistry.get('Field3');
          var parent =  elementRegistry.get('Form1');

          // when
          move("Field3", { x: 0, y: -2*FIELD_DISTANCE - 1 }, "Form1");
          var target = ifmlOrderingProvider.getOrdering(field, parent);

          // then
          expect(target.index).to.equal(0);
          expect(target.parent).to.equal(parent);
        }));
      });

    });

    describe('sub component to main component' , function() {
      it('should be ordered after the main component, old parent should be resized', inject(function(ifmlOrderingProvider, elementRegistry) {
            
          // given
          var subComponent =  elementRegistry.get('VA1');
          var main =  elementRegistry.get('List1');
    
          // when
          move("VA1", { x: 0, y: 2*FIELD_DISTANCE}, "List1");
          
          // then
          var target = ifmlOrderingProvider.getOrdering(subComponent, main);
          expect(target.index).to.equal(1);
          expect(target.parent).to.equal(main);
      }));
    });
  });


});
