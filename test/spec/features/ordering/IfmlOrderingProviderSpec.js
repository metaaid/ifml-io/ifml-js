import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import {
  move,
  attach,
  connect,
  expectZOrder
} from './Helper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features/modeling - ordering', function() {

  var testModules = [
    coreModule,
    modelingModule
  ];

  describe('all elements', function() {

    var diagramXML = require('../../../fixtures/ifml/allElements.ifml');
    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));

    describe('events', function() {

      describe('move', function() {

        it('should stay in front of Task', inject(function() {

          // when
          move('List1');

          // then
          expectZOrder('List1', 'Event_select_List');
        }));


        it('should stay in front of Task, moving both', inject(function() {

          // when
          move([ 'Event_select_List', 'List1' ]);

          // then
          expectZOrder('List1', 'Event_select_List');
        }));

      });

    });


    describe('labels', function() {


      describe('should stay always in front', function() {

        it('moving <Event_label> onto <ViewContainer>', inject(function() {

          // when
          move('Event_select_List_label', { x: 505, y: 175 }, 'ViewContainer1', false);

          // then
          expectZOrder('ViewContainer1', 'Event_select_List_label', 'Event_select_List_label');
        }));
      });

    });

    describe('annotations', function() {


      describe('should stay always in front', function() {

        it('moving Annotation onto ViewContainer', inject(function() {

          // when
          move('Annotation1', { x: -1050, y: 0 }, 'ViewContainer1', false);

          // then
          expectZOrder('ViewContainer1', 'Annotation1');
        }));

        it('moving Annotation onto ViewContainer', inject(function() {

          // when
          move('Window1', { x: -1150, y: 0 }, 'ViewContainer1', false);

          // then
          expectZOrder('ViewContainer1', 'Window1', 'Annotation1');
        }));
      });

    });

  });

  describe('connections', function() {

    var diagramXML = require('../../../fixtures/ifml/simple/navigationFlowInContainer.ifml');

    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


    it('should render NavigationFlow tasks', inject(function() {

      // when
      var connection = connect('Event', 'ViewComponent2');

      // then
      expectZOrder('Container', 'ViewComponent1', 'ViewComponent2', connection);
    }));

    it('should keep Ordering when moving container', inject(function() {

      // when
      move('Container', { x: -50, y: 0 }, '_myModel', false);

      // then
      expectZOrder('Container', 'ViewComponent1', 'ViewComponent2', 'NavigationFlow');
    }));

  });

});
