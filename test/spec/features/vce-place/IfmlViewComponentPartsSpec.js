import { bootstrapModeler, inject } from 'test/TestHelper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';
// lib/features/vcp-place
import autoPlaceModule from 'lib/features/auto-place';
import vcpPlacements from 'lib/features/vcp-place';
import ifmlOrderingProvider from 'lib/features/ordering';
import moveModule from 'diagram-js/lib/features/move';

import { asBounds, asTRBL } from 'diagram-js/lib/layout/LayoutUtil';
import { SUBCOMPONENT_DEFAULTHEIGHT, SUBCOMPONENT_OPTIONAL_PADDING_HORIZONTAL, SUBCOMPONENT_OPTIONAL_PADDING_TOP, SUBCOMPONENT_OPTIONAL_MARGIN_VERTICAL, SUBCOMPONENT_OPTIONAL_PADDINB_BOTTOM, VIEWCOMPONENT_PADDING_TOP, VIEWCOMPONENT_PADDING_BOTTOM, VIEWCOMPONENT_PADDING_HORIZONTAL } from '../../../../lib/features/vcp-place/ViewComponentPartsPlacement';
// import { move } from '../ordering/Helper';
// import elementRegistry from  'lib/features/element-registry';

import {
  createCanvasEvent as canvasEvent
} from 'test/util/MockEvents.js';

describe('features/modeling - ordering', function () {
  var testModules = [
    coreModule,
    modelingModule,
    autoPlaceModule,
    ifmlOrderingProvider,
    moveModule,
    vcpPlacements,
    {
      __init__: [ function(dragging) {
        dragging.setOptions({ manual: true });
      } ]
    }
  ];

  describe('viewcomponentparts', function () {
    var diagramXML = require('../../../fixtures/ifml/simple/databinding.ifml');
    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));
    

    describe('add subcomponent should resize parent', function () {
      it('autoplace a vizualisation attribute on a databinding', inject(function (
        ifmlAutoPlace,
        elementFactory,
        elementRegistry
      ) {
        // given
        var shape1 = elementFactory.createShape({
          id: 'VA1',
          type: 'ifml:VisualizationAttribute',
        });

        var source = elementRegistry.get('DataBinding1');

        // when
        ifmlAutoPlace.insert(source, shape1);

        // then
        var bounds = asBounds(asTRBL(shape1));
        var parentBounds = asBounds(asTRBL(source));
        expect(bounds.x).to.eql(412+10);
        expect(bounds.y).to.eql(193 + VIEWCOMPONENT_PADDING_TOP + SUBCOMPONENT_OPTIONAL_PADDING_TOP);
        expect(bounds.width).to.eql(120);
        expect(bounds.height).to.eql(SUBCOMPONENT_DEFAULTHEIGHT);
        expect(shape1.parent).to.eql(source);

        expect(parentBounds.height).to.eql(SUBCOMPONENT_DEFAULTHEIGHT + SUBCOMPONENT_OPTIONAL_PADDING_TOP + SUBCOMPONENT_OPTIONAL_PADDINB_BOTTOM);
      }));
    });

    it('add subcomponent', inject(function (modeling, elementFactory, elementRegistry, ifmlAutoPlace) {
      // given
      var source = elementRegistry.get('DataBinding1');

      // when
      var shape = modeling.createShape(
        { id: 'VA1', type: 'ifml:VisualizationAttribute' },
        { x: 820, y: 390 },
        source
      );
      // ifmlAutoPlace.insert(source, shape1);

      // then
      var bounds = asBounds(asTRBL(source));
      expect(bounds.height).to.eql(SUBCOMPONENT_DEFAULTHEIGHT + SUBCOMPONENT_OPTIONAL_PADDING_TOP + SUBCOMPONENT_OPTIONAL_PADDINB_BOTTOM);
      expect(shape.parent).to.eql(source);
    }));

    it('add two subcomponent', inject(function (modeling, elementFactory, elementRegistry, ifmlAutoPlace) {
      // given
      var source = elementRegistry.get('DataBinding1');

      // when
      modeling.createShape(
        { id: 'VA1', type: 'ifml:VisualizationAttribute' },
        { x: 820, y: 390 },
        source
      );
      modeling.createShape(
        { id: 'VA2', type: 'ifml:VisualizationAttribute' },
        { x: 820, y: 390 },
        source
      );

      // then
      var bounds = asBounds(asTRBL(source));
      expect(bounds.height).to.eql(2*SUBCOMPONENT_DEFAULTHEIGHT + SUBCOMPONENT_OPTIONAL_PADDING_TOP + SUBCOMPONENT_OPTIONAL_PADDINB_BOTTOM + SUBCOMPONENT_OPTIONAL_MARGIN_VERTICAL);
    }));
  });


  describe('viewcomponentparts', function () {
    
    var diagramXML = require('../../../fixtures/ifml/simple/viewcomponentParts.ifml');
    beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));
    
    var moveShape;
    
    beforeEach(inject(function(move, dragging, elementRegistry) {

      moveShape = function(shape, target, position) {
        var startPosition = { x: shape.x + shape.width / 2, y: shape.y + shape.height/2 };

        var moveToPositionAbsolute = {
          x: position.x + shape.x,
          y: position.y + shape.y
        }

        move.start(canvasEvent(startPosition), shape);

        dragging.hover({
          element: target,
          gfx: elementRegistry.getGraphics(target)
        });

        dragging.move(canvasEvent(moveToPositionAbsolute));
      };
    }));
    it('add va in the middle', inject(function (modeling, elementFactory, elementRegistry, ifmlAutoPlace) {
      // given
      var vc = elementRegistry.get('ViewComponent1');
      var source = elementRegistry.get('Databinding1');

      // when
      modeling.createShape(
        { id: 'VC1_VA1', type: 'ifml:VisualizationAttribute' },
        { x: 0, y: 0 },
        source
      );

      // then
      var bounds = asBounds(asTRBL(vc));
      var db1Height = SUBCOMPONENT_OPTIONAL_PADDING_TOP + SUBCOMPONENT_DEFAULTHEIGHT + SUBCOMPONENT_OPTIONAL_PADDINB_BOTTOM;
      var db2Height = SUBCOMPONENT_DEFAULTHEIGHT;
      var height = VIEWCOMPONENT_PADDING_TOP + db1Height + SUBCOMPONENT_OPTIONAL_MARGIN_VERTICAL + db2Height + VIEWCOMPONENT_PADDING_BOTTOM;
      expect(bounds.height).to.eql(height);
    }));
    it('resizing vc should resize sub components', inject(function (modeling, elementFactory, elementRegistry, ifmlAutoPlace) {
      // given
      var vc = elementRegistry.get('ViewComponent1');
      var db = elementRegistry.get('Databinding1');
      
      // when
      modeling.resizeShape(vc, { x: 500, y: 0, width: 400, height: 200 });

      // then
      expect(db.width).to.eql(400 - 2 * VIEWCOMPONENT_PADDING_HORIZONTAL);
      expect(db.width).to.eql(vc.width - 2 * VIEWCOMPONENT_PADDING_HORIZONTAL);
    }));

    it('resizing vc should resize sub sub components', inject(function (modeling, elementFactory, elementRegistry, ifmlAutoPlace) {
      // given
      var vc = elementRegistry.get('myListId');
      var va = elementRegistry.get('VA1');

      // when
      modeling.resizeShape(vc, { x: 500, y: 0, width: 400, height: 200 });

      // then
      expect(va.width).to.eql(400 - 2 * VIEWCOMPONENT_PADDING_HORIZONTAL - 2 * SUBCOMPONENT_OPTIONAL_PADDING_HORIZONTAL);
    }));

    it('move va to main viewcomponent should keep order', inject(function (modeling, elementFactory, elementRegistry, vcpPlacements, dragging) {
      // given
      var vc = elementRegistry.get('myListId');
      var va = elementRegistry.get('VA1');
      var db = elementRegistry.get('myDatabinding');

      // when
      moveShape(va, vc, { x: 0, y: 45 });
      dragging.end();

      // then
      expect(db.y).to.eql(VIEWCOMPONENT_PADDING_TOP);
      expect(va.y).to.eql(VIEWCOMPONENT_PADDING_TOP + SUBCOMPONENT_DEFAULTHEIGHT + SUBCOMPONENT_OPTIONAL_MARGIN_VERTICAL);

    }));
  });
});
