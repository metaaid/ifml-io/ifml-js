import { bootstrapModeler, getIfmlJS, inject } from "test/TestHelper";

import TestContainer from "mocha-test-container-support";

import { query as domQuery, queryAll as domQueryAll } from "min-dom";

import { is } from "lib/util/ModelUtil";

import { createCanvasEvent as canvasEvent } from "../../../util/MockEvents";

import contextPadModule from "lib/features/context-pad";
import coreModule from "lib/core";
import modelingModule from "lib/features/modeling";
import replaceMenuModule from "lib/features/popup-menu";
import createModule from "diagram-js/lib/features/create";
import customRulesModule from "../../../util/custom-rules";

describe("features - context-pad", function () {
  var testModules = [
    coreModule,
    modelingModule,
    contextPadModule,
    replaceMenuModule,
    customRulesModule,
    createModule,
  ];

  describe("remove action rules", function () {
    var diagramXML = require("../../../fixtures/ifml/allElements.ifml");

    beforeEach(
      bootstrapModeler(diagramXML, {
        modules: testModules,
      })
    );

    var deleteAction;

    beforeEach(inject(function (contextPad) {
      deleteAction = function (target) {
        return padEntry(contextPad.getPad(target).html, "delete");
      };
    }));

    it("should add delete action by default", inject(function (
      elementRegistry,
      contextPad
    ) {
      // given
      var element = elementRegistry.get("Event_select_List");

      // when
      contextPad.open(element);

      // then
      expect(deleteAction(element)).to.exist;
    }));

    it("should include delete action when rule returns true", inject(function (
      elementRegistry,
      contextPad,
      customRules
    ) {
      // given
      customRules.addRule("elements.delete", 1500, function () {
        return true;
      });

      var element = elementRegistry.get("Event_select_List");

      // when
      contextPad.open(element);

      // then
      expect(deleteAction(element)).to.exist;
    }));

    it("should NOT include delete action when rule returns false", inject(function (
      elementRegistry,
      contextPad,
      customRules
    ) {
      // given
      customRules.addRule("elements.delete", 1500, function () {
        return false;
      });

      var element = elementRegistry.get("Event_select_List");

      // when
      contextPad.open(element);

      // then
      expect(deleteAction(element)).not.to.exist;
    }));

    it("should call rules with [ element ]", inject(function (
      elementRegistry,
      contextPad,
      customRules
    ) {
      // given
      var element = elementRegistry.get("Event_select_List");

      customRules.addRule("elements.delete", 1500, function (context) {
        // element array is actually passed
        expect(context.elements).to.eql([element]);

        return true;
      });

      // then
      expect(function () {
        contextPad.open(element);
      }).not.to.throw;
    }));

    it("should include delete action when [ element ] is returned from rule", inject(function (
      elementRegistry,
      contextPad,
      customRules
    ) {
      // given
      customRules.addRule("elements.delete", 1500, function (context) {
        return context.elements;
      });

      var element = elementRegistry.get("Event_select_List");

      // when
      contextPad.open(element);

      // then
      expect(deleteAction(element)).to.exist;
    }));

    it("should NOT include delete action when [ ] is returned from rule", inject(function (
      elementRegistry,
      contextPad,
      customRules
    ) {
      // given
      customRules.addRule("elements.delete", 1500, function () {
        return [];
      });

      var element = elementRegistry.get("Event_select_List");

      // when
      contextPad.open(element);

      // then
      expect(deleteAction(element)).not.to.exist;
    }));

    describe("multi-element", function () {
      it("should add delete action by default", inject(function (
        elementRegistry,
        contextPad
      ) {
        // given
        var event = elementRegistry.get("Event_select_List"),
          task = elementRegistry.get("List1");

        // when
        contextPad.open([event, task]);

        // then
        // expect(deleteAction(event)).to.exist;
        expect(deleteAction([event, task])).to.exist;
      }));

      it("should NOT add delete action when rule returns false", inject(function (
        elementRegistry,
        contextPad,
        customRules
      ) {
        // given
        customRules.addRule("elements.delete", 1500, function () {
          return false;
        });

        var event = elementRegistry.get("Event_select_List"),
          task = elementRegistry.get("List1");

        // when
        contextPad.open([event, task]);

        // then
        expect(deleteAction([event, task])).not.to.exist;
      }));

      it("should trigger batch delete", inject(function (
        elementRegistry,
        contextPad,
        customRules
      ) {
        // given
        var event = elementRegistry.get("Event_select_List"),
          task = elementRegistry.get("List1");

        contextPad.open([event, task]);

        // when
        contextPad.trigger("click", padEvent("delete"));

        // then
        expect(elementRegistry.get("Event_select_List")).not.to.exist;
        expect(elementRegistry.get("List1")).not.to.exist;
      }));
    });

    describe("available entries", function () {
      function expectContextPadEntries(elementOrId, expectedEntries) {
        getIfmlJS().invoke(function (elementRegistry, contextPad) {
          var element =
            typeof elementOrId === "string"
              ? elementRegistry.get(elementOrId)
              : elementOrId;

          contextPad.open(element, true);

          var entries = contextPad._current.entries;

          expectedEntries.forEach(function (name) {
            if (name.charAt(0) === "!") {
              name = name.substring(1);

              expect(entries).not.to.have.property(name);
            } else {
              expect(entries).to.have.property(name);
            }
          });
        });
      }

      it("should provide ViewContainer entries", inject(function () {
        expectContextPadEntries("ViewContainer1", [
          "connect",
          "replace",
          "append.event",
          "change.isXOR",
          "change.isDefault",
          "change.isLandmark",
          "delete",
        ]);
      }));

      it("should provide Menu entries", inject(function () {
        expectContextPadEntries("Menu1", [
          "connect",
          "replace",
          "append.event",
          "change.isXOR",
          "change.isDefault",
          "change.isLandmark",
          "delete",
        ]);
      }));

      it("should provide Window1 entries", inject(function () {
        expectContextPadEntries("Window1", [
          "connect",
          "replace",
          "append.event",
          "change.isXOR",
          "change.isDefault",
          "change.isLandmark",
          "delete",
        ]);
      }));

      it("should provide Viewcomponent entries", inject(function () {
        expectContextPadEntries("ViewComponent1", [
          "connect",
          "replace",
          "append.databinding",
          "append.parameter",
          "append.activationExpression",
          "append.event",
          "delete",
        ]);
      }));

      it("should provide List entries", inject(function () {
        expectContextPadEntries("List1", [
          "connect",
          "replace",
          "append.databinding",
          "append.parameter",
          "append.activationExpression",
          "append.event",
          "append.event.select",
          "delete",
        ]);
      }));

      it("should provide Form entries", inject(function () {
        expectContextPadEntries("Form2", [
          "connect",
          "replace",
          "append.databinding",
          "append.parameter",
          "append.activationExpression",
          "append.event",
          "append.event.submit",
          "append.addSimpleField",
          "append.addSelectionField",
          "delete",
        ]);
      }));

      it("should not provide Form activationexpression if allready exist", inject(function () {
        expectContextPadEntries("Form1", [
          "connect",
          "replace",
          "append.databinding",
          "append.parameter",
          "append.event",
          "append.event.submit",
          "append.addSimpleField",
          "append.addSelectionField",
          "delete",
        ]);
      }));

      it("should provide Details entries", inject(function () {
        expectContextPadEntries("Details1", [
          "connect",
          "replace",
          "append.databinding",
          "append.parameter",
          "append.activationExpression",
          "append.event",
          "delete",
        ]);
      }));

      it("should provide Action entries", inject(function () {
        expectContextPadEntries("Action1", [
          "connect",
          "append.parameter",
          "append.expression",
          "append.event",
          "delete",
        ]);
      }));

      it("should provide ModuleDefinition1 entries", inject(function () {
        expectContextPadEntries("ModuleDefinition1", [
          "delete",
          "append.port.in",
          "append.port.out",
        ]);
      }));

      it("should provide Event entries", inject(function () {
        expectContextPadEntries("Event1", [
          "connect",
          "delete",
          "replace",
          "append.parameter",
          "append.activationExpression",
        ]);
      }));

      it("should provide OnSubmitEvent entries", inject(function () {
        expectContextPadEntries("Event_submit_Form1", [
          "connect",
          "delete",
          "replace",
          "append.parameter",
          "append.activationExpression",
        ]);
      }));

      it("should provide OnSelectEvent entries", inject(function () {
        expectContextPadEntries("Event_select_List", [
          "connect",
          "delete",
          "replace",
          "append.parameter",
          "append.activationExpression",
        ]);
      }));

      it("should provide SimpleField entries", inject(function () {
        expectContextPadEntries("SimpleField1", [
          "delete",
          "append.parameter",
          "append.activationExpression",
        ]);
      }));

      it("should provide SelectionField entries", inject(function () {
        expectContextPadEntries("SelectionField1", [
          "delete",
          "append.parameter",
          "append.activationExpression",
        ]);
      }));

      it("should provide ActivationExpression1 entries", inject(function () {
        expectContextPadEntries("ActivationExpression1", ["delete"]);
      }));

      it("should provide DataBinding entries", inject(function () {
        expectContextPadEntries("DataBinding1", [
          "delete",
          "append.parameter",
          "append.activationExpression",
        ]);
      }));

      it("should provide NavigationFlow entries", inject(function () {
        expectContextPadEntries("NavigationFlow1", [
          "delete",
          "append.parameterBindingGroup",
        ]);
      }));

      it("should provide Dataflow entries", inject(function () {
        expectContextPadEntries("Dataflow1", [
          "delete",
          "append.parameterBindingGroup",
        ]);
      }));

      it("should provide ParameterBindingGroup entries", inject(function () {
        expectContextPadEntries("ParameterBindingGroup1", [
          "delete",
          "append.parameterBinding",
        ]);
      }));

      it("should provide Port-in entries", inject(function () {
        expectContextPadEntries("PortDefinition_in1", [
          "delete",
          "connect",
          "change.inout",
        ]);
      }));

      it("should provide Port-out entries", inject(function () {
        expectContextPadEntries("PortDefinition_out1", [
          "delete",
          "connect",
          "change.inout",
        ]);
      }));
    });

    describe("replace", function () {
      var container;

      beforeEach(function () {
        container = TestContainer.get(this);
      });

      it("should show popup menu in the correct position", inject(function (
        elementRegistry,
        contextPad
      ) {
        // given
        var element = elementRegistry.get("ViewComponent1"),
          padding = 5,
          padMenuRect,
          replaceMenuRect;

        contextPad.open(element);

        // when
        contextPad.trigger("click", padEvent("replace"));

        padMenuRect = contextPad.getPad(element).html.getBoundingClientRect();
        replaceMenuRect = domQuery(
          ".ifml-replace",
          container
        ).getBoundingClientRect();

        // then
        expect(replaceMenuRect.left).to.be.at.most(padMenuRect.left);
        expect(replaceMenuRect.top).to.be.at.most(padMenuRect.bottom + padding);
      }));

      it("should hide wrench if replacement is disallowed", inject(function (
        elementRegistry,
        contextPad,
        customRules
      ) {
        // given
        var element = elementRegistry.get("ViewComponent1");

        // disallow replacement
        customRules.addRule("shape.replace", function (context) {
          return !is(context.element, "ifml:ViewComponent");
        });

        // when
        contextPad.open(element);

        var padNode = contextPad.getPad(element).html;

        // then
        expect(padEntry(padNode, "replace")).not.to.exist;
      }));

      it("should show wrench if replacement is allowed", inject(function (
        elementRegistry,
        contextPad,
        customRules
      ) {
        // given
        var element = elementRegistry.get("ViewComponent1");

        // allow replacement
        customRules.addRule("shape.replace", function (context) {
          return is(context.element, "ifml:ViewComponent");
        });

        // when
        contextPad.open(element);

        var padNode = contextPad.getPad(element).html;

        // then
        expect(padEntry(padNode, "replace")).to.exist;
      }));

      it("should show replacement entries for Viewcomponent", inject(function (
        elementRegistry,
        contextPad,
        popupMenu
      ) {
        // given
        var element = elementRegistry.get("ViewComponent1");
        // when
        contextPad.open(element);
        contextPad.trigger("click", padEvent("replace"));
        var replaceMenu = domQuery(".ifml-replace", container);
        expect(replaceMenu).to.exist;
        var entry = domQueryAll(
          '[data-id$="replace-with-form"]',
          popupMenu._current.container
        );
        expect(entry).to.exist;
        expect(entry).to.have.length(1);

        entry = domQueryAll(
          '[data-id$="replace-with-details"]',
          popupMenu._current.container
        );
        expect(entry).to.exist;
        expect(entry).to.have.length(1);

        entry = domQueryAll('[data-id$="replace-with-list"]', popupMenu._current.container);
        expect(entry).to.exist;
        expect(entry).to.have.length(1);
      }));

      it("should show replacement entries for ViewContainer", inject(function (
        elementRegistry,
        contextPad,
        popupMenu
      ) {
        // given
        var element = elementRegistry.get("ViewContainer1");
        // when
        contextPad.open(element);
        contextPad.trigger("click", padEvent("replace"));
        var replaceMenu = domQuery(".ifml-replace", container);
        expect(replaceMenu).to.exist;
        var entry = domQueryAll(
          '[data-id$="menu"]',
          popupMenu._current.container
        );
        expect(entry).to.exist;
        expect(entry).to.have.length(1);

        entry = domQueryAll(
          '[data-id$="window"]',
          popupMenu._current.container
        );
        expect(entry).to.exist;
        expect(entry).to.have.length(1);
      }));
    });
  });
});

function padEntry(element, name) {
  return domQuery('[data-action="' + name + '"]', element);
}

function padEvent(entry) {
  return getIfmlJS().invoke(function (overlays) {
    var target = padEntry(overlays._overlayRoot, entry);

    return {
      target: target,
      preventDefault: function () {},
      clientX: 100,
      clientY: 100,
    };
  });
}
