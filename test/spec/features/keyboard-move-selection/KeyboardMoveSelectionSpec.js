import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import keyboardMoveSelectionModule from 'diagram-js/lib/features/keyboard-move-selection';
import modelingModule from 'lib/features/modeling';
import rulesModule from 'lib/features/rules';

import { getMid } from 'diagram-js/lib/layout/LayoutUtil';


describe('features/keyboard-move-selection', function() {

  var diagramXML = require('../../../fixtures/ifml/allElements.ifml');

  var testModules = [
    coreModule,
    keyboardMoveSelectionModule,
    modelingModule,
    rulesModule
  ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  it('should move Menu', inject(function(elementRegistry, keyboardMoveSelection, selection) {

    // given
    var task = elementRegistry.get('Menu1');

    selection.select(task);

    var mid = getMid(task);

    // when
    keyboardMoveSelection.moveSelection('right');

    // then
    expect(getMid(task)).not.to.eql(mid);
  }));


  it('should move ViewContainer', inject(function(elementRegistry, keyboardMoveSelection, selection) {

    // given
    var participant = elementRegistry.get('ViewContainer1');

    selection.select(participant);

    var mid = getMid(participant);

    // when
    keyboardMoveSelection.moveSelection('right');

    // then
    expect(getMid(participant)).not.to.eql(mid);
  }));

});