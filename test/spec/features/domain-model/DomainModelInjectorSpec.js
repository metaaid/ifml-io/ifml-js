import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import distributeElements from 'lib/features/distribute-elements';
import domainModelProvider from 'lib/features/modeling'
import modelingModule from 'lib/features/modeling';
import eventBus from 'lib/core';
import coreModule from 'lib/core';

function last(arr) {
  return arr[arr.length - 1];
}


describe('features/DomainModelInjectorSpec', function() {

  var testModules = [ domainModelProvider, eventBus, modelingModule, coreModule ];


  describe('basics', function() {

    var basicXML = require('../../../fixtures/ifml/simple/databinding.ifml');

    beforeEach(bootstrapModeler(basicXML, { modules: testModules }));

    var elements;

    beforeEach(inject(function(elementRegistry, canvas) {
      elements = elementRegistry.filter(function(element) {
        return element.parent;
      });
    }));
  });
});
