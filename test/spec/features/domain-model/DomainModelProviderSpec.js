import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import distributeElements from 'lib/features/distribute-elements';
import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';

function last(arr) {
  return arr[arr.length - 1];
}

var TESTDomain = {
  "name": "TestModel",
  "uri": "./test.xmi",
  "prefix": "test",
  "associations": [],
  "types": [{
    "name": "testType",
    "id": "testTypeID",
    "type": "uml:Classifier",
    "properties": [{
        "name": "name",
        "type": "String",
      },
      {
        "name": "done",
        "type": "Boolean",
      },
    ]
  }, {
    "name": "testType2",
    "type": "uml:Classifier",
    "id": "testType2ID",
    "properties": []
  }]
};

const PROPERTY_NAME = {"name":"test:name","type":"String","ns":{"name":"test:name","id":"test:name","prefix":"test","localName":"name"}};
const PROPERTY_DONE = {"name":"test:done","type":"Boolean","ns":{"name":"test:done","id":"test:done","prefix":"test","localName":"done"}};
const PROPERTY_TESTTYPE = [PROPERTY_NAME, PROPERTY_DONE];

describe('features/DomainModelProviderSpec', function() {

  var testModules = [ distributeElements, modelingModule, coreModule ];


  describe('basics', function() {

    var basicXML = require('../../../fixtures/ifml/simple/databinding.ifml');

    beforeEach(bootstrapModeler(basicXML, { modules: testModules, moddleExtensions: [TESTDomain] }, ));



    it('should load default domain model', inject(function(domainModelProvider) {
      expect(domainModelProvider.domainTypes["test:testType"]).to.exist;
      expect(domainModelProvider.domainTypes["test:testType2"]).to.exist; 

      expect(domainModelProvider.domainTypes["test:testType"]).to.deep.equal({
        "name":"testType",
        "type":"uml:Classifier",
        "properties": PROPERTY_TESTTYPE,
        "prefix": "test",
        "xmiId": "testTypeID",
      });
      
      // expect(domainModelProvider.domainTypes["test:testType2ID"]).to.eql({name: 'testType2', type: 'uml:Classifier', id: 'testType2ID', properties: [], fullName: 'test:testType2'});
    }));

    it('should apply the classifier to the businessObject', inject(function(elementRegistry) {
      var db = elementRegistry.get('DataBinding1');
      var db2 = elementRegistry.get('DataBinding2');

      // with namespace and name
      expect(db.businessObject.domainConcept).to.exist; 
      expect(db.businessObject.domainConcept.classifier).to.exist;
      expect(db.businessObject.domainConcept.classifier.name).to.eql("test:testType");

      // with href and id
      expect(db2.businessObject.domainConcept).to.exist; 
      expect(db2.businessObject.domainConcept.classifier).to.exist;
      expect(db2.businessObject.domainConcept.classifier.name).to.eql("test:testType2");
    }));

    it('should create a classifier from types', inject(function(domainModelProvider, eventBus) {

      // when
      eventBus.fire('domainModel.changed', {
        config: [{
          prefix: 'test',
          types: [{
            name: "testType",
            id: "testTypeID"
          },{
            name: "testType2",
            id: "testType2ID"
          }]
        }]
      });


      expect(domainModelProvider.domainTypes["test:testType"]).to.jsonEqual({"name":"testType","type":"uml:Classifier","properties":[], "prefix": "test", "xmiId": "testTypeID"});
      expect(domainModelProvider.domainTypes["test:testType2"]).to.jsonEqual({"name":"testType2","type":"uml:Classifier","properties":[], "prefix": "test", "xmiId": "testType2ID"});

      expect(domainModelProvider.getTypes()).to.eql({
        'test:testType': {name: "testType",  "type": "uml:Classifier", "properties": [], "prefix": 'test', "xmiId": "testTypeID"},
        'test:testType2': {name: "testType2", "type": "uml:Classifier", "properties": [],  "prefix": 'test', "xmiId": "testType2ID"},
      });
      // expect(domainModelProvider.getTypeNames()).to.eql(['testType', 'testType2']);
    }));

    it('should update rendered type', inject(function(domainModelProvider, eventBus) {

      // given
      eventBus.fire('domainModel.changed', {
        config: [{
          prefix: 'test',
          types: [{
            name: "testType",
          },{
            name: "testType2",
          }]
        }]
      });

      // when
      var classifier = domainModelProvider.getClassifierFromUri('test:testType');

      // then
      expect(classifier["name"]).to.eql("test:testType");
    }));
  });
});
