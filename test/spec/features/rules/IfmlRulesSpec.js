import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import {
  expectCanConnect,
  expectCanCopy,
  expectCanCreate,
  expectCanDrop,
  expectCanInsert,
  expectCanMove,
  expectCanStartConnection
} from './Helper';

import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';


describe('features/modeling/rules - IfmlRules', function() {

  var testModules = [ coreModule, modelingModule ];
  var testXML = require('./../../../fixtures/ifml/allElements.ifml');
  beforeEach(bootstrapModeler(testXML, { modules: testModules }));

  
  // describe('nested viewcontainer', function() {

  //   var testXML = require('./../../../fixtures/ifml/view-container-nested.ifml');

    
  //     // beforeEach(inject(function(elementRegistry) {
  //     //   label = elementRegistry.get('MessageFlow_labeled').label;
  //     // }));

  //   beforeEach(bootstrapModeler(testXML, { modules: testModules }));

  //   // it('create viewcontainer on model', inject(function(elementFactory, elementRegistry) {

  //   //   // given
  //   //   var vc = elementRegistry.get('Parent');

  //   //   // then
  //   //   expectCanCreate([ vc ], '_myModel', true);
  //   // }));
  //   // it('create viewcontainer on child', inject(function(elementFactory, elementRegistry) {

  //   //   // given
  //   //   var vc = elementRegistry.get('Parent');

  //   //   // then
  //   //   expectCanMove([ vc ], 'Child1', false);
  //   // }));
  // });


  describe('connections', function() {
    
    describe('start', function() {

    it('can start connection from viewcomponent', inject(function(elementFactory, elementRegistry) {

      // given
      var vc = elementRegistry.get('ViewComponent1');

      // then
      expectCanStartConnection(vc, true);
    }));
    it('can start connection from events', inject(function(elementFactory, elementRegistry) {

      // given
      var vc = elementRegistry.get('Event1');

      // then
      expectCanStartConnection(vc, true);
    }));
    it('can start connection from portdefinitions', inject(function(elementFactory, elementRegistry) {

      // given
      var vc = elementRegistry.get('PortDefinition_in1');

      // then
      expectCanStartConnection(vc, true);
    }));
    
    it('Can not start connection if source doesn\'t exist', inject(function(elementFactory, elementRegistry) {
      expectCanStartConnection(null, null);
    }));

    it('can\'t start connection from labels', inject(function(elementFactory, elementRegistry) {

      // given
      var vc = elementRegistry.get('Event_select_List_label');

      // then
      expect(vc).to.exist;
      expectCanStartConnection(vc, null);
    }));
  }); 	
  describe('create ', function() {

      it('can start dataflow betwwen viewcomponents', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('ViewComponent1');
        var target = elementRegistry.get('List1');

        // then
        expectCanConnect(source, target, {
          dataflow: true,
          navigationflow: false
        });
      }));

      it('can start navigationflow from events to viewcomponents', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('Event1');
        var target = elementRegistry.get('List1');

        // then
        expectCanConnect(source, target, {
          dataflow: false,
          navigationflow: true
        });
      }));

      it('can not start connection from viewcomponents to events', inject(function(elementFactory, elementRegistry) {
        // The target of a DataFlow cannot be an Event.

        // given
        var source = elementRegistry.get('List1');
        var target = elementRegistry.get('Event1');

        // then
        expectCanConnect(source, target, {
          dataflow: false,
          navigationflow: false
        });
      }));

      it('can start connection from external viewcomponents to port-in', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('Event1');
        var target = elementRegistry.get('PortDefinition_in1');

        // then
        expectCanConnect(source, target, {
          dataflow: false,
          navigationflow: true
        });
      }));

      it('can start connection from port-out to external viewcomponents', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('PortDefinition_out1');
        var target = elementRegistry.get('List1');

        // then
        expectCanConnect(source, target, {
          dataflow: true,
          navigationflow: true
        });
      }));

      it('can start connection from port-in to internal viewcomponents', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('PortDefinition_in1');
        var target = elementRegistry.get('ViewComponentModuleInternal');

        // then
        expectCanConnect(source, target, {
          dataflow: true,
          navigationflow: true
        });
      }));

      it('can start connection from internal viewcomponents-event to port-out', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('EventModuleInternal');
        var target = elementRegistry.get('PortDefinition_out1');

        // then
        expectCanConnect(source, target, {
          dataflow: false,
          navigationflow: true
        });
      }));

      it('can not start connection from internal viewcomponents-event to port-in', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('EventModuleInternal');
        var target = elementRegistry.get('PortDefinition_in1');

        // then
        expectCanConnect(source, target, {
          dataflow: false,
          navigationflow: false
        });
      }));
      it('can not start connection from external viewcomponents-event to port-out', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('Event1');
        var target = elementRegistry.get('PortDefinition_out1');

        // then
        expectCanConnect(source, target, {
          dataflow: false,
          navigationflow: false
        });
      }));

      it('can not start connection from external element to internal element directly', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('Event1');
        var target = elementRegistry.get('ViewComponentModuleInternal');

        // then
        expectCanConnect(source, target, {
          dataflow: false,
          navigationflow: false
        });
      }));

      it('can not start connection from internal element to external element directly', inject(function(elementFactory, elementRegistry) {

        // given
        var source = elementRegistry.get('EventModuleInternal');
        var target = elementRegistry.get('List1');

        // then
        expectCanConnect(source, target, {
          dataflow: false,
          navigationflow: false
        });
      }));

      it('port-in connections should priortise navigationflows', inject(function(ifmlRules, elementRegistry) {

        // given
        var source = elementRegistry.get('PortDefinition_in1');
        var target = elementRegistry.get('ViewComponentModuleInternal');

        var conType = ifmlRules.canConnect(source,target).type;

        // then
        expect(conType).to.be.equals("ifml:NavigationFlow");
      }));

      it('port-out connections should priortise navigationflows', inject(function(ifmlRules, elementRegistry) {

        // given
        var source = elementRegistry.get('PortDefinition_out1');
        var target = elementRegistry.get('List1');

        var conType = ifmlRules.canConnect(source,target).type;

        // then
        expect(conType).to.be.equals("ifml:NavigationFlow");
      }));
    });
  });

  

  describe('resize', function() {
    function expectCanResize(elementId, expectedValue = true) {
      return inject(function(ifmlRules, elementRegistry) {
        var element = elementRegistry.get(elementId);

        // given
        var canResize = ifmlRules.canResize(element);
  
        // then
        expect(canResize).to.eql(expectedValue)
      })
    }

    it('can resize ViewElement', expectCanResize('ViewComponent1'));
    it('can resize Action', expectCanResize('Action1'));
    it('can resize ModuleDefinition', expectCanResize('ModuleDefinition1'));
    it('can resize Annotation', expectCanResize('Annotation1'));
    it('can resize ActivationExpression', expectCanResize('ActivationExpression1'));

    it('can not resize Event', expectCanResize('Port_in1', false));
    it('can not resize Event', expectCanResize('Event1', false));
    it('can not resize Label', expectCanResize('Event1_label', false));
  });

  describe('drop', function() {
    function expectCanDrop(srcId, targetId, expectedValue = true) {
      return inject(function(ifmlRules, elementRegistry) {
        var source = elementRegistry.get(srcId);
        var target = elementRegistry.get(targetId);

        expect(source).to.exist;
        expect(target).to.exist; 

        // given
        var canDrop = ifmlRules.canDrop(source, target);
  
        // then
        if(typeof expectedValue === 'string'){
            expect(canDrop.type).to.exist;
            expect(canDrop.type).to.eql(expectedValue);
        } else{
          expect(canDrop).to.eql(expectedValue)
        }
      })
    }

    it('can drop event on Viewcontainer', expectCanDrop('Event1', 'ViewContainer1', 'ifml:ViewElementEvent'));
    it('can drop event on ViewComponent', expectCanDrop('Event1', 'ViewComponent1', 'ifml:ViewElementEvent'));
    it('can drop event on Action', expectCanDrop('Event1', 'Action1', 'ifml:ActionEvent'));
    it('can drop event on Field', expectCanDrop('Event1', 'SimpleField1', 'ifml:ViewElementEvent'));

    it('can not drop event on ActionExpression', expectCanDrop('Event1', 'ActivationExpression1', false));
    it('can not drop event on ModuleDefinition', expectCanDrop('Event1', 'ModuleDefinition1', false));  
    it('can not drop event on ParameterBindingGroup', expectCanDrop('Event1', 'ParameterBindingGroup1', false));  
    it('can not drop event on BindingGroup', expectCanDrop('Event1', 'ParameterBindingGroup1', false));  
    it('can not drop event on Annotation', expectCanDrop('Event1', 'Annotation1', false));  

    it('can drop viewcontainer on ModuleDefinition', expectCanDrop('ViewContainer1', 'ModuleDefinition1', true));
    it('can drop viewcontainer on Viewcontainer', expectCanDrop('ViewContainer1', 'Menu1', true));
    it('can not drop viewcontainer on ViewComponent', expectCanDrop('ViewContainer1', 'ViewComponent1', false));
    
    it('can drop viewcomponent on ModuleDefinition', expectCanDrop('ViewComponent1', 'ModuleDefinition1', true));
    it('can drop viewcomponent on Viewcontainer', expectCanDrop('ViewComponent1', 'Menu1', true));
    it('can not drop viewcomponent on ViewComponent', expectCanDrop('ViewComponent1', 'ViewComponent1', false));
    
    it('can drop actions on ModuleDefinition', expectCanDrop('Action1', 'ModuleDefinition1', true));
    it('can drop actions on Viewcontainer', expectCanDrop('Action1', 'Menu1', true));
    it('can not drop actions on ViewComponent', expectCanDrop('Action1', 'ViewComponent1', false));

    it('can drop DataBinding on ViewComponent', expectCanDrop('DataBinding1', 'List1', true));
    it('can not drop DataBinding on ViewContainer', expectCanDrop('DataBinding1', 'ViewContainer1', false));
    it('can not drop DataBinding on ModuleDefinition', expectCanDrop('DataBinding1', 'ModuleDefinition1', false));
    
    it('can port DataBinding on ModuleDefinition', expectCanDrop('PortDefinition_in1', 'ModuleDefinition1', true));
    it('can not port DataBinding on ViewComponent', expectCanDrop('PortDefinition_in1', 'List1', false));
    it('can not drop DataBinding on ViewContainer', expectCanDrop('PortDefinition_in1', 'ViewContainer1', false));
    
    it('can drop ActivationExpression1 everywhere', expectCanDrop('ActivationExpression1', '_myModel', true));
    it('can drop ActivationExpression1 on ViewComponent', expectCanDrop('ActivationExpression1', 'List1', true));
    it('can drop ActivationExpression1 on ViewContainer', expectCanDrop('ActivationExpression1', 'ViewContainer1', true));
    
    it('can drop Label everywhere', expectCanDrop('Event_select_List_label', '_myModel', true));
    it('can drop Label on ViewComponent', expectCanDrop('Event_select_List_label', 'List1', true));
    it('can drop Label on ViewContainer', expectCanDrop('Event_select_List_label', 'ViewContainer1', true));
  });
});
