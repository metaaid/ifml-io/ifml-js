import {
  getIfmlJS
} from 'test/TestHelper';

import {
  isArray,
  isString,
  map
} from 'min-dash';


export function expectCanConnect(source, target, rules) {

  var results = {};

  getIfmlJS().invoke(function(ifmlRules) {

    source = get(source);
    target = get(target);

    if ('dataflow' in rules) {
      results.dataflow = ifmlRules.canConnectDataFlow(source, target);
    }

    if ('navigationflow' in rules) {
      results.navigationflow = ifmlRules.canConnectNavigationFlow(source, target);
    }
  });

  expect(results).to.eql(rules);
}


export function expectCanDrop(element, target, expectedResult) {

  var result = getIfmlJS().invoke(function(ifmlRules) {
    return ifmlRules.canDrop(get(element), get(target));
  });

  expect(result).to.eql(expectedResult);
}


export function expectCanCreate(shape, target, expectedResult) {

  var result = getIfmlJS().invoke(function(rules) {

    if (isArray(shape)) {
      return rules.allowed('elements.create', {
        elements: get(shape),
        target: get(target)
      });
    }

    return rules.allowed('shape.create', {
      shape: get(shape),
      target: get(target)
    });
  });

  expect(result).to.eql(expectedResult);
}


export function expectCanCopy(element, elements, expectedResult) {

  var result = getIfmlJS().invoke(function(rules) {
    return rules.allowed('element.copy', {
      element: element,
      elements: elements
    });
  });

  expect(result).to.eql(expectedResult);
}

export function expectCanStartConnection(element, expectedResult) {

  var result = getIfmlJS().invoke(function(rules) {
    return rules.allowed('connection.start', {
      source: element
    });
  });

  expect(result).to.eql(expectedResult);
}


export function expectCanInsert(element, target, expectedResult) {

  var result = getIfmlJS().invoke(function(ifmlRules) {
    return ifmlRules.canInsert(get(element), get(target));
  });

  expect(result).to.eql(expectedResult);
}


export function expectCanMove(elements, target, rules) {

  var results = {};

  elements = elements.map(get);

  getIfmlJS().invoke(function(ifmlRules) {

    target = get(target);

    if ('attach' in rules) {
      results.attach = ifmlRules.canAttach(elements, target);
    }

    if ('move' in rules) {
      results.move = ifmlRules.canMove(elements, target);
    }
  });

  expect(results).to.eql(rules);
}


/**
 * Retrieve element, resolving an ID with
 * the actual element.
 */
function get(elementId) {

  if (isArray(elementId)) {
    return map(elementId, get);
  }

  var element;

  if (isString(elementId)) {
    element = getIfmlJS().invoke(function(elementRegistry) {
      return elementRegistry.get(elementId);
    });

    if (!element) {
      throw new Error('element #' + elementId + ' not found');
    }

    return element;
  }

  return elementId;
}