import {
  bootstrapViewer,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import modelingModule from 'lib/features/modeling';
import ifmlSearchModule from 'lib/features/search';


describe('features - IFML search provider', function() {

  var testModules = [
    coreModule,
    modelingModule,
    ifmlSearchModule
  ];


    var diagramXML = require('./ifml-search.ifml');

    beforeEach(bootstrapViewer(diagramXML, { modules: testModules }));


    it('should not return root model Element', inject(function(ifmlSearch) {

      // given
      var pattern = '_myModel';

      // when
      var elements = ifmlSearch.find(pattern);

      // then
      expect(elements).to.have.length(0);
    }));
    

    it('find should return all elements that match label or ID', inject(function(ifmlSearch) {

      // given
      var pattern = '123456';

      // when
      var elements = ifmlSearch.find(pattern);

      // then
      expect(elements).length(3);
      elements.forEach(function(e) {
        expect(e).to.have.property('element');
        expect(e).to.have.property('primaryTokens');
        expect(e).to.have.property('secondaryTokens');
      });
    }));


    it('matches IDs', inject(function(ifmlSearch) {

      // given
      var pattern = 'action';

      // when
      var elements = ifmlSearch.find(pattern);

      // then
      expect(elements[0].primaryTokens).to.eql([
        { normal: 'has matched ID' }
      ]);
      expect(elements[0].secondaryTokens).to.eql([
        { normal: 'some_' },
        { matched: 'Action' },
        { normal: '_123456_id' }
      ]);
    }));


    describe('should split result into matched and non matched tokens', function() {

      it('matched all', inject(function(ifmlSearch) {

        // given
        var pattern = 'all matched';

        // when
        var elements = ifmlSearch.find(pattern);

        // then
        expect(elements[0].primaryTokens).to.eql([
          { matched: 'all matched' }
        ]);
      }));


      it('matched start', inject(function(ifmlSearch) {

        // given
        var pattern = 'before';

        // when
        var elements = ifmlSearch.find(pattern);

        // then
        expect(elements[0].primaryTokens).to.eql([
          { matched: 'before' },
          { normal: ' 321' }
        ]);
      }));


      it('matched middle', inject(function(ifmlSearch) {

        // given
        var pattern = 'middle';

        // when
        var elements = ifmlSearch.find(pattern);

        // then
        expect(elements[0].primaryTokens).to.eql([
          { normal: '123 ' },
          { matched: 'middle' },
          { normal: ' 321' }
        ]);
      }));


      it('matched end', inject(function(ifmlSearch) {

        // given
        var pattern = 'after';

        // when
        var elements = ifmlSearch.find(pattern);

        // then
        expect(elements[0].primaryTokens).to.eql([
          { normal: '123 ' },
          { matched: 'after' }
        ]);
      }));
  });

});
