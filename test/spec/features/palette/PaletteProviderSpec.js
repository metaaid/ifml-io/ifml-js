import {
  bootstrapModeler,
  getIfmlJS,
  inject
} from 'test/TestHelper';

import coreModule from 'lib/core';
import createModule from 'diagram-js/lib/features/create';
import modelingModule from 'lib/features/modeling';
import paletteModule from 'lib/features/palette';

import { createMoveEvent } from 'diagram-js/lib/features/mouse/Mouse';

import { is } from 'lib/util/ModelUtil';

import {
  createCanvasEvent as canvasEvent
} from '../../../util/MockEvents';

import {
  query as domQuery,
  queryAll as domQueryAll
} from 'min-dom';


describe('features/palette', function() {

  var diagramXML = require('../../../fixtures/ifml/empty.ifml');

  var testModules = [
    coreModule,
    createModule,
    modelingModule,
    paletteModule
  ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));


  it('should provide IFML modeling palette', inject(function(canvas) {

    // when
    var paletteElement = domQuery('.djs-palette', canvas._container);
    var entries = domQueryAll('.entry', paletteElement);

    // then
    expect(entries.length).to.equal(8);
  }));


  describe('elements', function() {

    function shouldDragSingleElement(type){
      return inject(function(dragging) {
        // when
        triggerPaletteEntry(type);

        // then
        var context = dragging.context(),
            elements = context.data.elements;

        expect(elements).to.have.length(1);
        expect(is(elements[0], type)).to.be.true;
      });

    }

    it('ViewContainer', shouldDragSingleElement('ifml:ViewContainer'));
    it('ViewComponent', shouldDragSingleElement('ifml:ViewComponent'));
    it('Action', shouldDragSingleElement('ifml:Action'));
    it('Event', shouldDragSingleElement('ifml:Event'));
    it('ModuleDefinition', shouldDragSingleElement('ifml:ModuleDefinition'));
    it('PortDefinition', shouldDragSingleElement('ifml:PortDefinition'));
  });
});

// helpers //////////

function triggerPaletteEntry(id) {
  getIfmlJS().invoke(function(palette) {
    var entry = palette.getEntries()[ id ];

    if (entry && entry.action && entry.action.click) {
      entry.action.click(createMoveEvent(0, 0));
    }
  });
}