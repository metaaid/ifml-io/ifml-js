import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import modelingModule from 'lib/features/modeling';
import replaceModule from 'lib/features/replace';
import coreModule from 'lib/core';



describe('features/replace - rules', function() {

  var diagramXML = require('../../../fixtures/ifml/allElements.ifml');

  var testModules = [ coreModule, modelingModule, replaceModule ];

  beforeEach(bootstrapModeler(diagramXML, { modules: testModules }));

  // No current replacement rules

});
