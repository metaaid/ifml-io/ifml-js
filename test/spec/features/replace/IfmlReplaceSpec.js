import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import {
  assign,
  pick
} from 'min-dash';

import modelingModule from 'lib/features/modeling';
import replaceModule from 'lib/features/replace';
import moveModule from 'diagram-js/lib/features/move';
import coreModule from 'lib/core';


import {
  is
} from 'lib/util/ModelUtil';



describe('features/replace - ifml replace', function() {

  var testModules = [
    coreModule,
    modelingModule,
    moveModule,
    replaceModule
  ];


  function shouldREplaceInto(element, replaceInto){

    return inject(function(elementRegistry, ifmlReplace) {

      // given
      var task = elementRegistry.get(element);
      var newElementData = {
        type: replaceInto
      };
      // when
      var newElement = ifmlReplace.replaceElement(task, newElementData);
      // then
      var businessObject = newElement.businessObject;
      expect(newElement).to.exist;
      expect(is(businessObject, replaceInto)).to.be.true;
    });
  }
  
  describe('elements replace', function() {
    var diagramXML = require('../../../fixtures/ifml/allElements.ifml');
      beforeEach(bootstrapModeler(diagramXML, {
        modules: testModules
      }));

    describe('viewcontainer', function() {
      it('should replace into menu', shouldREplaceInto('ViewContainer1', 'ifml:Menu'));
      it('should replace into window', shouldREplaceInto('ViewContainer1', 'ifml:Window'));
    });

    describe('menu', function() {
      it('should replace into vc', shouldREplaceInto('Menu1','ifml:ViewContainer'));
      it('should replace into window', shouldREplaceInto('Menu1','ifml:Window'));
    });

    describe('window', function() {
      it('should replace into viewcontainer', () => {
        shouldREplaceInto('Window1','ifml:ViewContainer')
      });
      it('should replace into menu', () => {
        shouldREplaceInto('Window1', 'ifml:Menu')
      });
    });
    

    describe('viewcomponent', function() {
      it('should replace into list', shouldREplaceInto('ViewComponent1','ifml:List'));
      it('should replace into details', shouldREplaceInto('ViewComponent1','ifml:Details'));
      it('should replace into form', shouldREplaceInto('ViewComponent1','ifml:Form'));
    });

    describe('list', function() {
      it('should replace into viewcomponent', shouldREplaceInto('List1','ifml:ViewComponent'));
      it('should replace into details', shouldREplaceInto('List1','ifml:Details'));
      it('should replace into form', shouldREplaceInto('List1','ifml:Form'));
    });
  });

  // // TODO: move to ifmlupdater where the magic happens
  describe('list replace', function() {
    var diagramXML = require('./List-multievents.ifml');
      beforeEach(bootstrapModeler(diagramXML, {
        modules: testModules
      }));
     it('should transfer selectevents to viewelementevents', inject(function(elementRegistry, ifmlReplace) {

      // given
      var task = elementRegistry.get("List1");
      var newElementData = {
        type: "ifml:ViewComponent"
      };
      // when
      var newElement = ifmlReplace.replaceElement(task, newElementData);
      // then
      var businessObject = newElement.businessObject;
      expect(newElement).to.exist;
      expect(is(businessObject, "ifml:ViewComponent")).to.be.true;
      expect(businessObject.get("selectEvent")).to.not.exist;
      expect(businessObject.get("viewElementEvents")).to.have.lengthOf(5);
    }));
     it('should transfer OnSelectEvents to selectevents', inject(function(elementRegistry, ifmlReplace) {

      // given
      var task = elementRegistry.get("ViewComponent1");
      var newElementData = {
        type: "ifml:List"
      };
      // when
      var newElement = ifmlReplace.replaceElement(task, newElementData);
      // then
      var businessObject = newElement.businessObject;
      expect(newElement).to.exist;
      expect(is(businessObject, "ifml:List")).to.be.true;
      expect(businessObject.get("selectEvent")).to.have.lengthOf(2);
      expect(businessObject.get("viewElementEvents")).to.have.lengthOf(3);
    }));
      
  });

});


// helpers ////////////////////////

function skipId(key, value) {

  if (key === 'id') {
    return;
  }

  return value;
}

function getBounds(shape) {
  return pick(shape, [ 'x', 'y', 'width', 'height' ]);
}