import {
  bootstrapModeler,
  inject
} from 'test/TestHelper';

import replacePreviewModule from 'lib/features/replace-preview';
import moveModule from 'diagram-js/lib/features/move';
import modelingModule from 'lib/features/modeling';
import coreModule from 'lib/core';

import {
  createCanvasEvent as canvasEvent
} from '../../../util/MockEvents';

import {
  assign
} from 'min-dash';

import {
  attr as svgAttr,
  clone as svgClone,
  innerSVG
} from 'tiny-svg';


describe('features/replace-preview', function() {

  var diagramXML = require('./../../../fixtures/ifml/simple/event-nolabel.ifml');

  var viewElementEvent,
      rootElement;

  var getGfx,
      moveShape;

  beforeEach(bootstrapModeler(diagramXML, {
    modules: [
      replacePreviewModule,
      moveModule,
      modelingModule,
      coreModule
    ]
  }));

  beforeEach(inject(function(canvas, elementRegistry, elementFactory, move, dragging) {

    viewElementEvent = elementRegistry.get('Event1');
    rootElement = canvas.getRootElement();

    /**
     * returns the gfx representation of an element type
     *
     * @param  {Object} elementData
     *
     * @return {Object}
     */
    getGfx = function(elementData) {
      assign(elementData, { x: 0, y: 0 });

      var tempShape = elementFactory.createShape(elementData);

      canvas.addShape(tempShape, rootElement);

      var gfx = svgClone(elementRegistry.getGraphics(tempShape));

      canvas.removeShape(tempShape);

      return gfx;
    };

    moveShape = function(shape, target, position) {
      var startPosition = {
        x: shape.x + 10 + (shape.width / 2),
        y: shape.y + 30 + (shape.height / 2)
      };

      move.start(canvasEvent(startPosition), shape);

      dragging.hover({
        element: target,
        gfx: elementRegistry.getGraphics(target)
      });

      dragging.move(canvasEvent(position));
    };

  }));


  // it('should draw replaced visuals at correct position', inject(function(dragging) {

  //   // when
  //   moveShape(viewElementEvent, rootElement, { x: 270, y:180 });

  //   // then
  //   var dragGroup = dragging.context().data.context.dragGroup;

  //   svgAttr(dragGroup.childNodes[0], 'display', 'inline');

  //   expect(dragGroup.childNodes[0].getBBox()).to.eql(dragGroup.childNodes[0].getBBox());
  // }));


  // it('should add dragger to context.visualReplacements once', inject(function(dragging) {

  //   // when
  //   moveShape(viewElementEvent, rootElement, { x: 275, y: 120 });
  //   moveShape(viewElementEvent, rootElement, { x: 280, y: 120 });
  //   moveShape(viewElementEvent, rootElement, { x: 285, y: 120 });

  //   // then
  //   var visualReplacements = dragging.context().data.context.visualReplacements;

  //   expect(visualReplacements[viewElementEvent.id]).to.exist;
  //   expect(Object.keys(visualReplacements).length).to.equal(1);

  // }));


  // it('should remove dragger from context.visualReplacements', inject(
  //   function(elementRegistry, dragging) {

  //     // given
  //     var subProcess2 = elementRegistry.get('SubProcess_2');

  //     // when
  //     moveShape(systemEvent, rootElement, { x: 275, y: 120 });
  //     moveShape(systemEvent, rootElement, { x: 280, y: 120 });
  //     moveShape(systemEvent, subProcess2, { x: 350, y: 120 });

  //     // then
  //     var visualReplacements = dragging.context().data.context.visualReplacements;

  //     expect(visualReplacements).to.be.empty;
  //   }
  // ));


  // it('should hide the replaced visual', inject(function(dragging) {

  //   // when
  //   moveShape(systemEvent, rootElement, { x: 280, y: 120 });

  //   // then
  //   var dragGroup = dragging.context().data.context.dragGroup;

  //   expect(svgAttr(dragGroup.childNodes[0], 'display')).to.equal('none');
  // }));


  // it('should replace systemevent event while hover over view element',
  //   inject(function(dragging, elementRegistry) {

  //     // when
  //     moveShape(systemEvent, rootElement, { x: 280, y: 120 });

  //     var context = dragging.context().data.context;

  //     // then
  //     // check if the visual replacement is a blank interrupting start event
  //     var startEventGfx = getGfx({ type: 'ifml:ViewElementEvent' });

  //     expect(innerSVG(context.dragGroup.childNodes[1])).to.equal(innerSVG(startEventGfx));
  //   })
  // );

  // it('should replace viewelementEvent with generic event',
  //   inject(function(dragging, elementRegistry) {

  //     // when
  //     moveShape(systemEvent, rootElement, { x: 280, y: 120 });

  //     var context = dragging.context().data.context;

  //     // then
  //     // check if the visual replacement is a blank interrupting start event
  //     var startEventGfx = getGfx({ type: 'ifml:Event' });

  //     expect(innerSVG(context.dragGroup.childNodes[1])).to.equal(innerSVG(startEventGfx));
  //   })
  // );

});
