import { bootstrapModeler, inject } from "test/TestHelper";

import copyPasteModule from "lib/features/copy-paste";
import coreModule from "lib/core";
import modelingModule from "lib/features/modeling";

import { getBusinessObject, is } from "lib/util/ModelUtil";

var HIGH_PRIORITY = 3000;

describe("features/copy-paste/ModdleCopy", function () {
  // var testModules = [copyPasteModule, coreModule, modelingModule];
  var testModules = [coreModule, modelingModule, copyPasteModule];

  var basicXML = require("../../../fixtures/ifml/allElements.ifml");

  beforeEach(
    bootstrapModeler(basicXML, {
      modules: testModules,
    })
  );

  describe("simple", function () {
    it("should copy primitive properties", inject(function (
      moddle,
      moddleCopy
    ) {
      // given
      var viewContainer = moddle.create("ifml:ViewContainer", {
        isXOR: true,
      });
      // when
      var menu = moddleCopy.copyElement(
        viewContainer,
        moddle.create("ifml:Menu")
      );
      // then
      expect(menu.isXOR).to.be.true;
      expectNoAttrs(menu);
    }));

    it("should copy arrays of properties", inject(function (
      moddle,
      moddleCopy
    ) {
      // given
      var form = moddle.create("ifml:Form"),
        event1 = moddle.create("ifml:Event", {
          name: "event1",
        }),
        event2 = moddle.create("ifml:Event", {
          name: "event2",
        });

      form.viewElementEvents = [event1, event2];

      // when
      var list = moddleCopy.copyElement(form, moddle.create("ifml:List"));

      // then
      var eventDefinitions = list.viewElementEvents;

      expect(eventDefinitions).to.have.length(2);
      expect(eventDefinitions[0].name).to.equal("event1");
      expect(eventDefinitions[1].name).to.equal("event2");

      expectNoAttrs(list);
    }));

    it("should NOT copy properties that are not allowed in target element", inject(function (
      moddle,
      moddleCopy
    ) {
      // given
      var modalWindow = moddle.create("ifml:Window", {
        isModal: true,
      });

      // when
      var serviceTask = moddleCopy.copyElement(
        modalWindow,
        moddle.create("ifml:ViewContainer")
      );

      // then
      expect(serviceTask.isModal).not.to.exist;

      expectNoAttrs(serviceTask);
    }));

    it("should NOT copy IDs if taken", inject(function (
      moddle,
      moddleCopy,
      canvas,
      modeling
    ) {
      // given
      var menu = modeling.createShape(
        { type: "ifml:Menu" },
        { x: 0, y: 0 },
        canvas.getRootElement()
      );
      var menuId = menu.id;
      expect(menuId).to.exist;

      // when
      var newMenu = moddleCopy.copyElement(
        menu.businessObject,
        moddle.create("ifml:Menu")
      );

      // then
      expect(newMenu.$id).not.to.equal(menuId);

      expectNoAttrs(newMenu);
    }));

    it("should copy IDs if free", inject(function (
      moddle,
      moddleCopy,
      canvas,
      modeling
    ) {
      // given
      var menu = modeling.createShape(
        { type: "ifml:Menu" },
        { x: 0, y: 0 },
        canvas.getRootElement()
      );
      var menuId = menu.id;

      // when
      modeling.removeShape(menu);
      var newMenu = moddleCopy.copyElement(
        menu.businessObject,
        moddle.create("ifml:Menu")
      );

      // then
      expect(newMenu.$id).to.equal(menuId);

      expectNoAttrs(newMenu);
    }));

    it("should NOT copy references", inject(function (moddle, moddleCopy) {
      // given
      var menu1 = moddle.create("ifml:Menu"),
        menu2 = moddle.create("ifml:Menu"),
        dataFlow = moddle.create("ifml:DataFlow");

      dataFlow.set("sourceInteractionFlowElement", menu1);
      dataFlow.set("targetInteractionFlowElement", menu2);
      menu1.get("outInteractionFlows").push(menu1);
      menu2.get("inInteractionFlows").push(menu2);

      // when
      var newMenu = moddleCopy.copyElement(menu1, moddle.create("ifml:Menu"));

      // then
      expect(menu1.get("outInteractionFlows")).to.have.length(1);
      expect(newMenu.get("outInteractionFlows")).to.have.length(0);
    }));
  });
});

// helpers //////////

function expectNoAttrs(element) {
  expect(element.$attrs).to.be.empty;
}
