import { bootstrapModeler, getIfmlJS, inject } from "test/TestHelper";

import ifmlCopyPasteModule from "lib/features/copy-paste";
import copyPasteModule from "diagram-js/lib/features/copy-paste";
import coreModule from "lib/core";
import modelingModule from "lib/features/modeling";

import {
  find,
  forEach,
  isArray,
  isNumber,
  keys,
  map,
  pick,
  reduce,
} from "min-dash";

import { getBusinessObject, getDi, is } from "lib/util/ModelUtil";

describe("features/copy-paste", function () {
  var testModules = [
    ifmlCopyPasteModule,
    copyPasteModule,
    coreModule,
    modelingModule,
  ];

  var xml = require("../../../fixtures/ifml/allElements.ifml");
  // copyPropertiesXML = require("./copy-properties.bpmn"),
  // propertiesXML = require("./properties.bpmn"),
  // complexXML = require("./complex.bpmn"),
  // collaborationXML = require("./collaboration.bpmn"),
  // collaborationMultipleXML = require("./collaboration-multiple.bpmn"),
  // collaborationAssociationsXML = require("./data-associations.bpmn"),
  // eventBasedGatewayXML = require("./event-based-gateway.bpmn");

  describe("basic diagram", function () {
    beforeEach(
      bootstrapModeler(xml, {
        modules: testModules,
      })
    );

    it("should paste twice", inject(function (
      elementRegistry,
      canvas,
      copyPaste
    ) {
      // given
      var firstViewComp = elementRegistry.get("ViewComponent1"),
        rootElement = canvas.getRootElement();

      // when
      copyPaste.copy(firstViewComp);

      var childCount = rootElement.children.length;

      copyPaste.paste({
        element: rootElement,
        point: {
          x: 1000,
          y: 1000,
        },
      });

      var elements = copyPaste.paste({
        element: rootElement,
        point: {
          x: 2000,
          y: 1000,
        },
      });

      // then
      expect(rootElement.children).to.have.length(childCount + 2);

      var viewcomp = elements.filter(function (element) {
        return is(element, "ifml:ViewComponent");
      });

      expect(viewcomp[0].id).not.to.equal(firstViewComp.id);
    }));

    it("should copy color properties", inject(function (
      canvas,
      copyPaste,
      elementRegistry,
      modeling
    ) {
      // given
      var task = elementRegistry.get("ViewComponent1"),
        rootElement = canvas.getRootElement(),
        fill = "#ff0000",
        stroke = "#00ff00";

      // when
      modeling.setColor(task, { fill: fill, stroke: stroke });

      copyPaste.copy(task);

      var elements = copyPaste.paste({
        element: rootElement,
        point: {
          x: 1000,
          y: 1000,
        },
      });

      // then
      task = find(elements, function (element) {
        return is(element, "ifml:ViewComponent");
      });

      var di = getDi(task);

      expect(di.get("fill")).to.equal(fill);
      expect(di.get("stroke")).to.equal(stroke);

      // TODO @barmac: remove when we drop bpmn.io properties
      expect(di.fill).to.equal(fill);
      expect(di.stroke).to.equal(stroke);
    }));

    it("should copy label", inject(function (
      canvas,
      copyPaste,
      elementRegistry,
      modeling
    ) {
      // given
      var event = elementRegistry.get("Event_select_List"),
        listElement = elementRegistry.get("List1");

      copyPaste.copy(event);

      // when
      var elements = copyPaste.paste({
        element: listElement,
        point: {
          x: 50,
          y: 50,
        },
      });

      // then
      expect(elements).to.have.length(2);

      var eventCopy = find(elements, function (element) {
        return is(element, "ifml:OnSelectEvent");
      });

      var eventCopyBo = getBusinessObject(eventCopy);
      var eventCopyDi = getDi(eventCopy);
      var eventCopyLabel = eventCopyDi.ownedLabel;
      
      expect(eventCopyBo).to.exist;
      expect(eventCopyBo.name).to.equal("test");
      
      expect(eventCopyDi).to.exist;
      expect(eventCopyLabel).to.exist;
    }));

    it("should copy name property", inject(function (
      canvas,
      copyPaste,
      elementRegistry,
      modeling
    ) {
      // given 
      var viewcomp = elementRegistry.get("ViewComponent1"),
        rootElement = canvas.getRootElement();

      copyPaste.copy(viewcomp);

      modeling.removeShape(viewcomp);

      // when
      var elements = copyPaste.paste({
        element: rootElement,
        point: {
          x: 300,
          y: 300,
        },
      });

      // then
      expect(elements).to.have.length(1);

      viewcomp = find(elements, function (element) {
        return is(element, "ifml:ViewComponent");
      });

      var startEventBo = getBusinessObject(viewcomp);

      expect(startEventBo.name).to.equal("My ViewComponent");
    }));

    it("should wire DIs correctly", inject(function (
      canvas,
      copyPaste,
      elementRegistry,
      modeling
    ) {
      // given
      var viewcomp = elementRegistry.get("ViewComponent1"),
        rootElement = canvas.getRootElement();

      copyPaste.copy(viewcomp);
      // modeling.removeShape(viewcomp);

      // when
      var elements = copyPaste.paste({
        element: rootElement,
        point: {
          x: 300,
          y: 300,
        },
      });

      // then
      var bo = elements[0].businessObject;
      var di = getDi(elements[0]);

      expect(di).to.exist;
      expect(di.modelElement).to.exist;
      expect(di.modelElement).to.equal(bo);
    }));
  });
});

// helpers //////////

/**
 * Integration test involving copying, pasting, moving, undoing and redoing.
 *
 * @param {string|Array<string>} elementIds
 */
function integrationTest(elementIds) {
  if (!isArray(elementIds)) {
    elementIds = [elementIds];
  }

  return function () {
    getIfmlJS().invoke(function (
      canvas,
      commandStack,
      copyPaste,
      elementRegistry,
      modeling
    ) {
      // given
      var allElements = elementRegistry.getAll();

      var initialContext = {
          length: allElements.length,
          ids: getPropertyForElements(allElements, "id"),
          types: getPropertyForElements(allElements, "type"),
        },
        currentContext;

      var elements = map(elementIds, function (elementId) {
        return elementRegistry.get(elementId);
      });

      // (1) copy elements
      copyPaste.copy(elements);

      // (2) remove elements
      modeling.removeElements(elements);

      var rootElement = canvas.getRootElement();

      // (3) paste elements
      copyPaste.paste({
        element: rootElement,
        point: {
          x: 500,
          y: 500,
        },
      });

      // (4) move all elements except root
      modeling.moveElements(elementRegistry.filter(isRoot), { x: 50, y: -50 });

      // when
      // (5) undo moving, pasting and removing
      commandStack.undo();
      commandStack.undo();
      commandStack.undo();

      elements = elementRegistry.getAll();

      currentContext = {
        length: elements.length,
        ids: getPropertyForElements(elements, "id"),
      };

      // then
      expect(initialContext.length).to.equal(currentContext.length);
      expectCollection(initialContext.ids, currentContext.ids, true);

      // when
      // (6) redo removing, pasting and moving
      commandStack.redo();
      commandStack.redo();
      commandStack.redo();

      elements = elementRegistry.getAll();

      currentContext = {
        length: elements.length,
        ids: getPropertyForElements(elements, "id"),
        types: getPropertyForElements(elements, "type"),
      };

      // then
      expect(initialContext.length).to.equal(currentContext.length);
      expectCollection(initialContext.ids, currentContext.ids, false);
      expectCollection(initialContext.types, currentContext.types, true);
    });
  };
}

function isRoot(element) {
  return !!element.parent;
}

function getPropertyForElements(elements, property) {
  return map(elements, function (element) {
    return element[property];
  });
}

function expectCollection(collection1, collection2, contains) {
  expect(collection1).to.have.length(collection2.length);

  forEach(collection2, function (element) {
    if (!element.parent) {
      return;
    }

    if (contains) {
      expect(collection1).to.contain(element);
    } else {
      expect(collection1).not.to.contain(element);
    }
  });
}

function getAllElementsInTree(tree, depth) {
  var depths;

  if (isNumber(depth)) {
    depths = pick(tree, [depth]);
  } else {
    depths = tree;
  }

  return reduce(
    depths,
    function (allElements, depth) {
      return allElements.concat(depth);
    },
    []
  );
}

function findDescriptorInTree(elements, tree, depth) {
  var foundDescriptors = _findDescriptorsInTree(elements, tree, depth);

  if (foundDescriptors.length !== 1) {
    return false;
  }

  return foundDescriptors[0];
}

function _findDescriptorsInTree(elements, tree, depth) {
  if (!isArray(elements)) {
    elements = [elements];
  }

  var depths;

  if (isNumber(depth)) {
    depths = pick(tree, [depth]);
  } else {
    depths = tree;
  }

  return reduce(
    elements,
    function (foundDescriptors, element) {
      var foundDescriptor = reduce(depths, function (foundDescriptor, depth) {
        return (
          foundDescriptor ||
          find(depth, function (descriptor) {
            return element === descriptor.id || element.id === descriptor.id;
          })
        );
      });

      if (foundDescriptor) {
        return foundDescriptors.concat(foundDescriptor);
      }

      return foundDescriptors;
    },
    []
  );
}

/**
 * Copy elements.
 *
 * @param {Array<string|djs.model.Base} elements
 *
 * @returns {Object}
 */
function copy(elements) {
  if (!isArray(elements)) {
    elements = [elements];
  }

  return getIfmlJS().invoke(function (copyPaste, elementRegistry) {
    elements = elements.map(function (element) {
      element = elementRegistry.get(element.id || element);

      expect(element).to.exist;

      return element;
    });

    return copyPaste.copy(elements);
  });
}
