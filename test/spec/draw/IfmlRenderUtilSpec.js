import {
  isTypedEvent,
  isCollection,
  getSemantic,
  getCirclePath,
  getRoundRectPath,
  getDiamondPath,
  getRectPath,
  getFillColor,
  getStrokeColor,
  getTextColor,
  getConnectionColor,
} from 'lib/draw/IfmlRenderUtil';

import { getDi } from '../../../lib/util/ModelUtil';

describe('IfmlRenderUtil', function () {

  it('should expose isTypedEvent', function () {

    expect(isTypedEvent).to.be.a('function');

  });


  it('should expose isCollection', function () {

    expect(isCollection).to.be.a('function');

  });


  it('should expose getDi', function () {

    expect(getDi).to.be.a('function');

  });


  it('should expose getSemantic', function () {

    expect(getSemantic).to.be.a('function');

  });

  it('should expose getCirclePath', function () {

    expect(getCirclePath).to.be.a('function');

  });

  it('should expose getRoundRectPath', function () {

    expect(getRoundRectPath).to.be.a('function');

  });

  it('should expose getDiamondPath', function () {

    expect(getDiamondPath).to.be.a('function');

  });

  it('should expose getRectPath', function () {

    expect(getRectPath).to.be.a('function');

  });

  it('should expose getFillColor', function () {

    expect(getFillColor).to.be.a('function');

  });

  it('should expose getStrokeColor', function () {

    expect(getStrokeColor).to.be.a('function');

  });

  it('should expose getTextColor', function () {

    expect(getTextColor).to.be.a('function');

  });

  it('should expose getConnectionColor', function () {

    expect(getConnectionColor).to.be.a('function');

  });

});