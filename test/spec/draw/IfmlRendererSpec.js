import {
  bootstrapModeler,
  bootstrapViewer,
  inject
} from 'test/TestHelper';

import {
  create as svgCreate
} from 'tiny-svg';

import coreModule from 'lib/core';
import rendererModule from 'lib/draw';
import modelingModule from 'lib/features/modeling';

import {
  query as domQuery
} from 'min-dom';

import {
  isAny
} from 'lib/features/modeling/util/ModelingUtil';

import {
  getDi
} from 'lib/draw/IfmlRenderUtil';

function checkErrors(err, warnings) {
  expect(warnings).to.be.empty;
  expect(err).not.to.exist;
}


describe('draw - ifml renderer', function () {

  it('should render container', function () {
    var xml = require('../../fixtures/ifml/simple/container.ifml');
    return bootstrapViewer(xml).call(this).then(function (result) {

      checkErrors(result.error, result.warnings);
    });
  });

  it('should render component', function () {
    var xml = require('../../fixtures/ifml/simple/component.ifml');
    return bootstrapViewer(xml).call(this).then(function (result) {

      checkErrors(result.error, result.warnings);
    });
  });

  it('should render action', function () {
    var xml = require('../../fixtures/ifml/simple/action.ifml');
    return bootstrapViewer(xml).call(this).then(function (result) {

      checkErrors(result.error, result.warnings);
    });
  });

  it('should render event', function () {
    var xml = require('../../fixtures/ifml/simple/event.ifml');
    return bootstrapViewer(xml).call(this).then(function (result) {

      checkErrors(result.error, result.warnings);
    });
  });

  it('should render dataFlow', function () {
    var xml = require('../../fixtures/ifml/simple/dataFlow.ifml');
    return bootstrapViewer(xml).call(this).then(function (result) {

      checkErrors(result.error, result.warnings);
    });
  });

  it('should render navigationFlow', function () {
    var xml = require('../../fixtures/ifml/simple/navigationFlow.ifml');
    return bootstrapViewer(xml).call(this).then(function (result) {

      checkErrors(result.error, result.warnings);
    });
  });


  it('should render list', function () {
    var xml = require('../../fixtures/ifml/simple/list.ifml');

    return bootstrapViewer(xml).call(this).then(function (result) {

      var err = result.error;
      expect(err).not.to.exist;

      inject(function (elementRegistry) {
        var callActivityGfx = elementRegistry.getGraphics('myListId');
        var label = domQuery('.djs-label tspan', callActivityGfx);
        expect(label.textContent).to.include("myList")
      })();
    });
  });

  // onselectevent

  it('should render form', function () {
    var xml = require('../../fixtures/ifml/simple/form.ifml');

    return bootstrapViewer(xml).call(this).then(function (result) {

      var err = result.error;
      expect(err).not.to.exist;

      inject(function (elementRegistry) {
        var callActivityGfx = elementRegistry.getGraphics('myFormId');
        var label = domQuery('.djs-label tspan', callActivityGfx);
        expect(label.textContent).to.include("myForm");
      })();
    });
  });

  it('should render form-field', function () {
    var xml = require('../../fixtures/ifml/simple/form-field.ifml');

    return bootstrapViewer(xml).call(this).then(function (result) {

      var err = result.error;
      expect(err).not.to.exist;

      inject(function (elementRegistry) {
        var callActivityGfx = elementRegistry.getGraphics('myFormId');
        var label = domQuery('.djs-label tspan', callActivityGfx);
        expect(label.textContent).to.include("myForm");
      })();
    });
  });

  it('should render form-submit', function () {
    var xml = require('../../fixtures/ifml/simple/form-submit.ifml');

    return bootstrapViewer(xml).call(this).then(function (result) {

      var err = result.error;
      expect(err).not.to.exist;

      inject(function (elementRegistry) {
        var callActivityGfx = elementRegistry.getGraphics('myOnSubmitEvent');
        expect(callActivityGfx).to.exist;
      })();
    });
  });

  it('should render parameterbindingGroup', function () {
    var xml = require('../../fixtures/ifml/simple/form-action-binded.ifml');

    return bootstrapViewer(xml).call(this).then(function (result) {

      var err = result.error;
      expect(err).not.to.exist;

      inject(function (elementRegistry) {
        var callActivityGfx = elementRegistry.getGraphics('myParameterbinding');
        expect(callActivityGfx).to.exist;
      })();
    });
  });

  // Databinding
  it('should render databinding', function () {
    var xml = require('../../fixtures/ifml/simple/databinding.ifml');

    return bootstrapViewer(xml).call(this).then(function (result) {

      var err = result.error;
      expect(err).not.to.exist;

      inject(function (elementRegistry) {
        var callActivityGfx = elementRegistry.getGraphics('DataBinding1');
        expect(callActivityGfx).to.exist;
      })();
    });
  });

  // Modules
});