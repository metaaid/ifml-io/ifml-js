import TestContainer from 'mocha-test-container-support';

import Diagram from 'diagram-js/lib/Diagram';

import ViewerDefaultExport from '../../index.js';

import Viewer from 'lib/Viewer';

import {
  createViewer
} from 'test/TestHelper';
import { getDi } from '../../lib/util/ModelUtil';


describe('Viewer', function () {

  var container;

  beforeEach(function () {
    container = TestContainer.get(this);
  });


  it('should import simple navigation', function () {
    var xml = require('../fixtures/ifml/simple.ifml');

    // when
    return createViewer(container, Viewer, xml).then(function (result) {

      var err = result.error;
      var warnings = result.warnings;
      var viewer = result.viewer;

      // then
      expect(err).not.to.exist;
      expect(warnings).to.be.empty;

      var definitions = viewer.getDefinitions();

      expect(definitions).to.exist;
      expect(definitions).to.eql(viewer._definitions);
    });
  });


  it('should re-import simple navigation', function () {

    var xml = require('../fixtures/ifml/simple.ifml');

    // given
    return createViewer(container, Viewer, xml).then(function (result) {

      var viewer = result.viewer;

      // when
      // mimic re-import of same diagram
      return viewer.importXML(xml).then(function (result) {

        // then
        expect(result.warnings).to.be.empty;
      });

    });
  });


  it('should be instance of Diagram', function () {

    // when
    var viewer = new Viewer({
      container: container
    });

    // then
    expect(viewer).to.be.instanceof(Diagram);
  });


  describe('overlay support', function () {

    it('should allow to add overlays', function () {

      var xml = require('../fixtures/ifml/simple.ifml');

      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        var viewer = result.viewer;

        expect(err).not.to.exist;

        // when
        var overlays = viewer.get('overlays'),
          elementRegistry = viewer.get('elementRegistry');

        // then
        expect(overlays).to.exist;
        expect(elementRegistry).to.exist;

        // when
        overlays.add('ViewContainer1', {
          position: {
            bottom: 0,
            right: 0
          },
          html: '<div style="max-width: 50px">YUP GREAT STUFF!</div>'
        });

        // then
        expect(overlays.get({
          element: 'ViewContainer1'
        }).length).to.equal(1);
      });

    });

  });


  describe('editor actions support', function () {

    it('should not ship per default', function () {

      // given
      var viewer = new Viewer();

      // when
      var editorActions = viewer.get('editorActions', false);

      // then
      expect(editorActions).not.to.exist;
    });

  });


  describe('error handling', function () {

    function expectMessage(e, expectedMessage) {

      expect(e).to.exist;

      if (expectedMessage instanceof RegExp) {
        expect(e.message).to.match(expectedMessage);
      } else {
        expect(e.message).to.equal(expectedMessage);
      }
    }

    function expectWarnings(warnings, expected) {

      expect(warnings.length).to.equal(expected.length);

      warnings.forEach(function (w, idx) {
        expectMessage(w, expected[idx]);
      });
    }


    it('should handle non-ifml input', function () {

      var xml = 'invalid stuff';

      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;

        expect(err).to.exist;

        expectMessage(err, /missing start tag/);
      });
    });

    it('should handle invalid IFMLNode#modelElement', function () {

      var xml = require('../fixtures/ifml/error/vc-no-modelelement.ifml');

      // when
      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        var warnings = result.warnings;

        // then
        expect(err).not.to.exist;

        expectWarnings(warnings, [
          'unresolved reference <ViewContainer1>',
          'no modelElement referenced in <ifmldi:IFMLNode id="ViewContainer1_di" />'
        ]);
      });
    });


    it('should handle duplicate ids', function () {

      var xml = require('../fixtures/ifml/error/duplicate-ids.ifml');

      // when
      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        var warnings = result.warnings;

        // then
        expect(err).not.to.exist;

        expectWarnings(warnings, [
          /duplicate ID <ViewContainer>/
        ]);
      });
    });


    it('should throw error due to missing diagram', function () {

      var xml = require('../fixtures/ifml/error/empty-definitions.ifml');

      // when
      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;

        // then
        expect(err.message).to.eql('no diagram to display');
      });
    });

  });


  describe('dependency injection', function () {

    it('should provide self as <ifmljs>', function () {

      var xml = require('../fixtures/ifml/simple.ifml');

      return createViewer(container, Viewer, xml).then(function (result) {

        var viewer = result.viewer;
        var err = result.error;

        expect(viewer.get('ifmljs')).to.equal(viewer);
        expect(err).not.to.exist;
      });
    });


    it('should allow Diagram#get before import', function () {

      // when
      var viewer = new Viewer({
        container: container
      });

      // then
      var eventBus = viewer.get('eventBus');

      expect(eventBus).to.exist;
    });


    it('should keep references to services across re-import', function () {

      // given
      var someXML = require('../fixtures/ifml/simple.ifml'),
        otherXML = require('../fixtures/ifml/basic.ifml');

      var viewer = new Viewer({
        container: container
      });

      var eventBus = viewer.get('eventBus'),
        canvas = viewer.get('canvas');

      // when
      return viewer.importXML(someXML).then(function () {

        // then
        expect(viewer.get('canvas')).to.equal(canvas);
        expect(viewer.get('eventBus')).to.equal(eventBus);

        return viewer.importXML(otherXML);
      }).then(function () {

        // then
        expect(viewer.get('canvas')).to.equal(canvas);
        expect(viewer.get('eventBus')).to.equal(eventBus);
      });
    });
  });


  describe('creation', function () {

    var testModules = [{
      logger: ['type', function () {
        this.called = true;
      }]
    }];

    // given
    var xml = require('../fixtures/ifml/simple.ifml');

    var viewer;

    afterEach(function () {
      viewer.destroy();
    });

    it('should override default modules', function () {

      // given
      viewer = new Viewer({
        container: container,
        modules: testModules
      });

      // when
      return viewer.importXML(xml).catch(function (err) {

        // then
        expect(err.message).to.equal('No provider for "ifmlImporter"! (Resolving: ifmlImporter)');
      });

    });


    it('should add module to default modules', function () {

      // given
      viewer = new Viewer({
        container: container,
        additionalModules: testModules
      });

      // when
      return viewer.importXML(xml).then(function (result) {

        // then
        var logger = viewer.get('logger');
        expect(logger.called).to.be.true;
      });

    });


    it('should use custom size and position', function () {

      // when
      viewer = new Viewer({
        container: container,
        width: 200,
        height: 100,
        position: 'fixed'
      });

      // then
      expect(viewer._container.style.position).to.equal('fixed');
      expect(viewer._container.style.width).to.equal('200px');
      expect(viewer._container.style.height).to.equal('100px');
    });
  });


  describe('configuration', function () {

    var xml = require('../fixtures/ifml/simple.ifml');

    it('should configure Canvas', function () {

      // given
      var viewer = new Viewer({
        container: container,
        canvas: {
          deferUpdate: true
        }
      });

      // when
      return viewer.importXML(xml).then(function (result) {

        var canvasConfig = viewer.get('config.canvas');

        // then
        expect(canvasConfig.deferUpdate).to.be.true;
      });

    });


    describe('container', function () {

      it('should attach if provided', function () {

        var xml = require('../fixtures/ifml/simple.ifml');

        var viewer = new Viewer({
          container: container
        });

        return viewer.importXML(xml).then(function (result) {

          expect(viewer._container.parentNode).to.equal(container);
        });
      });


      it('should not attach if absent', function () {

        var xml = require('../fixtures/ifml/simple.ifml');

        var viewer = new Viewer();

        return viewer.importXML(xml).then(function (result) {

          expect(viewer._container.parentNode).to.equal(null);
        });
      });

    });

  });


  describe('#importXML', function () {

    it('should emit <import.*> events', function () {

      // given
      var viewer = new Viewer({
        container: container
      });

      var xml = require('../fixtures/ifml/simple.ifml');

      var events = [];

      viewer.on([
        'import.parse.start',
        'import.parse.complete',
        'import.render.start',
        'import.render.complete',
        'import.done'
      ], function (e) {

        // log event type + event arguments
        events.push([
          e.type,
          Object.keys(e).filter(function (key) {
            return key !== 'type';
          })
        ]);
      });

      // when
      return viewer.importXML(xml).then(function (result) {

        // then
        expect(events).to.eql([
          ['import.parse.start', ['xml']],
          ['import.parse.complete', ['error', 'definitions', 'elementsById', 'references', 'warnings']],
          ['import.render.start', ['definitions']],
          ['import.render.complete', ['error', 'warnings']],
          ['import.done', ['error', 'warnings']]
        ]);
      });
    });


    it('should work without callback', function (done) {

      // given
      var viewer = new Viewer({
        container: container
      });

      var xml = require('../fixtures/ifml/simple.ifml');

      // when
      viewer.importXML(xml);

      // then
      viewer.on('import.done', function (event) {
        done();
      });
    });


    describe('multiple ifml elements', function () {

      var multipleXML = require('../fixtures/ifml/multiple-diagrams.ifml');


      it('should import default without ifml diagram specified', function () {

        // when
        return createViewer(container, Viewer, multipleXML).then(function (result) {

          var err = result.error;

          // then
          expect(err).not.to.exist;
        });
      });


      it('should import ifmlDiagram specified by id', function () {

        // when
        return createViewer(container, Viewer, multipleXML, 'dia2').then(function (result) {

          var err = result.error;

          // then
          expect(err).not.to.exist;
        });
      });


      it('should handle diagram not found', function () {

        // given
        var xml = require('../fixtures/ifml/multiple-diagrams.ifml');

        // when
        return createViewer(container, Viewer, xml, 'Diagram_IDontExist').then(function (result) {

          var err = result.error;

          // then
          expect(err).to.exist;
          expect(err.message).to.eql('IFMLDiagram <Diagram_IDontExist> not found');
        });
      });


      describe('without callback', function () {

        it('should open default', function (done) {

          // given
          var viewer = new Viewer({
            container: container
          });

          // when
          viewer.importXML(multipleXML);

          // then
          viewer.on('import.done', function (event) {
            done(event.error);
          });
        });


        it('should open specific ifml Diagram', function (done) {

          // given
          var viewer = new Viewer({
            container: container
          });

          // when
          viewer.importXML(multipleXML, 'dia2');

          // then
          viewer.on('import.done', function (event) {
            done(event.error);
          });
        });

      });

    });

  });


  describe('#importDefinitions', function () {

    describe('single diagram', function () {

      var xml = require('../fixtures/ifml/simple.ifml'),
        viewer,
        definitions;

      beforeEach(function () {
        return createViewer(container, Viewer, xml, null).then(function (result) {

          var error = result.error;
          var tmpViewer = result.viewer;

          if (error) {
            throw error;
          }

          definitions = tmpViewer.getDefinitions();

          tmpViewer.destroy();
        });
      });

      beforeEach(function () {
        viewer = new Viewer({
          container: container
        });
      });

      afterEach(function () {
        viewer.destroy();
      });


      it('should emit <import.*> events', function () {

        // given
        var events = [];

        viewer.on([
          'import.parse.start',
          'import.parse.complete',
          'import.render.start',
          'import.render.complete',
          'import.done'
        ], function (e) {

          // log event type + event arguments
          events.push([
            e.type,
            Object.keys(e).filter(function (key) {
              return key !== 'type';
            })
          ]);
        });

        // when
        return viewer.importDefinitions(definitions).then(function () {

          // then
          expect(events).to.eql([
            ['import.render.start', ['definitions']],
            ['import.render.complete', ['error', 'warnings']]
          ]);
        });
      });


      it('should work without callback', function (done) {

        // given
        viewer.on('import.render.complete', function (context) {

          // then
          done(context.error);
        });

        // when
        viewer.importDefinitions(definitions);
      });

    });


    describe('multipl ifml diagram elements', function () {

      var multipleXML = require('../fixtures/ifml/multiple-diagrams.ifml'),
        viewer,
        definitions;

      beforeEach(function () {
        return createViewer(container, Viewer, multipleXML).then(function (result) {

          var error = result.error;
          var tmpViewer = result.viewer;

          if (error) {
            throw error;
          }

          definitions = tmpViewer.getDefinitions();

          tmpViewer.destroy();
        });
      });

      beforeEach(function () {
        viewer = new Viewer({
          container: container
        });
      });

      afterEach(function () {
        viewer.destroy();
      });


      it('should import default without ifmlDiagram specified', function () {

        // when
        return viewer.importDefinitions(definitions);
      });


      it('should import ifmlDiagram specified by id', function () {

        // when
        return viewer.importDefinitions(definitions, 'dia2');
      });


      it('should handle diagram not found', function () {

        // when
        return viewer.importDefinitions(definitions, 'Diagram_IDontExist').catch(function (err) {

          // then
          expect(err).to.exist;
          expect(err.message).to.eql('IFMLDiagram <Diagram_IDontExist> not found');
        });
      });


      describe('without callback', function () {

        it('should open default', function (done) {

          // given
          viewer.on('import.render.complete', function (event) {

            // then
            done(event.error);
          });

          // when
          viewer.importDefinitions(definitions);
        });


        it('should open specifie ifml Diagram', function (done) {

          // given
          viewer.on('import.render.complete', function (event) {

            // then
            done(event.error);
          });

          // when
          viewer.importDefinitions(definitions, 'dia2');
        });

      });

    });
  });


  describe('#open', function () {

    var multipleXMLSimple = require('../fixtures/ifml/multiple-diagrams.ifml'),
      multipleXMLOverlappingDI = require('../fixtures/ifml/multiple-diagrams-overlapping-di.ifml'),
      diagram1 = 'dia1',
      diagram2 = 'dia2';


    it('should open the first diagram if id was not provided', function () {

      var viewer, renderedDiagram;

      // when
      return createViewer(container, Viewer, multipleXMLSimple, diagram1).then(function (result) {

        var err = result.error;
        viewer = result.viewer;

        expect(err).not.to.exist;

        renderedDiagram = getDi(viewer.get('canvas').getRootElement());

        return viewer.open();
      }).then(function () {

        // then
        var rootElement = viewer.get('canvas').getRootElement();
        expect(getDi(rootElement)).to.equal(renderedDiagram);
      });
    });


    it('should switch between diagrams', function () {

      var viewer, definitions;

      // when
      return createViewer(container, Viewer, multipleXMLSimple, diagram1).then(function (result) {

        var err = result.error;
        var warnings = result.warnings;
        viewer = result.viewer;

        // then

        expect(err).not.to.exist;

        expect(warnings).to.be.empty;

        definitions = viewer.getDefinitions();

        expect(definitions).to.exist;

        return viewer.open(diagram2);
      }).then(function (result) {

        // then
        var warnings = result.warnings;

        expect(warnings).to.be.empty;

        expect(definitions).to.equal(viewer.getDefinitions());

        var elementRegistry = viewer.get('elementRegistry');

        expect(elementRegistry.get('ViewContainer1')).to.not.exist;
        expect(elementRegistry.get('ViewContainer2')).to.exist;
      });
    });


    it('should switch between diagrams with overlapping DI', function () {

      var viewer, definitions;

      // when
      return createViewer(container, Viewer, multipleXMLOverlappingDI, diagram1).then(function (result) {

        var err = result.error;
        var warnings = result.warnings;
        viewer = result.viewer;

        // then
        expect(err).not.to.exist;

        expect(warnings).to.be.empty;

        definitions = viewer.getDefinitions();

        expect(definitions).to.exist;

        return viewer.open(diagram2);
      }).then(function (result) {

        var warnings = result.warnings;

        expect(warnings).to.be.empty;

        expect(definitions).to.equal(viewer.getDefinitions());
      });
    });


    it('should complete with error if xml was not imported', function () {

      // given
      var viewer = new Viewer();

      // when
      return viewer.open().catch(function (err) {

        // then
        expect(err).to.exist;
        expect(err.message).to.eql('no XML imported');

        var definitions = viewer.getDefinitions();

        expect(definitions).to.not.exist;
      });

    });


    it('should open with error if diagram does not exist', function () {

      var viewer, definitions;

      // when
      return createViewer(container, Viewer, multipleXMLSimple, diagram1).then(function (result) {

        var err = result.error;
        var warnings = result.warnings;
        viewer = result.viewer;

        // then
        expect(err).not.to.exist;

        expect(warnings).to.be.empty;

        definitions = viewer.getDefinitions();

        expect(definitions).to.exist;

        return viewer.open('Diagram_IDontExist');
      }).catch(function (err) {

        // then
        expect(err).to.exist;
        expect(err.message).to.eql('IFMLDiagram <Diagram_IDontExist> not found');

        // definitions stay the same
        expect(viewer.getDefinitions()).to.eql(definitions);
      });
    });


    it('should emit <import.*> events', function () {

      var viewer = new Viewer({
        container: container
      });

      var events = [];

      return viewer.importXML(multipleXMLSimple, diagram1).then(function (result) {

        // given
        viewer.on([
          'import.parse.start',
          'import.parse.complete',
          'import.render.start',
          'import.render.complete',
          'import.done'
        ], function (e) {

          // log event type + event arguments
          events.push([
            e.type,
            Object.keys(e).filter(function (key) {
              return key !== 'type';
            })
          ]);
        });

        // when
        return viewer.open(diagram2);
      }).then(function () {

        // then
        expect(events).to.eql([
          ['import.render.start', ['definitions']],
          ['import.render.complete', ['error', 'warnings']]
        ]);
      });
    });

  });


  describe('#saveXML', function () {

    it('should export XML', function () {

      // given
      var xml = require('../fixtures/ifml/simple.ifml');

      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        var viewer = result.viewer;

        expect(err).not.to.exist;

        // when
        return viewer.saveXML({
          format: true
        });
      }).then(function (result) {

        var xml = result.xml;

        // then
        expect(xml).to.contain('<?xml version="1.0" encoding="UTF-8"?>');
        expect(xml).to.contain('<xmi:XMI');
        expect(xml).to.contain('  ');
      });
    });


    it('should emit <saveXML.*> events', function () {

      var xml = require('../fixtures/ifml/simple.ifml');

      var viewer;
      var events = [];

      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        viewer = result.viewer;

        expect(err).not.to.exist;

        viewer.on([
          'saveXML.start',
          'saveXML.serialized',
          'saveXML.done'
        ], function (e) {

          // log event type + event arguments
          events.push([
            e.type,
            Object.keys(e).filter(function (key) {
              return key !== 'type';
            })
          ]);
        });

        return viewer.importXML(xml);
      }).then(function (result) {

        // when
        return viewer.saveXML();
      }).then(function () {

        // then
        expect(events).to.eql([
          ['saveXML.start', ['definitions']],
          ['saveXML.serialized', ['error', 'xml']],
          ['saveXML.done', ['error', 'xml']]
        ]);
      });
    });

  });


  describe('#saveSVG', function () {

    function currentTime() {
      return new Date().getTime();
    }

    function validSVG(svg) {
      var expectedStart = '<?xml version="1.0" encoding="utf-8"?>';
      var expectedEnd = '</svg>';


      expect(svg.indexOf(expectedStart)).to.equal(0);
      expect(svg.indexOf(expectedEnd)).to.equal(svg.length - expectedEnd.length);

      // expect header to be written only once
      expect(svg.indexOf('<svg width="100%" height="100%">')).to.equal(-1);
      expect(svg.indexOf('<g class="viewport"')).to.equal(-1);

      var parser = new DOMParser();
      var svgNode = parser.parseFromString(svg, 'image/svg+xml');

      // [comment, <!DOCTYPE svg>, svg]
      expect(svgNode.childNodes).to.have.length(3);

      // no error body
      expect(svgNode.body).not.to.exist;

      // FIXME(nre): make matcher
      return true;
    }


    it('should export svg', function () {

      // given
      var xml = require('../fixtures/ifml/simple.ifml');

      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        var viewer = result.viewer;

        if (err) {
          throw err;
        }

        // when
        return viewer.saveSVG();
      }).then(function (result) {

        var svg = result.svg;

        // then
        expect(validSVG(svg)).to.be.true;
      });
    });


    it('should export huge svg', function () {

      this.timeout(5000);

      // given
      var xml = require('../fixtures/ifml/complete.ifml');

      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        var viewer = result.viewer;

        if (err) {
          throw err;
        }

        // when
        return viewer.saveSVG();
      }).then(function (result) {

        var svg = result.svg;

        var time = currentTime();

        // then
        expect(validSVG(svg)).to.be.true;

        // no svg export should not take too long
        expect(currentTime() - time).to.be.below(1000);
      });
    });


    it('should remove outer-makers on export', function () {

      // given
      var xml = require('../fixtures/ifml/simple.ifml');

      function appendTestRect(svgDoc) {
        var rect = document.createElementNS(svgDoc.namespaceURI, 'rect');
        rect.setAttribute('class', 'outer-bound-marker');
        rect.setAttribute('width', 500);
        rect.setAttribute('height', 500);
        rect.setAttribute('x', 10000);
        rect.setAttribute('y', 10000);
        svgDoc.appendChild(rect);
      }

      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        var viewer = result.viewer;

        if (err) {
          throw err;
        }

        var svgDoc = viewer._container.childNodes[1].childNodes[1];

        appendTestRect(svgDoc);
        appendTestRect(svgDoc);

        expect(svgDoc.querySelectorAll('.outer-bound-marker')).to.exist;

        // when
        return viewer.saveSVG();
      }).then(function (result) {

        var svg = result.svg;

        var svgDoc = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        svgDoc.innerHTML = svg;

        // then
        expect(validSVG(svg)).to.be.true;
        expect(svgDoc.querySelector('.outer-bound-marker')).to.be.null;

      });
    });


    it('should emit <saveSVG.*> events', function () {

      var xml = require('../fixtures/ifml/simple.ifml');

      var viewer;
      var events = [];

      return createViewer(container, Viewer, xml).then(function (result) {

        var err = result.error;
        viewer = result.viewer;

        expect(err).not.to.exist;

        viewer.on([
          'saveSVG.start',
          'saveSVG.done'
        ], function (e) {

          // log event type + event arguments
          events.push([
            e.type,
            Object.keys(e).filter(function (key) {
              return key !== 'type';
            })
          ]);
        });

        return viewer.importXML(xml);
      }).then(function () {

        // when
        return viewer.saveSVG();
      }).then(function () {

        // then
        expect(events).to.eql([
          ['saveSVG.start', []],
          ['saveSVG.done', ['error', 'svg']]
        ]);
      });
    });

  });


  describe('#on', function () {

    it('should fire with given three', function () {

      // given
      var viewer = new Viewer({
        container: container
      });

      var xml = require('../fixtures/ifml/simple.ifml');

      // when
      viewer.on('foo', 1000, function () {
        return 'bar';
      }, viewer);

      // then
      return viewer.importXML(xml).then(function () {
        var eventBus = viewer.get('eventBus');

        var result = eventBus.fire('foo');

        expect(result).to.equal('bar');
      });
    });
  });


  describe('#off', function () {

    var xml = require('../fixtures/ifml/simple.ifml');

    it('should remove listener permanently', function () {

      // given
      var viewer = new Viewer({
        container: container
      });

      var handler = function () {
        return 'bar';
      };

      viewer.on('foo', 1000, handler);

      // when
      viewer.off('foo');

      // then
      return viewer.importXML(xml).then(function () {
        var eventBus = viewer.get('eventBus');

        var result = eventBus.fire('foo');

        expect(result).not.to.exist;
      });
    });


    it('should remove listener on existing diagram instance', function () {

      // given
      var viewer = new Viewer({
        container: container
      });

      var handler = function () {
        return 'bar';
      };

      viewer.on('foo', 1000, handler);

      // when
      return viewer.importXML(xml).then(function () {
        var eventBus = viewer.get('eventBus');

        // when
        viewer.off('foo', handler);

        var result = eventBus.fire('foo');

        expect(result).not.to.exist;
      });
    });
  });


  describe('#destroy', function () {

    it('should remove traces in document tree', function () {

      // given
      var viewer = new Viewer({
        container: container
      });

      // when
      viewer.destroy();

      // then
      expect(viewer._container.parentNode).not.to.exist;
    });

  });


  describe('#attachTo', function () {

    it('should attach the viewer', function () {

      var xml = require('../fixtures/ifml/simple.ifml');

      var viewer = new Viewer();

      return viewer.importXML(xml).then(function (result) {

        // assume
        expect(viewer._container.parentNode).not.to.exist;

        var resizedSpy = sinon.spy();

        viewer.on('canvas.resized', resizedSpy);

        // when
        viewer.attachTo(container);

        // then
        expect(viewer._container.parentNode).to.equal(container);

        // should trigger resized
        expect(resizedSpy).to.have.been.called;
      });
    });
  });


  describe('#detach', function () {

    it('should detach the viewer', function () {

      var xml = require('../fixtures/ifml/simple.ifml');

      var viewer = new Viewer({
        container: container
      });

      return viewer.importXML(xml).then(function (result) {

        // assume
        expect(viewer._container.parentNode).to.equal(container);

        // when
        viewer.detach();

        // then
        expect(viewer._container.parentNode).not.to.exist;
      });
    });
  });


  describe('#clear', function () {

    it('should NOT clear if no diagram', function () {

      // given
      var viewer = new Viewer({
        container: container
      });

      var eventBus = viewer.get('eventBus');

      var spy = sinon.spy();

      eventBus.on('diagram.clear', spy);

      // when
      viewer.clear();

      // then
      expect(spy).not.to.have.been.called;
    });


    it('should not throw if diagram is already empty', function () {

      // given
      var viewer = new Viewer({
        container: container
      });

      function clearDiagram() {
        viewer.clear();
      }

      // then
      expect(clearDiagram).to.not.throw();
    });

  });


  it('default export', function () {
    expect(ViewerDefaultExport).to.equal(Viewer);
  });


  // TODO: custom elements/extensions

  it('should handle missing root namespace', function () {

    var xml = require('../fixtures/ifml/error/missing-namespace.ifml');

    // when
    return createViewer(container, Viewer, xml).then(function (result) {

      var err = result.error;
      var warnings = result.warnings;

      // then
      expect(err).to.exist;
      expect(err.message).to.eql('no diagram to display');

      expect(warnings).to.have.length(1);

      expect(warnings[0].message).to.match(/unparsable content <X> detected/);
    });
  });

  it('should handle no model', function () {

    var xml = require('../fixtures/ifml/error/missing-model.ifml');

    // when
    return createViewer(container, Viewer, xml).then(function (result) {

      var err = result.error;
      var warnings = result.warnings;

      // then
      expect(err).to.exist;
      expect(err.message).to.eql('No model found in the xmi');

      // expect(warnings).to.have.length(1);

      expect(warnings[0].message).to.match(/unparsable content <IFMLModel> detected/);
    });
  });
});