describe('ifml-modeler', function() {

  it('should expose globals', function() {

    var IfmlJS = window.IfmlJS;

    // then
    expect(IfmlJS).to.exist;
    expect(new IfmlJS()).to.exist;
  });


  it('should expose Viewer and NavigatedViewer', function() {

    var IfmlJS = window.IfmlJS;

    // then
    expect(IfmlJS.NavigatedViewer).to.exist;
    expect(new IfmlJS.NavigatedViewer()).to.exist;

    expect(IfmlJS.Viewer).to.exist;
    expect(new IfmlJS.Viewer()).to.exist;
  });


  it('should import initial diagram', function(done) {

    var IfmlJS = window.IfmlJS;

    // then
    /* global testImport */
    testImport(IfmlJS, done);
  });

});