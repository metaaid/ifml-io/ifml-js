describe('ifml-navigated-viewer', function() {

  it('should expose globals', function() {

    var IfmlJS = window.IfmlJS;

    // then
    expect(IfmlJS).to.exist;
    expect(new IfmlJS()).to.exist;
  });


  it('should import initial diagram', function(done) {

    var IfmlJS = window.IfmlJS;

    // then
    /* global testImport */
    testImport(IfmlJS, done);
  });

});