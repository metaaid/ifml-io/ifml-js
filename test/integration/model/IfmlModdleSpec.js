import IfmlModdle from 'ifml-moddle';


describe('ifml-moddle', function() {

  function parse(xml) {
    var moddle = new IfmlModdle();
    return moddle.fromXML(xml, 'xmi:XMI');
  }


  describe('browser support', function() {

    it('should parse simple xmi', function() {

      var xml =
        '<?xml version="1.0" encoding="UTF-8"?>' +
        '<xmi:XMI xmlns:ifml="http://www.omg.org/spec/IFML/20140301" ' +
                           'targetNamespace="http://ifml.io/schema/ifml">' +
          '<ifml:IFMLModel xmi:id="myModel"></ifml:IFMLModel>' +
        '</xmi:XMI>';

      // when
      return parse(xml).then(function(result) {

        var xmi = result.rootElement; 

        // then

        expect(xmi.ownedElement.length).to.equal(1);
        expect(xmi.ownedElement[0].$id).to.equal('myModel');
      });
    });


    it('should parse complex xml', function() {

      var xml = require('../../fixtures/ifml/complete.ifml');

      var start = new Date().getTime();

      // when
      return parse(xml).then(function() {

        // then
        // parsing a XML document should not take too long
        expect((new Date().getTime() - start)).to.be.below(1000);
      });
    });

  });

});
