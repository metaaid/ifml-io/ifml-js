import Modeler from 'lib/Modeler';

import TestContainer from 'mocha-test-container-support';

function delay(fn) {
  setTimeout(fn, 10);
}


describe.skip('scenario - successive reopening', function () {

  var container;

  beforeEach(function () {
    container = TestContainer.get(this);
  });


  var boundaryXML = require('../fixtures/ifml/boundary-events.ifml'),
    containersXML = require('../fixtures/ifml/containers.ifml'),
    flowMarkersXML = require('../fixtures/ifml/flow-markers.ifml'),
    simpleXML = require('../fixtures/ifml/simple.ifml');

  var allDiagrams = [
    boundaryXML,
    containersXML,
    flowMarkersXML,
    simpleXML
  ];

  it('should import 100 diagrams', function (done) {

    // this test needs time
    this.timeout(30000);

    var count = 0;

    // given
    var modeler = new Modeler({
      container: container
    });

    modeler.on('import.done', function (event) {

      if (event.error) {
        console.error('ERROR', event.error);
      }

      if (event.warnings && event.warnings.length) {
        // console.warn('WARNINGS', event.warnings);
      }
    });

    function finish(err) {
      modeler.destroy();

      done(err);
    }

    function importNext() {

      if (count === 100) {
        return finish();
      }

      var i = count % allDiagrams.length;

      var xml = allDiagrams[i];

      // when
      modeler.importXML(xml).then(function () {
        count++;

        delay(importNext);
      }).catch(function (err) {
        return finish(err);
      });
    }

    // when
    importNext();
  });

});