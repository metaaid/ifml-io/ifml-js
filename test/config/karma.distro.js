/* global process */

// configures browsers to run test against
// any of [ 'ChromeHeadless', 'Chrome', 'Firefox', 'IE', 'PhantomJS' ]
var browsers = (process.env.TEST_BROWSERS || 'ChromeHeadless').split(',');

// use puppeteer provided Chrome for testing

var VARIANT = process.env.VARIANT || "ifml-modeler";
var NODE_ENV = process.env.NODE_ENV;

var env = (NODE_ENV === 'production' ? 'production.min' : 'development');
var ifmlJsPath = 'dist/' + VARIANT  + '.' + env + '.js';

module.exports = function(karma) {
  karma.set({

    basePath: '../../',

    frameworks: [
      'mocha',
      'sinon-chai'
    ],

    files: [
      ifmlJsPath,
      'dist/assets/app.css',
      'dist/assets/diagram-js.css',
      { pattern: 'resources/initial.ifml', included: false },
      { pattern: 'dist/assets/**/*', included: false },
      'test/distro/helper.js',
      'test/distro/' + VARIANT + '.js'
    ],

    reporters: ['progress'],

    browsers,

    customLaunchers: {
      ChromeDebugging: {
        base: 'Chrome',
        flags: ['--remote-debugging-port=9333', '--no-sandbox']
      },
      FirefoxHeadless: {
        base: 'Firefox',
        flags: ['---headless']
      }
    },

    browserNoActivityTimeout: 30000,

    singleRun: true,
    autoWatch: false
  });

};
