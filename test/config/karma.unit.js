var path = require('path');

var collectTranslations = process.env.COLLECT_TRANSLATIONS;
var singleStart = process.env.SINGLE_START;
var coverage = process.env.COVERAGE;

// configures browsers to run test against
// any of [ 'ChromeDebugging', 'Chrome', 'FirefoxHeadless', 'Firefox', 'IE', 'PhantomJS' ]
var defaultProgress =  coverage ? 'progress,coverage' : 'progress';
var browsers = (process.env.TEST_BROWSERS || 'ChromeDebugging').split(',');
var progress = (process.env.TEST_PROGRESS || defaultProgress).split(',');


var basePath = '../..';

var absoluteBasePath = path.resolve(path.join(__dirname, basePath));

var suite = coverage ? 'test/coverageBundle.js' : 'test/testBundle.js';

module.exports = function (karma) {

  var config = {

    basePath,

    plugins: [
      require('karma-webpack'),
      require('karma-env-preprocessor'),
      require('karma-sinon-chai'),
      require('karma-mocha'),
      require('karma-chrome-launcher'),
      require('karma-firefox-launcher'),
      require('karma-coverage'),
    ],

    // plugins: {
    //   // webpack: webpack
    //   webpack: webpack
    // },

    frameworks: [
      'mocha',
      'sinon-chai'
    ],

    files: [suite],

    preprocessors: {
      [suite]: ['webpack', 'env', ]
    },

    reporters: progress,

    coverageReporter: {
      reporters: [{
        type: 'lcov',
        subdir: '.'
      }]
    },
    browsers: browsers,

    customLaunchers: {
      ChromeDebugging: {
        base: 'Chrome',
        flags: ['--remote-debugging-port=9333', '--no-sandbox']
      },
      FirefoxHeadless: {
        base: 'Firefox',
        flags: ['---headless']
      }
    },

    browserNoActivityTimeout: 30000,

    singleRun: true,
    autoWatch: false,

    webpack: {
      mode: 'development',
      module: {
        rules: [{
          test: require.resolve('../TestHelper.js'),
          sideEffects: true
        }, {
          test: /\.css|\.ifml/,
          use: 'raw-loader'
        }].concat(coverage ? {
          test: /\.js$/,
          use: {
            loader: 'istanbul-instrumenter-loader',
            options: {
              esModules: true
            }
          },
          include: /lib\.*/,
          exclude: /node_modules/
        } : [])
      },
      resolve: {
        mainFields: [
          'dev:module',
          'browser',
          'module',
          'main'
        ],
        modules: [
          'node_modules',
          absoluteBasePath
        ]
      },
      devtool: 'eval-source-map'
    }
  };

  if (collectTranslations) {
    config.plugins = [].concat(config.plugins || ['karma-*'], require('./translation-reporter'));
    config.reporters = [].concat(config.reporters || [], 'translation-reporter');
    config.envPreprocessor = [].concat(config.envPreprocessor || [], 'COLLECT_TRANSLATIONS');
  }

  if (singleStart) {
    config.browsers = [].concat(config.browsers, 'Debug');
    config.envPreprocessor = [].concat(config.envPreprocessor || [], 'SINGLE_START');
  }

  karma.set(config);
};