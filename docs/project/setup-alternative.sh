#!/bin/bash

###
# Setup script to be executed in a ifml.io project root (some empty folder chosen by YOU). Use if you do not want to rely on npm link.
###

base=$(pwd)

echo cloning repositories

git clone git@github.com:bpmn-io/diagram-js.git
git clone git@gitlab.com:metaaid/ifml-io/ifml-js.git
git clone git@gitlab.com:metaaid/ifml-io/ifml-moddle.git

echo done.

echo setup diagram-js

cd $base/diagram-js
npm install

echo setup ifml-moddle

cd $base/ifml-moddle
npm install

echo setup bpmn-js

cd $base/bpmn-js
mkdir node_modules
ln -s $base/ifml-moddle node_modules/ifml-moddle
ln -s $base/diagram-js node_modules/diagram-js
npm install

cd $base

echo all done.
