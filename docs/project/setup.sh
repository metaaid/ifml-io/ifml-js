#!/bin/bash

###
# Setup script to be executed in a bpmn.io project root (some empty folder chosen by YOU)
###

base=$(pwd)

echo cloning repositories

git clone git@github.com:bpmn-io/diagram-js.git
git clone git@gitlab.com:metaaid/ifml-io/ifml-js.git
git clone git@gitlab.com:metaaid/ifml-io/ifml-moddle.git

echo done.

echo setup diagram-js

cd $base/diagram-js
npm install

echo setup ifml-moddle

cd $base/ifml-moddle
npm install

echo setup ifml-js

cd $base/ifml-js
npm install
npm link ../diagram-js
npm link ../ifml-moddle

cd $base

echo all done.
