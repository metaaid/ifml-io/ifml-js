@echo off

rem ###
rem # Setup script to be executed in a ifml.io project root (some empty folder chosen by YOU)
rem ##

set BASE=%CD%

echo cloning repositories

git clone git@github.com:bpmn-io/diagram-js.git
git clone git@gitlab.com:metaaid/ifml-io/ifml-js.git
git clone git@gitlab.com:metaaid/ifml-io/ifml-moddle.git

echo done.


echo setup diagram-js

cd %BASE%\diagram-js
npm install


echo setup ifml-moddle

cd %BASE%\ifml-moddle
npm install


echo prepare setup ifml-js

mkdir %BASE%\ifml-js\node_modules

rem link ifml-js
mklink /D %BASE%\ifml-js\node_modules\ifml-moddle %BASE%\ifml-moddle
mklink /D %BASE%\ifml-js\node_modules\diagram-js %BASE%\diagram-js


echo setup ifml-js

cd %BASE%\ifml-js
npm install


cd %BASE%
