# Project Setup

This document describes the necessary steps to setup a `ifml-js` development environment.


## TLDR;

On Linux, OS X or Windows? [git](http://git-scm.com/), [NodeJS](nodejs.org) and [npm](https://www.npmjs.org/doc/cli/npm.html) ready? Check out the [setup script section](https://gitlab.com/metaaid/ifml-io/ifml-js/-/tree/master/docs/project/SETUP.md#setup-via-script) below.


## Manual Steps

Make sure you have [git](http://git-scm.com/), [NodeJS](nodejs.org) and [npm](https://www.npmjs.org/doc/cli/npm.html)  installed before you continue.


### Get Project + Dependencies

The following projects from the [bpmn-io](https://github.com/bpmn-io) project on GitHub

* [diagram-js](https://github.com/bpmn-io/diagram-js)

The following projects from the [ifml-io](https://gitlab.com/metaaid/ifml-io) project on GitLab

* [ifml-js](git@gitlab.com:metaaid/ifml-io/ifml-js.git)
* [ifml-moddle](git@gitlab.com:metaaid/ifml-io/ifml-moddle.git)

and clone them into a common directory via

```
git clone git@github.com:bpmn-io/diagram-js.git
git clone git@gitlab.com:metaaid/ifml-io/ifml-js.git
git clone git@gitlab.com:metaaid/ifml-io/ifml-moddle.git
```

### Link Projects

[Link dependent projects](https://docs.npmjs.com/cli/link) between each other to pick up changes immediately.

```
.
├─ifml-js
│   └─node_modules
│       ├─diagram-js <link>
│       └─ifml-moddle <link>
├─diagram-js
├─ifml-moddle
```

#### On OS X, Linux

Use [npm-link](https://docs.npmjs.com/cli/link) or `ln -s <target> <link>`.

#### On Windows

Use `mklink /d <link> <target>` [(docs)](http://technet.microsoft.com/en-us/library/cc753194.aspx).

### Install Dependencies

Execute `npm install` on each of the projects to grab their dependencies.


### Verify Things are O.K.

Execute `npm run all` on each project. Things should be fine.


## Setup via Script

The whole setup can be automated through setup scripts for [Linux/OS X](https://gitlab.com/metaaid/ifml-io/ifml-js/-/tree/master/docs/project/setup.sh) and [Windows](https://gitlab.com/metaaid/ifml-io/ifml-js/-/tree/master/docs/project/setup.bat).
