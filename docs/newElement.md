# Create a new Element workflow
## Insert Method

### ContextPadProvider

add a new key to the `actions` var, depending on the type where the insert should be allowed
```js

if (isAny(element, ['ifml:Action', 'ifml:ViewElement'])) {
    assign(actions, {
        'append.event': {
            group: "myGroup", // a group id to group actions
            className: "className", // a class name to style the entry with css
            title: "title", // hover title
            iconText: "text", // A text that is displayed next to the icon (should be used in a unique group because of the wide)
            action: { // functions, that are executed on events
                dragstart: insertStart,
                click: insert
            
            }
        }
    );
  }
```

### PaletteProvider (PaletteProvider.js)

create new entry

### Create Icon

place it as css dunno

## Placement

Placement can be automaticly (used by ContextPadProvider) or by manual dragging the element from PaletteProvider

### Autoplacement (for ContextPadProvider)

Elements can be automaticly placed using the `autoplace` (`autoPlace`, `autoPlace.start`, `autoPlace.end`) event, handled by `AutoPlace`.
Additional Elements can be defined in `IfmlAutoPlaceUtil`. Its expected to return an array:

```js
{
    x: 0,
    y: 0,
    // optional widht and height (> 10), else default 
    height: 10, // optional height, > 10
    width: 10, // optional width, > 10
};
```

The `height` and `weight` are optional, if not defined the default width and height from `ElementFactory` are used.

## Create default Size

Create an entry in `ElementFactory`

## Render Element (IfmlRenderer)

The Added Element-Type needs a handler in the Renderer.

## Update Rules (IfmlRules)

Specify the rules for the Element:

- can the element be dropped on a parent element `canDrop`
- can the element be resized `canResize`
- if the Element canbe used for connections as source or target `canStartConnection`, `canConnect`

## Update Model (IfmlUpdater)

The `updateSemanticParent` method needs to return the field name, where the model element is stored.

## Add Custom Properties not displayed by the Diagram-Editor (IFMLPropertiesProvider)

## Serialize the model
## Load the model - add to TreeWalker (IfmltreeWalker)
