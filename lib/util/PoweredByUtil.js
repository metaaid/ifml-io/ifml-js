/**
 * This file must not be changed or exchanged.
 *
 * @see http://bpmn.io/license for more information.
 */

import {
  domify,
  delegate as domDelegate
} from 'min-dom';


// inlined ../../resources/logo.svg
var BPMNIO_LOGO_SVG = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.02 5.57" width="53" height="21" style="vertical-align:middle"><path fill="currentColor" d="M1.88.92v.14c0 .41-.13.68-.4.8.33.14.46.44.46.86v.33c0 .61-.33.95-.95.95H0V0h.95c.65 0 .93.3.93.92zM.63.57v1.06h.24c.24 0 .38-.1.38-.43V.98c0-.28-.1-.4-.32-.4zm0 1.63v1.22h.36c.2 0 .32-.1.32-.39v-.35c0-.37-.12-.48-.4-.48H.63zM4.18.99v.52c0 .64-.31.98-.94.98h-.3V4h-.62V0h.92c.63 0 .94.35.94.99zM2.94.57v1.35h.3c.2 0 .3-.09.3-.37v-.6c0-.29-.1-.38-.3-.38h-.3zm2.89 2.27L6.25 0h.88v4h-.6V1.12L6.1 3.99h-.6l-.46-2.82v2.82h-.55V0h.87zM8.14 1.1V4h-.56V0h.79L9 2.4V0h.56v4h-.64zm2.49 2.29v.6h-.6v-.6zM12.12 1c0-.63.33-1 .95-1 .61 0 .95.37.95 1v2.04c0 .64-.34 1-.95 1-.62 0-.95-.37-.95-1zm.62 2.08c0 .28.13.39.33.39s.32-.1.32-.4V.98c0-.29-.12-.4-.32-.4s-.33.11-.33.4z"/><path fill="currentColor" d="M0 4.53h14.02v1.04H0zM11.08 0h.63v.62h-.63zm.63 4V1h-.63v2.98z"/></svg>';
var IFMLIO_LOGO_SVG = '<?xml version="1.0" encoding="UTF-8"?><svg fill="#666" height="10mm" version="1.1" viewBox="0 0 23.416 12" xmlns="http://www.w3.org/2000/svg"><g transform="translate(-67.244 -80.6)"><g stroke-width=".26458" aria-label="ifml.io"><path d="m67.244 80.6v1.2043h1.0195v-1.2043zm17.232 0v1.2043h1.0852v-1.2043zm-13.545 0.01505c-0.78301 0-1.1743 0.39132-1.1743 1.1743v1.5613h-0.71358v0.87727h0.71358v6.3335h1.0256v-6.3335h1.041v-0.87727h-1.041v-1.3234c0-0.19822 0.03-0.32203 0.08951-0.37159 0.05952-0.0595 0.18327-0.08915 0.37159-0.08915h0.57984v-0.95144zm9.099 0v9.9463h1.026v-9.9464zm-12.758 2.7356v7.2107h0.96336v-7.2108zm5.2946 0v7.2107h1.026v-5.7986c0-0.19822 0.03942-0.33687 0.11875-0.41617 0.07936-0.07935 0.20817-0.11876 0.38658-0.11876h0.59483c0.19822 0 0.32203 0.02964 0.37159 0.08915 0.05951 0.05951 0.08915 0.18327 0.08915 0.3716v5.8727h1.026v-5.7985c0-0.19822 0.03981-0.33688 0.11911-0.41617 0.07935-0.07935 0.20816-0.11876 0.38657-0.11876h0.59448c0.19823 0 0.32237 0.02965 0.37194 0.08915 0.05954 0.05951 0.08915 0.18327 0.08915 0.3716v5.8727h1.0256v-6.0364c0-0.78302-0.39132-1.1743-1.1743-1.1743h-0.59481c-0.5055 0-0.80775 0.19802-0.90688 0.59448-0.15858-0.39646-0.52021-0.59448-1.0852-0.59448h-0.59482c-0.43611 0-0.70869 0.16362-0.81772 0.4907v-0.4907zm11.939 0v7.2107h1.026v-7.2108zm3.4641 0c-0.78302 0-1.1747 0.39132-1.1747 1.1743v4.8617c0 0.78302 0.39166 1.1747 1.1747 1.1747h1.5163c0.78304 0 1.1746-0.39166 1.1746-1.1747v-4.8617c0-0.78301-0.3916-1.1743-1.1746-1.1743zm0.29742 0.87727h0.92184c0.18828 0 0.31211 0.02965 0.37159 0.08915 0.05937 0.05953 0.08915 0.18327 0.08915 0.3716v4.5347c0 0.19824-0.03022 0.32709-0.08915 0.38657-0.05935 0.04962-0.18331 0.07421-0.37159 0.07421h-0.92184c-0.18833 0-0.31214-0.0246-0.3716-0.07421-0.05951-0.05951-0.0895-0.18833-0.0895-0.38657v-4.5347c0-0.18833 0.02999-0.31213 0.0895-0.3716 0.05952-0.05951 0.18327-0.08917 0.3716-0.08917zm-6.0956 4.6681v1.6654h1.2189v-1.6654zm-14.927 2.6845v1.0195h23.416v-1.0195z" stroke-width=".16885"/></g></g></svg>';
export var BPMNIO_IMG = BPMNIO_LOGO_SVG;
export var IFMLIO_IMG = IFMLIO_LOGO_SVG;

function css(attrs) {
  return attrs.join(';');
}

export var LINK_STYLES = css([
  'color: #404040'
]);

var LIGHTBOX_STYLES = css([
  'z-index: 1001',
  'position: fixed',
  'top: 0',
  'left: 0',
  'right: 0',
  'bottom: 0'
]);

var BACKDROP_STYLES = css([
  'width: 100%',
  'height: 100%',
  'background: rgba(40,40,40,0.2)'
]);

var NOTICE_STYLES = css([
  'position: absolute',
  'left: 50%',
  'top: 40%',
  'transform: translate(-50%)',
  'width: 260px',
  'padding: 10px',
  'background: white',
  'box-shadow: 0 1px 4px rgba(0,0,0,0.3)',
  'font-family: Helvetica, Arial, sans-serif',
  'font-size: 14px',
  'line-height: 1.3'
]);

var LIGHTBOX_MARKUP =
  '<div class="bjs-powered-by-lightbox" style="' + LIGHTBOX_STYLES + '">' +
    '<div class="backdrop" style="' + BACKDROP_STYLES + '"></div>' +
    '<div class="notice" style="' + NOTICE_STYLES + '">' +
    '<div style="display: flex;">' +
    '<a href="https://ifml.io" target="_blank" rel="noopener" style="margin: 15px 20px 15px 10px; align-self: center;' + LINK_STYLES + '">' +
    IFMLIO_IMG +
    '</a>' +
    '<span>' +
    'Web-based tooling for IFML diagrams ' +
    'powered by <a href="https://ifml.io" target="_blank" rel="noopener">ifml.io</a>.' +
    '</span>' +
    '</div>' +
    '<h5 style="margin: 10px 0 0 0">Based on:</h5>' +
    '<div style="display: flex">' +
      '<a href="https://bpmn.io" target="_blank" rel="noopener" style="margin: 15px 20px 15px 10px; align-self: center;' + LINK_STYLES + '">' +
      BPMNIO_IMG +
      '</a>' +
      '<span>' +
      'Web-based tooling for BPMN, DMN and CMMN diagrams ' +
      'powered by <a href="https://bpmn.io" target="_blank" rel="noopener">bpmn.io</a>.' +
      '</span>' +
      '</div>' +
    '</div>' +
  '</div>';


var lightbox;

export function open() {

  if (!lightbox) {
    lightbox = domify(LIGHTBOX_MARKUP);

    domDelegate.bind(lightbox, '.backdrop', 'click', function(event) {
      document.body.removeChild(lightbox);
    });
  }

  document.body.appendChild(lightbox);
}
