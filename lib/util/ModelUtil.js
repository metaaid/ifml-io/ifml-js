/**
 * Is an element of the given IFML type?
 *
 * @param  {djs.model.Base|ModdleElement} element
 * @param  {string} type
 *
 * @return {boolean}
 */
export function is(element, type) {
  var bo = getBusinessObject(element);

  return bo && (typeof bo.$instanceOf === 'function') && bo.$instanceOf(type);
}


/**
 * Return the business object for a given element.
 *
 * @param  {djs.model.Base|ModdleElement} element
 *
 * @return {ModdleElement}
 */
export function getBusinessObject(element) {
  return (element && element.businessObject) || element;
}

/**
 * checks if the given port is used as input
 * @param {djs.model.Base|ModdleElement} port
 */
export function portIsInput(port) {
  port = getBusinessObject(port);
  return port.get("direction") == 'in';
}
export function portIsOutput(port) {
  port = getBusinessObject(port);
  return port.get("direction") == 'out';
}

/**
 * Return the di object for a given element.
 *
 * @param  {djs.model.Base} element
 *
 * @return {ModdleElement}
 */
export function getDi(element) {
  return element && element.di;
}