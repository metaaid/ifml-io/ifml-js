import {
  has,
  isFunction
} from 'min-dash';

// TODO(nikku): remove with future bpmn-js version

/**
 * Wraps APIs to check:
 *
 * 1) If a callback is passed -> Warn users about callback deprecation.
 * 2) If Promise class is implemented in current environment.
 *
 * @private
 */
export function wrapForCompatibility(api) {

  return function() {

    if (!window.Promise) {
      throw new Error('Promises is not supported in this environment. Please polyfill Promise.');
    }

      return api.apply(this, arguments);
  };
}


// TODO(nikku): remove with future bpmn-js version

var DI_ERROR_MESSAGE = 'Tried to access di from the businessObject. The di is available through the diagram element only. For more information, see https://github.com/bpmn-io/bpmn-js/issues/1472';

export function ensureCompatDiRef(businessObject) {

  // bpmnElement can have multiple independent DIs
  if (!has(businessObject, 'di')) {
    Object.defineProperty(businessObject, 'di', {
      get: function() {
        throw new Error(DI_ERROR_MESSAGE);
      }
    });
  }
} 