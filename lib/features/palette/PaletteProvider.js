import {
  assign
} from 'min-dash';


/**
 * A palette provider for BPMN 2.0 elements.
 */
export default function PaletteProvider(
    palette, create, elementFactory,
    spaceTool, lassoTool, handTool,
    globalConnect, translate) {

  this._palette = palette;
  this._create = create;
  this._elementFactory = elementFactory;
  this._spaceTool = spaceTool;
  this._lassoTool = lassoTool;
  this._handTool = handTool;
  this._globalConnect = globalConnect;
  this._translate = translate;

  palette.registerProvider(this);
}

PaletteProvider.$inject = [
  'palette',
  'create',
  'elementFactory',
  'spaceTool',
  'lassoTool',
  'handTool',
  'globalConnect',
  'translate'
];


PaletteProvider.prototype.getPaletteEntries = function(element) {

  var actions = {},
      create = this._create,
      elementFactory = this._elementFactory,
      spaceTool = this._spaceTool,
      lassoTool = this._lassoTool,
      handTool = this._handTool,
      globalConnect = this._globalConnect,
      translate = this._translate;

  function createAction(type, group, className, title, options) {

    function createListener(event) {
      var shape = elementFactory.createShape(assign({
        type: type
      }, options));

      if (options) {
        var di = getDi(shape);
        di.isExpanded = options.isExpanded;
      }

      create.start(event, shape);
    }

    var shortType = type.replace(/^ifml:/, '');

    return {
      group: group,
      className: className,
      title: title || translate('Create {type}', {
        type: shortType
      }),
      action: {
        dragstart: createListener,
        click: createListener
      }
    };
  }

  assign(actions, {

    'space-tool': {
      group: 'tools',
      className: 'icon-space-tool',
      title: translate('Activate the create/remove space tool'),
      action: {
        click: function(event) {
          spaceTool.activateSelection(event);
        }
      }
    },
    'global-connect-tool': {
      group: 'tools',
      className: 'icon-connection-multi',
      title: translate('Activate the global connect tool'),
      action: {
        click: function(event) {
          globalConnect.toggle(event);
        }
      }
    },
    'tool-separator': {
      group: 'tools',
      separator: true
    },

    'ifml:ViewContainer': createAction(
      'ifml:ViewContainer', 'element', 'icon-viewcontainer',
      translate('Create ViewContainer')
    ),
    'ifml:ViewComponent': createAction(
      'ifml:ViewComponent', 'element', 'icon-viewcomponent',
      translate('Create ViewComponent')
    ),
    'ifml:Action': createAction(
      'ifml:Action', 'element', 'icon-action',
      translate('Create Action')
    ),
    'ifml:Event': createAction(
      'ifml:Event', 'element', 'icon-event',
      translate('Create Event')
    ),
    'tool-separator2': {
      group: 'modules',
      separator: true
    },
    'ifml:ModuleDefinition': createAction(
      'ifml:ModuleDefinition', 'modules', 'icon-module',
      translate('Create Module definition')
    ),
    'ifml:PortDefinition': createAction(
      'ifml:PortDefinition', 'modules', 'icon-port-out',
      translate('Create Port for a Module')
    ),
  });

  return actions;
};