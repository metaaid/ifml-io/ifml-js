import { getNewViewComponentPartPosition, isInnerViewComponentPart } from '../vcp-place/ViewComponentPartsPlacement';
import {
  getNewShapePosition
} from './IfmlAutoPlaceUtil';


/**
 * IFML auto-place behavior.
 *
 * @param {EventBus} eventBus
 */
export default function AutoPlace(eventBus, modeling) {
  eventBus.on('autoPlace', function(context) {
    var shape = context.shape,
        source = context.source;

    if (isInnerViewComponentPart(shape)) {
      var position = getNewViewComponentPartPosition(source, shape);
      context.shape.width = position.width;
      return position;
    }

    return getNewShapePosition(source, shape, modeling);
  });

  /**
   * Append shape to source at appropriate position.
   *
   * @param {djs.model.Shape} source
   * @param {djs.model.Shape} shape
   *
   * @return {djs.model.Shape} appended shape
   */
  this.insert = function(source, shape, hints) {

    eventBus.fire('autoPlace.start', {
      source: source,
      shape: shape
    });

    // allow others to provide the position
    var position = eventBus.fire('autoPlace', {
      source: source,
      shape: shape
    });

    var newShape = modeling.appendShape(source, shape, position, source, hints);

    eventBus.fire('autoPlace.end', {
      source: source,
      shape: newShape
    });

    return newShape;
  };
}

AutoPlace.$inject = [
  'eventBus',
  'modeling',
];