import AutoPlaceModule from 'diagram-js/lib/features/auto-place';

import IfmlAutoPlace from './IfmlAutoPlace';

export default {
  __depends__: [AutoPlaceModule],
  __init__: ['ifmlAutoPlace'],
  ifmlAutoPlace: ['type', IfmlAutoPlace]
};