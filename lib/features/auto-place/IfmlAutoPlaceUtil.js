import {
  is,
  getBusinessObject
} from '../../util/ModelUtil';

import {
  getMid,
  getOrientation,
  asTRBL
} from 'diagram-js/lib/layout/LayoutUtil';

import {
  find
} from 'min-dash';

import {
  generateGetNextPosition,
  getConnectedDistance
} from 'diagram-js/lib/features/auto-place/AutoPlaceUtil';
import {
  isAny
} from '../modeling/util/ModelingUtil';

export var VIEWCOMPONENT_PADDING_HORIZONTAL = 20;
export var VIEWCOMPONENT_PADDING_VERTICAL = 5;
export var VIEWCOMPONENT_PADDING_BOTTOM = 30;
export var VIEWCOMPONENT_MINWIDTH = 140;
export var SUBCOMPONENT_PADDING_HORIZONTAL = 10;
export var SUBCOMPONENT_PADDING_TOP = 20;


var PLACEMENT_DETECTION_PAD = 0;

export var DEFAULT_DISTANCE = 10;
/**
 * Find the new position for the target element to
 * connect to source.
 *
 * @param  {djs.model.Shape} source
 * @param  {djs.model.Shape} element
 *
 * @return {Point}
 */
export function getNewShapePosition(source, element, modeling) {

  if (isAny(element, ['ifml:ParameterBindingGroup'])) {
    return getNextUpperRightPosition(source, element, true);
  }
  if (isAny(element, ['ifml:Annotation', 'ifml:ActivationExpression'])) {
    return getNextUpperRightPosition(source, element, false);
  }

  if (is(element, 'ifml:ViewComponent')) {
    return getComponentPosition(source, element);
  }

  if (isAny(element, ['ifml:Event'])) {
    return getNextPostionClockwise(source, element);
  }

  if (isAny(element, ['ifml:PortDefinition'])) {
    var bo = getBusinessObject(element);
    var start = bo.get('direction') == "in" ? "left" : "right";
    return getNextPostionClockwise(source, element, start);
  }

  throw Error('Auto shape position for ' + element.type + ' in ' + source.type + ' not found');
}


/**
 * Always try to place element right of source;
 * compute actual distance from previous nodes in flow.
 */
export function getComponentPosition(source, element) {

  var sourceTrbl = asTRBL(source);
  var sourceMid = getMid(source);

  var horizontalDistance = getConnectedDistance(source, {
    filter: function (connection) {
      return is(connection, 'ifml:InteractionFlow');
    }
  });

  var margin = 30,
    minDistance = 80,
    orientation = 'left';

  var position = {
    x: sourceTrbl.right + horizontalDistance + element.width / 2,
    y: sourceMid.y + getVerticalDistance(orientation, minDistance)
  };

  var nextPositionDirection = {
    y: {
      margin: margin,
      minDistance: minDistance
    }
  };

  return findFreePosition(source, element, position, generateGetNextPosition(nextPositionDirection));
}


function getVerticalDistance(orientation, minDistance) {
  if (orientation.indexOf('top') != -1) {
    return -1 * minDistance;
  } else if (orientation.indexOf('bottom') != -1) {
    return minDistance;
  } else {
    return 0;
  }
}

function getMiddle(point1, point2) {
  return {
    x: point2.x + Math.round((point2.x - point1.x) / 2),
    y: point2.y + Math.round((point2.y - point1.y) / 2),
  };
}

function getConnectionMid(connection) {
  var lastX = connection.waypoints[0].x;
  var lastY = connection.waypoints[0].y;
  var length = 0;
  var waypointLengths = [];
  connection.waypoints.forEach(function (waypoint) {
    var x = waypoint.x - lastX;
    var y = waypoint.y - lastY;
    var l = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    length += l;
    waypointLengths.push(length);
    lastX = waypoint.x;
    lastY = waypoint.y;
  });
  var goalLength = Math.round(length / 2);

  for (var i in waypointLengths) {
    var l = waypointLengths[i];
    if (l > goalLength) {
      return getMiddle(connection.waypoints[i - 1], connection.waypoints[i]);
    }
  }
}

/**
 * Always try to place text annotations top right of source.
 */
export function getNextUpperRightPosition(source, element, sourceIsConnection) {
  var startPosition;

  if (sourceIsConnection) {
    startPosition = getConnectionMid(source);
    startPosition.y -= 50;
    startPosition.x -= element.width / 2;
  } else {
    var sourceTrbl = asTRBL(source);

    startPosition = {
      x: sourceTrbl.right + element.width / 2,
      y: sourceTrbl.top - 50 - element.height / 2
    };
  }


  var nextPositionDirection = {
    y: {
      margin: -30,
      minDistance: 20
    }
  };

  return findFreePosition(source, element, startPosition, generateGetNextPosition(nextPositionDirection));
}

/**
 * Always try to place text annotations top right of source.
 */
export function getNextPostionClockwise(source, element, start = "right") {
  var startPosition;

  var sourceTrbl = asTRBL(source);

  if (start == "left") {
    startPosition = {
      x: sourceTrbl.left,
      y: sourceTrbl.bottom - source.height / 2
    };
  } else {
    startPosition = {
      x: sourceTrbl.right,
      y: sourceTrbl.bottom - source.height / 2
    };
  }


  var nextPositionDirection = {
    y: {
      margin: 30,
      minDistance: element.height + 5
    }
  };

  var marign = 5;

  // var directions = {
  //   'margin': margin,
  //   'directions':
  //   'down': {
  //     y: {
  //       minDistance: element.height + 5
  //     }
  //   },
  //   'left'
  // };

  var directions = ["down", "left", "up", "right", "down"];
  var margin = 1;


  var generateGetNextPositionOnEdge = (margin, sourceTrbl) => {

    var direction = 0;

    let next = function (element, previousPosition, connectedAtPosition) {

      var nextPosition = {
        x: previousPosition.x,
        y: previousPosition.y
      };

      switch (directions[direction]) {
        case 'down':
          nextPosition.y += element.height + margin;
          if (nextPosition.y > sourceTrbl.bottom) {
            nextPosition.y = sourceTrbl.bottom;
            direction++;
          }
          return nextPosition;
        case 'left':
          nextPosition.x -= (element.width + margin);
          if (nextPosition.x < sourceTrbl.left) {
            nextPosition.x = sourceTrbl.left;
            direction++;
          }
          return nextPosition;
        case 'up':
          nextPosition.y -= (element.height + margin);
          if (nextPosition.y < sourceTrbl.top) {
            nextPosition.y = sourceTrbl.top;
            direction++;
          }
          return nextPosition;
        case 'right':
          nextPosition.x += element.width + margin;
          if (nextPosition.x > sourceTrbl.right) {
            nextPosition.x = sourceTrbl.right;
            direction++;
          }
          return nextPosition;

      }
      return null;
    }

    return next;
  }
  return findFreePosition(source, element, startPosition, generateGetNextPositionOnEdge(margin, sourceTrbl));
}


export function findFreePosition(source, element, position, getNextPosition) {
  var connectedAtPosition;

  while ((connectedAtPosition = getElementdAtPosition(source, position, element))) {
    position = getNextPosition(element, position, connectedAtPosition);
  }

  return position;
}


function getAutoPlaceClosure(source) {

  var allConnected = getConnected(source);

  if (source.host) {
    allConnected = allConnected.concat(getConnected(source.host));
  }

  if (source.attachers) {
    allConnected = allConnected.concat(source.attachers.reduce(function (shapes, attacher) {
      return shapes.concat(getConnected(attacher));
    }, []));
  }

  return allConnected;
}

export function getElementdAtPosition(source, position, element) {

  var bounds = {
    x: position.x - (element.width / 2),
    y: position.y - (element.height / 2),
    width: element.width,
    height: element.height
  };

  var closure = getAutoPlaceClosure(source, element);
  closure = closure.concat(source.children);

  return find(closure, function (target) {

    if (target === element) {
      return false;
    }

    var orientation = getOrientation(target, bounds, PLACEMENT_DETECTION_PAD);

    return orientation === 'intersect';
  });
}

function getConnected(element) {
  return getTargets(element).concat(getSources(element));
}

function getSources(shape) {
  return shape.incoming.map(function (connection) {
    return connection.source;
  });
}

function getTargets(shape) {
  return shape.outgoing.map(function (connection) {
    return connection.target;
  });
}