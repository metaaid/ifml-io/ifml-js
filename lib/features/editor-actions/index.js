import EditorActionsModule from 'diagram-js/lib/features/editor-actions';

import IfmlEditorActions from './IfmlEditorActions';

export default {
  __depends__: [
    EditorActionsModule
  ],
  editorActions: [ 'type', IfmlEditorActions ]
};
