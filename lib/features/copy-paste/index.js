import CopyPasteModule from "diagram-js/lib/features/copy-paste";

import IfmlCopyPaste from "./IfmlCopyPaste";
import ModdleCopy from "./ModdleCopy";

export default {
  __depends__: [CopyPasteModule],
  __init__: ["IfmlCopyPaste", "moddleCopy"],
  IfmlCopyPaste: ["type", IfmlCopyPaste],
  moddleCopy: ["type", ModdleCopy],
};
