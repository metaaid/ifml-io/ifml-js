import IfmlConnectSnapping from './IfmlConnectSnapping';
import BpmnCreateMoveSnapping from './BpmnCreateMoveSnapping';
import IfmlEventEdgeSnapping from './IfmlEventEdgeSnapping';
import SnappingModule from 'diagram-js/lib/features/snapping';

export default {
  __depends__: [SnappingModule],
  __init__: [
    'connectSnapping',
    'eventSnapping',
    'createMoveSnapping',
  ],
  connectSnapping: ['type', IfmlConnectSnapping],
  eventSnapping: ['type', IfmlEventEdgeSnapping],
  createMoveSnapping: ['type', BpmnCreateMoveSnapping]
};