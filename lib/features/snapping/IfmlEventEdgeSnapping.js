import {
  bottomLeft,
  isSnapped,
  mid,
  setSnapped
} from 'diagram-js/lib/features/snapping/SnapUtil';

import {
  isCmd
} from 'diagram-js/lib/features/keyboard/KeyboardUtil';

import {
  asTRBL,
  getMid,
  getOrientation
} from 'diagram-js/lib/layout/LayoutUtil';

import {
  is
} from '../../util/ModelUtil';

import {
  isAny
} from '../modeling/util/ModelingUtil';

import {
  forEach,
  some
} from 'min-dash';
import CreateMoveSnapping from 'diagram-js/lib/features/snapping/CreateMoveSnapping';

var LOW_PRIORITY = 500;

var BOUNDARY_TO_HOST_THRESHOLD = 40;

var TARGET_BOUNDS_PADDING = 20,
  TASK_BOUNDS_PADDING = 10;

var TARGET_CENTER_PADDING = 20;

var AXES = ['x', 'y'];

var abs = Math.abs;

/**
 * Snap during connect.
 *
 * @param {EventBus} eventBus
 * @param {Injector} injector
 */
export default function IfmlEventEdgeSnapping(eventBus, injector) {
  injector.invoke(CreateMoveSnapping, this);
  eventBus.on([

    // 'shape.move.hover',
    // 'create.move.hover',
    // 'create.hover',
    'create.move',
    'create.end',
    'shape.move.move',
    'shape.move.end'
  ], 1500, function (event) {


    var context = event.context,
      target = context.target,
      shape = context.shape;

    // console.log("c", target, !isSnapped(event));
    if (target) { // && !isSnapped(event) && canAttach(shape, target, event)
      // console.log("SNAPM", !isSnapped(event));
      // snapEvent(event, shape, target);
    }

    // var context = event.context,
    //   canExecute = context.canExecute,
    //   shape = context.shape,
    //   hover = event.hover,
    //   element = shape.businessObject;

    // if (!is(element, "ifml:Event")) {
    //   return;
    // }
    // // do NOT snap on CMD
    // if (event.originalEvent && isCmd(event.originalEvent)) {
    //   return;
    // }

    // // snap hover
    // if (canExecute && hover && is(element, 'ifml:Event')) {
    //   console.log("SNAPM", element, hover);
    //   snapToPosition(event, {
    //     x: 1,
    //     y: 1
    //   });
    //   // snapBoundaryEventLoop(event, shape, hover)
    // }
  });

  function snapEvent(event, shape, target) {
    var targetTRBL = asTRBL(target);

    var direction = getEventAttachment(event, target);

    if (/top/.test(direction)) {
      setSnapped(event, 'y', targetTRBL.top);
    } else
    if (/bottom/.test(direction)) {
      setSnapped(event, 'y', targetTRBL.bottom);
    }

    if (/left/.test(direction)) {
      setSnapped(event, 'x', targetTRBL.left);
    } else
    if (/right/.test(direction)) {
      setSnapped(event, 'x', targetTRBL.right);
    }
  }

  function getEventAttachment(position, target) {

    var orientation;

    // var definition = getDefinition(target);

    // if (is(target, 'cmmn:PlanItem') && is(definition, 'cmmn:Stage')) {
    //   orientation = getCornerlessOrientation(position, target, -15, 20);

    // } else
    // if (is(definition, 'cmmn:Milestone')) {
    //   orientation = getCornerlessOrientation(position, target, -3, 7);

    // } else {
    //   orientation = getOrientation(position, target, -15);
    // }

    // orientation = getOrientation(position, target, -15);
    orientation = getCornerlessOrientation(position, target, -15, 20);

    if (orientation !== 'intersect') {
      return orientation;
    } else {
      return null;
    }
  }

  function getCornerlessOrientation(position, target, padding, offset) {

    // don't snap to top left corner
    if (position.x - offset < target.x &&
      position.y - offset < target.y) {
      return 'corner';
    }

    // don't snap to top right corner
    if (position.x + offset > target.x + target.width &&
      position.y - offset < target.y) {
      return 'corner';
    }

    // don't snap to bottom left corner
    if (position.x - offset < target.x &&
      position.y + offset > target.y + target.height) {
      return 'corner';
    }

    // don't snap to bottom right corner
    if (position.x + offset > target.x + target.width &&
      position.y + offset > target.y + target.height) {
      return 'corner';
    }

    return getOrientation(position, target, padding);
  }

}

IfmlEventEdgeSnapping.$inject = [
  'eventBus',
  'injector',
];


IfmlEventEdgeSnapping.prototype.initSnap = function (event) {
  var snapContext = CreateMoveSnapping.prototype.initSnap.call(this, event);

  var shape = event.shape;

  // snap to docking points
  forEach(shape.outgoing, function (connection) {
    var docking = connection.waypoints[0];

    docking = docking.original || docking;

    snapContext.setSnapOrigin(connection.$id + '-docking', {
      x: docking.x - event.x,
      y: docking.y - event.y
    });
  });

  forEach(shape.incoming, function (connection) {
    var docking = connection.waypoints[connection.waypoints.length - 1];

    docking = docking.original || docking;

    snapContext.setSnapOrigin(connection.$id + '-docking', {
      x: docking.x - event.x,
      y: docking.y - event.y
    });
  });

  return snapContext;
};


IfmlEventEdgeSnapping.prototype.addSnapTargetPoints = function (snapPoints, shape, target) {
  CreateMoveSnapping.prototype.addSnapTargetPoints.call(this, snapPoints, shape, target);

  var snapTargets = this.getSnapTargets(shape, target);

  // snap to docking points
  forEach(shape.incoming, function (connection) {

    if (!includes(snapTargets, connection.source)) {
      snapPoints.add('mid', getMid(connection.source));
    }

    var docking = connection.waypoints[0];

    snapPoints.add(connection.$id + '-docking', docking.original || docking);
  });


  forEach(shape.outgoing, function (connection) {

    if (!includes(snapTargets, connection.target)) {
      snapPoints.add('mid', getMid(connection.target));
    }

    var docking = connection.waypoints[connection.waypoints.length - 1];

    snapPoints.add(connection.$id + '-docking', docking.original || docking);
  });

  return snapPoints;
};


// helpers //////////

// snap to target if event in target
function snapToShape(event, target, padding) {
  AXES.forEach(function (axis) {
    var dimensionForAxis = getDimensionForAxis(axis, target);

    if (event[axis] < target[axis] + padding) {
      setSnapped(event, axis, target[axis] + padding);
    } else if (event[axis] > target[axis] + dimensionForAxis - padding) {
      setSnapped(event, axis, target[axis] + dimensionForAxis - padding);
    }
  });
}

// snap to target mid if event in target mid
function snapToTargetMid(event, target) {
  var targetMid = mid(target);

  AXES.forEach(function (axis) {
    if (isMid(event, target, axis)) {
      setSnapped(event, axis, targetMid[axis]);
    }
  });
}

// snap to prevent loop overlapping boundary event
function snapBoundaryEventLoop(event, source, target) {
  var context = event.context;

  if (isReverse(context)) {
    return;
  }

  var sourceMid = mid(source),
    orientation = getOrientation(sourceMid, target, -10),
    axes = [];

  // if (/top|bottom/.test(orientation)) {
  axes.push('x');

  // }

  // if (/left|right/.test(orientation)) {
  axes.push('y');

  // }

  axes.forEach(function (axis) {
    var coordinate = event[axis],
      newCoordinate;

    if (abs(coordinate - sourceMid[axis]) < BOUNDARY_TO_HOST_THRESHOLD) {
      if (coordinate > sourceMid[axis]) {
        newCoordinate = sourceMid[axis] + BOUNDARY_TO_HOST_THRESHOLD;
      } else {
        newCoordinate = sourceMid[axis] - BOUNDARY_TO_HOST_THRESHOLD;
      }

      setSnapped(event, axis, newCoordinate);
    }
  });
}

function snapToPosition(event, position) {
  setSnapped(event, 'x', position.x);
  setSnapped(event, 'y', position.y);
}

function isType(attrs, type) {
  return attrs && attrs.type === type;
}

function isAnyType(attrs, types) {
  return some(types, function (type) {
    return isType(attrs, type);
  });
}

function getDimensionForAxis(axis, element) {
  return axis === 'x' ? element.width : element.height;
}

function getTargetBoundsPadding(target) {
  if (is(target, 'bpmn:Task')) {
    return TASK_BOUNDS_PADDING;
  } else {
    return TARGET_BOUNDS_PADDING;
  }
}

function isMid(event, target, axis) {
  return event[axis] > target[axis] + TARGET_CENTER_PADDING &&
    event[axis] < target[axis] + getDimensionForAxis(axis, target) - TARGET_CENTER_PADDING;
}

function isReverse(context) {
  var hover = context.hover,
    source = context.source;

  return hover && source && hover === source;
}

function includes(array, value) {
  return array.indexOf(value) !== -1;
}