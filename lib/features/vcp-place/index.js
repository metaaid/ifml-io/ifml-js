import ViewComponentPartsPlacement from './ViewComponentPartsPlacement';


export default {
  __init__: [
    'vcpPlacements'
  ],
  vcpPlacements: ['type', ViewComponentPartsPlacement]
};