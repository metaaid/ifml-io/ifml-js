import AutoResize from 'diagram-js/lib/features/auto-resize/AutoResize';

import {
  isSnapped,
  setSnapped,
} from 'diagram-js/lib/features/snapping/SnapUtil';

import { transform as svgTransform } from 'tiny-svg';

import { query as domQuery } from 'min-dom';

import inherits from 'inherits';

import { is } from '../../util/ModelUtil';

import { isAny } from '../modeling/util/ModelingUtil';

export var VIEWCOMPONENT_DEFAULTHEIGHT = 80;
export var VIEWCOMPONENT_PADDING_TOP = 40;
export var VIEWCOMPONENT_PADDING_HORIZONTAL = 20;
export var VIEWCOMPONENT_PADDING_BOTTOM = 30;

export var VIEWCOMPONENT_MARGIN_VERTICAL = 5;

export var SUBCOMPONENT_OPTIONAL_PADDING_TOP = 30;
export var SUBCOMPONENT_OPTIONAL_PADDING_HORIZONTAL = 10;
export var SUBCOMPONENT_OPTIONAL_PADDINB_BOTTOM = 10;
export var SUBCOMPONENT_OPTIONAL_MARGIN_VERTICAL = 10;
export var SUBCOMPONENT_DEFAULTHEIGHT = 30;

var LOW_PRIORITY = 500;
var HIGH_PRIORITY = 2000;

var getChildren = function (gfx) {
  if (!gfx.children[1]) {
    return [];
  }
  var children = gfx.children[1].children;

  return Array.from(children)
    .map(function (child) {
      return child.children[0];
    })
    .filter(function (child) {
      return child.classList.contains('innerViewComponentPart');
    });
};

var getSibblings = function (gfx) {
  return getChildren(gfx.parentElement);
};

export function isInnerViewComponentPart(shape) {
  return (
    isAny(shape, ['ifml:ViewComponentPart', 'ifml:Parameter']) &&
    !isAny(shape, ['ifml:Expression'])
  );
}

function calculateHeight(shape) {
  const lastChild = lastInnerViewComponentPart(shape);

  if (lastChild) {
    var paddingBot;
    const top = shape.y;

    if (isInnerViewComponentPart(shape)) {
      paddingBot = SUBCOMPONENT_OPTIONAL_MARGIN_VERTICAL;
    } else {
      paddingBot = VIEWCOMPONENT_PADDING_BOTTOM;
    }
    var lowestBot = lastChild.y + lastChild.height;
    return lowestBot - top + paddingBot;
  } else {
    if (is(shape, 'ifml:ViewComponent')) {
      return VIEWCOMPONENT_DEFAULTHEIGHT;
    } else {
      return SUBCOMPONENT_DEFAULTHEIGHT;
    }
  }
}

function fixPositionForVCP(element, modeling, force = false) {
  var makeSmaller,
    elWidth,
    elHeight,
    elLeft,
    elTop,
    paddingLeft,
    paddingBot,
    paddingTop;

  var lastChild = lastInnerViewComponentPart(element);

  if (is(element, 'ifml:ViewComponent')) {
    // if the element is a view component, we just adjust the height if needed
    elWidth = element.width;
    paddingBot = VIEWCOMPONENT_PADDING_BOTTOM;
  } else if (is(element.parent, 'ifml:ViewComponent')) {
    // if the element is a view component part and inside a view component, we adjust the positioning
    elWidth = element.parent.width - 2 * VIEWCOMPONENT_PADDING_HORIZONTAL;
    paddingTop = VIEWCOMPONENT_PADDING_TOP;
    paddingLeft = VIEWCOMPONENT_PADDING_HORIZONTAL;
  } else {
    // if the element is a view component part and inside a view component part, we also adjust the positioning
    elWidth =
      element.parent.width - 2 * SUBCOMPONENT_OPTIONAL_PADDING_HORIZONTAL;
    paddingLeft = SUBCOMPONENT_OPTIONAL_PADDING_HORIZONTAL;
    paddingTop = SUBCOMPONENT_OPTIONAL_PADDING_TOP;
  }

  if (paddingLeft || paddingTop) {
    elLeft = element.parent.x + paddingLeft;
    paddingBot = SUBCOMPONENT_OPTIONAL_PADDINB_BOTTOM;

    // check if there is a element above the current one, else use the default padding
    var elementAbove = getElementAbove(element);
    if (elementAbove) {
      elTop =
        elementAbove.y +
        elementAbove.height +
        SUBCOMPONENT_OPTIONAL_MARGIN_VERTICAL;
    } else {
      elTop = element.parent.y + paddingTop;
    }
  } else {
    elLeft = element.x;
    elTop = element.y;
  }

  if (lastChild) {
    var lowestBot = lastChild.y + lastChild.height;
    elHeight = lowestBot - element.y + paddingBot;
  } else {
    if (is(element, 'ifml:ViewComponent')) {
      elHeight = VIEWCOMPONENT_DEFAULTHEIGHT;
    } else {
      elHeight = SUBCOMPONENT_DEFAULTHEIGHT;
    }
  }

  makeSmaller = isInnerViewComponentPart(element);

  if (
    force ||
    ((element.height != elHeight ||
      element.width != elWidth ||
      element.x != elLeft ||
      element.y != elTop) &&
      (element.height < elHeight || makeSmaller))
  ) {
    modeling.resizeShape(element, {
      x: elLeft,
      y: elTop,
      width: elWidth,
      height: elHeight,
    });
  }
}

/**
 * Sub class of the AutoResize module which implements a IFML
 * specific resize function.
 */
export default function ViewComponentPartsPlacement(
  injector,
  modeling,
  eventBus,
  elementRegistry
) {
  injector.invoke(AutoResize, this);

  // save all the isInnerViewComponentPart sahpe positions and remove the current element
  eventBus.on('shape.move.start', HIGH_PRIORITY, function (event) {
    var currentShape = event.shape;
    if (isInnerViewComponentPart(currentShape)) {
      var context = event.context;
      context.oldParent = currentShape.parent;
    }
  });
  // resize view component if a inner view component part is added, and the view component is to small
  this.postExecuted('shape.create', LOW_PRIORITY, function (event) {
    var context = event.context;
    const shape = event.shape || context.shape;
    if (isInnerViewComponentPart(shape)) {
      fixPositionForVCP(shape, modeling, true);
    }
  });
  this.postExecuted('shape.resize', LOW_PRIORITY, function (event) {
    var context = event.context;
    const shape = event.shape || context.shape;

    if (isInnerViewComponentPart(shape)) {
      // get last child
      fixPositionForVCP(shape.parent, modeling);
      var nextSibling = getElementBelow(shape);

      if (nextSibling) {
        // TODO: optimize that the parent is only "fixed" once
        fixPositionForVCP(nextSibling, modeling);
      }
      // resize all children
      var children = shape.children.filter(function (child) {
        return isInnerViewComponentPart(child);
      });
      children.forEach(function (child) {
        fixPositionForVCP(child, modeling);
      });
    }
  });

  eventBus.on('shape.move.end', LOW_PRIORITY, function (event) {
    const currentShape = event.shape;
    if (isInnerViewComponentPart(currentShape)) {
      const context = event.context;
      const parentElement = currentShape.parent;
      if (isInnerViewComponentPart(context.oldParent)) {
        fixPositionForVCP(context.oldParent, modeling);
      }
      rearangeViewComponentParts(parentElement, modeling, currentShape);
      // update width
      fixPositionForVCP(currentShape, modeling);
    }
  });

  eventBus.on('shape.move.move', HIGH_PRIORITY, function (event) {
    const shape = event.shape;

    if (isInnerViewComponentPart(shape)) {
      var target = event.context.target;

      if (
        target &&
        !isSnapped(event) &&
        (isAny(target, ['ifml:ViewComponent']) ||
          isInnerViewComponentPart(target))
      ) {
        var viewContainer;
        if (isAny(target, ['ifml:ViewComponent'])) {
          viewContainer = target;
        } else {
          viewContainer = target.parent;
        }
        var containerGFX = elementRegistry.getGraphics(viewContainer);
        var itemGFX = elementRegistry.getGraphics(shape);

        var x = viewContainer.x + VIEWCOMPONENT_PADDING_HORIZONTAL;
        var startY = viewContainer.y + VIEWCOMPONENT_PADDING_TOP;

        setSnapped(event, 'x', x + shape.width / 2);

        rearangeGFX(
          containerGFX,
          itemGFX,
          startY,
          VIEWCOMPONENT_MARGIN_VERTICAL,
          getSibblings,
          event
        );
      }
    }
  });

  this.postExecuted(['shape.resize'], HIGH_PRIORITY, function (event) {
    var context = event.context,
      shape = context.shape;

    if (isAny(shape, ['ifml:ViewComponent'])) {
      // update children width
      shape.children.forEach(function (child) {
        var childIsInnerViewComponentPart = isInnerViewComponentPart(child);
        var isSub = false;

        if (childIsInnerViewComponentPart) {
          var paddingH = isSub
            ? SUBCOMPONENT_OPTIONAL_PADDING_HORIZONTAL
            : VIEWCOMPONENT_PADDING_HORIZONTAL;
          var fixedWidth = shape.width - paddingH * 2;

          modeling.resizeShape(child, {
            x: shape.x + paddingH,
            y: child.y,
            width: fixedWidth,
            height: child.height,
          });
        }
      });
    }
  });
}

function lastInnerViewComponentPart(shape, ignoreElement) {
  for (var i = shape.children.length - 1; i >= 0; i--) {
    if (
      isInnerViewComponentPart(shape.children[i]) &&
      shape.children[i] !== ignoreElement
    ) {
      return shape.children[i];
    }
  }
  return null;
}

function getElementAbove(shape) {
  var parent = shape.parent;
  var children = parent.children.filter(function (child) {
    return isInnerViewComponentPart(child);
  });

  var index = children.indexOf(shape);
  if (index > 0) {
    return parent.children[index - 1];
  }
  return null;
}

function getElementBelow(shape) {
  var parent = shape.parent;
  var children = parent.children.filter(function (child) {
    return isInnerViewComponentPart(child);
  });

  var index = children.indexOf(shape);
  if (index < children.length - 1) {
    return parent.children[index + 1];
  }
  return null;
}

export function getNewViewComponentPartPosition(container, ignoreElement) {
  var parentIsViewComponent = is(container, 'ifml:ViewComponent');
  var top = container.y;
  var lastChild = lastInnerViewComponentPart(container, ignoreElement);
  if (lastChild) {
    top = lastChild.y + (lastChild.height || 0);
    top += VIEWCOMPONENT_MARGIN_VERTICAL;
  } else {
    top += parentIsViewComponent
      ? VIEWCOMPONENT_PADDING_TOP
      : SUBCOMPONENT_OPTIONAL_PADDING_TOP;
  }

  var hPadding = parentIsViewComponent
    ? VIEWCOMPONENT_PADDING_HORIZONTAL
    : SUBCOMPONENT_OPTIONAL_PADDING_HORIZONTAL;

  var position = {
    x: container.x + hPadding,
    y: top,
    width: container.width - hPadding * 2,
  };
  return position;
}

function getPositionInViewComponent(shape, viewComponent) {
  var children = viewComponent.children;
  for (var i = 0; i < children.length; i++) {
    if (isInnerViewComponentPart(children[i]) && children[i].y > shape.y) {
      return i;
    }
  }
  return i - 1;
}

export function getYPositionFromGFX(gfx) {
  var matrix = svgTransform(gfx).matrix;
  return matrix.f;
}

function getPositionFromYInGfxChildren(elementY, children) {
  for (var i = 0; i < children.length; i++) {
    if (getYPositionFromGFX(children[i]) > elementY) {
      return i;
    }
  }
  return i;
}
export function getPositionInGfx(element, parentElement) {
  var y = getYPositionFromGFX(element);
  var children = getChildren(parentElement.parentElement);
  // remove element from children
  var elementIndex = children.indexOf(element);
  if (elementIndex >= 0) {
    children.splice(elementIndex, 1);
  }

  return getPositionFromYInGfxChildren(y, children);
}

function rearangeGFX(gfx, element, startY, margin, getSibblings, event) {
  var children = getSibblings(gfx);
  var childrenWithoutElement = children.filter(function (child) {
    return child != element;
  });
  var position = getPositionFromYInGfxChildren(event.y, childrenWithoutElement);
  var elementIndex = children.indexOf(element);
  if (elementIndex >= 0) {
    children.splice(elementIndex, 1);
    children.splice(position, 0, element);
  } else {
    children.splice(position, 0, element);
  }

  children.forEach(function (child) {
    var matrix = svgTransform(child).matrix;

    if (startY != matrix.f) {
      matrix.f = startY;
    }
    startY += child.children[0].getBBox().height;
    startY += margin;
  });

  var containerMatrix = svgTransform(gfx).matrix;
  var containerY = containerMatrix.f;
  if (containerY < startY) {
    gfx.y = startY;
    var rectVis = gfx.children.item(0).children.item(0);
    rectVis.setAttribute('height', startY - containerY + 10);
  }
}

function rearangeViewComponentParts(parent, modeling, movedShape) {
  var parentIsViewComponent = is(parent, 'ifml:ViewComponent');
  var paddingTop = parentIsViewComponent
    ? VIEWCOMPONENT_PADDING_TOP
    : SUBCOMPONENT_OPTIONAL_PADDING_TOP;
  var currentY = parent.y + paddingTop;

  var index = 0;
  parent.children.forEach(function (shape) {
    if (isInnerViewComponentPart(shape)) {
      if (shape.y != currentY || shape == movedShape) {
        shape.y = currentY;
        modeling.moveShape(shape, { x: 0, y: 0 }, parent, index);
      }
      if (shape.children.length > 0) {
        rearangeViewComponentParts(shape, modeling);
      }
      currentY += shape.height + VIEWCOMPONENT_MARGIN_VERTICAL;
    }
    index++;
  });
}

ViewComponentPartsPlacement.$inject = [
  'injector',
  'modeling',
  'eventBus',
  'elementRegistry',
];

inherits(ViewComponentPartsPlacement, AutoResize);
