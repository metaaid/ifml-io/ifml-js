import { is } from '../../util/ModelUtil';

import {
  domify,
  query as domQuery,
  classes as domClasses,
  delegate as domDelegate,
} from 'min-dom';


/**
 * Displays overlays that can be clicked in order to drill
 * down into a DMN element.
 */
export default class DomainModelInjector {

  constructor(injector, eventBus, canvas, overlays, config, elementRegistry) {
    this._injector = injector;
    this._eventBus = eventBus;
    this._overlays = overlays;
    this._canvas  = canvas ;
    this._elementRegistry  = elementRegistry ;

    this._config = config || [];

    
    this.HTML_BUTTON = '<div class="editor-button edit-domainModel">Domainmodel</div>';
    // TODO: inject mof2txt for xmi parsing
    this.HTML_OVERLAY = `
      <div class="domainEditor">
        <div class="editor-button close">X</div>
        <textarea class="json"></textarea>
        <div class="editor-button save">Save</div>
      </div>`;

    var parent = canvas.getContainer(),
        button = domify(this.HTML_BUTTON),
        overlay = domify(this.HTML_OVERLAY);

    parent.appendChild(button);
    parent.appendChild(overlay);

    // bind button to toggle hide class for the overlay
    domDelegate.bind(parent, '.edit-domainModel', 'click', (event) => {
      domClasses(overlay).toggle('open');
    });
    domDelegate.bind(parent, '.close', 'click', (event) => {
      domClasses(overlay).toggle('open');
    });
    domDelegate.bind(parent, '.save', 'click', (event) => {
      this.saveDomainModel();
    });

    this.updateTextArea();
  }

  saveDomainModel(text) {
    var parent = this._canvas.getContainer(),
        textarea = domQuery('.domainEditor .json', parent);

    this._config = JSON.parse(textarea.value);
    this._eventBus.fire("domainModel.changed", {config: this._config});
  }
  updateTextArea(text) {
    var parent = this._canvas.getContainer(),
        textarea = domQuery('.domainEditor .json', parent);

    
    textarea.value = JSON.stringify(this._config, null, 3);
  }
  
  /**
   * Add overlay to an element that enables drill down.
   *
   * @param {Object} element Element to add overlay to.
   * @param {string} className
   *        CSS class that will be added to overlay in order to display icon.
   */
  addOverlay(element, className) {
    const html = domify(`
      <div class="domainEditor">
      HU ?
      </div>
    `);

    const overlayId = this._overlays.add(element, {
      position: {
        top: 2,
        left: 2
      },
      html
    });

    // TODO(nikku): can we remove renamed to drillDown.enabled
    if (this._config.enabled !== false) {
      domClasses(html).add('interactive');

      this.bindEventListener(element, html, overlayId);
    }
  }

  /**
   * @param {Object} element
   * @param {string} id
   */
  bindEventListener(element, id) {
    const overlays = this._overlays,
          eventBus = this._eventBus;
    const html = domify(`
      <div class="domainEditor">
        HUH ?
      </div>
    `);


    const overlaysRoot = overlays._overlayRoot;

    // this.addOverlay(this.canvas, element, "dmn-icon-drd");
    this._overlays.add(this._elementRegistry.get('_myModel'), {
      position: {
        top: 2,
        left: 2
      },
      html
    });


    domDelegate.bind(overlaysRoot, '[data-overlay-id="' + id + '"]', 'click', () => {
      
      const triggerDefault = eventBus.fire('drillDown.click', {
        element
      });

      if (triggerDefault === false) {
        return;
      }

      this.drillDown(element);
    });
  }


  /**
   * Drill down into the specific element.
   *
   * @param  {djs.model.Base} element
   *
   * @return {boolean} whether drill down was executed
   */
  drillDown(element) {

    const parent = this._injector.get('_parent', false);

    // no parent; skip drill down
    if (!parent) {
      return false;
    }

    const view = parent.getView(element.businessObject);

    // no view to drill down to
    if (!view) {
      return false;
    }

    parent.open(view);

    return true;
  }

}

// DomainModelInjector.prototype._init = function() {
//   var canvas = this._canvas,
//       eventBus = this._eventBus;

//   var parent = canvas.getContainer(),
//       container = this._container = domify(this.HTML_MARKUP);

//   parent.appendChild(container);

//   this.nameElement = domQuery('.dmn-definitions-name', this._container);
//   this.idElement = domQuery('.dmn-definitions-id', this._container);

//   domClasses(html).add('interactive');

//   this.bindEventListener(container, "domainEdit");

//   // domDelegate.bind(
//   //   container,
//   //   '.dmn-definitions-name, .dmn-definitions-id', 'mousedown',
//   //   function(event) {
//   //     event.stopPropagation();
//   //   }
//   // );

//   // eventBus.fire('definitionIdView.create', {
//   //   html: container
//   // });
// };



DomainModelInjector.$inject = [
  'injector',
  'eventBus',
  'canvas',
  'overlays',
  'config.moddleExtensions',
  'elementRegistry',
];