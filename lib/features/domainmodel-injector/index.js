import OverlaysModule from 'diagram-js/lib/features/overlays';

import DomainModelInjector from './DomainModelInjector';

export default {
  __depends__: [
    OverlaysModule
  ],
  __init__: [ 'domainModelInjector' ],
  domainModelInjector: [ 'type', DomainModelInjector ]
};
