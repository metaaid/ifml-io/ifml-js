import PreviewSupportModule from 'diagram-js/lib/features/preview-support';

import IfmlReplacePreview from './IfmlReplacePreview';

export default {
  __depends__: [
    PreviewSupportModule
  ],
  __init__: ['ifmlReplacePreview'],
  ifmlReplacePreview: ['type', IfmlReplacePreview]
};
