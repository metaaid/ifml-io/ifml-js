import BehaviorModule from './behavior';
import RulesModule from '../rules';
import OrderingModule from '../ordering';
import ReplaceModule from '../replace';

import CommandModule from 'diagram-js/lib/command';
import TooltipsModule from 'diagram-js/lib/features/tooltips';
import LabelSupportModule from 'diagram-js/lib/features/label-support';
import AttachSupportModule from 'diagram-js/lib/features/attach-support';
import SelectionModule from 'diagram-js/lib/features/selection';
import ChangeSupportModule from 'diagram-js/lib/features/change-support';
import SpaceToolModule from 'diagram-js/lib/features/space-tool';

import IfmlFactory from './IfmlFactory';
import IfmlUpdater from './IfmlUpdater';
import ElementFactory from './ElementFactory';
import Modeling from './Modeling';
import IfmlLayouter from './IfmlLayouter';
import DomainModelProvider from './DomainModelProvider';
import CroppingConnectionDocking from 'diagram-js/lib/layout/CroppingConnectionDocking';

export default {
  __init__: [
    'modeling',
    'ifmlUpdater',
    'domainModelProvider',
  ],
  __depends__: [
    BehaviorModule,
    RulesModule,
    OrderingModule,
    ReplaceModule,
    CommandModule,
    TooltipsModule,
    LabelSupportModule,
    AttachSupportModule,
    SelectionModule,
    ChangeSupportModule,
    SpaceToolModule
  ],
  factory: ['type', IfmlFactory],
  ifmlUpdater: ['type', IfmlUpdater],
  elementFactory: ['type', ElementFactory],
  modeling: ['type', Modeling],
  layouter: ['type', IfmlLayouter],
  connectionDocking: ['type', CroppingConnectionDocking],
  domainModelProvider: ['type', DomainModelProvider],
};