import {
  map
} from 'min-dash';

class Type{
  constructor(name, prefix, type = "uml:Classifier", properties = [], id = null){
    this.name = name;
    this.type = type;
    this.properties = properties;
    this.prefix = prefix;
    this.xmiId = id;
  }
  // create from array
  static fromArray(typeArray, packageDesc){
    packageDesc["prefix"] = packageDesc["prefix"] ? packageDesc["prefix"] : "default";
    typeArray["type"] = "type" in typeArray ? typeArray["type"] : "uml:Classifier";
    typeArray["properties"] = "properties" in typeArray ? typeArray["properties"] : [];
    typeArray["id"] = "id" in typeArray ? typeArray["id"] : null;
  
    return new Type(typeArray["name"], packageDesc["prefix"], typeArray["type"], typeArray["properties"], typeArray["id"]);
  }

  get fullName(){
    return this.prefix + ":" + this.name;
  }
}

export default function DomainModelProvider(factory, moddle, config, canvas, eventBus) {
  // BaseElementFactory.call(this);

  this._factory = factory;
  this._moddle = moddle;
  this._canvas = canvas;
  this.domainTypes = {};
  this._config = config || [];

  this.updateFromConfig();

  eventBus.on('domainModel.changed', (configEvent) => {
    this._config = configEvent["config"];
    this.updateFromConfig();
  });
}

DomainModelProvider.prototype.updateFromConfig = function () {
  this.domainTypes = {};
  this._config.forEach(packageDesc => {
    packageDesc["types"].forEach(type => {
      this.addTypeFromPackage(type, packageDesc);
    });
  });
};

DomainModelProvider.prototype.addTypeFromPackage = function (type, packageDesc) {
  var type = Type.fromArray(type, packageDesc);
  this.addType(type);
};

DomainModelProvider.prototype.addType = function (type) {

  this.domainTypes[type.fullName] = type;
};

DomainModelProvider.prototype.getTypes = function () {
  return this.domainTypes;
};

DomainModelProvider.prototype.getTypeNames = function () {
  return map(this.domainTypes, function (t) {
    return t.name;
  });
};

DomainModelProvider.prototype.createTypeByURI = function (uri) {
  return this.getClassifierFromUri(uri);
};

DomainModelProvider.prototype.createStructuralFeatureLink = function (classifier, name) {
  var structuralFeature = this._factory.create('ifml:UMLStructuralFeature');
  var features = this.getStructuralFeatures(classifier);

  for (var i in features) {
    if (features[i].name == name) {
      structuralFeature.structuralFeature = features[i];
    }
  }

  return structuralFeature;
};

DomainModelProvider.prototype.getStructuralFeatures = function (classifier) {
  if (!classifier || !classifier.name) {
    return [];
  }
  classifier = this.getClassifierFromUri(classifier.name);
  return classifier && classifier.ownedAttribute ? classifier.ownedAttribute : [];
};

DomainModelProvider.prototype.getClassifierFromUri = function (uri) {
  var type = this.domainTypes[uri];
  if (typeof type == 'undefined') {
    try {
      return this._moddle.registry.getEffectiveDescriptor("primitivetypes:" + uri);
    } catch (ex) {
      return uri;
    }
  }
  var classifier = this._factory.create(type["fullName"]);
  // overwrite Type if it's not specified in the json
  if (!classifier.$descriptor.allTypes[0].type) {
    classifier.$descriptor.allTypes[0].type = 'uml:Classifier';
  }
  return classifier.$descriptor;
};

DomainModelProvider.prototype.getFieldByType = function (typeString) {
  var connectionField = null;
  switch (typeString) {
    case 'UMLDomainConcept':
      connectionField = "classifier";
      break;
    case 'UMLBehavior':
      connectionField = "behavior";
      break;
    case 'UMLBehavioralFeature':
      connectionField = "behavioralFeature";
      break;
    default:
      throw Error("specified uml feature " + typeString + " not supported");
  }

  return connectionField;
};

DomainModelProvider.prototype.getDomainElementsFromURI = function (uri, domainConceptType) {
  domainConceptType = domainConceptType || 'UMLDomainConcept';
  var domainModel = this._getDomainModel();
  var connectionField = this.getFieldByType(domainConceptType);
  var concepts = domainModel.get('domainElements');
  for (var i in concepts) {
    if (connectionField in concepts[i] && concepts[i][connectionField]) {
      var type = concepts[i][connectionField];
      var localName = type["ns"]["localName"];
      var fullName = type["name"];
      if (localName == uri || fullName == uri) {
        return concepts[i];
      }
    }
  }
  var concept = this._factory.create('ifml:' + domainConceptType);
  var classifier = this.getClassifierFromUri(uri);
  concept.set('classifier', classifier);
  concepts.push(concept);

  return concept;
};

DomainModelProvider.prototype.getTypeFromName = function (name, domainConceptType) {
  for (var packageName in this._moddle.registry.packageMap) {
    var pck = this._moddle.registry.packageMap[packageName];
    for (var typeIndex in pck["types"]) {
      if (pck["types"][typeIndex]["name"] == name) {
        return this._moddle.registry.getEffectiveDescriptor(pck["prefix"] + ":" + name);
      }
    }
  }
  return null;
};

DomainModelProvider.prototype.getDomainConceptFromName = function (name, domainConceptType) {
  domainConceptType = domainConceptType || 'UMLDomainConcept';
  var domainModel = this._getDomainModel();
  var connectionField = this.getFieldByType(domainConceptType);
  var concepts = domainModel.get('domainElements');
  for (var i in concepts) {
    if (connectionField in concepts[i] && concepts[i][connectionField]) {
      var type = concepts[i][connectionField];
      var localName = type["ns"]["localName"];
      var fullName = type["name"];
      if (localName == name || fullName == name) {
        return type;
      }
    }
  }
  return null;
};


DomainModelProvider.prototype._getDomainModel = function () {
  var rootElement = this._canvas.getRootElement();
  var ifmlModel = rootElement.businessObject.$parent;
  var domainModel = ifmlModel.get('domainModel');
  if (!domainModel) {
    domainModel = this._factory.create("ifml:DomainModel");
    ifmlModel.set("domainModel", domainModel);
  }
  return domainModel;
};

DomainModelProvider.$inject = [
  'factory',
  'moddle',
  'config.moddleExtensions',
  'canvas',
  'eventBus'
];