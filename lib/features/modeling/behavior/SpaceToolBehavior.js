import {
  forEach
} from 'min-dash';

import {
  is
} from '../../../util/ModelUtil';
import { isAny } from '../util/ModelingUtil';

import {
  ANNOTATION_MIN_DIMENSIONS,
  VIEWELEMENT_MIN_DIMENSIONS,
  MODULE_MIN_DIMENSIONS
} from './ResizeBehavior';



export default function SpaceToolBehavior(eventBus) {
  eventBus.on('spaceTool.getMinDimensions', function (context) {
    var shapes = context.shapes,
      minDimensions = {};


    forEach(shapes, function (shape) {
      var id = shape.id;
      if (is(shape, 'ifml:Annotation')) {
        minDimensions[id] = ANNOTATION_MIN_DIMENSIONS;
      }
      if (isAny(shape, ['ifml:Module', 'ifml:ModuleDefinition'])) {
        minDimensions[id] = MODULE_MIN_DIMENSIONS;
      }
      if (isAny(shape, ['ifml:Module', 'ifml:ViewElement'])) {
        minDimensions[id] = VIEWELEMENT_MIN_DIMENSIONS;
      }
    });

    return minDimensions;
  });
}

SpaceToolBehavior.$inject = ['eventBus'];
