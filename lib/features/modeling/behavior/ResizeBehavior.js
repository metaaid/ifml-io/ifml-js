import { is } from '../../../util/ModelUtil';
import { isAny } from '../util/ModelingUtil';


var HIGH_PRIORITY = 1500;

export var ANNOTATION_MIN_DIMENSIONS = {
  width: 50,
  height: 30
};

export var VIEWELEMENT_MIN_DIMENSIONS = {
  width: 150,
  height: 75
};
export var MODULE_MIN_DIMENSIONS = {
  width: 300,
  height: 150
};


/**
 * Set minimum bounds/resize constraints on resize.
 *
 * @param {EventBus} eventBus
 */
export default function ResizeBehavior(eventBus) {
  eventBus.on('resize.start', HIGH_PRIORITY, function (event) {
    var context = event.context,
      shape = context.shape;


    // TODO: maybe cleaner
    /* 
    dirty hack to ignore events that placed on the edge
    all event types are overwritten with labels that are ignored by diagram-js
     */
    context.realChildrenTypes = [];
    shape.children.forEach(function (child) {

      var isBBox = !isAny(child, ['ifml:Event', 'ifml:Port', 'ifml:PortDefinition']);

      context.realChildrenTypes.push(child.type);
      if (!isBBox) {
        child.type = "label";
      }
    });
    
    if (is(shape, 'ifml:Annotation')) {
      context.minDimensions = ANNOTATION_MIN_DIMENSIONS;
    }
    if (is(shape, 'ifml:ViewElement')) {
      context.minDimensions = VIEWELEMENT_MIN_DIMENSIONS;
    }
    if (isAny(shape, ['ifml:Module', 'ifml:ModuleDefinition'])) {
      context.minDimensions = MODULE_MIN_DIMENSIONS;
    }
  });
  eventBus.on('resize.end', HIGH_PRIORITY, function (event) {
    var context = event.context,
      shape = context.shape;

    shape.children.forEach(function (child, index) {
      child.type = context.realChildrenTypes[index];
    });
  });
}

ResizeBehavior.$inject = ['eventBus'];