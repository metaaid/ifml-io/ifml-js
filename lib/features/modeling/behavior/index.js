import AppendBehavior from './AppendBehavior';
import CreateEventBehavior from './CreateEventBehavior';
import CreateModuleBehavior from './CreateModuleBehavior';
import LabelBehavior from './LabelBehavior';
import ModelingFeedback from './ModelingFeedback';
import ReplaceConnectionBehavior from './ReplaceConnectionBehavior';
import ReplaceElementBehaviour from './ReplaceElementBehaviour';
import ResizeBehavior from './ResizeBehavior';
import SpaceToolBehavior from './SpaceToolBehavior';
import UnclaimIdBehavior from './UnclaimIdBehavior';

export default {
  __init__: [
    'appendBehavior',
    'CreateEventBehavior',
    'CreateModuleBehavior',
    'labelBehavior',
    'modelingFeedback',
    'replaceConnectionBehavior',
    'replaceElementBehaviour',
    'resizeBehavior',
    'spaceToolBehavior',
    'unclaimIdBehavior',
  ],
  appendBehavior: ['type', AppendBehavior],
  CreateEventBehavior: ['type', CreateEventBehavior],
  CreateModuleBehavior: ['type', CreateModuleBehavior],
  labelBehavior: ['type', LabelBehavior],
  modelingFeedback: ['type', ModelingFeedback],
  replaceConnectionBehavior: ['type', ReplaceConnectionBehavior],
  replaceElementBehaviour: ['type', ReplaceElementBehaviour],
  resizeBehavior: ['type', ResizeBehavior],
  spaceToolBehavior: ['type', SpaceToolBehavior],
  unclaimIdBehavior: ['type', UnclaimIdBehavior],
};