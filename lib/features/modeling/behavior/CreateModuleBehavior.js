import inherits from 'inherits';

import CommandInterceptor from 'diagram-js/lib/command/CommandInterceptor';

import { is } from '../../../util/ModelUtil';


/**
 * Make sure Modules and Ports have a  Definition
 */
export default function CreateModuleBehavior(eventBus, factory, moddle) {

  CommandInterceptor.call(this, eventBus);

  this.preExecute('shape.create', function(event) {

    var context = event.context,
        shape = context.shape;

    if (is(shape, 'ifml:Module') && shape.type !== 'label') {

      // create a DataObject every time a DataObjectReference is created
      var definition = factory.create('ifml:ModuleDefinition');

      // set the reference to the DataObject
      shape.businessObject.moduleDefinition = definition;
    }
    if (is(shape, 'ifml:Port') && shape.type !== 'label') {

      // create a DataObject every time a DataObjectReference is created
      var definition = factory.create('ifml:PortDefinition');

      // set the reference to the DataObject
      shape.businessObject.portDefinition = definition;
    }
  });

}

CreateModuleBehavior.$inject = [
  'eventBus',
  'factory',
  'moddle'
];

inherits(CreateModuleBehavior, CommandInterceptor);