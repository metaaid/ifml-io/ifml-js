import inherits from 'inherits';

import { is } from '../../../util/ModelUtil';

import CommandInterceptor from 'diagram-js/lib/command/CommandInterceptor';

import { getParent } from '../util/ModelingUtil';

/**
 * A handler responsible for updating specialized events on creation/move
 */
export default function CreateEventBehavior(injector, ifmlRules, elementFactory, factory) {
  injector.invoke(CommandInterceptor, this);

  this.executed('shape.move', function(event) {
    var context = event.context,
        parent = context.parent || context.newParent,
        shape = context.shape;

    if (is(shape, 'ifml:Event')) {

      var attr = ifmlRules.canDrop(shape, parent);
      var type = attr && attr.type;

      if (type && !is(shape.businessObject, type)) {
        var newElement = factory.create(type);
        newElement.$parent = shape.businessObject.$parent;
        newElement.di = shape.businessObject.di;
        newElement.di.modelElement = newElement;
        context.shape.businessObject = newElement;
      }
    }
  });

  this.preExecute('shape.create', 1500, function(event) {
    var context = event.context,
        parent = context.parent || context.newParent,
        shape = context.shape;

    if (is(shape, 'ifml:Event')) {

      var attr = ifmlRules.canDrop(shape, parent);
      var type = attr && attr.type;

      if (type && !is(shape.businessObject, type)) {
        var newElement = elementFactory.create('shape', attr);
        context.shape = newElement;
      }
    }
  });
}


CreateEventBehavior.$inject = ['injector', 'ifmlRules', 'elementFactory', 'factory'];

inherits(CreateEventBehavior, CommandInterceptor);