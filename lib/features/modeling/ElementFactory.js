import {
  assign,
  forEach,
  isObject
} from 'min-dash';

import inherits from 'inherits';

import {
  is
} from '../../util/ModelUtil';

import BaseElementFactory from 'diagram-js/lib/core/ElementFactory';

import {
  DEFAULT_LABEL_SIZE
} from '../../util/LabelUtil';
import {
  isAny
} from './util/ModelingUtil';
import { ensureCompatDiRef } from '../../util/CompatibilityUtil';

export const EVENT_WIDTH = 20;
export const EVENT_HEIGHT = 20;

/**
 * A bpmn-aware factory for diagram-js shapes
 */
export default function ElementFactory(factory, moddle, translate) {
  BaseElementFactory.call(this);

  this._factory = factory;
  this._moddle = moddle;
  this._translate = translate;
}

inherits(ElementFactory, BaseElementFactory);

ElementFactory.$inject = [
  'factory',
  'moddle',
  'translate'
];

ElementFactory.prototype.baseCreate = BaseElementFactory.prototype.create;

ElementFactory.prototype.create = function (elementType, attrs) {

  // no special magic for labels,
  // we assume their businessObjects have already been created
  // and wired via attrs
  if (elementType === 'label') {    
    var di = attrs.di || this._factory.createDiLabel();
    return this.baseCreate(elementType, assign({ type: 'label', di: di }, DEFAULT_LABEL_SIZE, attrs));
  }

  return this.createIfmlElement(elementType, attrs);
};

ElementFactory.prototype.createIfmlElement = function (elementType, attrs) {
  var size,
    translate = this._translate;

  attrs = attrs || {};

  var businessObject = attrs.businessObject;
  var di = attrs.di;

  if (!businessObject) {
    if (!attrs.type) {
      throw new Error(translate('no shape type specified'));
    }
    if (!attrs.createOptions) {
      attrs.createOptions = {};
    }
    businessObject = this._factory.create(attrs.type, attrs.createOptions);
    ensureCompatDiRef(businessObject);
  }

  if (elementType == 'connection') {
    attrs = assign({
      children: []
    }, attrs);
  }

  if (!isModdleDi(di)) {
    var diAttrs = assign(
      di || {},
      { id: businessObject.$id + '_di' }
    );
    if (elementType === 'root') {
      di = this._factory.createDiModel(businessObject, diAttrs);
    } else if (elementType === 'connection') {
      di = this._factory.createDiEdge(businessObject, diAttrs);
    } else {
      di = this._factory.createDiShape(businessObject, {}, diAttrs);
    }
    di.$id = businessObject.$id + '_di'
  }

  if (attrs.isExpanded) {
    applyAttribute(di, attrs, 'isExpanded');
  }

  size = this.getDefaultSize(businessObject, di);

  attrs = assign({
    businessObject: businessObject,
    id: businessObject.$id
  }, size, attrs, {
    businessObject: businessObject,
    di: di
  });

  return this.baseCreate(elementType, attrs);
};


ElementFactory.prototype.getDefaultSize = function (semantic, di) {

  if (is(semantic, 'ifml:ViewComponent')) {
    return {
      width: 180,
      height: 80
    };
  }

  if (is(semantic, 'ifml:ViewContainer')) {
    return {
      width: 300,
      height: 200
    };
  }

  if (is(semantic, 'ifml:Event')) {
    return {
      width: EVENT_WIDTH,
      height: EVENT_HEIGHT
    };
  }

  if (is(semantic, 'ifml:Action')) {
    return {
      width: 80,
      height: 60
    };
  }

  if (isAny(semantic, ['ifml:ViewComponentPart', 'ifml:Parameter', 'ifml:ParameterBinding', 'uml:OpaqueBehavior'])) {
    return {
      width: 140,
      height: 30
    };
  }

  if (is(semantic, 'ifml:ActivationExpression')) {
    return {
      width: 180,
      height: 40
    };
  }

  if (is(semantic, 'ifml:ParameterBindingGroup')) {
    return {
      width: 220,
      height: 40
    };
  }

  if (is(semantic, 'ifml:ModuleDefinition')) {
    return {
      width: 500,
      height: 300
    };
  }
  if (is(semantic, 'ifml:PortDefinition')) {
    return {
      width: 20,
      height: 20
    };
  }

  return {
    width: 100,
    height: 80
  };
};

// helpers //////////////////////

/**
 * Apply attributes from a map to the given element,
 * remove attribute from the map on application.
 *
 * @param {Base} element
 * @param {Object} attrs (in/out map of attributes)
 * @param {Array<string>} attributeNames name of attributes to apply
 */
function applyAttributes(element, attrs, attributeNames) {

  forEach(attributeNames, function (property) {
    if (attrs[property] !== undefined) {
      applyAttribute(element, attrs, property);
    }
  });
}

/**
 * Apply named property to element and drain it from the attrs
 * collection.
 *
 * @param {Base} element
 * @param {Object} attrs (in/out map of attributes)
 * @param {string} attributeName to apply
 */
function applyAttribute(element, attrs, attributeName) {
  element[attributeName] = attrs[attributeName];

  delete attrs[attributeName];
}

function isModdleDi(element) {
  return isAny(element, [
    'ifmldi:IFMLNode',
    'ifmldi:IFMLDiagramElement',
    'ifmldi:IFMLCompartment',
    'ifmldi:IFMLDiagram',
  ]);
}