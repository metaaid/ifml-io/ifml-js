import { map, assign, pick } from "min-dash";

import { isAny } from "./util/ModelingUtil";

import { is } from "../../util/ModelUtil";

import IfmlModdle from 'ifml-moddle';

export default function IfmlFactory(moddle, eventBus) {
  this._model = moddle;

  
  eventBus.on('domainModel.changed', (configEvent) => {
    var domainExtension = configEvent["config"];
    var ids = this._model.ids;
    var moddleOptions = assign({}, domainExtension);
    this._model = new IfmlModdle(moddleOptions);
    this._model.ids = ids;
  });
}

IfmlFactory.$inject = ["moddle", "eventBus"];

IfmlFactory.prototype._needsId = function (element) {
  return isAny(element, [
    "ifml:InteractionFlowModelElement",
    "ifml:DomainElement",
    "ifml:Annotation",
  ]);
};

IfmlFactory.prototype._ensureId = function (bo, id) {
  if (id) {
    bo.$id = id;
    this._model.ids.claim(id, bo);
    return;
  }

  // generate semantic ids for elements
  var prefix;

  if (is(bo, "ifml:ViewContainer")) {
    prefix = "ViewContainer";
  } else if (is(bo, "ifml:Event")) {
    prefix = "Event";
  } else if (is(bo, "ifml:Action")) {
    prefix = "Action";
  } else if (is(bo, "ifml:ViewCompontent")) {
    prefix = "ViewCompontent";
  } else if (is(bo, "ifml:InteractionFlow")) {
    prefix = "Flow";
  } else if (is(bo, "ifml:UMLDomainConcept")) {
    prefix = "Concept";
  } else if (is(bo, "ifml:DynamicBehavior")) {
    prefix = "Behavior";
  } else {
    prefix = (bo.$type || "").replace(/^[^:]*:/g, "");
  }

  prefix += "_";

  if (!bo.$id && this._needsId(bo)) {
    bo.$id = this._model.ids.nextPrefixed(prefix, bo);
  }
};

IfmlFactory.prototype.create = function (type, attrs) {
  attrs = attrs || {};
  var bo, id;

  if (attrs.id){
    id = attrs.id;
  }

  switch (type) {
    case "ifml:DynamicBehavior":
      var action = attrs["action"];
      var behaviorType = attrs["type"];
      var dynBeh = this._model.create("ifml:DynamicBehavior");
      if (behaviorType == "ifml:UMLBehavior") {
        var beh = this._model.create("ifml:UMLBehavior");
        var umlBeh = this._model.create("uml:OpaqueBehavior");

        this._ensureId(dynBeh);
        action.set("dynamicBehavior", dynBeh);
        dynBeh.set("behaviorConcept", beh);
        beh.set("behavior", umlBeh);
        umlBeh.set("language", "OCL");
        umlBeh.set("body", "");
        bo = dynBeh;
      } else {
        throw Error(
          "dynamic behavior of type " + behaviorType + " is not supported"
        );
      }
      break;
    default:
      bo = this._model.create(type, attrs);
  }

  this._ensureId(bo, id);
  return bo;
};

IfmlFactory.prototype.createDiLabel = function () {
  return this.create("ifmldi:IFMLLabel", {
    bounds: this.createDiBounds(),
  });
};

IfmlFactory.prototype.createDiShape = function (semantic, attrs) {
  return this.create("ifmldi:IFMLNode", assign({
        modelElement: semantic,
        bounds: this.createDiBounds(),
      },
      attrs
    )
  );
};

IfmlFactory.prototype.createDiBounds = function (bounds) {
  return this.create("dc:Bounds", bounds);
};

IfmlFactory.prototype.createDiWaypoints = function (waypoints) {
  var self = this;

  return map(waypoints, function (pos) {
    return self.createDiWaypoint(pos);
  });
};

IfmlFactory.prototype.createDiWaypoint = function (point) {
  return this.create("dc:Point", pick(point, ["x", "y"]));
};

IfmlFactory.prototype.createDiEdge = function (semantic, attrs) {
  
  return this.create("ifmldi:IFMLConnection", assign({
        modelElement: semantic,
        waypoint: this.createDiWaypoints([])
      },
      attrs
    )
  );
};

IfmlFactory.prototype.createDiModel = function (semantic, attrs) {
  return this.create("ifml:InteractionFlowModel", assign({
    modelElement: semantic,
  }, attrs));
};
