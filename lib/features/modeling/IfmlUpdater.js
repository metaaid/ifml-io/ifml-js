import { assign, forEach } from "min-dash";

import inherits from "inherits";

import { remove as collectionRemove } from "diagram-js/lib/util/Collections";

import { Label } from "diagram-js/lib/model";

import { getBusinessObject, getDi, is, portIsInput } from "../../util/ModelUtil";

import { isAny } from "./util/ModelingUtil";

import CommandInterceptor from "diagram-js/lib/command/CommandInterceptor";
import { isExternalElement } from "../../util/LabelUtil";

/**
 * A handler responsible for updating the underlying IFML XML + DI
 * once changes on the diagram happen
 */
export default function IfmlUpdater(
  config,
  injector,
  eventBus,
  factory,
  elementFactory,
  elementRegistry,
  connectionDocking,
  create,
  translate
) {
  config = config || {};
  CommandInterceptor.call(this, eventBus);

  this._factory = factory;
  this._elementFactory = elementFactory;
  this._elementRegistry = elementRegistry;
  this._translate = translate;
  this._eventBus = eventBus;
  this._create = create;

  if (config.ifmlAutoPlace !== false) {
    this._ifmlAutoPlace = injector.get("ifmlAutoPlace", false);
  }

  var self = this;

  // connection cropping //////////////////////

  // crop connection ends during create/update
  function cropConnection(e) {
    var context = e.context,
      hints = context.hints || {},
      connection;

    if (!context.cropped && hints.createElementsBehavior !== false) {
      connection = context.connection;
      connection.waypoints = connectionDocking.getCroppedWaypoints(connection);
      context.cropped = true;
    }
  }

  this.executed(["connection.layout", "connection.create"], cropConnection);

  this.reverted(["connection.layout"], function (e) {
    delete e.context.cropped;
  });

  // IFML + DI update //////////////////////

  // update parent
  function updateParent(e) {
    var context = e.context;

    self.updateParent(context.shape || context.connection, context.oldParent);
  }

  function reverseUpdateParent(e) {
    var context = e.context;

    var element = context.shape || context.connection,
      // oldParent is the (old) new parent, because we are undoing
      oldParent = context.parent || context.newParent;

    self.updateParent(element, oldParent);
  }

  this.executed(
    [
      "shape.move",
      "shape.create",
      "shape.delete",
      "connection.create",
      "connection.move",
      "connection.delete",
    ],
    ifIfml(updateParent)
  );

  this.reverted(
    [
      "shape.move",
      "shape.create",
      "shape.delete",
      "connection.create",
      "connection.move",
      "connection.delete",
    ],
    ifIfml(reverseUpdateParent)
  );

  // update bounds
  function updateBounds(e) {
    var shape = e.context.shape;

    if (!isAny(shape, ["ifml:BaseElement", "ifml:Annotation"])) {
      return;
    }

    self.updateBounds(shape);
  }

  this.executed(
    ["shape.move", "shape.create", "shape.resize"],
    ifIfml(function (event) {
      var element = event.element;
      // exclude labels because they're handled separately during shape.changed
      if (event.context.shape.type === "label") {
        return;
      }

      updateBounds(event);
    })
  );

  this.reverted(["shape.move", "shape.create", "shape.resize"], ifIfml(function (event) {
      var element = event.element;
      // exclude labels because they're handled separately during shape.changed
      if (event.context.shape.type === "label") {
        return;
      }

      updateBounds(event);
    })
  );

  // Handle labels separately. This is necessary, because the label bounds have to be updated
  // every time its shape changes, not only on move, create and resize.
  eventBus.on("shape.changed", function (event) {
    var element = event.element;
    if (element.type === "label") {
      updateBounds({
        context: {
          shape: element,
        },
      });
    }
  });

  // attach / detach connection
  function updateConnection(e) {
    self.updateConnection(e.context);
  }

  this.executed(
    [
      "connection.create",
      "connection.move",
      "connection.delete",
      "connection.reconnect",
    ],
    ifIfml(updateConnection)
  );

  this.reverted(
    [
      "connection.create",
      "connection.move",
      "connection.delete",
      "connection.reconnect",
    ],
    ifIfml(updateConnection)
  );

  // update waypoints
  function updateConnectionWaypoints(e) {
    self.updateConnectionWaypoints(e.context.connection);
  }

  this.executed(
    ["connection.layout", "connection.move", "connection.updateWaypoints"],
    ifIfml(updateConnectionWaypoints)
  );

  this.reverted(
    ["connection.layout", "connection.move", "connection.updateWaypoints"],
    ifIfml(updateConnectionWaypoints)
  );

  // update conditional/default flows
  this.executed("connection.reconnect", ifIfml(function (event) {
      var context = event.context,
        connection = context.connection,
        oldSource = context.oldSource,
        newSource = context.newSource,
        connectionBo = getBusinessObject(connection),
        oldSourceBo = getBusinessObject(oldSource);

      // remove default from old source flow on reconnect to new source
      // if source changed
      if (oldSource !== newSource && oldSourceBo.default === connectionBo) {
        context.oldDefault = oldSourceBo.default;

        delete oldSourceBo.default;
      }
    })
  );

  this.reverted(
    "connection.reconnect",
    ifIfml(function (event) {
      var context = event.context,
        connection = context.connection,
        oldSource = context.oldSource,
        newSource = context.newSource,
        connectionBo = getBusinessObject(connection),
        oldSourceBo = getBusinessObject(oldSource),
        newSourceBo = getBusinessObject(newSource);

      // add condition to connection on revert reconnect to new source
      if (context.oldConditionExpression) {
        connectionBo.conditionExpression = context.oldConditionExpression;
      }

      // add default to old source on revert reconnect to new source
      if (context.oldDefault) {
        oldSourceBo.default = context.oldDefault;

        delete newSourceBo.default;
      }
    })
  );

  // update attachments
  function updateAttachment(e) {
    self.updateAttachment(e.context);
  }

  this.executed(["element.updateAttachment"], ifIfml(updateAttachment));
  this.reverted(["element.updateAttachment"], ifIfml(updateAttachment));
}

inherits(IfmlUpdater, CommandInterceptor);

IfmlUpdater.$inject = [
  "config",
  "injector",
  "eventBus",
  "factory",
  "elementFactory",
  "elementRegistry",
  "connectionDocking",
  "create",
  "translate",
];

// implementation //////////////////////

IfmlUpdater.prototype.updateAttachment = function (context) {
  var shape = context.shape,
    businessObject = shape.businessObject,
    host = shape.host;

  businessObject.attachedToRef = host && host.businessObject;
};

IfmlUpdater.prototype.updateParent = function (element, oldParent) {
  // do not update IFML label parent
  if (element instanceof Label) {
    return;
  }
  
  // do not update external elements, unless its deleted
  if ((isExternalElement(element) && !!oldParent && !!element.parent)) {
    element.parent = oldParent;
    return;
  }

  // do not update associations parent, if the element allready has one

  var parentShape = element.host || element.parent;

  // connections are stored within the model or a module
  if (is(element, "ifml:InteractionFlow")) {
    while (
      parentShape &&
      !isAny(parentShape, [
        "ifml:ModuleDefinition",
        "ifml:InteractionFlowModel",
      ])
    ) {
      parentShape = parentShape.parent;
    }
  }

  var businessObject = element.businessObject,
    parentBusinessObject = parentShape && parentShape.businessObject,
    parentDi = parentBusinessObject && getDi(parentShape);

  this.updateSemanticParent(businessObject, parentBusinessObject, element);
  this.updateDiParent(getDi(element), parentDi);
};

IfmlUpdater.prototype.updateBounds = function (shape) {
  var di = getDi(shape);

  var target = shape instanceof Label ? this._getLabel(di) : di;

  var bounds = target.bounds;

  if (!bounds) {
    bounds = this._factory.createDiBounds();
    target.set("bounds", bounds);
  }

  assign(bounds, {
    x: shape.x,
    y: shape.y,
    width: shape.width,
    height: shape.height,
  });
};

// update existing sourceElement and targetElement information
IfmlUpdater.prototype.updateDiConnection = function (connection, newSource, newTarget) {
  var di = getDi(connection);
  var newSourceDi = getDi(newSource);
  var newTargetDi = getDi(newTarget);
  if (di.sourceElement && di.sourceElement.modelElement !== newSource) {
    di.sourceElement = newSource && newSourceDi;
  }

  if (di.targetElement && di.targetElement.modelElement !== newTarget) {
    di.targetElement = newTarget && newTargetDi;
  }
};

IfmlUpdater.prototype.updateDiParent = function (di, parentDi) {
  // if (parentDi && !is(parentDi, 'ifmldi:IFMLDiagram')) {
  //   parentDi = parentDi.$parent;
  // }
  if (di.$parent === parentDi) {
    return;
  }
  var parent = parentDi || di.$parent;
  if (!parent) {
    throw new Error("No parent found");
  }

  var planeElements = parent.get("ownedElement");

  if (parentDi) {
    planeElements.push(di);
    di.$parent = parentDi;
  } else {
    collectionRemove(planeElements, di);
    di.$parent = null;
  }
};

IfmlUpdater.prototype.getFieldName = function (
  businessObject,
  parent,
  translate
) {
  var fieldName;

  if (is(businessObject, "ifml:PortDefinition")) {
    var isIn = portIsInput(businessObject);
    fieldName = isIn ? "inputPorts" : "outputPorts";
  } else if (is(parent, "ifml:ModuleDefinition")) {
    fieldName = "interactionFlowModelElement";
  } else if (is(businessObject, "ifml:ActivationExpression")) {
    fieldName = "activationExpression";
  } else if (is(businessObject, "ifml:Field")) {
    fieldName = "parameters";
  } else if (is(parent, "ifml:DataBinding")) {
    if (is(businessObject, "ifml:VisualizationAttribute")) {
      fieldName = "visualizationAttribute";
    } else if (is(businessObject, "ifml:ViewComponentPart")) {
      fieldName = "subViewComponentParts";
    }
  } else if (is(businessObject, "ifml:ViewComponentPart")) {
    if (is(businessObject, "ifml:ConditionalExpression")) {
      fieldName = "conditionalExpression";
    } else if (is(businessObject, "ifml:DynamicBehavior")) {
      fieldName = "dynamicBehavior";
    } else {
      fieldName = "viewComponentParts";
    }
  } else if (is(businessObject, "ifml:BehaviorConcept")) {
    fieldName = "behaviorConcept";
  } else if (is(businessObject, "uml:Behavior")) {
    fieldName = "behavior";
  } else if (is(businessObject, "ifml:Parameter")) {
    fieldName = "parameters";
  } else if (is(businessObject, "ifml:ParameterBindingGroup")) {
    fieldName = "parameterBindingGroup";
  } else if (is(businessObject, "ifml:ParameterBinding")) {
    fieldName = "parameterBindings";
  } else if (is(businessObject, "ifml:Event")) {
    if (is(businessObject, "ifml:OnSelectEvent") && is(parent, "ifml:List")) {
      fieldName = "selectEvent";
    } else if (is(businessObject, "ifml:ViewElementEvent")) {
      fieldName = "viewElementEvents";
    }

    // if (is(businessObject, 'ifml:SystemEvent')) {
    //   fieldName = 'systemEvents';
    // }
    if (is(businessObject, "ifml:ActionEvent")) {
      fieldName = "actionEvents";
    }
    if (
      isAny(parent, [
        "ifml:InteractionFlowExpression",
        "ifml:ActivationExpression",
      ])
    ) {
      fieldName = "event";
    }
  } else if (is(parent, "ifml:InteractionFlowModel")) {
    fieldName = "interactionFlowModelElements";
  } else if (is(businessObject, "ifml:Annotation")) {
    fieldName = "annotations";
  }  else if (is(parent, "ifml:ViewContainer")) {
    if (is(businessObject, "ifml:Action")) {
      fieldName = "actions";
    } else {
      fieldName = "viewElements";
    }
  }
  if (!fieldName) {
    throw new Error(
      translate("no parent for {element} in {parent}", {
        element: businessObject && businessObject.$id,
        parent: parent && parent.$id,
      })
    );
  }

  return fieldName;
};

IfmlUpdater.prototype._getElement = function (semantic) {
  return this._elementRegistry.get(semantic.$id);
};

IfmlUpdater.prototype.getParameters = function (element, direction) {
  var semantic = getBusinessObject(element);
  var possibleParameters = semantic.get("parameters");

  if (is(semantic, "ifml:ViewComponent")) {
    var viewComponentParts = semantic.get("viewComponentParts");
    possibleParameters = possibleParameters.concat(viewComponentParts);
  }

  return possibleParameters.filter(
    (el) =>
      is(el, "ifml:Parameter") &&
      (el.direction == direction || el.direction == "undefined")
  );
};

IfmlUpdater.prototype.getElementForParameterLookup = function (semantic) {
  var el = this._getElement(semantic);

  if (is(el, "ifml:Event")) {
    el = el.parent;
  }

  return el;
};

IfmlUpdater.prototype.getFirstParameterOrCreate = function (
  element,
  direction,
  defaultName = ""
) {
  var ifmlAutoPlace = this._ifmlAutoPlace;
  var create = this._create;
  var parameters = this.getParameters(element, direction);
  if (parameters.length == 0) {
    var newParameter = this._elementFactory.createShape({
      type: "ifml:Parameter",
    });
    newParameter.businessObject.set("direction", direction);
    newParameter.businessObject.set("name", defaultName);
    if (ifmlAutoPlace) {
      ifmlAutoPlace.insert(element, newParameter);
    } else {
      debugger;
      create.start(event, newParameter, {
        source: element,
      });
    }
    return newParameter;
  }

  return this._getElement(parameters[0]);
};

IfmlUpdater.prototype.updateSemanticParent = function (
  businessObject,
  newParent,
  element
) {
  var translate = this._translate,
    oldParent = businessObject.$parent;

  if (oldParent === newParent) {
    return;
  }

  if (is(businessObject, "ifml:ParameterBinding")) {
    // parameter do not exist -> create
    if (!oldParent && newParent) {
      var sourceElement = this.getElementForParameterLookup(
        newParent.$parent.sourceInteractionFlowElement
      );
      var targetElement = this.getElementForParameterLookup(
        newParent.$parent.targetInteractionFlowElement
      );

      var paramenterElementSource = this.getFirstParameterOrCreate(
        sourceElement,
        "out"
      );
      var paraSource = getBusinessObject(paramenterElementSource);
      var outNameBo = getBusinessObject(paramenterElementSource).get("name");
      var paramenterElementTarget = this.getFirstParameterOrCreate(
        targetElement,
        "in",
        outNameBo
      );
      paramenterElementTarget.businessObject.set(
        "type",
        paraSource.get("type")
      );

      businessObject.sourceParameter = paramenterElementSource.businessObject;
      businessObject.targetParameter = paramenterElementTarget.businessObject;
    }

    // the parameter are stored in the components
    if (newParent) {
      var newGroup = newParent.$parent;
      var sourceParameter =
        newGroup.sourceInteractionFlowElement.get("parameters");
      sourceParameter.push(businessObject.sourceParameter);
      var targetParameter =
        newGroup.targetInteractionFlowElement.get("parameters");
      targetParameter.push(businessObject.targetParameter);
      if (oldParent) {
        var oldGroup = oldParent.$parent;
        collectionRemove(
          oldGroup.sourceInteractionFlowElement.parameters,
          businessObject.sourceParameter
        );
        collectionRemove(
          oldGroup.targetInteractionFlowElement.parameters,
          businessObject.targetParameter
        );
      }
    }
  }

  // remove child/property from old parent
  if (oldParent) {
    var fieldNameOld = this.getFieldName(businessObject, oldParent, translate);
    var fieldDescripterOld =
      oldParent.$model.properties.model.getPropertyDescriptor(
        oldParent,
        fieldNameOld
      );

    if (!fieldDescripterOld) {
      throw new Error(
        translate(
          'parent "{parent}" does not have the field "{fieldNameNew}"',
          {
            fieldNameOld: fieldNameOld,
            parent: oldParent.$id,
          }
        )
      );
    }
    var isArray = fieldDescripterOld.isMany;

    // remove from old parent
    var fieldValue = oldParent.get(fieldNameOld);

    if (isArray) {
      collectionRemove(fieldValue, businessObject);
    } else {
      oldParent.set(fieldNameOld, null);
    }
  }

  if (!newParent) {
    businessObject.$parent = null;
  } else {
    var fieldNameNew = this.getFieldName(businessObject, newParent, translate);
    var fieldDescripterNew =
      newParent.$model.properties.model.getPropertyDescriptor(
        newParent,
        fieldNameNew
      );

    if (!fieldDescripterNew) {
      throw new Error(
        translate(
          'parent "{parent}" does not have the field "{fieldNameNew}"',
          {
            fieldNameNew: fieldNameNew,
            parent: newParent.$id,
          }
        )
      );
    }
    var isArrayNew = fieldDescripterNew.isMany;

    // add to new parent
    var fieldValueNew = newParent.get(fieldNameNew);
    if (isArrayNew) {
      fieldValueNew.push(businessObject);
    } else {
      newParent.set(fieldNameNew, businessObject);
    }
    businessObject.$parent = newParent;
  }
};

IfmlUpdater.prototype.updateConnectionWaypoints = function (connection) {
  var connectionDi = getDi(connection);
  connectionDi.set("waypoint", this._factory.createDiWaypoints(connection.waypoints));
};

IfmlUpdater.prototype.updateConnection = function (context) {
  var connection = context.connection,
    businessObject = getBusinessObject(connection),
    newSource = getBusinessObject(connection.source),
    newTarget = getBusinessObject(connection.target);

  if (businessObject.sourceInteractionFlowElement !== newSource) {
    // if (inverseSet) {
    collectionRemove(
      businessObject.sourceInteractionFlowElement &&
        businessObject.sourceInteractionFlowElement.get("outInteractionFlows"),
      businessObject
    );

    if (newSource && newSource.get("outInteractionFlows")) {
      newSource.get("outInteractionFlows").push(businessObject);
    }

    businessObject.sourceInteractionFlowElement = newSource;
  }

  if (businessObject.targetInteractionFlowElement !== newTarget) {
    collectionRemove(
      businessObject.targetInteractionFlowElement &&
        businessObject.targetInteractionFlowElement.get("inInteractionFlows"),
      businessObject
    );

    if (newTarget && newTarget.get("inInteractionFlows")) {
      newTarget.get("inInteractionFlows").push(businessObject);
    }

    businessObject.targetInteractionFlowElement = newTarget;
  }

  this.updateConnectionWaypoints(connection);

  this.updateDiConnection(connection, connection.source, connection.target);
};

// helpers //////////////////////

IfmlUpdater.prototype._getLabel = function (di) {
  if (!di.ownedLabel) {
    di.ownedLabel = this._factory.createDiLabel();
  }

  return di.ownedLabel;
};

/**
 * Make sure the event listener is only called
 * if the touched element is a BPMN element.
 *
 * @param  {Function} fn
 * @return {Function} guarded function
 */
function ifIfml(fn) {
  return function (event) {
    var context = event.context,
      element = context.shape || context.connection;

    if (isAny(element, ["ifml:BaseElement", "ifml:Annotation"])) {
      fn(event);
    }
  };
}
