import IfmlAutoResize from './IfmlAutoResize';


export default {
  __init__: [
    'ifmlAutoResize'
  ],
  ifmlAutoResize: ['type', IfmlAutoResize]
};