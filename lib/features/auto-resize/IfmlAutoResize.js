import AutoResize from 'diagram-js/lib/features/auto-resize/AutoResize';

import inherits from 'inherits';

import {
  is
} from '../../util/ModelUtil';

import {
  VIEWCOMPONENT_PADDING_HORIZONTAL,
  VIEWCOMPONENT_MINWIDTH,
  SUBCOMPONENT_PADDING_HORIZONTAL,
  VIEWCOMPONENT_PADDING_BOTTOM,
  VIEWCOMPONENT_PADDING_VERTICAL,
} from '../auto-place/IfmlAutoPlaceUtil';
import {
  EVENT_HEIGHT,
  EVENT_WIDTH
} from '../modeling/ElementFactory';
import {
  isAny
} from '../modeling/util/ModelingUtil';

/**
 * Sub class of the AutoResize module which implements a IFML
 * specific resize function.
 */
export default function IfmlAutoResize(injector, modeling, eventBus) {
  injector.invoke(AutoResize, this);

  eventBus.on(['resize.start'], 800, function (event) {
    var context = event.context,
      shape = context.shape;
    var bo = context.shape.businessObject;
    if (is(bo, 'ifml:ViewComponent')) {
      context.resizeConstraints.min.right = shape.x + VIEWCOMPONENT_MINWIDTH;
    }
  });

  this.postExecuted(['shape.resize'], function (event) {
    var context = event.context,
      shape = context.shape;

    if (isAny(shape, ['ifml:ViewComponent', 'ifml:Module', 'ifml:ModuleDefinition'])) {
      // update children width
      shape.children.forEach(function (child) {
        var childIsPort = isAny(child, ['ifml:Port', 'ifml:PortDefinition']);
        var childIsEvent = is(child, 'ifml:Event');

        if (childIsEvent || childIsPort) {
          var oldBounds = context.oldBounds;
          var newX = null;
          var newY = null;
          if (oldBounds.x != shape.x) {
            var isLeft = child.x == oldBounds.x - EVENT_WIDTH / 2;
            if (isLeft) {
              newX = shape.x - EVENT_WIDTH / 2;
            }
          }

          // right bound changed
          if (oldBounds.width != shape.width) {
            var isRight = child.x == oldBounds.x + oldBounds.width - EVENT_WIDTH / 2;
            if (isRight) {
              newX = shape.x + shape.width - EVENT_WIDTH / 2;
            }
          }

          // top bound changed
          if (oldBounds.y != shape.y) {
            var isTop = child.y == oldBounds.y - EVENT_HEIGHT / 2;
            if (isTop) {
              newY = shape.y - EVENT_HEIGHT / 2;
            }
          }

          // bottom bound changed
          if (oldBounds.height != shape.height) {
            var isBottom = child.y == oldBounds.y + oldBounds.height - EVENT_HEIGHT / 2;
            if (isBottom) {
              newY = shape.y + shape.height - EVENT_HEIGHT / 2;
            }
          }
          if (newX !== null) {
            modeling.resizeShape(child, {
              x: newX,
              y: child.y,
              width: child.width,
              height: child.height,
            });
          }
          if (newY !== null) {
            modeling.resizeShape(child, {
              x: child.x,
              y: newY,
              width: child.width,
              height: child.height,
            });
          }
        }
      });
    }
  });

}

IfmlAutoResize.$inject = [
  'injector',
  'modeling',
  'eventBus',
];

inherits(IfmlAutoResize, AutoResize);


/**
 * Resize shapes and lanes.
 *
 * @param {djs.model.Shape} target
 * @param {Bounds} newBounds
 * @param {Object} hints
 */
IfmlAutoResize.prototype.resize = function (target, newBounds, hints) {
  this._modeling.resizeShape(target, newBounds, null, hints);
};