import KeyboardModule from "diagram-js/lib/features/keyboard";

import IfmlKeyboardBindings from "./IfmlKeyboardBindings";

export default {
  __depends__: [KeyboardModule],
  __init__: ["keyboardBindings"],
  keyboardBindings: ["type", IfmlKeyboardBindings],
};
