import { assign, forEach, isArray } from "min-dash";

import {
  getBusinessObject,
  is,
  portIsInput,
  portIsOutput,
} from "../../util/ModelUtil";

import { isAny } from "../modeling/util/ModelingUtil";

import { hasPrimaryModifier } from "diagram-js/lib/util/Mouse";

import { query as domQuery, domify as domify } from "min-dom";

/**
 * A provider for BPMN 2.0 elements context pad
 */
export default function ContextPadProvider(
  config,
  injector,
  eventBus,
  contextPad,
  modeling,
  elementFactory,
  connect,
  create,
  popupMenu,
  canvas,
  rules,
  translate,
  commandStack
) {
  config = config || {};

  contextPad.registerProvider(this);

  this._contextPad = contextPad;

  this._modeling = modeling;

  this._elementFactory = elementFactory;
  this._connect = connect;
  this._create = create;
  this._popupMenu = popupMenu;
  this._canvas = canvas;
  this._rules = rules;
  this._translate = translate;
  this._commandStack = commandStack;

  if (config.autoPlace !== false) {
    this._autoPlace = injector.get("autoPlace", false);
  }
  if (config.ifmlAutoPlace !== false) {
    this._ifmlAutoPlace = injector.get("ifmlAutoPlace", false);
  }

  eventBus.on("create.end", 250, function (event) {
    var context = event.context,
      shape = context.shape;

    if (!hasPrimaryModifier(event) || !contextPad.isOpen(shape)) {
      return;
    }

    var entries = contextPad.getEntries(shape);

    if (entries.replace) {
      entries.replace.action.click(event, shape);
    }
  });

  // append iconText
  eventBus.on("contextPad.open", function (event) {
    var current = event.current;
    var html = current.pad.html;
    var entries = current.entries;
    forEach(entries, function (entry, id) {
      if (entry.iconText) {
        var entryHtml = domQuery('[data-action="' + id + '"]', html);
        entryHtml.appendChild(
          domify('<div class="iconText">' + entry.iconText + "</div>")
        );
      }
    });
  });
}

ContextPadProvider.$inject = [
  "config.contextPad",
  "injector",
  "eventBus",
  "contextPad",
  "modeling",
  "elementFactory",
  "connect",
  "create",
  "popupMenu",
  "canvas",
  "rules",
  "translate",
  "commandStack",
];

ContextPadProvider.prototype.getMultiElementContextPadEntries = function (
  elements
) {
  var modeling = this._modeling;

  var actions = {};

  if (this._isDeleteAllowed(elements)) {
    assign(actions, {
      delete: {
        group: "edit",
        className: "icon-trash",
        title: this._translate("Remove"),
        action: {
          click: function (event, elements) {
            modeling.removeElements(elements.slice());
          },
        },
      },
    });
  }

  return actions;
};

/**
 * @param {djs.model.Base[]} elements
 * @return {boolean}
 */
ContextPadProvider.prototype._isDeleteAllowed = function (elements) {
  var baseAllowed = this._rules.allowed("elements.delete", {
    elements: elements,
  });

  if (isArray(baseAllowed)) {
    return every(baseAllowed, function (element) {
      return includes(baseAllowed, element);
    });
  }

  return baseAllowed;
};

ContextPadProvider.prototype.getContextPadEntries = function (element) {
  var contextPad = this._contextPad,
    modeling = this._modeling,
    elementFactory = this._elementFactory,
    connect = this._connect,
    create = this._create,
    popupMenu = this._popupMenu,
    canvas = this._canvas,
    rules = this._rules,
    autoPlace = this._autoPlace,
    ifmlAutoPlace = this._ifmlAutoPlace,
    translate = this._translate;

  var actions = {};

  if (element.type === "label") {
    return actions;
  }

  var businessObject = element.businessObject;

  function startConnect(event, element) {
    connect.start(event, element);
  }

  function removeElement(e) {
    modeling.removeElements([element]);
  }

  function getReplaceMenuPosition(element) {
    var Y_OFFSET = 5;

    var diagramContainer = canvas.getContainer(),
      pad = contextPad.getPad(element).html;

    var diagramRect = diagramContainer.getBoundingClientRect(),
      padRect = pad.getBoundingClientRect();

    var top = padRect.top - diagramRect.top;
    var left = padRect.left - diagramRect.left;

    var pos = {
      x: left,
      y: top + padRect.height + Y_OFFSET,
    };

    return pos;
  }

  function insertAction(type, className, title, options) {
    if (typeof title !== "string") {
      options = title;
      title = translate("Append {type}", {
        type: type.replace(/^ifml:/, ""),
      });
    }

    function insertStart(event, element) {
      var shape = elementFactory.createShape(
        assign(
          {
            type: type,
          },
          options
        )
      );

      create.start(event, shape, {
        source: element,
      });
    }

    var insert = ifmlAutoPlace
      ? function (event, element) {
          var shape = elementFactory.createShape(
            assign(
              {
                type: type,
              },
              options
            )
          );

          ifmlAutoPlace.insert(element, shape);
        }
      : insertStart;

    return {
      group: options && options["iconText"] ? options["iconText"] : "model",
      className: className,
      title: title,
      iconText: options ? options["iconText"] : null,
      action: {
        dragstart: insertStart,
        click: insert,
      },
    };
  }

  function appendEvent(className, title, options, eventName = "ifml:Event") {
    function appendListener(event, element) {
      var shape = elementFactory.createCriterionShape("ifml:Event");
      create.start(event, shape, element);
    }

    return insertAction(eventName, className, title, options);
  }

  function insertExpression(action) {
    action = getBusinessObject(action);

    return insertStereotypLike("ifml:DynamicBehavior", null, {
      createOptions: {
        type: "ifml:UMLBehavior",
        action: action,
      },
    });
  }

  function insertStereotypLike(realType, title, options) {
    options = options || {};
    var shortType = realType
      .replace(/^ifml:/, "")
      .replace(/^ocl:/, "")
      .replace(/^uml:/, "");
    options["iconText"] = options["iconText"] ? options["iconText"] : shortType;
    title = title ? title : "add " + shortType;
    return insertAction(realType, "icon-stereotyp", title, options);
  }

  if (isAny(element, ["ifml:Action", "ifml:ViewElement"])) {
    assign(actions, {
      "append.event": appendEvent("icon-event", "Append Event"),
    });
  }

  if (isAny(element, ["ifml:List"])) {
    assign(actions, {
      "append.event.select": insertAction(
        "ifml:OnSelectEvent",
        "icon-event-select",
        "Append OnSelectEvent"
      ),
    });
  }
  if (isAny(element, ["ifml:Form"])) {
    assign(actions, {
      "append.event.submit": insertAction(
        "ifml:OnSubmitEvent",
        "icon-event-enter",
        "Append OnSubmitEvent"
      ),
    });
  }
  if (is(businessObject, 'ifml:ViewElement')) {
    assign(actions, {
      'append.text-annotation': insertAction(
        'ifml:Annotation',
        'icon-text-annotation',
        "Add Annotation"
      )
    });
  }

  if (isAny(element, ["ifml:ViewContainer"])) {
    var cmdStack = this._commandStack;

    assign(actions, {
      "change.isXOR": {
        group: "default",
        title: "toggle is XOR",
        iconText: "[XOR]",
        action: {
          click: function () {
            var isXOR = !businessObject.isXOR;

            cmdStack.execute("element.updateProperties", {
              element: element,
              properties: {
                isXOR: isXOR,
              },
            });
          },
        },
      },
    });
    assign(actions, {
      "change.isDefault": {
        group: "default",
        title: "toggle is Default",
        iconText: "[D]",
        action: {
          click: function () {
            var isDefault = !businessObject.isDefault;

            cmdStack.execute("element.updateProperties", {
              element: element,
              properties: {
                isDefault: isDefault,
              },
            });
          },
        },
      },
    });
    assign(actions, {
      "change.isLandmark": {
        group: "default",
        title: "toggle is Landmark",
        iconText: "[L]",
        action: {
          click: function () {
            var isLandmark = !businessObject.isLandmark;

            cmdStack.execute("element.updateProperties", {
              element: element,
              properties: {
                isLandmark: isLandmark,
              },
            });
          },
        },
      },
    });
  }

  if (isAny(element, ["ifml:ViewComponent", "ifml:DataBinding"])) {
    assign(actions, {
      "append.databinding": insertStereotypLike("ifml:DataBinding"),
    });
  }
  if (isAny(element, ["ifml:Form"])) {
    // TODO: get all specified ifml:Field
    assign(actions, {
      "append.addSimpleField": insertStereotypLike("ifml:SimpleField"),
    });
    assign(actions, {
      "append.addSelectionField": insertStereotypLike("ifml:SelectionField"),
    });
  }

  if (is(element, "ifml:DataBinding")) {
    // TODO: and doesnt have conditional expression
    if (
      !businessObject.conditionalExpression ||
      businessObject.conditionalExpression.length == 0
    ) {
      assign(actions, {
        "append.conditionalExpression": insertStereotypLike(
          "ifml:ConditionalExpression",
          "set ConditionalExpression"
        ),
      });
    }
    assign(actions, {
      "append.visualizationAttribute": insertStereotypLike(
        "ifml:VisualizationAttribute",
        "add VisualizationAttributes",
        {
          iconText: "VisualizationAttributes",
        }
      ),
    });
  }

  if (is(element, "ifml:InteractionFlowModel")) {
    assign(actions, {
      "append.interactionFlowModel": insertStereotypLike(
        "ifml:InteractionFlowModel"
      ),
    });
  }

  if (is(element, "ifml:InteractionFlowElement")) {
    assign(actions, {
      "append.parameter": insertStereotypLike("ifml:Parameter"),
    });
  }

  if (is(element, "ifml:Action")) {
    assign(actions, {
      "append.expression": insertExpression(element),
    });
  }

  if (is(element, "ifml:InteractionFlow")) {
    if (
      !businessObject.parameterBindingGroup ||
      businessObject.parameterBindingGroup.length == 0
    ) {
      assign(actions, {
        "append.parameterBindingGroup": insertStereotypLike(
          "ifml:ParameterBindingGroup"
        ),
      });
    }
  }
  if (is(element, "ifml:ParameterBindingGroup")) {
    assign(actions, {
      "append.parameterBinding": insertStereotypLike("ifml:ParameterBinding"),
    });
  }

  if (
    isAny(element, ["ifml:ViewComponentPart", "ifml:Event", "ifml:ViewElement"])
  ) {
    if (
      !businessObject.activationExpression ||
      businessObject.activationExpression.length == 0
    ) {
      assign(actions, {
        "append.activationExpression": insertStereotypLike(
          "ifml:ActivationExpression",
          "set ActivationExpression"
        ),
      });
    }
  }

  if (isAny(element, ["ifml:ModuleDefinition"])) {
    assign(actions, {
      "append.port.in": insertAction(
        "ifml:PortDefinition",
        "icon-port-in",
        "Append Port (in)",
        {
          createOptions: {
            direction: "in",
          },
        }
      ),
      "append.port.out": insertAction(
        "ifml:PortDefinition",
        "icon-port-out",
        "Append Port (out)",
        {
          createOptions: {
            direction: "out",
          },
        }
      ),
    });
  }

  if (isAny(element, ["ifml:PortDefinition"])) {
    var isIn = portIsInput(element);
    var newDirection = isIn ? "out" : "in";
    var icon = "icon-port-" + newDirection;
    var cmdStack = this._commandStack;

    assign(actions, {
      "change.inout": {
        group: "default",
        className: icon,
        title: "Change direction to " + newDirection,
        action: {
          click: function () {
            cmdStack.execute("element.updateProperties", {
              element: element,
              properties: {
                direction: newDirection,
              },
            });
          },
        },
      },
    });
  }

  if (isAny(businessObject, ["ifml:InteractionFlowModelElement", "ifml:Annotation"])) {
    assign(actions, {
      connect: {
        group: "connect",
        className: "icon-connection-multi",
        title: translate(
          "Connect using " +
            (businessObject.isForCompensation
              ? ""
              : "Sequence/MessageFlow or ") +
            "Association"
        ),
        action: {
          click: startConnect,
          dragstart: startConnect,
        },
      },
    });
  }

  if (!popupMenu.isEmpty(element, "ifml-replace")) {
    // Replace menu entry
    assign(actions, {
      replace: {
        group: "edit",
        className: "icon-screw-wrench",
        title: translate("Change type"),
        action: {
          click: function (event, element) {
            var position = assign(getReplaceMenuPosition(element), {
              cursor: {
                x: event.x,
                y: event.y,
              },
            });

            popupMenu.open(element, "ifml-replace", position);
          },
        },
      },
    });
  }

  // delete element entry, only show if allowed by rules
  var deleteAllowed = rules.allowed("elements.delete", {
    elements: [element],
  });

  if (isArray(deleteAllowed)) {
    // was the element returned as a deletion candidate?
    deleteAllowed = deleteAllowed[0] === element;
  }

  if (deleteAllowed) {
    assign(actions, {
      delete: {
        group: "edit",
        className: "icon-trash",
        title: translate("Remove"),
        action: {
          click: removeElement,
        },
      },
    });
  }

  return actions;
};
