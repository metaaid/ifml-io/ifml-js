import {
  pick,
  assign,
  filter,
  forEach,
  isArray,
  isUndefined,
  has
} from 'min-dash';

import {
  isExpanded
} from '../../util/DiUtil';

import {
  getPropertyNames
} from '../copy-paste/ModdleCopy';

function copyProperties(source, target, properties) {
  if (!isArray(properties)) {
    properties = [properties];
  }

  forEach(properties, function (property) {
    if (!isUndefined(source[property])) {
      target[property] = source[property];
    }
  });
}

var CUSTOM_PROPERTIES = [];


function toggeling(element, target) {

  var oldCollapsed = (
    element && has(element, 'collapsed') ? element.collapsed : !isExpanded(element)
  );

  var targetCollapsed;

  if (target && (has(target, 'collapsed') || has(target, 'isExpanded'))) {

    // property is explicitly set so use it
    targetCollapsed = (
      has(target, 'collapsed') ? target.collapsed : !target.isExpanded
    );
  } else {

    // keep old state
    targetCollapsed = oldCollapsed;
  }

  if (oldCollapsed !== targetCollapsed) {
    element.collapsed = oldCollapsed;
    return true;
  }

  return false;
}



/**
 * This module takes care of replacing BPMN elements
 */
export default function IfmlReplace(
  factory,
  elementFactory,
  moddleCopy,
  modeling,
  replace,
  selection
) {

  /**
   * Prepares a new business object for the replacement element
   * and triggers the replace operation.
   *
   * @param  {djs.model.Base} element
   * @param  {Object} target
   * @param  {Object} [hints]
   *
   * @return {djs.model.Base} the newly created element
   */
  function replaceElement(element, target, hints) {
    hints = hints || {};

    var type = target.type,
      oldBusinessObject = element.businessObject;

    var newBusinessObject = factory.create(type);

    var newElement = {
      type: type,
      businessObject: newBusinessObject,
    };

    newElement.di = {};

    // colors will be set to DI
    copyProperties(element.di, newElement.di, [
      'fill',
      'stroke',
    ]);

    var elementProps = getPropertyNames(oldBusinessObject.$descriptor),
      newElementProps = getPropertyNames(newBusinessObject.$descriptor, true),
      copyProps = intersection(elementProps, newElementProps);

    var properties = filter(copyProps, function(propertyName) {
      // so the applied properties from 'target' don't get lost
      if (has(newBusinessObject, propertyName)) {
        return false;
      }
    });

    // initialize special properties defined in target definition
    assign(newBusinessObject, pick(target, CUSTOM_PROPERTIES));

    newBusinessObject = moddleCopy.copyElement(
      oldBusinessObject,
      newBusinessObject,
      properties
    );

    newElement.di = {};

    // fill and stroke will be set to DI
    copyProperties(element.di, newElement.di, [
      'fill',
      'stroke'
    ]);

    newElement = replace.replaceElement(element, newElement, hints);

    if (hints.select !== false) {
      selection.select(newElement);
    }

    return newElement;
  }

  this.replaceElement = replaceElement;
}

IfmlReplace.$inject = [
  'factory',
  'elementFactory',
  'moddleCopy',
  'modeling',
  'replace',
  'selection'
];

/**
 * Compute intersection between two arrays.
 */
function intersection(a1, a2) {
  return a1.filter(function (el) {
    return a2.indexOf(el) !== -1;
  });
}