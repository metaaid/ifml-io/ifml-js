import translate from 'diagram-js/lib/i18n/translate';

import IfmlOrderingProvider from './IfmlOrderingProvider';

export default {
  __depends__: [
    translate
  ],
  __init__: [ 'ifmlOrderingProvider' ],
  ifmlOrderingProvider: [ 'type', IfmlOrderingProvider ]
};