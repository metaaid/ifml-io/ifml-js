import inherits from 'inherits';

import OrderingProvider from 'diagram-js/lib/features/ordering/OrderingProvider';

import { isAny } from '../modeling/util/ModelingUtil';

import { findIndex, find } from 'min-dash';
import { isExternalElement } from '../../util/LabelUtil';
import {
  getPositionInGfx, isInnerViewComponentPart,
} from '../vcp-place/ViewComponentPartsPlacement';

/**
 * a simple ordering provider that makes sure:
 *
 * (0) labels and groups are rendered always on top
 * (1) elements are ordered by a {level} property
 */
export default function IfmlOrderingProvider(
  eventBus,
  translate,
  elementRegistry
) {
  var self = this;
  OrderingProvider.call(this, eventBus);

  const DEFAULT_ORDER = 5;
  // external elemetns should be rendered on top
  var externalOrder = {
    level: 15,
  };

  var orders = [
    //TODO: OwnerAssociation = LVL 15
    {
      type: 'ifml:InteractionFlow',
      order: {
        level: 6,
        containers: [
          // 'ifml:ViewContainer',
          'ifml:Module',
          'ifml:ModuleDefinition',
          'ifml:InteractionFlowModel',
        ],
      },
    },
  ];

  function computeOrder(element) {
    if (element.labelTarget) {
      return {
        level: 10,
      };
    }

    if (isExternalElement(element)) {
      return externalOrder;
    }

    var entry = find(orders, function (o) {
      return isAny(element, [o.type]);
    });

    return (
      (entry && entry.order) || {
        level: DEFAULT_ORDER,
      }
    );
  }

  function getOrder(element) {
    var order = element.order;

    if (!order) {
      element.order = order = computeOrder(element);
    }

    return order;
  }

  function findActualParent(element, newParent, containers) {
    var actualParent = newParent;

    while (actualParent) {
      if (isAny(actualParent, containers)) {
        break;
      }

      actualParent = actualParent.parent;
    }

    if (!actualParent) {
      throw new Error(
        translate('no parent for {element} in {parent}', {
          element: element.$id,
          parent: newParent.id,
        })
      );
    }

    return actualParent;
  }

  this.getOrdering = function (element, newParent) {
    if (isExternalElement(element) && element) {
      return {
        index: -1,
      };
    }

    var elementOrder, insertIndex, currentIndex;
    // order viewcomponentparts by their y position
    if (isInnerViewComponentPart(element)) {
      var gfx = elementRegistry.getGraphics(element);
      if(!gfx){
        insertIndex = -1;
      } else {
        var gfxParent = elementRegistry.getGraphics(newParent);
        insertIndex = getPositionInGfx(gfx, gfxParent);
      }
    } else {
      elementOrder = getOrder(element);
      if (elementOrder.containers) {
        newParent = findActualParent(
          element,
          newParent,
          elementOrder.containers
        );
      }
      currentIndex = newParent.children.indexOf(element);
      insertIndex = findIndex(newParent.children, function (child) {

        // do not compare with labels, they are created
        // in the wrong order (right after elements) during import and
        // mess up the positioning.
        if (!element.labelTarget && child.labelTarget) {
          return false;
        }

        var childOrder = getOrder(child);

        return elementOrder.level < childOrder.level;
      });
      // if the element is already in the child list at
      // a smaller index, we need to adjust the insert index.
      // this takes into account that the element is being removed
      // before being re-inserted
      if (insertIndex !== -1) {
        if (currentIndex !== -1 && currentIndex < insertIndex) {
          insertIndex -= 1;
        }
      }
    }

    return {
      index: insertIndex,
      parent: newParent,
    };
  };
}

IfmlOrderingProvider.$inject = [
  'eventBus',
  'translate',
  'elementRegistry',
];

inherits(IfmlOrderingProvider, OrderingProvider);
