import {
  getBusinessObject,
  is
} from '../../util/ModelUtil';

const HIDE_NS = true;

function getSemanticLink(element) {
  var sem = getBusinessObject(element);

  if (is(sem, 'ifml:DynamicBehavior')) {
    sem = sem.get('behaviorConcept').get("behavior");
  }
  if (is(sem, 'ifml:UMLBehavior')) {
    sem = sem.get("behavior");
  }

  return sem;
}

function getLabelAttr(semantic) {
  if (is(semantic, 'uml:OpaqueBehavior')) {
    return 'body';
  }

  if (is(semantic, 'uml:NamedElement')) {
    return 'name';
  }

  if (is(semantic, 'ifml:Annotation')) {
    return 'text';
  }

  if (is(semantic, 'ifml:ActivationExpression')) {
    return 'body';
  }
}

function getTypedText(text) {
  if (!text) {
    return [null, null];
  }
  var split = text.split(":", 2);
  var name = split[0].trim();
  var type = null;

  if (split[1]) {
    type = split[1].trim();
  }

  return [name, type];
}

function getTypeAttr(semantic) {
  if (is(semantic, 'uml:TypedElement')) {
    return 'type';
  }
  return null;
}

export function getLabel(element) {
  var semantic = getSemanticLink(element.businessObject),
    attr = getLabelAttr(semantic),
    typeAttr = getTypeAttr(semantic),
    label = '';

  if (attr) {
    label = semantic[attr] || '';
  }

  if (typeAttr && semantic[typeAttr] && semantic[typeAttr]["name"]) {
    var type = (HIDE_NS) ? semantic[typeAttr]["ns"]["localName"] : semantic[typeAttr]["name"];
    label += " : " + type;
  }

  return label;
}


export function setLabel(element, text, isExternal, domainModelProvider) {
  var semantic = getSemanticLink(element.businessObject),
    attr = getLabelAttr(semantic),
    typeAttr = getTypeAttr(semantic);

  var [text, typeTxt] = getTypedText(text);

  if (attr) {
    semantic[attr] = text;
  }

  if (typeAttr && typeTxt) {
    semantic.set(typeAttr, domainModelProvider.getTypeFromName(typeTxt));
  }

  return element;
}