import SearchPadModule from 'diagram-js/lib/features/search-pad';

import IfmlSearchProvider from './IfmlSearchProvider';


export default {
  __depends__: [
    SearchPadModule
  ],
  __init__: [ 'ifmlSearch'],
  ifmlSearch: [ 'type', IfmlSearchProvider ]
};
