"use strict";

import { getBusinessObject } from "../../../util/ModelUtil";

import textInputField from "./TextInputEntryFactory";
import checkboxField from "./CheckboxEntryFactory";
import selectBoxField from "./SelectEntryFactory";
import comboBoxField from "./ComboEntryFactory";
import textBoxField from "./TextBoxEntryFactory";
import validationAwareTextInputField from "./ValidationAwareTextInput";
import tableField from "./TableEntryFactory";
import labelEntry from "./LabelFactory";
import link from "./LinkEntryFactory";
import autoSuggestTextBoxField from "./AutoSuggestTextBoxFactory";
import collapsible from "./CollapsibleEntryFactory";
import toggleSwitch from "./ToggleSwitchEntryFactory";

import cmdHelper from "../helper/CmdHelper";

// helpers ////////////////////////////////////////

function ensureNotNull(prop) {
  if (!prop) {
    throw new Error(prop + " must be set.");
  }

  return prop;
}

/**
 * sets the default parameters which are needed to create an entry
 *
 * @param options
 * @returns {{id: *, description: (*|string), get: (*|Function), set: (*|Function),
 *            validate: (*|Function), html: string}}
 */
var setDefaultParameters = function (options) {
  var getBo = function (element) {
    var el = (options && options.element) || element;
    var bo = (options && options.bo) || getBusinessObject(el);
    return bo;
  };

  // default method to fetch the current value of the input field
  var defaultGet = function (element) {
    var bo = getBo(element),
      res = {},
      prop = ensureNotNull(options.modelProperty);
    res[prop] = bo.get(prop);

    return res;
  };

  // default method to set a new value to the input field
  var defaultSet = function (element, values) {
    var el = options.element || element;
    var res = {},
      prop = ensureNotNull(options.modelProperty);

    if (values[prop] !== "") {
      res[prop] = values[prop];
    } else {
      res[prop] = undefined;
    }

    if (options.bo) {
      options.bo.set(prop, res[prop]);
      return cmdHelper.updateProperties(el, {});
    } else {
      return cmdHelper.updateProperties(el, res);
    }
  };

  // default validation method
  var defaultValidate = function () {
    return {};
  };

  return {
    id: options.id,
    description: options.description || "",
    get: options.get || defaultGet,
    set: options.set || defaultSet,
    validate: options.validate || defaultValidate,
    html: "",
  };
};

export default function EntryFactory() {}

/**
 * Generates an text input entry object for a property panel.
 * options are:
 * - id: id of the entry - String
 *
 * - description: description of the property - String
 *
 * - label: label for the input field - String
 *
 * - set: setter method - Function
 *
 * - get: getter method - Function
 *
 * - validate: validation mehtod - Function
 *
 * - modelProperty: name of the model property - String
 *
 * - buttonAction: Object which contains the following properties: - Object
 * ---- name: name of the [data-action] callback - String
 * ---- method: callback function for [data-action] - Function
 *
 * - buttonShow: Object which contains the following properties: - Object
 * ---- name: name of the [data-show] callback - String
 * ---- method: callback function for [data-show] - Function
 *
 * @param options
 * @returns the propertyPanel entry resource object
 */
EntryFactory.textField = function (translate, options) {
  return textInputField(translate, options, setDefaultParameters(options));
};

EntryFactory.validationAwareTextField = function (translate, options) {
  return validationAwareTextInputField(
    translate,
    options,
    setDefaultParameters(options)
  );
};

/**
 * Generates a checkbox input entry object for a property panel.
 * options are:
 * - id: id of the entry - String
 *
 * - description: description of the property - String
 *
 * - label: label for the input field - String
 *
 * - set: setter method - Function
 *
 * - get: getter method - Function
 *
 * - validate: validation method - Function
 *
 * - modelProperty: name of the model property - String
 *
 * @param options
 * @returns the propertyPanel entry resource object
 */
EntryFactory.checkbox = function (translate, options) {
  return checkboxField(translate, options, setDefaultParameters(options));
};

EntryFactory.textBox = function (translate, options) {
  return textBoxField(translate, options, setDefaultParameters(options));
};

EntryFactory.selectBox = function (translate, options) {
  return selectBoxField(translate, options, setDefaultParameters(options));
};

EntryFactory.comboBox = function (translate, options) {
  return comboBoxField(translate, options);
};

EntryFactory.table = function (translate, options) {
  return tableField(translate, options);
};

EntryFactory.label = function (options) {
  return labelEntry(options);
};

EntryFactory.link = function (translate, options) {
  return link(translate, options);
};

EntryFactory.autoSuggest = function (translate, options) {
  return autoSuggestTextBoxField(
    translate,
    options,
    setDefaultParameters(options)
  );
};

EntryFactory.collapsible = function (options) {
  return collapsible(options);
};

EntryFactory.toggleSwitch = function (translate, options) {
  return toggleSwitch(translate, options, setDefaultParameters(options));
};
