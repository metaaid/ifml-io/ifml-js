'use strict';

import { forEach } from 'min-dash';
import CreateAndReferenceElementHandler from './CreateAndReferenceHandler';
import CreateBusinessObjectListHandler from './CreateBusinessObjectListHandler';
import MultiCommandHandler from './MultiCommandHandler';
import UpdateBusinessObjectListHandler from './UpdateBusinessObjectListHandler';
import UpdateBusinessObjectHandler from './UpdateBusinessObjectHandler';

var HANDLERS = {
  'properties-panel.update-businessobject': UpdateBusinessObjectHandler,
  'properties-panel.create-and-reference': CreateAndReferenceElementHandler,
  'properties-panel.create-businessobject-list': CreateBusinessObjectListHandler,
  'properties-panel.update-businessobject-list': UpdateBusinessObjectListHandler,
  'properties-panel.multi-command-executor': MultiCommandHandler
};


function CommandInitializer(eventBus, commandStack) {

  eventBus.on('diagram.init', function() {
    forEach(HANDLERS, function(handler, id) {
      commandStack.registerHandler(id, handler);
    });
  });
}

CommandInitializer.$inject = ['eventBus', 'commandStack'];

export default {
  __init__: [CommandInitializer]
};