import inherits from 'inherits';

import PropertiesActivator from '../PropertiesActivator';
import idProps from './parts/IdProps';
import nameProps from './parts/NameProps';
import viewComponentPartsProps from './parts/ViewComponentPartsProps';
import expressionsProps from './parts/ExpressionsProps';
import visualizationAttribute from './parts/VisualizationAttributeProps';
import parameterBindingProps from './parts/ParameterBindingProps';
import viewContainerProps from './parts/ViewContainerProps';
import viewComponentProp from './parts/ViewComponentProp';

function createGeneralTabGroups(
  element, canvas, ifmlFactory,
  elementRegistry, translate, domainModelProvider) {

  var generalGroup = {
    id: 'general',
    label: translate('General') + " (" + element.type + ")",
    entries: []
  };
  idProps(generalGroup, element, translate);
  nameProps(generalGroup, element, ifmlFactory, canvas, translate);

  viewComponentPartsProps(generalGroup, element, ifmlFactory, translate, domainModelProvider);
  viewContainerProps(generalGroup, element, ifmlFactory, translate, domainModelProvider);
  viewComponentProp(generalGroup, element, ifmlFactory, translate, domainModelProvider);
  expressionsProps(generalGroup, element, ifmlFactory, translate, domainModelProvider);
  visualizationAttribute(generalGroup, element, ifmlFactory, translate, domainModelProvider);
  parameterBindingProps(generalGroup, element, ifmlFactory, translate, domainModelProvider);

  var documentationGroup = {
    id: 'documentation',
    label: translate('Documentation'),
    entries: []
  };

  // documentationProps(documentationGroup, element, ifmlFactory, translate);

  return [
    generalGroup,

    // detailsGroup,
    documentationGroup
  ];

}

export default function IfmlPropertiesProvider(
  eventBus, canvas, ifmlFactory, elementRegistry, translate, domainModelProvider) {

  PropertiesActivator.call(this, eventBus);

  this.getTabs = function (element) {

    var generalTab = {
      id: 'general',
      label: translate('General'),
      groups: createGeneralTabGroups(
        element, canvas, ifmlFactory, elementRegistry, translate, domainModelProvider)
    };

    return [
      generalTab
    ];
  };
}

IfmlPropertiesProvider.$inject = [
  'eventBus',
  'canvas',
  'factory',
  'elementRegistry',
  'translate',
  'domainModelProvider',
];

inherits(IfmlPropertiesProvider, PropertiesActivator);