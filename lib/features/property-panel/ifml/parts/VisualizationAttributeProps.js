import entryFactory from '../../factory/EntryFactory';
import { getBusinessObject, is } from '../../../../util/ModelUtil';
import cmdHelper from '../../helper/CmdHelper';

/**
 * Generate a form field specific textField using entryFactory.
 *
 * @param {Function} translate
 * @param {string} options.id
 * @param {string} options.label
 * @param {string} options.modelProperty
 * @param {Function} options.validate
 *
 * @return {Object} an entryFactory.textField object
 */
export default function(group, element, ifmlFactory, translate, domainModelProvider) {

  if (!is(element, 'ifml:VisualizationAttribute')) {
    return;
  }

  var classifier = null;
  var visualizationAttribute = getBusinessObject(element);
  var dataBinding = visualizationAttribute.$parent;

  if (dataBinding && dataBinding.domainConcept) {
    classifier = dataBinding.domainConcept.classifier;
  }

  var typeOptions = [];
  var properties = domainModelProvider.getStructuralFeatures(classifier);
  for (var id in properties) {
    var typeName = properties[id].name;
    typeOptions.push({
      name: typeName,
      value: typeName
    });
  }

  // [FormData] form field type combo box
  group.entries.push(entryFactory.comboBox(translate, {
    id: 'form-field-properties',
    label: translate('Structural Feature'),
    selectOptions: typeOptions,
    modelProperty: 'featureConcept',
    emptyParameter: true,

    get: function(e, node) {

      var featureConcept = visualizationAttribute.featureConcept;
      if (featureConcept && featureConcept.structuralFeature && featureConcept.structuralFeature.name) {
        return {
          'featureConcept': featureConcept.structuralFeature.name
        };
      }
      return {};
    },
    set: function(e, value, node) {
      if (value) {
        var feature = domainModelProvider.createStructuralFeatureLink(classifier, value.featureConcept);

        return [cmdHelper.updateBusinessObject(element, visualizationAttribute, { featureConcept: feature })];
      }
    }
  }));
}
