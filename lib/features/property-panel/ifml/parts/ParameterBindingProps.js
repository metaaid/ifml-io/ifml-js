import { getBusinessObject, is } from '../../../../util/ModelUtil';
import parameterProps from './ParameterProps';

/**
 * Generate a form field specific textField using entryFactory.
 *
 * @param {Function} translate
 * @param {string} options.id
 * @param {string} options.label
 * @param {string} options.modelProperty
 * @param {Function} options.validate
 *
 * @return {Object} an entryFactory.textField object
 */
export default function(group, element, ifmlFactory, translate, domainModelProvider) {

  if (!is(element, 'ifml:ParameterBinding')) {
    return;
  }

  var bo = getBusinessObject(element);
  var sourceParameter = bo.sourceParameter;
  var targetParameter = bo.targetParameter;

  parameterProps(group, element, ifmlFactory, translate, domainModelProvider, 'Source Parameter', 'source', sourceParameter);
  parameterProps(group, element, ifmlFactory, translate, domainModelProvider, 'Target Parameter', 'target', targetParameter);
}
