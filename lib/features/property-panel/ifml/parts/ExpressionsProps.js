import entryFactory from '../../factory/EntryFactory';
import { getBusinessObject, is } from '../../../../util/ModelUtil';

/**
 * Generate a form field specific textField using entryFactory.
 *
 * @param {Function} translate
 * @param {string} options.id
 * @param {string} options.label
 * @param {string} options.modelProperty
 * @param {Function} options.validate
 *
 * @return {Object} an entryFactory.textField object
 */
export default function(group, element, ifmlFactory, translate, domainModelProvider, bo, addId) {
  bo = bo || null;
  addId = addId || '';

  bo = bo || getBusinessObject(element);

  if (!is(bo, 'ifml:Expression')) {
    return;
  }

  group.entries.push(entryFactory.textField(translate, {
    id: 'form-field-language' + addId,
    label: translate('Language'),
    modelProperty: 'language',
    bo: bo
  }));

  group.entries.push(entryFactory.textField(translate, {
    id: 'form-field-body' + addId,
    label: translate('Code of the Expression'),
    modelProperty: 'body',
    bo: bo
  }));
}
