// 'use strict';

//     properties = require('./implementation/Properties'),
//     find = require('lodash/find'),
//     each = require('lodash/forEach');


import cmdHelper from '../../helper/CmdHelper';
import elementHelper from '../../helper/ElementHelper';
import entryFactory from '../../factory/EntryFactory';
import formHelper from '../../helper/FormHelper';

// import viewComponentParts from '../../helper/FormHelper'

import {
  getBusinessObject,
  is
} from '../../../../util/ModelUtil';
import viewComponentParts from './implementation/ViewComponentElements';

import ViewComponentPartHelper from '../../helper/ViewComponentPartHelper';



import find from 'lodash/find';

var removeEntry = ViewComponentPartHelper.removeEntry;
var getEntries = ViewComponentPartHelper.getViewComponentParts;

function ensureFormKeyAndDataSupported(element) {
  return is(element, 'ifml:ViewComponent') || is(element, 'ifml:DataBinding');
}

export default function (group, element, ifmlFactory, translate, domainModelProvider) {

  if (!ensureFormKeyAndDataSupported(element)) {
    return;
  }

  // element = getBusinessObject(element);

  /**
   * Return the currently selected form field querying the form field select box
   * from the DOM.
   *
   * @param  {djs.model.Base} element
   * @param  {DOMElement} node - DOM element of any form field text input
   *
   * @return {ModdleElement} the currently selected form field
   */
  function getSelectedViewComponentPart(element, node) {
    if (is(element, 'ifml:ViewComponentPart')) {
      return getBusinessObject(element);
    }
    if (is(element, 'ifml:DataBinding')) {
      return element;
    }
    var selected = partSelection.getSelected(element, node.parentNode);

    if (selected.idx === -1) {
      return;
    }

    var viewComponent = getBusinessObject(element);

    return ViewComponentPartHelper.getPartByIndex(viewComponent, selected.idx);
  }

  if (is(element, 'ifml:ViewContainer')) {

    var vpTypeSelect = entryFactory.selectBox(translate, {
      id: 'viewContainerType',
      label: translate('Component Type'),
      modelProperty: 'type',
      description: 'the type of the part that should be added below',
      selectOptions: [{
        'name': 'List',
        'value': 'ifml:List'
      }, {
        'name': 'Form',
        'value': 'ifml:Form'
      }, {
        'name': 'Details',
        'value': 'ifml:Details'
      }],
      get: function (element, node) {
        return {
          type: node.value
        };
      },
      set: function (element, values, node) {}
    });
  }
  if (is(element, 'ifml:ViewComponent')) {


    var vpTypeSelect = entryFactory.selectBox(translate, {
      id: 'viewComponentType',
      label: translate('Type of new Part'),
      modelProperty: 'type',
      description: 'the type of the part that should be added below',
      selectOptions: [{
        'name': 'Data binding',
        'value': 'ifml:DataBinding'
      }, {
        'name': 'Conditional expression',
        'value': 'ifml:DynamicBehavior'
      }, {
        'name': 'Visualization attribute',
        'value': 'ifml:VisualizationAttribute'
      }, {
        'name': 'Dynamic behavior',
        'value': 'ifml:DynamicBehavior'
      }],
      get: function (element, node) {
        return {
          type: node.value
        };
      },
      set: function (element, values, node) {

        // console.log(values, node);
        // var bo = getBusinessObject(element),
        // activationExpression = values.activationExpression || undefined;

        // return cmdHelper.updateBusinessObject(element, bo, { 'activationExpression': activationExpression });
      }
    });

    group.entries.push(vpTypeSelect);

    // [FormData] form field select box
    var partSelection = viewComponentParts(element, ifmlFactory, {
      id: 'view-component-parts',
      label: translate('ViewComponent parts'),
      modelProperty: 'id',
      prefix: 'vcPart',
      createViewComponentPart: function (element, viewComponentParts, value) {
        var select = vpTypeSelect.select;
        var type = select.value;
        var bo = getBusinessObject(element);
        var commands = [];

        if (!viewComponentParts) {
          viewComponentParts = [];
        }

        var viewComponentPart = elementHelper.createElement(type, {
          values: []
        }, bo, ifmlFactory);

        var updateCmd = cmdHelper.addElementsTolist(
          element,
          bo,
          'viewComponentParts',
          [viewComponentPart],
          []
        );

        commands.push(updateCmd);

        return updateCmd;
      },
      removeExtensionElement: function (element, viewComponentParts, value, idx) {
        var formData = getEntries(getBusinessObject(element), 'ifml:ViewComponentPart')[0],
          entry = formData.fields[idx],
          commands = [];

        if (formData.fields.length < 2) {
          commands.push(removeEntry(getBusinessObject(element), element, formData));
        } else {
          commands.push(cmdHelper.removeElementsFromList(element, formData, 'fields', null, [entry]));

          if (entry.id === formData.get('businessKey')) {
            commands.push(cmdHelper.updateBusinessObject(element, formData, {
              'businessKey': undefined
            }));
          }
        }

        return commands;
      },
      getElements: function (element) {
        return getEntries(getBusinessObject(element));
      },
      hideExtensionElements: function (element, node) {
        return false;
      }
    });
    group.entries.push(partSelection);


    // [FormData] Form Field label
    group.entries.push(entryFactory.label({
      id: 'form-field-header',
      labelText: translate('Viewcomponent part'),
      showLabel: function (element, node) {
        return !!getSelectedViewComponentPart(element, node);
      }
    }));

    // [FormData] form field id text input field
    group.entries.push(entryFactory.validationAwareTextField(translate, {
      id: 'form-field-id',
      label: translate('ID (process variable name)'),
      modelProperty: 'name',

      getProperty: function (element, node) {
        var selectedFormField = getSelectedViewComponentPart(element, node) || {};

        return selectedFormField.id;
      },

      setProperty: function (element, properties, node) {
        var formField = getSelectedViewComponentPart(element, node);

        return cmdHelper.updateBusinessObject(element, formField, properties);
      },

      hidden: function (element, node) {
        return !getSelectedViewComponentPart(element, node);
      },

      validate: function (element, values, node) {

        var formField = getSelectedViewComponentPart(element, node);

        if (formField) {

          var idValue = values.id;

          if (!idValue || idValue.trim() === '') {
            return {
              id: 'Form field id must not be empty'
            };
          }

          var formFields = formHelper.getFormFields(element);

          var existingFormField = find(formFields, function (f) {
            return f !== formField && f.id === idValue;
          });

          if (existingFormField) {
            return {
              id: 'Form field id already used in form data.'
            };
          }
        }
      }
    }));

  }

  var typeOptions = [];
  var types = domainModelProvider.getTypes();
  for (var id in types) {
    var typeName = types[id].fullName;
    typeOptions.push({
      name: typeName,
      value: id
    });
  }

  // [FormData] form field type combo box
  group.entries.push(entryFactory.comboBox(translate, {
    id: 'form-field-type',
    label: translate('Type'),
    selectOptions: typeOptions,
    modelProperty: 'domainConcept',
    emptyParameter: true,

    get: function (e, node) {
      var selectedViewComponentPart = getSelectedViewComponentPart(element, node);

      if (selectedViewComponentPart) {
        var domainConcept = selectedViewComponentPart.get('domainConcept');
        if (domainConcept && domainConcept.classifier && domainConcept.classifier.name) {
          return {
            'domainConcept': domainConcept.classifier.name
          };
        }
        return {};
      } else {
        return {};
      }
    },
    set: function (e, value, node) {
      if (value && value.domainConcept) {
        var selectedViewComponentPart = getSelectedViewComponentPart(element, node);
        var concept = domainModelProvider.getDomainElementsFromURI(value.domainConcept);

        return cmdHelper.updateBusinessObject(element, selectedViewComponentPart, {
          domainConcept: concept
        });
      }
    },
    hidden: function (e, node) {
      return !(getSelectedViewComponentPart(element, node) || is(element, 'ifml:DataBinding'));
    }
  }));
}