
import nameEntryFactory from './implementation/Name';
import CategoryHelper from '../../helper/CategoryHelper';

import { getBusinessObject, is } from '../../../../util/ModelUtil';

export default function(group, element, ifmlFactory, canvas, translate, options) {
  if (!options) {
    options = {};
  }

  function initializeCategory(semantic) {
    var rootElement = canvas.getRootElement(),
        definitions = getBusinessObject(rootElement).$parent,
        categoryValue = CategoryHelper.createCategoryValue(definitions, ifmlFactory);

    semantic.categoryValueRef = categoryValue;

  }

  function setGroupName(element, values) {
    var bo = getBusinessObject(element),
        categoryValueRef = bo.categoryValueRef;

    if (!categoryValueRef) {
      initializeCategory(bo);
    }

    // needs direct call to update categoryValue properly
    return {
      cmd: 'element.updateLabel',
      context: {
        element: element,
        newLabel: values.categoryValue
      }
    };
  }

  function getGroupName(element) {
    var bo = getBusinessObject(element),
        value = (bo.categoryValueRef || {}).value;

    return { categoryValue: value };
  }

  var nameOptions = {
    id: options.id
  };

  if (is(element, 'ifml:Annotation')) {
    nameOptions.modelProperty = 'text';
    nameOptions.label = translate('Text');
  }

  // name
  group.entries = group.entries.concat(nameEntryFactory(element, nameOptions, translate));
}
