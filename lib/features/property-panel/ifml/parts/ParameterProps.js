import entryFactory from '../../factory/EntryFactory';
import {
  getBusinessObject,
  is
} from '../../../../util/ModelUtil';
import typedProps from './TypedProps';
import expressionsProps from './ExpressionsProps';
import idProps from './IdProps';

/**
 * Generate a form field specific textField using entryFactory.
 *
 * @param {Function} translate
 * @param {string} options.id
 * @param {string} options.label
 * @param {string} options.modelProperty
 * @param {Function} options.validate
 *
 * @return {Object} an entryFactory.textField object
 */
export default function (group, element, ifmlFactory, translate, domainModelProvider, parameterName, id, bo) {
  parameterName = parameterName || 'Parameter';
  id = id || 'single';

  bo = bo && getBusinessObject(bo) || getBusinessObject(element);

  if (!is(bo, 'ifml:Parameter')) {
    return;
  }

  var parameter = getBusinessObject(element);

  group.entries.push(entryFactory.textField(translate, {
    id: 'form-field-parameter-name' + id,
    label: translate(parameterName + ' Name'),
    modelProperty: 'name',
    bo: bo
  }));

  idProps(group, element, translate, {
    id: 'parameter-id' + id
  }, bo);

  var defaultValue = bo.get('defaultValue');
  // if (!bo.get('defaultValue')) {
  //   defaultValue = ifmlFactory.create('ifml:Expression');
  //   bo.set('defaultValue', defaultValue);
  // }
  expressionsProps(group, parameter.defaultValue, ifmlFactory, translate, domainModelProvider, defaultValue, id);
  typedProps(group, parameter, ifmlFactory, translate, domainModelProvider, bo, id);

  group.entries.push(entryFactory.selectBox(translate, {
    id: 'form-parameter-lowervalue' + id,
    label: translate(parameterName + ' direction'),
    modelProperty: 'direction',
    selectOptions: [{
      name: 'in',
      value: 'in'
    }, {
      name: 'out',
      value: 'out'
    }],
    bo: bo
  }));

  group.entries.push(entryFactory.textField(translate, {
    id: 'form-field-parameter-lowervalue' + id,
    label: translate(parameterName + ' lower value'),
    modelProperty: 'lowerValue',
    bo: bo
  }));

  group.entries.push(entryFactory.textField(translate, {
    id: 'form-parameter-uppervalue' + id,
    label: translate(parameterName + ' upper value'),
    modelProperty: 'upperValue',
    bo: bo
  }));

  group.entries.push(entryFactory.checkbox(translate, {
    id: 'form-field-parameter-isordered' + id,
    label: translate(parameterName + ' is orderd'),
    modelProperty: 'isOrdered',
    bo: bo
  }));
}