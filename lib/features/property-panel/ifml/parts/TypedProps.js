import entryFactory from '../../factory/EntryFactory';
import { is } from '../../../../util/ModelUtil';
import cmdHelper from '../../helper/CmdHelper';

/**
 * Generate a form field specific textField using entryFactory.
 *
 * @param {Function} translate
 * @param {string} options.id
 * @param {string} options.label
 * @param {string} options.modelProperty
 * @param {Function} options.validate
 *
 * @return {Object} an entryFactory.textField object
 */
export default function(group, element, ifmlFactory, translate, domainModelProvider, bo, addId) {
  addId = addId || '';
  bo = bo || getBusinessObject(element);

  if (!is(bo, 'ifml:TypedElement')) {
    return;
  }
  var typeOptions = [];
  var types = domainModelProvider.getTypes();
  for (var id in types) {
    var typeName = types[id].name;
    typeOptions.push({
      name: typeName,
      value: id
    });
  }

  group.entries.push(entryFactory.comboBox(translate, {
    id: 'form-field-type' + addId,
    label: translate('Type'),
    selectOptions: typeOptions,
    modelProperty: 'type',
    emptyParameter: true,
    bo: bo,

    get: function(e, node) {
      var type = bo.get('type');
      if (type && type.name) {
        return {
          'type': type.name
        };
      }
      return {};
    },
    set: function(e, value, node) {
      if (value) {
        var type = domainModelProvider.createTypeByURI(value.type);

        return cmdHelper.updateBusinessObject(element, bo, { type: type });
      }
    }
  }));
}
