// 'use strict';

//     properties = require('./implementation/Properties'),
//     find = require('lodash/find'),
//     each = require('lodash/forEach');


import cmdHelper from '../../helper/CmdHelper';
import elementHelper from '../../helper/ElementHelper';
import entryFactory from '../../factory/EntryFactory';
import formHelper from '../../helper/FormHelper';

// import viewComponentParts from '../../helper/FormHelper'

import {
  getBusinessObject,
  is
} from '../../../../util/ModelUtil';
import viewComponentParts from './implementation/ViewComponentElements';

import ViewComponentPartHelper from '../../helper/ViewComponentPartHelper';



import find from 'lodash/find';
import CmdHelper from '../../helper/CmdHelper';

var removeEntry = ViewComponentPartHelper.removeEntry;
var getEntries = ViewComponentPartHelper.getViewComponentParts;

function ensureFormKeyAndDataSupported(element) {
  return is(element, 'ifml:ViewContainer');
}

export default function (group, element, ifmlFactory, translate, domainModelProvider) {

  if (!ensureFormKeyAndDataSupported(element)) {
    return;
  }

  let subtype = entryFactory.selectBox(translate, {
    id: 'viewComponentType',
    label: translate('Container Type'),
    modelProperty: 'type',
    description: 'the type of the part that should be added below',
    selectOptions: [{
      'name': 'None',
      'value': 'ifml:ViewContainer'
    }, {
      'name': 'Menu',
      'value': 'ifml:Menu'
    }, {
      'name': 'Window',
      'value': 'ifml:Window'
    }],
    get: function (element, node) {
      let type = element.businessObject.$type;

      return {
        type: type
      };
    },
    set: function (element, value, node) {
      element.businessObject.$type = value;

      CmdHelper.updateBusinessObject(element, businessObject, newProperties);
    }
  });
  group.entries.push(subtype);
}