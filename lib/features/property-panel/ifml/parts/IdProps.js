import entryFactory from "../../factory/EntryFactory";
import cmdHelper from "../../helper/CmdHelper";
import { isIdValid } from "../../Utils";
import { getBusinessObject } from "../../../../util/ModelUtil";

export default function (group, element, translate, options, bo) {
  if (!options) {
    options = {};
  }

  var description = options && options.description;
  bo = bo || getBusinessObject(element);

  // Id
  group.entries.push(
    entryFactory.validationAwareTextField(translate, {
      id: "$id",
      label: translate("Id"),
      description: description && translate(description),
      modelProperty: "$id",
      getProperty: function () {
        return bo.$id;
      },
      setProperty: function (element, properties) {
        element = element.labelTarget || element;

        return cmdHelper.updateProperties(element, properties);
      },
      validate: function (e, values) {
        var idValue = values.$id;
        var idError = isIdValid(bo, idValue, translate);

        return idError
          ? {
              id: idError,
            }
          : {};
      },
    })
  );
}
