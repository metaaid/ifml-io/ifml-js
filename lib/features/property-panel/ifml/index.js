import IfmlPropertiesProvider from './IFMLPropertiesProvider';

export default {
  __init__: ['propertiesProvider'],
  propertiesProvider: ['type', IfmlPropertiesProvider]
};