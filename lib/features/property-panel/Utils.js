import {
  query as domQuery,
  clear as domClear,
  classes as domClasses,
  domify
} from 'min-dom';

import { is } from '../../util/ModelUtil';

import { forEach } from 'min-dash';

import Ids from 'ids';


var SPACE_REGEX = /\s/;

// for QName validation as per http://www.w3.org/TR/REC-xml/#NT-NameChar
var QNAME_REGEX = /^([a-z][\w-.]*:)?[a-z_][\w-.]*$/i;

// for ID validation as per BPMN Schema (QName - Namespace)
var ID_REGEX = /^[a-z_][\w-.]*$/i;

var HTML_ESCAPE_MAP = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  '\'': '&#39;'
};

export function selectedOption(selectBox) {
  if (selectBox.selectedIndex >= 0) {
    return selectBox.options[selectBox.selectedIndex].value;
  }
}


export function selectedType(elementSyntax, inputNode) {
  var typeSelect = domQuery(elementSyntax, inputNode);
  return selectedOption(typeSelect);
}


/**
 * Retrieve the root element the document this
 * business object is contained in.
 *
 * @return {ModdleElement}
 */
export function getRoot(businessObject) {
  var parent = businessObject;
  while (parent.$parent) {
    parent = parent.$parent;
  }
  return parent;
}


/**
 * filters all elements in the list which have a given type.
 * removes a new list
 */
export function filterElementsByType(objectList, type) {
  var list = objectList || [];
  var result = [];
  forEach(list, function(obj) {
    if (is(obj, type)) {
      result.push(obj);
    }
  });
  return result;
}


export function findRootElementsByType(businessObject, referencedType) {
  var root = getRoot(businessObject);

  return filterElementsByType(root.rootElements, referencedType);
}


export function removeAllChildren(domElement) {
  while (domElement.firstChild) {
    domElement.removeChild(domElement.firstChild);
  }
}


/**
 * adds an empty option to the list
 */
export function addEmptyParameter(list) {
  return list.push({ 'label': '', 'value': '', 'name': '' });
}


/**
 * returns a list with all root elements for the given parameter 'referencedType'
 */
export function refreshOptionsModel(businessObject, referencedType) {
  var model = [];
  var referableObjects = findRootElementsByType(businessObject, referencedType);
  forEach(referableObjects, function(obj) {
    model.push({
      label: (obj.name || '') + ' (id=' + obj.id + ')',
      value: obj.id,
      name: obj.name
    });
  });
  return model;
}


/**
 * fills the drop down with options
 */
export function updateOptionsDropDown(domSelector, businessObject, referencedType, entryNode) {
  var options = refreshOptionsModel(businessObject, referencedType);
  addEmptyParameter(options);
  var selectBox = domQuery(domSelector, entryNode);
  domClear(selectBox);

  forEach(options, function(option) {
    var optionEntry = domify('<option value="' + escapeHTML(option.value) + '">' + escapeHTML(option.label) + '</option>');
    selectBox.appendChild(optionEntry);
  });
  return options;
}


/**
 * checks whether the id value is valid
 *
 * @param {ModdleElement} bo
 * @param {String} idValue
 * @param {Function} translate
 *
 * @return {String} error message
 */
export function isIdValid(bo, idValue, translate) {
  var assigned = bo.$model.ids.assigned(idValue);

  var idExists = assigned && assigned !== bo;

  if (!idValue || idExists) {
    return translate('Element must have an unique id.');
  }

  return validateId(idValue, translate);
}


export function validateId(idValue, translate) {

  if (containsSpace(idValue)) {
    return translate('Id must not contain spaces.');
  }

  if (!ID_REGEX.test(idValue)) {

    if (QNAME_REGEX.test(idValue)) {
      return translate('Id must not contain prefix.');
    }

    return translate('Id must be a valid QName.');
  }
}


export function containsSpace(value) {
  return SPACE_REGEX.test(value);
}


/**
 * generate a semantic id with given prefix
 */
export function nextId(prefix) {
  var ids = new Ids([32, 32, 1]);

  return ids.nextPrefixed(prefix);
}


export function triggerClickEvent(element) {
  var evt;
  var eventType = 'click';

  if (document.createEvent) {
    try {

      // Chrome, Safari, Firefox
      evt = new MouseEvent((eventType), { view: window, bubbles: true, cancelable: true });
    } catch (e) {

      // IE 11, PhantomJS (wat!)
      evt = document.createEvent('MouseEvent');

      evt.initEvent((eventType), true, true);
    }
    return element.dispatchEvent(evt);
  } else {

    // Welcome IE
    evt = document.createEventObject();

    return element.fireEvent('on' + eventType, evt);
  }
}


export function escapeHTML(str) {
  str = '' + str;

  return str && str.replace(/[&<>"']/g, function(match) {
    return HTML_ESCAPE_MAP[match];
  });
}


export function createDropdown(dropdown) {
  var menu = dropdown.menu;

  var dropdownNode = domify(
    '<div class="group__dropdown">' +
    '<button class="group__dropdown-button">' +
    '<svg width="16" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path fill="currentColor" d="M96 184c39.8 0 72 32.2 72 72s-32.2 72-72 72-72-32.2-72-72 32.2-72 72-72zM24 80c0 39.8 32.2 72 72 72s72-32.2 72-72S135.8 8 96 8 24 40.2 24 80zm0 352c0 39.8 32.2 72 72 72s72-32.2 72-72-32.2-72-72-72-72 32.2-72 72z"></path></svg>' +
    '</button>' +
    '<div class="group__dropdown-menu"></div>' +
    '</div>'
  );

  var buttonNode = domQuery('.group__dropdown-button', dropdownNode),
      menuNode = domQuery('.group__dropdown-menu', dropdownNode);

  buttonNode.addEventListener('click', function(event) {
    domClasses(dropdownNode).toggle('group__dropdown--open');

    createOnGlobalClick(event);
  });

  forEach(menu, function(menuItem) {
    var menuItemNode = domify('<div class="group__dropdown-menu-item" data-dropdown-action="' +
      menuItem.id +
      '">' + escapeHTML(menuItem.label) + '</div>');

    menuItemNode.addEventListener('click', function() {
      menuItem.onClick();

      domClasses(dropdownNode).remove('group__dropdown--open');
    });

    menuNode.appendChild(menuItemNode);
  });

  var _onGlobalClick;

  function createOnGlobalClick(_event) {
    function onGlobalClick(event) {
      if (event === _event) {
        return;
      }

      var target = event.target;

      if (menuNode !== target && !menuNode.contains(target)) {
        domClasses(dropdownNode).remove('group__dropdown--open');

        document.removeEventListener('click', onGlobalClick);
      }
    }

    if (_onGlobalClick) {
      document.removeEventListener('click', _onGlobalClick);
    }

    document.addEventListener('click', onGlobalClick);

    _onGlobalClick = onGlobalClick;
  }

  return dropdownNode;
}
