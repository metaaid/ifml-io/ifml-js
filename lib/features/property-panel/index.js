
import PropertiesPanel from './PropertiesPanel';
import Cmd from './cmd';

export default {
  __depends__: [
    Cmd,

    // translate
  ],
  __init__: ['propertiesPanel'],
  propertiesPanel: ['type', PropertiesPanel]
};
