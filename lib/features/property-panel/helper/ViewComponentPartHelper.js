import { is } from '../../../util/ModelUtil';
import elementHelper from './ElementHelper';
import cmdHelper from './CmdHelper';

export default function ViewComponentPartHelper() { }

var getViewComponentParts = function(bo) {
  return bo.get('viewComponentParts') || [];
};

ViewComponentPartHelper.getViewComponentParts = function(bo, type) {
  var viewComponentParts = getViewComponentParts(bo);

  return viewComponentParts;

  // if (typeof viewComponentParts !== 'undefined') {
  //   var extensionValues = viewComponentParts.get('values');
  //   if (typeof extensionValues !== 'undefined') {
  //     var elements = extensionValues.filter(function (value) {
  //       return is(value, type);
  //     });
  //     if (elements.length) {
  //       return elements;
  //     }
  //   }
  // }
};

ViewComponentPartHelper.addEntry = function(bo, element, entry, bpmnFactory) {
  var viewComponentParts = bo.get('viewComponentParts');

  // if there is no viewComponentParts list, create one
  if (!viewComponentParts) {

    // TODO: Ask Daniel which operation costs more
    viewComponentParts = elementHelper.createElement('bpmn:ExtensionElements', { values: [entry] }, bo, bpmnFactory);
    return { viewComponentParts: viewComponentParts };
  } else {

    // add new failedJobRetryExtensionElement to existing viewComponentParts list
    return cmdHelper.addElementsTolist(element, viewComponentParts, 'values', [entry]);
  }
};

ViewComponentPartHelper.removeEntry = function(bo, element, entry) {
  var viewComponentParts = bo.get('viewComponentParts');

  if (!viewComponentParts) {

    // return an empty command when there is no viewComponentParts list
    return {};
  }

  return cmdHelper.removeElementsFromList(element, viewComponentParts, 'values', 'viewComponentParts', [entry]);
};
ViewComponentPartHelper.getPartByIndex = function(bo, idx) {

  var parts = this.getViewComponentParts(bo);

  return parts[idx];
};

