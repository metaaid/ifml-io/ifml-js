import DistributeElementsModule from 'diagram-js/lib/features/distribute-elements';

import IfmlDistributeElements from './IfmlDistributeElements';
import DistributeElementsMenuProvider from './DistributeElementsMenuProvider';


export default {
  __depends__: [
    DistributeElementsModule
  ],
  __init__: [ 'IfmlDistributeElements' ],
  IfmlDistributeElements: [ 'type', IfmlDistributeElements ],
  distributeElementsMenuProvider: [ 'type', DistributeElementsMenuProvider ]
};
