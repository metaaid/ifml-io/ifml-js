import {
  filter
} from 'min-dash';

import {
  isAny
} from '../modeling/util/ModelingUtil';


/**
 * Registers element exclude filters for elements that
 * currently do not support distribution.
 */
export default function IfmlDistributeElements(distributeElements) {

  distributeElements.registerFilter(function (elements) {
    return filter(elements, function (element) {
      var cannotDistribute = isAny(element, [
        'ifml:InteractionFlow',
        'ifml:Event',
        'ifml:Parameter',
        'ifml:Expression',
        'ifml:ParameterBinding',
        'ifml:ParameterBindingGroup',
        'ifml:Port',
        'ifml:PortDefinition',
        'ifml:ViewComponentPart',
        'ifml:Annotation',
      ]);

      return !(element.labelTarget || cannotDistribute);
    });
  });
}

IfmlDistributeElements.$inject = ['distributeElements'];