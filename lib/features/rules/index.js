import RulesModule from 'diagram-js/lib/features/rules';

import IfmlRules from './IfmlRules';

export default {
  __depends__: [
    RulesModule
  ],
  __init__: [ 'ifmlRules' ],
  ifmlRules: [ 'type', IfmlRules ]
};
