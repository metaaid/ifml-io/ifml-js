import {
  every,
  find,
  forEach,
  some
} from 'min-dash';

import inherits from 'inherits';

import {
  is,
  getBusinessObject,
  portIsInput
} from '../../util/ModelUtil';

import {
  getParent,
  isAny
} from '../modeling/util/ModelingUtil';

import {
  hasExternalLabel,
  isExternalElement,
  isLabel
} from '../../util/LabelUtil';

import RuleProvider from 'diagram-js/lib/features/rules/RuleProvider';

/**
 * BPMN specific modeling rule
 */
export default function IfmlRules(eventBus) {
  RuleProvider.call(this, eventBus);
}

inherits(IfmlRules, RuleProvider);

IfmlRules.$inject = ['eventBus'];

IfmlRules.prototype.init = function () {

  this.addRule('connection.start', function (context) {
    var source = context.source;

    return canStartConnection(source);
  });

  this.addRule('connection.create', function (context) {
    var source = context.source,
      target = context.target,
      hints = context.hints || {},
      targetParent = hints.targetParent,
      targetAttach = hints.targetAttach;

    // don't allow incoming connections on
    // newly created boundary events
    // to boundary events
    if (targetAttach) {
      return false;
    }

    // temporarily set target parent for scoping
    // checks to work
    if (targetParent) {
      target.parent = targetParent;
    }

    try {
      return canConnect(source, target);
    } finally {

      // unset temporary target parent
      if (targetParent) {
        target.parent = null;
      }
    }
  });

  this.addRule('connection.reconnect', function (context) {

    var connection = context.connection,
      source = context.source,
      target = context.target;

    return canConnect(source, target, connection);
  });

  this.addRule('connection.updateWaypoints', function (context) {
    return {
      type: context.connection.type
    };
  });

  this.addRule('shape.resize', function (context) {

    var shape = context.shape,
      newBounds = context.newBounds;

    return canResize(shape, newBounds);
  });

  this.addRule('elements.create', function (context) {
    var elements = context.elements,
      position = context.position,
      target = context.target;

    return every(elements, function (element) {
      if (isConnection(element)) {
        return canConnect(element.source, element.target, element);
      }

      if (element.host) {
        return canAttach(element, element.host, null, position);
      }

      return canCreate(element, target, null, position);
    });
  });

  this.addRule('elements.move', function (context) {

    var target = context.target,
      shapes = context.shapes,
      position = context.position;

    return canAttach(shapes, target, null, position) ||
      canReplace(shapes, target, position) ||
      canMove(shapes, target, position) ||
      canInsert(shapes, target, position);
  });

  this.addRule('shape.create', function (context) {
    return canCreate(
      context.shape,
      context.target,
      context.source,
      context.position
    );
  });

  this.addRule('shape.attach', function (context) {
    return canAttach(
      context.shape,
      context.target,
      null,
      context.position
    );
  });

  this.addRule('element.copy', function (context) {
    var element = context.element,
      elements = context.elements;

    return canCopy(elements, element);
  });
};

IfmlRules.prototype.canConnectDataFlow = canConnectDataFlow;

IfmlRules.prototype.canConnectNavigationFlow = canConnectNavigationFlow;

IfmlRules.prototype.canMove = canMove;

IfmlRules.prototype.canAttach = canAttach;

IfmlRules.prototype.canReplace = canReplace;

IfmlRules.prototype.canDrop = canDrop;

IfmlRules.prototype.canInsert = canInsert;

IfmlRules.prototype.canCreate = canCreate;

IfmlRules.prototype.canConnect = canConnect;

IfmlRules.prototype.canResize = canResize;

IfmlRules.prototype.canCopy = canCopy;

IfmlRules.prototype.twoDiffrenModules = twoDiffrenModules;

/**
 * Utility functions for rule checking
 */

/**
 * Checks if given element can be used for starting connection.
 *
 * @param  {Element} source
 * @return {boolean}
 */
function canStartConnection(element) {
  if (nonExistingOrLabel(element)) {
    return null;
  }

  return isAny(element, [
    'ifml:ViewElement',
    'ifml:Event',
    'ifml:PortDefinition',
  ]);
}

function nonExistingOrLabel(element) {
  return !element || isLabel(element);
}

function isSame(a, b) {
  return a === b;
}

function hasEventDefinition(element, eventDefinition) {
  var bo = getBusinessObject(element);

  if (is(element, 'ifml:ViewElement')) {
    return !!find(bo.viewElementEvents || [], function (definition) {
      return is(definition, eventDefinition);
    });
  } else if (is(element, 'ifml:Action')) {
    return !!find(bo.actionEvents || [], function (definition) {
      return is(definition, eventDefinition);
    });
  } else {
    return !!find(bo.event || [], function (definition) {
      return is(definition, eventDefinition);
    });
  }
}

function isConnection(element) {
  return element.waypoints;
}

function getParents(element) {

  var parents = [];

  while (element) {
    element = element.parent;

    if (element) {
      parents.push(element);
    }
  }

  return parents;
}

export function isParent(possibleParent, element) {
  var allParents = getParents(element);
  return allParents.indexOf(possibleParent) !== -1;
}

function firstModule(element) {
  var allParents = getParents(element);
  for(var index in allParents) {
    var el = allParents[index];
    if(isAny(el, ['ifml:ModuleDefinition', 'ifml:Module'])){
      return el;
    }
  };
  return null;
}

/**
 * Checks if an source can be connected by a data/navigation-Flow to a target, those connection will also be created automaticly on inserts
 * @param {*} source 
 * @param {*} target 
 * @param {*} connection 
 * @returns 
 */
function canConnect(source, target, connection) {

  if (nonExistingOrLabel(source) || nonExistingOrLabel(target)) {
    return null;
  }

  // no parent can be connected with a child
  if (target.parent == source) {
    return false;
  }

  if (canConnectNavigationFlow(source, target)) {
    return {
      type: 'ifml:NavigationFlow'
    };
  }

  if (canConnectDataFlow(source, target)) {
    return {
      type: 'ifml:DataFlow'
    };
  }

  return false;
}

/**
 * Can an element be dropped into the target element
 *
 * @return {boolean}
 */
function canDrop(element, target, position) {
  var targetIsRoot = is(target, 'ifml:InteractionFlowModel');

  // can move labels 
  if (isLabel(element)) {
    return true;
  }

  // can move external elements
  if (isExternalElement(element)) {
    return true;
  }

  if (is(element, 'ifml:Event')) {
    var eventType = '';
    if (isAny(target, ['ifml:ViewElement', 'ifml:ViewComponentPart'])) {
      // if(!is(element, 'ifml:ViewElementEvent')){ // check if needed
        eventType = 'ifml:ViewElementEvent';
      // }
    } else if (is(target, 'ifml:Action')) {
      // if(!is(element, 'ifml:ActionEvent')){
        eventType = 'ifml:ActionEvent';
      // }
    } else {
      return false;
    }
    return {
      type: eventType
    };
  }
  
  if (isAny(element, ['ifml:Annotation'])) {
    return true;
  }

  if (isAny(element, ['ifml:ViewComponentPart']) && isAny(target, ['ifml:ViewComponent'])) {
    return true;
  }
  if (isAny(element, ['ifml:VisualizationAttribute']) && isAny(target, ['ifml:DataBinding'])) {
    return true;
  }


  if (isAny(element, ['ifml:Port']) && isAny(target, ['ifml:Module'])) {
    return true;
  }
  if (isAny(element, ['ifml:PortDefinition']) && (isAny(target, ['ifml:ModuleDefinition']))) {
    return true;
  }
  if (isAny(element, ['ifml:ViewComponent', 'ifml:ViewContainer', 'ifml:Action', 'ifml:ModuleDefinition']) && (isAny(target, ['ifml:ViewContainer', 'ifml:ModuleDefinition']) || targetIsRoot)) {
    return true;
  }

  return false;
}


function canAttach(elements, target, source, position) {
  return false;
}


/**
 * Defines how to replace elements for a given target.
 *
 * Returns an array containing all elements which will be replaced.
 *
 * @example
 *
 *  [{ id: 'IntermediateEvent_2',
 *     type: 'bpmn:StartEvent'
 *   },
 *   { id: 'IntermediateEvent_5',
 *     type: 'bpmn:EndEvent'
 *   }]
 *
 * @param  {Array} elements
 * @param  {Object} target
 *
 * @return {Object} an object containing all elements which have to be replaced
 */
function canReplace(elements, target, position) {

  if (!target) {
    return false;
  }

  var canExecute = {
    replacements: []
  };

  return canExecute.replacements.length ? canExecute : false;
}

function canMove(elements, target) {

  // allow default move check to start move operation
  if (!target) {
    return true;
  }

  return elements.every(function (element) {
    return canDrop(element, target);
  });
}

function canCreate(shape, target, source, position) {

  if (!target) {
    return false;
  }

  if (isLabel(shape)) {
    return true;
  }

  if (isSame(source, target)) {
    return false;
  }

  // ensure we do not drop the element
  // into source
  if (source && isParent(source, target)) {
    return false;
  }

  return canDrop(shape, target, position) || canInsert(shape, target, position);
}

function canResize(shape, newBounds) {
  if (isExternalElement(shape) || isAny(shape, ['ifml:ViewElement', 'ifml:Action', 'ifml:ModuleDefinition', 'ifml:Module'])) {
    return true;
  }
  return false;
}

function twoDiffrenModules(source, target){

  if(!isAny(target, ['ifml:PortDefinition', 'ifml:Port']) || !isAny(source, ['ifml:PortDefinition', 'ifml:Port'])){
    var sourceModule = firstModule(source);
    var targetModule = firstModule(target);
    return sourceModule !== targetModule;
  }
  return false;
}

// all possible combinations for connections
var possibleConnectionElements = {
  dataflow: {
    src: ['ifml:ViewElement', 'ifml:Port', 'ifml:PortDefinition'],
    target: ['ifml:ViewElement', 'ifml:Action', 'ifml:Port', 'ifml:PortDefinition']
  },
  navigationflow: {
    src: ['ifml:Event', 'ifml:Port', 'ifml:PortDefinition'],
    target: ['ifml:ViewElement', 'ifml:Action', 'ifml:Port', 'ifml:PortDefinition']
  }
};

function canConnectSpecificFlow(source,target, type){
  var possibleSourceElements = possibleConnectionElements[type].src;
  var possibleTargetElements = possibleConnectionElements[type].target;
  var targetIsPort = isAny(target, ['ifml:PortDefinition', 'ifml:Port']);
  var sourceIsPort = isAny(source, ['ifml:PortDefinition', 'ifml:Port']);

  if(targetIsPort || sourceIsPort ){
    var portElement = targetIsPort ? target : source;
    var conElement = targetIsPort ? source : target;
    var port = getBusinessObject(portElement);
    var parentModule = portElement.parent;
    var isIn = port.get("direction") == 'in';
    var internalElement = isParent(parentModule, conElement);

    if(targetIsPort){
      if (isIn && !internalElement || !isIn && internalElement) {
        return isAny(conElement, possibleSourceElements);
      }
    }
    if(sourceIsPort){
      if (isIn && internalElement || !isIn && !internalElement) {
        return isAny(conElement, possibleTargetElements);
      }
    }
    return false;
  }
  // connections in or outside modules only allowed through ports
  if (twoDiffrenModules(source,target)){
    return false;
  }

  return isAny(source, possibleSourceElements) && isAny(target, possibleTargetElements);
}

function canConnectDataFlow(source, target) {
  return canConnectSpecificFlow(source,target, "dataflow");
}

function canConnectNavigationFlow(source, target) {
  return canConnectSpecificFlow(source,target, "navigationflow");
}

function canInsert(shape, flow, position) {
  if (!flow) {
    return false;
  }

  if (Array.isArray(shape)) {
    if (shape.length !== 1) {
      return false;
    }

    shape = shape[0];
  }

  if (flow.source === shape ||
    flow.target === shape) {
    return false;
  }

  // return true if we can drop on the
  // underlying flow parent
  //
  // at this point we are not really able to talk
  // about connection rules (yet)

  // return (
  //   isAny(flow, ['ifml:InteractionFlow']) &&
  //   // !isLabel(flow) &&
  //   // is(shape, 'bpmn:FlowNode') &&
  //   // !is(shape, 'bpmn:BoundaryEvent') &&
  //   canDrop(shape, flow.parent, position));
  return false;
}

function canCopy(elements, element) {
  if (isLabel(element)) {
    return true;
  }

  return true;
}