import { assign } from "min-dash";

import { is } from "../util/ModelUtil";

import { isLabelExternal, getExternalLabelBounds } from "../util/LabelUtil";

import { getMid } from "diagram-js/lib/layout/LayoutUtil";

import { isExpanded } from "../util/DiUtil";

import { getLabel } from "../features/label-editing/LabelUtil";

import { elementToString } from "./Util";

function elementData(semantic, di, attrs) {
  return assign(
    {
      id: semantic.$id,
      type: semantic.$type,
      businessObject: semantic,
      di: di,
    },
    attrs
  );
}

function getWaypoints(di, source, target) {
  var waypoints = di.waypoint;

  if (!waypoints || waypoints.length < 2) {
    return [getMid(source), getMid(target)];
  }

  return waypoints.map(function (p) {
    return {
      x: p.x,
      y: p.y,
    };
  });
}

function notYetDrawn(translate, semantic, refSemantic, property) {
  return new Error(
    translate(
      "element {element} referenced by {referenced}#{property} not yet drawn",
      {
        element: elementToString(refSemantic),
        referenced: elementToString(semantic),
        property: property,
      }
    )
  );
}

/**
 * An importer that adds ifml elements to the canvas
 *
 * @param {EventBus} eventBus
 * @param {Canvas} canvas
 * @param {ElementFactory} elementFactory
 * @param {ElementRegistry} elementRegistry
 * @param {Function} translate
 * @param {TextRenderer} textRenderer
 */
export default function IfmlImporter(
  eventBus,
  canvas,
  elementFactory,
  elementRegistry,
  translate,
  textRenderer
) {
  this._eventBus = eventBus;
  this._canvas = canvas;
  this._elementFactory = elementFactory;
  this._elementRegistry = elementRegistry;
  this._translate = translate;
  this._textRenderer = textRenderer;
}

IfmlImporter.$inject = [
  "eventBus",
  "canvas",
  "elementFactory",
  "elementRegistry",
  "translate",
  "textRenderer",
];

/**
 * Add ifml element (semantic) to the canvas onto the
 * specified parent shape.
 */
IfmlImporter.prototype.add = function (semantic, di, parentElement) {
  var element,
    translate = this._translate,
    hidden;

  var parentIndex;

  // ROOT ELEMENT
  // handle the special case that we deal with a
  // invisible root element (process or collaboration)
  if (is(di, "ifmldi:IFMLDiagram")) {
    //   // add a virtual element (not being drawn)
    element = this._elementFactory.createRoot(elementData(semantic, di));

    this._canvas.setRootElement(element);
  }

  // diagram has no presentation

  // SHAPE
  else if (is(di, "di:Shape")) {
    var collapsed = !isExpanded(semantic, di);
    hidden = parentElement && (parentElement.hidden || parentElement.collapsed);

    var bounds = di.bounds;

    element = this._elementFactory.createShape(
      elementData(semantic, di, {
        collapsed: collapsed,
        hidden: hidden,
        x: Math.round(bounds.x),
        y: Math.round(bounds.y),
        width: Math.round(bounds.width),
        height: Math.round(bounds.height),
        isFrame: false,
      })
    );

    // TODO: check
    // if (is(semantic, 'ifml:Event')) {
    //   this._attachBoundary(semantic, element);
    // }

    // parentElement.id = parentElement.

    this._canvas.addShape(element, parentElement, parentIndex);
  }

  // CONNECTION
  else if (is(di, "ifmldi:IFMLConnection")) {
    var source = this._getSource(semantic),
      target = this._getTarget(semantic);

    hidden = parentElement && (parentElement.hidden || parentElement.collapsed);

    element = this._elementFactory.createConnection(
      elementData(semantic, di, {
        hidden: hidden,
        source: source,
        target: target,
        waypoints: getWaypoints(di, source, target),
      })
    );

    this._canvas.addConnection(element, parentElement, parentIndex);
  } else {
    throw new Error(
      translate("unknown di {di} for element {semantic}", {
        di: elementToString(di),
        semantic: elementToString(semantic),
      })
    );
  }

  // (optional) LABEL
  if (isLabelExternal(semantic) && getLabel(element)) {
    this.addLabel(semantic, di, element);
  }

  this._eventBus.fire("modelElement.added", {
    element: element,
  });

  return element;
};

/**
 * Attach the boundary element to the given host
 *
 * @param {ModdleElement} boundarySemantic
 * @param {djs.model.Base} boundaryElement
 */
IfmlImporter.prototype._attachBoundary = function (
  boundarySemantic,
  boundaryElement
) {
  var translate = this._translate;
  var hostSemantic = boundarySemantic.attachedToRef;

  if (!hostSemantic) {
    throw new Error(
      translate("missing {semantic}#attachedToRef", {
        semantic: elementToString(boundarySemantic),
      })
    );
  }

  var host = this._elementRegistry.get(hostSemantic.$id),
    attachers = host && host.attachers;

  if (!host) {
    throw notYetDrawn(
      translate,
      boundarySemantic,
      hostSemantic,
      "attachedToRef"
    );
  }

  // wire element.host <> host.attachers
  boundaryElement.host = host;

  if (!attachers) {
    host.attachers = attachers = [];
  }

  if (attachers.indexOf(boundaryElement) === -1) {
    attachers.push(boundaryElement);
  }
};

/**
 * add label for an element
 */
IfmlImporter.prototype.addLabel = function (semantic, di, element) {
  var bounds, text, label;

  bounds = getExternalLabelBounds(semantic, element);

  text = getLabel(element);

  if (text) {
    // get corrected bounds from actual layouted text
    bounds = this._textRenderer.getExternalLabelBounds(bounds, text);
  }

  var eData = elementData(semantic, di, {
    id: semantic.$id + "_label",
    labelTarget: element,
    type: "label",
    hidden: element.hidden || !getLabel(element),
    x: Math.round(bounds.x),
    y: Math.round(bounds.y),
    width: Math.round(bounds.width),
    height: Math.round(bounds.height),
  });

  label = this._elementFactory.createLabel(eData);

  return this._canvas.addShape(label, element.parent);
};

/**
 * Return the drawn connection end based on the given side.
 *
 * @throws {Error} if the end is not yet drawn
 */
IfmlImporter.prototype._getEnd = function (semantic, side) {
  var element,
    translate = this._translate;

  element = semantic[side] && this._getElement(semantic[side]);

  if (element) {
    return element;
  }

  throw new Error(
    translate("{semantic}#{side} Ref not specified", {
      semantic: elementToString(semantic),
      side: side,
    })
  );
};

IfmlImporter.prototype._getSource = function (semantic) {
  return this._getEnd(semantic, "sourceInteractionFlowElement");
};

IfmlImporter.prototype._getTarget = function (semantic) {
  return this._getEnd(semantic, "targetInteractionFlowElement");
};

IfmlImporter.prototype._getElement = function (semantic) {
  return this._elementRegistry.get(semantic.$id);
};

// helpers ////////////////////

function isPointInsideBBox(bbox, point) {
  var x = point.x,
    y = point.y;

  return (
    x >= bbox.x &&
    x <= bbox.x + bbox.width &&
    y >= bbox.y &&
    y <= bbox.y + bbox.height
  );
}
