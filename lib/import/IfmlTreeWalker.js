/* based on http://www.ifml.org/wp-content/uploads/IFML-DD-DI-DG.pdf
 */
import { filter, forEach } from "min-dash";

import { isAny } from "../features/modeling/util/ModelingUtil";

import { elementToString } from "./Util";

import {
  ensureCompatDiRef
} from '../util/CompatibilityUtil';

/**
 * Returns true if an element has the given meta-model type
 *
 * @param  {ModdleElement}  element
 * @param  {string}         type
 *
 * @return {boolean}
 */
function is(element, type) {
  return element.$instanceOf(type);
}

export default function IfmlTreeWalker(handler, translate) {
  // list of containers already walked
  var handledElements = {};

  // list of elements to handle deferred to ensure
  // prerequisites are drawn
  var deferred = [];
  var diMap = {};

  // Helpers //////////////////////

  function contextual(fn, ctx) {
    return function (e) {
      fn(e, ctx);
    };
  }

  function handled(element) {
    handledElements[element.$id] = element;
  }

  function isHandled(element) {
    return handledElements[element.$id];
  }

  function visit(element, ctx) {
    if (!element) {
      return;
    }

    var gfx = element.gfx;

    // avoid multiple rendering of elements
    if (gfx) {
      throw new Error(
        translate("already rendered {element}", {
          element: elementToString(element),
        })
      );
    }

    // call handler
    return handler.element(element, diMap[element.$id], ctx);
  }

  function visitInteractionFlow(flow, parent) {
    var context = visit(flow, parent);
    
    if (flow.parameterBindingGroup) {
      visitInteractionFlowElement(flow.parameterBindingGroup, context);
    }
  }

  function visitInteractionFlowElement(element, context) {
    var currentContext = visit(element, context);

    // innerViewComponentPart need to be added first (parameters + viewcomponentpars)
    if (is(element, "ifml:ViewComponentPart")) {
      forEach(
        element.subViewComponentParts,
        contextual(visitInteractionFlowElement, currentContext)
      );
    }

    if (is(element, "ifml:ViewElement")) {
      forEach(
        element.parameters,
        contextual(visitInteractionFlowElement, currentContext)
      );
      
      forEach(
        element.viewElementEvents,
        contextual(visitInteractionFlowElement, currentContext)
      );
      if (is(element, "ifml:List")) {
        forEach(
          element.selectEvent,
          contextual(visitInteractionFlowElement, currentContext)
        );
      }
    }
    if (is(element, "ifml:Action")) {
      forEach(
        element.actionEvents,
        contextual(visitInteractionFlowElement, currentContext)
      );
      if (element.dynamicBehavior) {
        visitInteractionFlowElement(element.dynamicBehavior, currentContext);
      }
    }

    if (
      isAny(element, [
        "ifml:ViewComponentPart",
        "ifml:ViewElement",
        "ifml:Event",
      ])
    ) {
      if (element.activationExpression) {
        visitInteractionFlowElement(
          element.activationExpression,
          currentContext
        );
      }
    }

    if (is(element, "ifml:DataBinding")) {
      forEach(
        element.visualizationAttribute,
        contextual(visitInteractionFlowElement, currentContext)
      );
    }
    if (is(element, "ifml:ParameterBindingGroup")) {
      forEach(
        element.parameterBindings,
        contextual(visitInteractionFlowElement, currentContext)
      );
    }

    if (isAny(element, ["ifml:ModuleDefinition", "ifml:Module"])) {
      forEach(
        element.modules,
        contextual(visitInteractionFlowElement, currentContext)
      );
      forEach(
        element.interactionFlowModelElement,
        contextual(visitInteractionFlowElement, currentContext)
      );
      forEach(
        element.outputPorts,
        contextual(visitInteractionFlowElement, currentContext)
      );
      forEach(
        element.inputPorts,
        contextual(visitInteractionFlowElement, currentContext)
      );
    }

    if (is(element, "ifml:ViewComponent")) {
      forEach(
        element.viewComponentParts,
        contextual(visitInteractionFlowElement, currentContext)
      );
    } else if (is(element, "ifml:ViewContainer")) {
      forEach(
        element.viewElements,
        contextual(visitInteractionFlowElement, currentContext)
      );
      forEach(
        element.actions,
        contextual(visitInteractionFlowElement, currentContext)
      );
    } else if (isAny(element, ["ifml:PortDefinition", "ifml:Port"])) {
      forEach(
        element.ports,
        contextual(visitInteractionFlowElement, currentContext)
      );
    }
    
    if (is(element, "ifml:BaseElement") && element.annotations && element.annotations.length > 0) {
      forEach(element.annotations, contextual(visitInteractionFlowElement, currentContext));
    }
  }

  function visitFlowModelElements(element, context) {
    if (is(element, "ifml:InteractionFlow")) {
      visitInteractionFlow(element, context);
    } else {
      visitInteractionFlowElement(element, context);
    }
  }

  function visitDiagram(diagramDI) {
    var interactionModel = diagramDI.modelElement;
    if (!interactionModel) {
      throw Error("No model found in the xmi", context);
    }
    var context = handler.root(interactionModel, diagramDI);
    forEach(
      interactionModel.interactionFlowModelElements,
      contextual(visitFlowModelElements, context)
    );
  }

  function visitIfDi(element, ctx) {
    try {
      var gfx = diMap[element.$id] && visit(element, ctx);

      handled(element);

      return gfx;
    } catch (e) {
      logError(e.message, {
        element: element,
        error: e,
      });

      console.error(
        translate("failed to import {element}", {
          element: elementToString(element),
        })
      );
      console.error(e);
    }
  }

  function logError(message, context) {
    handler.error(message, context);
  }

  // DI handling //////////////////////

  function registerDi(di) {
    var modelElement = di.modelElement;

    if (modelElement) {
      if (diMap[modelElement.$id]) {
        logError(
          translate("multiple DI elements defined for {element}", {
            element: elementToString(modelElement),
          }),
          {
            element: modelElement,
          }
        );
      } else {
        diMap[modelElement.$id] = di;
        ensureCompatDiRef(modelElement);
      }
    } else {
      logError(
        translate("no modelElement referenced in {element}", {
          element: elementToString(di),
        }),
        {
          element: di,
        }
      );
    }
  }

  function handleDiagramElement(diagramElement) {
    // nodes and connections can have ownedElements
    registerDi(diagramElement);
    forEach(diagramElement.ownedElement, handleDiagramElement);
  }

  function handleNode(node) {
    registerDi(node);
    forEach(node.ownedElement, handleDiagramElement);
  }

  function handleDiagram(diagram) {
    handleNode(diagram);
  }

  // Semantic handling //////////////////////

  /**
   * Handle definitions and return the rendered diagram (if any)
   *
   * @param {ModdleElement} definitions to walk and import
   * @param {ModdleElement} [diagram] specific diagram to import and display
   *
   * @throws {Error} if no diagram to display could be found
   */
  function handleDefinitions(definitions, diagram) {
    // make sure we walk the correct modelElement
    if (definitions.ownedElement) {  
      var diagrams = definitions.ownedElement.filter((e) =>
        e.$instanceOf("ifmldi:IFMLDiagram")
      );

      if (diagram && diagrams.indexOf(diagram) === -1) {
        throw new Error(translate("diagram not part of ifml:IFMLModel"));
      }

      if (!diagram && diagrams && diagrams.length) {
        diagram = diagrams[0];
      }
    }

    // no diagram -> nothing to import
    if (!diagram) {
      throw new Error(translate("no diagram to display"));
    }

    // load DI from selected diagram only
    diMap = {};
    handleDiagram(diagram);
    visitDiagram(diagram);

    // handle all deferred elements
    handleDeferred(deferred);
  }

  function handleDeferred() {
    var fn;

    // drain deferred until empty
    while (deferred.length) {
      fn = deferred.shift();

      fn();
    }
  }

  // API //////////////////////

  return {
    handleDeferred: handleDeferred,
    handleDefinitions: handleDefinitions,
    registerDi: registerDi,
  };
}
