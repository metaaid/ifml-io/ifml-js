import translate from 'diagram-js/lib/i18n/translate';

import IfmlImporter from './IfmlImporter';

export default {
  __depends__: [
    translate
  ],
  ifmlImporter: [ 'type', IfmlImporter ]
};