import IfmlTreeWalker from './IfmlTreeWalker';


/**
 * The ImportIFMLDiagram result.
 *
 * @typedef {Object} ImportIFMLDiagramResult
 *
 * @property {Array<string>} warnings
 */

/**
* The ImportIFMLDiagram error.
*
* @typedef {Error} ImportIFMLDiagramError
*
* @property {Array<string>} warnings
*/

/**
 * Import the definitions into a diagram.
 *
 * Errors and warnings are reported through the specified callback.
 *
 * @param  {djs.Diagram} diagram
 * @param  {ModdleElement<Definitions>} definitions
 * @param  {ModdleElement<IFMLDiagram>} [IFMLDiagram] the diagram to be rendered
 * (if not provided, the first one will be rendered)
 *
 * Returns {Promise<ImportIFMLDiagramResult, ImportIFMLDiagramError>}
 */
export function ImportIFMLDiagram(diagram, definitions, IFMLDiagram) {

  var importer,
      eventBus,
      translate;

  var error,
      warnings = [];

  /**
   * Walk the diagram semantically, importing (=drawing)
   * all elements you encounter.
   *
   * @param {ModdleElement<Definitions>} definitions
   * @param {ModdleElement<IFMLDiagram>} IFMLDiagram
   */
  function render(definitions, IFMLDiagram) {

    var visitor = {

      root: function(element, di) {
        return importer.add(element, di);
      },

      element: function(element, di, parentShape) {
        return importer.add(element, di, parentShape);
      },

      error: function(message, context) {
        warnings.push({ message: message, context: context });
      }
    };

    var walker = new IfmlTreeWalker(visitor, translate);

    // traverse IFML document model,
    // starting at definitions
    walker.handleDefinitions(definitions, IFMLDiagram);
  }

  return new Promise(function(resolve, reject) {
    try {
      importer = diagram.get('ifmlImporter');
      eventBus = diagram.get('eventBus');
      translate = diagram.get('translate');

      eventBus.fire('import.render.start', { definitions: definitions });

      render(definitions, IFMLDiagram);

      eventBus.fire('import.render.complete', {
        error: error,
        warnings: warnings
      });

      return resolve({ warnings: warnings });
    } catch (e) {

      e.warnings = warnings;
      return reject(e);
    }
  });
}
