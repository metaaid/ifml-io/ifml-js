import IfmlRenderer from './IfmlRenderer';
import TextRenderer from './TextRenderer';

import PathMap from './PathMap';

export default {
  __init__: [ 'ifmlRenderer' ],
  ifmlRenderer: [ 'type', IfmlRenderer ],
  textRenderer: [ 'type', TextRenderer ],
  pathMap: [ 'type', PathMap ]
};
