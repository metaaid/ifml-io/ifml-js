import inherits from 'inherits';

import { isObject, assign, forEach } from 'min-dash';

import BaseRenderer from 'diagram-js/lib/draw/BaseRenderer';

import { getLabel } from '../features/label-editing/LabelUtil';

import { getBusinessObject, is, portIsInput } from '../util/ModelUtil';

import { createLine } from 'diagram-js/lib/util/RenderUtil';

import {
  getSemantic,
  getFillColor,
  getStrokeColor,
  getConnectionColor,
  getTextColor,
} from './IfmlRenderUtil';

import { query as domQuery } from 'min-dom';

import {
  append as svgAppend,
  attr as svgAttr,
  create as svgCreate,
  classes as svgClasses,
} from 'tiny-svg';

import { transform } from 'diagram-js/lib/util/SvgTransformUtil';

import Ids from 'ids';
import { isAny } from '../features/modeling/util/ModelingUtil';

var RENDERER_IDS = new Ids();

var VIEWCOMPONENT_BORDER_RADIUS = 10;
var INNER_OUTER_DIST = 3;

var DEFAULT_FILL_OPACITY = 0.95;
var TEXT_BOUNDS_PADDING = 10;

export default function IfmlRenderer(
  config,
  eventBus,
  styles,
  pathMap,
  canvas,
  textRenderer,
  priority
) {
  BaseRenderer.call(this, eventBus, priority);

  var defaultFillColor = config && config.defaultFillColor,
    defaultStrokeColor = config && config.defaultStrokeColor,
    defaultConnectionColor = config && config.defaultConnectionColor;

  var rendererId = RENDERER_IDS.next();

  var markers = {};

  var computeStyle = styles.computeStyle;

  function addMarker(id, options) {
    var attrs = assign(
      {
        fill: 'black',
        strokeWidth: 1,
        strokeLinecap: 'round',
        strokeDasharray: 'none',
      },
      options.attrs
    );

    var ref = options.ref || {
      x: 0,
      y: 0,
    };

    var scale = options.scale || 1;

    // fix for safari / chrome / firefox bug not correctly
    // resetting stroke dash array
    if (attrs.strokeDasharray === 'none') {
      attrs.strokeDasharray = [10000, 1];
    }

    var marker = svgCreate('marker');

    svgAttr(options.element, attrs);

    svgAppend(marker, options.element);

    svgAttr(marker, {
      id: id,
      viewBox: '0 0 20 20',
      refX: ref.x,
      refY: ref.y,
      markerWidth: 20 * scale,
      markerHeight: 20 * scale,
      orient: 'auto',
    });

    var defs = domQuery('defs', canvas._svg);

    if (!defs) {
      defs = svgCreate('defs');

      svgAppend(canvas._svg, defs);
    }

    svgAppend(defs, marker);

    markers[id] = marker;
  }

  function colorEscape(str) {
    // only allow characters and numbers
    return str.replace(/[^0-9a-zA-z]+/g, '_');
  }

  function marker(type, fill, stroke) {
    var id =
      type +
      '-' +
      colorEscape(fill) +
      '-' +
      colorEscape(stroke) +
      '-' +
      rendererId;

    if (!markers[id]) {
      createMarker(id, type, fill, stroke);
    }

    return 'url(#' + id + ')';
  }

  function createMarker(id, type, fill, stroke) {
    if (type === 'event') {
      var messageflowStart = svgCreate('circle');
      svgAttr(messageflowStart, {
        cx: 6,
        cy: 6,
        r: 3.5,
      });

      addMarker(id, {
        element: messageflowStart,
        attrs: {
          fill: fill,
          stroke: stroke,
        },
        ref: {
          x: 6,
          y: 6,
        },
      });
    }

    if (type === 'navigationflow-end' || type === 'dataflow-end') {
      var sequenceflowEnd = svgCreate('path');
      svgAttr(sequenceflowEnd, {
        d: 'M 1 5 L 11 10 L 1 15 Z',
      });

      addMarker(id, {
        element: sequenceflowEnd,
        ref: {
          x: 11,
          y: 10,
        },
        scale: 0.5,
        attrs: {
          fill: stroke,
          stroke: stroke,
        },
      });
    }

    if (type === 'conditional-flow-marker') {
      var conditionalflowMarker = svgCreate('path');
      svgAttr(conditionalflowMarker, {
        d: 'M 0 10 L 8 6 L 16 10 L 8 14 Z',
      });

      addMarker(id, {
        element: conditionalflowMarker,
        attrs: {
          fill: fill,
          stroke: stroke,
        },
        ref: {
          x: -1,
          y: 10,
        },
        scale: 0.5,
      });
    }

    if (type === 'conditional-default-flow-marker') {
      var conditionaldefaultflowMarker = svgCreate('path');
      svgAttr(conditionaldefaultflowMarker, {
        d: 'M 6 4 L 10 16',
      });

      addMarker(id, {
        element: conditionaldefaultflowMarker,
        attrs: {
          stroke: stroke,
        },
        ref: {
          x: 0,
          y: 10,
        },
        scale: 0.5,
      });
    }
  }

  function drawCircle(parentGfx, width, height, offset, attrs) {
    if (isObject(offset)) {
      attrs = offset;
      offset = 0;
    }

    offset = offset || 0;

    attrs = computeStyle(attrs, {
      stroke: 'black',
      strokeWidth: 2,
      fill: 'white',
    });

    if (attrs.fill === 'none') {
      delete attrs.fillOpacity;
    }

    var cx = width / 2,
      cy = height / 2;

    var circle = svgCreate('circle');
    svgAttr(circle, {
      cx: cx,
      cy: cy,
      r: Math.round((width + height) / 4 - offset),
    });
    svgAttr(circle, attrs);

    svgAppend(parentGfx, circle);

    return circle;
  }

  function drawRect(parentGfx, width, height, r, offset, attrs) {
    if (isObject(offset)) {
      attrs = offset;
      offset = 0;
    }

    if (!isObject(offset)) {
      offset = {
        x: offset,
        y: offset,
      };
    }

    attrs = computeStyle(attrs, {
      stroke: 'black',
      strokeWidth: 2,
      fill: 'white',
    });

    var rect = svgCreate('rect');
    svgAttr(rect, {
      x: offset.x,
      y: offset.y,
      width: width - offset.x * 2,
      height: height - offset.y * 2,
      rx: r,
      ry: r,
    });
    svgAttr(rect, attrs);

    svgAppend(parentGfx, rect);

    return rect;
  }

  function drawHexa(parentGfx, width, height, attrs, offset) {
    offset = typeof offset == 'undefined' ? 0.1 : offset;
    var xOffsetLeft = offset * width;
    var xOffsetRight = width - xOffsetLeft;
    var y_2 = height / 2;

    var points = [
      {
        x: xOffsetLeft,
        y: 0,
      },
      {
        x: xOffsetRight,
        y: 0,
      },
      {
        x: width,
        y: y_2,
      },
      {
        x: xOffsetRight,
        y: height,
      },
      {
        x: xOffsetLeft,
        y: height,
      },
      {
        x: 0,
        y: y_2,
      },
    ];

    var pointsString = points
      .map(function (point) {
        return point.x + ',' + point.y;
      })
      .join(' ');

    attrs = computeStyle(attrs, {
      stroke: 'black',
      strokeWidth: 2,
      fill: 'white',
    });

    var polygon = svgCreate('polygon');
    svgAttr(polygon, {
      points: pointsString,
    });
    svgAttr(polygon, attrs);

    svgAppend(parentGfx, polygon);

    return polygon;
  }

  function drawLine(parentGfx, waypoints, attrs) {
    attrs = computeStyle(attrs, ['no-fill'], {
      stroke: 'black',
      strokeWidth: 2,
      fill: 'none',
    });

    var line = createLine(waypoints, attrs);

    svgAppend(parentGfx, line);

    return line;
  }

  function drawPath(parentGfx, d, attrs) {
    attrs = computeStyle(attrs, ['no-fill'], {
      strokeWidth: 2,
      stroke: 'black',
    });

    var path = svgCreate('path');
    svgAttr(path, {
      d: d,
    });
    svgAttr(path, attrs);

    svgAppend(parentGfx, path);

    return path;
  }

  function drawMarker(type, parentGfx, path, attrs) {
    return drawPath(
      parentGfx,
      path,
      assign(
        {
          'data-marker': type,
        },
        attrs
      )
    );
  }

  function as(type) {
    return function (parentGfx, element) {
      return handlers[type](parentGfx, element);
    };
  }

  function renderer(type) {
    return handlers[type];
  }

  function renderEventContent(element, parentGfx) {
    if (is(element, 'ifml:OnSelectEvent')) {
      return renderer('ifml:OnSelectEvent')(parentGfx, element);
    } else if (is(element, 'ifml:OnSubmitEvent')) {
      return renderer('ifml:OnSubmitEvent')(parentGfx, element);
    }

    return null;
  }

  function renderLabel(parentGfx, label, options) {
    options = assign(
      {
        size: {
          width: 100,
        },
      },
      options
    );

    var text = textRenderer.createText(label || '', options);

    svgClasses(text).add('djs-label');

    svgAppend(parentGfx, text);

    return text;
  }

  function renderEmbeddedLabel(parentGfx, element, align, text) {
    text = text ? text : getLabel(element);

    return renderLabel(parentGfx, text, {
      box: element,
      align: align,
      padding: 5,
      style: {
        fill: getTextColor(element, defaultStrokeColor),
      },
    });
  }

  function renderExternalLabel(parentGfx, element) {
    var box = {
      width: 90,
      height: 30,
      x: element.width / 2 + element.x,
      y: element.height / 2 + element.y,
    };

    return renderLabel(parentGfx, getLabel(element), {
      box: box,
      fitBox: true,
      style: assign({}, textRenderer.getExternalStyle(), {
        fill: getTextColor(element, defaultStrokeColor),
      }),
    });
  }

  function renderViewContainerLabel(parentGfx, text, element) {
    var textBox = renderLabel(parentGfx, text, {
      box: {
        height: 30,
        width: element.width,
      },
      align: 'left-middle',
      style: {
        fill: getTextColor(element, defaultStrokeColor),
      },
    });

    var boxUnderline = [
      {
        x: 0,
        y: 30,
      },
      {
        x: element.width,
        y: 30,
      },
    ];

    drawLine(parentGfx, boxUnderline, {
      stroke: getStrokeColor(element, defaultStrokeColor),
    });
    transform(textBox, TEXT_BOUNDS_PADDING, 0, 0, 0);
  }

  function createPathFromWaypoints(waypoints) {
    var pathData = 'm  ' + waypoints[0].x + ',' + waypoints[0].y;
    for (var i = 1; i < waypoints.length; i++) {
      pathData += 'L' + waypoints[i].x + ',' + waypoints[i].y + ' ';
    }
    return pathData;
  }

  function createPathFromConnection(connection) {
    var waypoints = connection.waypoints;
    return createPathFromWaypoints(waypoints);
  }

  function createPathBetweenShapes(shape1, shape2) {
    var left = function (shape) {
      return {
        x: shape.x,
        y: shape.y + Math.round(shape.height / 2),
      };
    };
    var bottom = function (shape) {
      return {
        x: shape.x + Math.round(shape.width / 2),
        y: shape.y + shape.height,
      };
    };
    var right = function (shape) {
      return {
        x: shape.x + shape.width,
        y: shape.y + Math.round(shape.height / 2),
      };
    };
    var top = function (shape) {
      return {
        x: shape.x + Math.round(shape.width / 2),
        y: shape.y,
      };
    };
    var center = function (shape) {
      return {
        x: shape.x + Math.round(shape.width / 2),
        y: shape.y + Math.round(shape.height / 2),
      };
    };
    var center1 = center(shape1);
    var center2 = center(shape2);
    var dX = center2.x - center1.x;
    var dY = center2.y - center1.y;
    var radians = Math.atan2(dY, dX);
    var degrees = (radians * 180) / Math.PI;
    degrees = (degrees + 360) % 360;

    var waypoints = null;

    if ((degrees >= 0 && degrees <= 45) || degrees > 315) {
      waypoints = [right(shape1), left(shape2)];
    } else if (degrees > 45 && degrees <= 135) {
      waypoints = [bottom(shape1), top(shape2)];
    } else if (degrees > 135 && degrees <= 225) {
      waypoints = [left(shape1), right(shape2)];
    } else if (degrees > 225 && degrees <= 315) {
      waypoints = [top(shape1), bottom(shape2)];
    }

    return waypoints;
  }

  function extendConnectionRelativeToShape(shape1, shape2) {
    shape1 = {
      x: shape1.x - shape2.x,
      y: shape1.y - shape2.y,
      width: shape1.width,
      height: shape1.height,
    };
    shape2 = {
      x: 0,
      y: 0,
      width: shape2.width,
      height: shape2.height,
    };
    return createPathBetweenShapes(shape1, shape2);
  }

  function getMiddle(point1, point2) {
    return {
      x: point1.x + Math.round((point2.x - point1.x) / 2),
      y: point1.y + Math.round((point2.y - point1.y) / 2),
    };
  }

  function getConnectionMid(waypoints) {
    var lastX = waypoints[0].x;
    var lastY = waypoints[0].y;
    var length = 0;
    var waypointLengths = [];
    waypoints.forEach(function (waypoint) {
      var x = waypoint.x - lastX;
      var y = waypoint.y - lastY;
      var l = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
      length += l;
      waypointLengths.push(length);
      lastX = waypoint.x;
      lastY = waypoint.y;
    });
    var goalLength = Math.round(length / 2);

    for (var i in waypointLengths) {
      var l = waypointLengths[i];
      if (l > goalLength) {
        return getMiddle(waypoints[i - 1], waypoints[i]);
      }
    }
  }

  function extendConnectionRelativeToConnection(waypoints, shape) {
    var cMid = getConnectionMid(waypoints);

    var point = {
      x: -shape.x + cMid.x,
      y: -shape.y + cMid.y,
      width: 0,
      height: 0,
    };

    shape = {
      x: 0,
      y: 0,
      width: shape.width,
      height: shape.height,
    };

    return createPathBetweenShapes(point, shape);
  }

  function drawContainedExternalElementConnection(
    parentGfx,
    element,
    lineOptions
  ) {
    var connectionPath;
    var parentElement = element.parent;

    if (is(parentElement, 'ifml:InteractionFlow')) {
      connectionPath = extendConnectionRelativeToConnection(
        parentElement.waypoints,
        element
      );
    } else {
      connectionPath = extendConnectionRelativeToShape(parentElement, element);
    }

    var defaultLineOptions = {
      strokeDasharray: '10, 12',
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeWidth: '1.5px',
      stroke: getConnectionColor(element, defaultConnectionColor),
    };

    var lineAttrs = assign(defaultLineOptions, lineOptions);

    return drawLine(parentGfx, connectionPath, lineAttrs);
  }

  function markAsInnerViewComponentPart(gfx) {
    svgClasses(gfx.parentElement).add('innerViewComponentPart');
  }

  function drawWhiteBox(parentGfx, element, attrs, type, content) {
    var rect = drawRect(
      parentGfx,
      element.width,
      element.height,
      0,
      0,
      assign(
        {
          fill: getFillColor(element, 'white'),
          fillOpacity: 1,
          stroke: getStrokeColor(element, 'gray'),
        },
        attrs
      )
    );

    var text = '«' + type + '» ' + content;
    renderLabel(parentGfx, text, {
      align: 'left-top',
      padding: {
        top: 8,
        left: 10,
      },
      box: {
        width: element.width,
      },
    });

    return rect;
  }

  function drawExpressionBox(parentGfx, element, attrs, type) {
    var expression = getSemantic(element);
    var content = 'undefined';
    if (expression && expression.body) {
      content = expression.body;
    }
    return drawWhiteBox(parentGfx, element, attrs, type, content);
  }

  var handlers = (this.handlers = {
    'ifml:Event': function (parentGfx, element, attrs) {
      let r = drawCircle(parentGfx, element.width, element.height, attrs);
      renderEventContent(element, parentGfx);
      return r;
    },
    'ifml:ViewElementEvent': as('ifml:Event'),
    'ifml:ActionEvent': as('ifml:Event'),

    // 'ifml:SystemEvent': as('ifml:Event'),
    'ifml:ViewComponent': function (parentGfx, element, attrs) {
      attrs = attrs || {};
      attrs.stroke = 'gray';
      attrs.fill = getFillColor(element, 'lightgray');
      var bo = getSemantic(element);

      if (!('fillOpacity' in attrs)) {
        attrs.fillOpacity = DEFAULT_FILL_OPACITY;
      }
      var rect = drawRect(
        parentGfx,
        element.width,
        element.height,
        VIEWCOMPONENT_BORDER_RADIUS,
        0,
        attrs
      );
      var prepend = '';

      if (bo.get('isModal')) {
        prepend += '« Modal » ';
      } else if (bo.get('isNewWindow')) {
        prepend += '« Modeless » ';
      } else if (bo.$type.split) {
        prepend += '«' + bo.$type.split(':')[1] + '» ';
      }

      var text = prepend + bo.name;
      renderEmbeddedLabel(parentGfx, element, 'center-top', text);

      return rect;
    },
    'ifml:ViewElement': function (parentGfx, element, attrs) {
      attrs = attrs || {};

      if (!('fillOpacity' in attrs)) {
        attrs.fillOpacity = DEFAULT_FILL_OPACITY;
      }

      return drawRect(
        parentGfx,
        element.width,
        element.height,
        VIEWCOMPONENT_BORDER_RADIUS,
        0,
        attrs
      );
    },
    'ifml:ViewContainer': function (parentGfx, element, attrs) {
      var rect = drawRect(
        parentGfx,
        element.width,
        element.height,
        0,
        0,
        assign(
          {
            fill: getFillColor(element, defaultFillColor),
            fillOpacity: DEFAULT_FILL_OPACITY,
            stroke: getStrokeColor(element, defaultStrokeColor),
          },
          attrs
        )
      );

      var semantic = getSemantic(element);
      var text = semantic.name ? semantic.name : 'data';

      var prepend = '';
      prepend += semantic.isXOR ? '[XOR] ' : '';
      prepend += semantic.isDefault ? '[D] ' : '';
      prepend += semantic.isLandmark ? '[L] ' : '';

      if (semantic.$type.split) {
        prepend += '«' + semantic.$type.split(':')[1] + '» ';
      }
      text = prepend + text;

      renderViewContainerLabel(parentGfx, text, element);

      return rect;
    },
    'ifml:DataBinding': function (parentGfx, element, attrs) {
      var dataBinding = getSemantic(element);
      var content = 'undefined';
      if (
        dataBinding &&
        dataBinding.domainConcept &&
        dataBinding.domainConcept.classifier
      ) {
        content = dataBinding.domainConcept.classifier.name;
      }
      markAsInnerViewComponentPart(parentGfx);
      return drawWhiteBox(parentGfx, element, attrs, 'DataBinding', content);
    },
    'ifml:DynamicBehavior': function (parentGfx, element, attrs) {
      var dynBeh = getSemantic(element);
      var behavior = dynBeh.get('behaviorConcept').get('behavior');
      return drawWhiteBox(
        parentGfx,
        element,
        attrs,
        behavior.language,
        behavior.body
      );
    },
    'ifml:Field': function (parentGfx, element, attrs) {
      markAsInnerViewComponentPart(parentGfx);
      return drawWhiteBox(
        parentGfx,
        element,
        attrs,
        element.type,
        getLabel(element),
      );
    },
    'ifml:VisualizationAttribute': function (parentGfx, element, attrs) {
      var vAttribute = getSemantic(element);
      var content = 'undefined';
      if (
        vAttribute &&
        vAttribute.featureConcept &&
        vAttribute.featureConcept.structuralFeature
      ) {
        content = vAttribute.featureConcept.structuralFeature.name;
      }
      markAsInnerViewComponentPart(parentGfx);
      return drawWhiteBox(
        parentGfx,
        element,
        attrs,
        'VisualizationAttributes',
        content,
      );
    },
    'ifml:Parameter': function (parentGfx, element, attrs) {
      var bo = getBusinessObject(element);
      var parent = getBusinessObject(element.parent);

      if (parent.$instanceOf('ifml:PortDefinition')) {
        renderLabel(parentGfx, bo.get('name'), {
          align: 'left-top',
          padding: {
            top: 8,
            left: 10,
          },
          box: {
            width: element.width,
          },
        });
        return;
      }
      if (bo.direction == 'in') {
        return null;
      }
      var parameter = getSemantic(element);
      markAsInnerViewComponentPart(parentGfx);
      return drawWhiteBox(
        parentGfx,
        element,
        attrs,
        'Parameter',
        parameter.name,
      );
    },
    'ifml:ConditionalExpression': function (parentGfx, element, attrs) {
      return drawExpressionBox(
        parentGfx,
        element,
        attrs,
        'ConditionalExpression'
      );
    },
    'ifml:ParameterBinding': function (parentGfx, element, attrs) {
      var parameterBinding = getSemantic(element);

      var rect = drawRect(
        parentGfx,
        element.width,
        element.height,
        0,
        0,
        assign(
          {
            // fill: getFillColor(element, "white"),
            fillOpacity: 0,
            stroke: 0,
          },
          attrs
        )
      );

      var source = parameterBinding.sourceParameter;
      var target = parameterBinding.targetParameter;

      var sourceName = source && source.name;
      var sourceType = (source && source.type && source.type.name) || '';
      var targetName = target && target.name;
      var targetType = (target && target.type && target.type.name) || '';

      var sourceStr = sourceName + (sourceType ? ': ' + sourceType : '');
      var targetStr = targetName + (targetType ? ': ' + targetType : '');

      var text = sourceStr + ' → ' + targetStr;
      renderLabel(parentGfx, text, {
        align: 'left-top',
        padding: {
          top: 0,
          left: 3,
          right: 3,
          bottom: 0,
        },
        box: {
          width: element.width,
        },
      });

      return rect;
    },
    'ifml:ActivationExpression': function (parentGfx, element, attrs) {
      var pathData = pathMap.getScaledPath('ACTIVATION_EXPRESSION_PATH', {
        xScaleFactor: 1,
        yScaleFactor: 1,
        containerWidth: element.width,
        containerHeight: element.height,
        position: {
          mx: 0,
          my: 0,
        },
      });

      var elementObject = drawPath(parentGfx, pathData, {
        fill: getFillColor(element, 'lightgray'),
        fillOpacity: DEFAULT_FILL_OPACITY,
        stroke: getStrokeColor(element, 'gray'),
      });

      drawContainedExternalElementConnection(parentGfx, element);

      var text = '«ActivationExpression»\n' + getSemantic(element).body || '';
      renderLabel(parentGfx, text, {
        box: element,
        align: 'left-top',
        padding: 5,
      });

      return elementObject;
    },
    'ifml:ParameterBindingGroup': function (parentGfx, element, attrs) {
      var pathData = pathMap.getScaledPath('PARALELOGRAMM', {
        xScaleFactor: 1,
        yScaleFactor: 1,
        containerWidth: element.width,
        containerHeight: element.height,
        position: {
          mx: 20,
          my: 0,
        },
      });

      var elementObject = drawPath(parentGfx, pathData, {
        fill: getFillColor(element, 'lightgray'),
        fillOpacity: DEFAULT_FILL_OPACITY,
        stroke: getStrokeColor(element, 'gray'),
      });

      drawContainedExternalElementConnection(parentGfx, element, {
        strokeDasharray: null,
      });

      var text = (text = '«ParameterBindingGroup»\n');
      var horizontalPadding = Math.round(element.width * 0.05) + 3;
      renderLabel(parentGfx, text, {
        box: element,
        align: 'left-top',
        padding: {
          top: 5,
          left: horizontalPadding,
          right: horizontalPadding,
          bottom: 5,
        },
      });

      return elementObject;
    },
    'ifml:ModuleDefinition': function (parentGfx, element, attrs) {
      attrs = attrs || {};
      attrs.stroke = 'gray';
      attrs.fill = getFillColor(element, 'lightgray');

      if (!('fillOpacity' in attrs)) {
        attrs.fillOpacity = DEFAULT_FILL_OPACITY;
      }
      var rect = drawRect(
        parentGfx,
        element.width,
        element.height,
        0,
        0,
        attrs
      );
      renderEmbeddedLabel(parentGfx, element, 'center-top');

      return rect;
    },
    'ifml:Module': as('ifml:ModuleDefinition'),
    'ifml:PortDefinition': function (parentGfx, element, attrs) {
      attrs = attrs || {};
      attrs.stroke = 'gray';
      var isIn = portIsInput(element);
      var background = isIn ? 'white' : 'black';
      attrs.fill = getFillColor(element, background);

      if (!('fillOpacity' in attrs)) {
        attrs.fillOpacity = DEFAULT_FILL_OPACITY;
      }
      var rect = drawRect(
        parentGfx,
        element.width,
        element.height,
        0,
        0,
        attrs
      );
      renderEmbeddedLabel(parentGfx, element, 'center-top');

      return rect;
    },
    'ifml:Port': as('ifml:PortDefinition'),
    'ifml:Action': function (parentGfx, element) {
      var attrs = {
        fill: getFillColor(element, 'lightgray'),
        fillOpacity: DEFAULT_FILL_OPACITY,
        stroke: getStrokeColor(element, defaultStrokeColor),
      };

      var path = drawHexa(parentGfx, element.width, element.height, attrs);
      renderEmbeddedLabel(parentGfx, element, 'center-middle');

      return path;
    },
    'ifml:NavigationFlow': function (parentGfx, element) {
      var pathData = createPathFromConnection(element);

      var fill = getFillColor(element, defaultFillColor),
        stroke = getConnectionColor(element, defaultConnectionColor);

      var attrs = {
        strokeLinejoin: 'round',
        markerEnd: marker('navigationflow-end', fill, stroke),
        stroke: stroke,
      };

      return drawPath(parentGfx, pathData, attrs);
    },
    'ifml:DataFlow': function (parentGfx, element) {
      var fill = getFillColor(element, defaultFillColor),
        stroke = getConnectionColor(element, defaultConnectionColor);

      var pathData = createPathFromConnection(element);

      var attrs = {
        markerEnd: marker('dataflow-end', fill, stroke),
        strokeDasharray: '10, 12',
        strokeLinecap: 'round',
        strokeLinejoin: 'round',
        strokeWidth: '1.5px',
        stroke: stroke,
      };

      var path = drawPath(parentGfx, pathData, attrs);
      return path;
    },
    label: function (parentGfx, element) {
      return renderExternalLabel(parentGfx, element);
    },
    'ifml:Annotation': function (parentGfx, element) {
      var style = {
        fill: 'none',
        stroke: 'none',
      };

      var textElement = drawRect(
        parentGfx,
        element.width,
        element.height,
        0,
        0,
        style
      );

      var textPathData = pathMap.getScaledPath('TEXT_ANNOTATION', {
        xScaleFactor: 1,
        yScaleFactor: 1,
        containerWidth: element.width,
        containerHeight: element.height,
        position: {
          mx: 0.0,
          my: 0.0,
        },
      });

      drawPath(parentGfx, textPathData, {
        stroke: getStrokeColor(element, defaultStrokeColor),
      });

      var text = getSemantic(element).text || '';
      renderLabel(parentGfx, text, {
        box: element,
        align: 'left-top',
        padding: 5,
        style: {
          fill: getStrokeColor(element, defaultStrokeColor),
        },
      });
      drawContainedExternalElementConnection(parentGfx, element);

      return textElement;
    },
    'ifml:OnSelectEvent': function (parentGfx, element) {
      var pathData = pathMap.getScaledPath('EVENT_SELECT', {
        xScaleFactor: 1,
        yScaleFactor: 1,
        containerWidth: element.width,
        containerHeight: element.height,
        position: {
          mx: 0.27,
          my: 0.5,
        },
      });

      return drawPath(parentGfx, pathData, {
        strokeWidth: 1,
        stroke: getStrokeColor(element, defaultStrokeColor),
        fill: getStrokeColor(element, defaultStrokeColor),
      });
    },
    'ifml:OnSubmitEvent': function (parentGfx, element) {
      var pathData = pathMap.getScaledPath('EVENT_SUBMIT', {
        xScaleFactor: 0.9,
        yScaleFactor: 0.9,
        containerWidth: element.width,
        containerHeight: element.height,
        position: {
          mx: 0.4,
          my: 0.4,
        },
      });

      return drawPath(parentGfx, pathData, {
        strokeWidth: 1,
        stroke: getStrokeColor(element, defaultStrokeColor),
        fill: getStrokeColor(element, defaultStrokeColor),
      });
    },
  });
  // extension API, use at your own risk
  this._drawPath = drawPath;
}

inherits(IfmlRenderer, BaseRenderer);

IfmlRenderer.$inject = [
  'config.ifmlRenderer',
  'eventBus',
  'styles',
  'pathMap',
  'canvas',
  'textRenderer',
];

IfmlRenderer.prototype.canRender = function (element) {
  return isAny(element, [
    'ifml:Annotation',
    'ifml:InteractionFlowModelElement',
    'ifml:DomainElement',
  ]);
};

IfmlRenderer.prototype.drawShape = function (parentGfx, element) {
  var drawType = element.type;

  if (drawType != 'label') {
    if (element.businessObject.$instanceOf('ifml:Event')) {
      drawType = 'ifml:Event';
    } else if (element.businessObject.$instanceOf('ifml:Field')) {
      drawType = 'ifml:Field';
    }
    if (!(drawType in this.handlers)) {
      if (element.businessObject.$instanceOf('ifml:ViewContainer')) {
        drawType = 'ifml:ViewContainer';
      }
      if (element.businessObject.$instanceOf('ifml:ViewComponent')) {
        drawType = 'ifml:ViewComponent';
      }
    }
  }
  var h = this.handlers[drawType];

  if (!h) {
    throw Error('handler for ' + drawType + ' is not defined!');
  }

  /* jshint -W040 */
  return h(parentGfx, element);
};

IfmlRenderer.prototype.drawConnection = function (parentGfx, element) {
  var type = element.type;
  var h = this.handlers[type];

  /* jshint -W040 */
  return h(parentGfx, element);
};
