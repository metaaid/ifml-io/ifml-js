FROM node:alpine3.16

WORKDIR /app

ENV CHROME_BIN='/usr/bin/chromium-browser'
ENV DISPLAY=:99.0

COPY lib lib
COPY test test
COPY tasks tasks
COPY styles styles
COPY resources resources
# COPY index.js index.js
COPY package.json package.json
# COPY rollup.config.js rollup.config.js
# COPY webpack.config.js webpack.config.js
COPY .yarnrc.yml .yarnrc.yml
COPY *.js /app
COPY .npmignore .npmignore
COPY entrypoint.sh entrypoint.sh

RUN apk upgrade --no-cache --available \
    && apk add chromium firefox xvfb xorg-server \
    && corepack enable \
    && yarn config set enableImmutableInstalls 0 \
    && yarn plugin import version \
    && yarn remove ifml-moddle ifml-font \
    && yarn add ifml-moddle ifml-font \
    && yarn install \
    && chmod +x ./entrypoint.sh \
    && ls -al \
    && cat package.json


ENTRYPOINT [ "./entrypoint.sh" ]