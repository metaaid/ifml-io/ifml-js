const CopyWebpackPlugin = require('copy-webpack-plugin');
const PnpWebpackPlugin = require(`pnp-webpack-plugin`);
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

var dest = './dist/';
var src = './lib/';
var assetPath = './assets/';


module.exports = {
  experiments: {
    layers: true,
  },
  entry: {
    viewer: { import: src + 'Viewer.js', filename: 'ifml-viewer.js'},
    navViewer: { 
      import: src + 'NavigatedViewer.js', 
      filename: 'ifml-navigated-viewer.js',
    },
    modeler: { import: src + 'Modeler.js', filename: 'ifml-modeler.js', layer:"IfmlJS"},
    css: { import: './styles/app.less', filename: 'tmp/app.css'},
  },
  output: {
    libraryTarget: 'umd',
  },
  module: {
    rules: [{
        test: /\.ifml/,
        use: 'raw-loader'
      }, {
        test: /\.less$/i,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "less-loader",
        ],
      }, 
    ],
  },
  
  resolveLoader: {
    plugins: [PnpWebpackPlugin.moduleLoader(module)],
  },
  resolve: {
    // support reading TypeScript and JavaScript files, 📖 -> https://github.com/TypeStrong/ts-loader
    extensions: ['.js'],
    plugins: [PnpWebpackPlugin],
  },
  // devServer: {
  //   static: {
  //     directory: path.join(__dirname, 'dist'),
  //   },
  //   compress: true,
  //   port: 9000,
  // },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'assets/app.css'
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: require.resolve("diagram-js/assets/diagram-js.css"),
          to: assetPath,
        },
        {
          from: require.resolve("ifml-font/dist/css/ifml-font.css"),
          to: assetPath + "ifml-font/css",
        },
        {
          from: require.resolve("ifml-font/dist/css/ifml-font-embedded.css"),
          to: assetPath + "ifml-font/css",
        },
        {
          from: require.resolve("ifml-font/dist/font/ifml-font.eot"),
          to: assetPath + "ifml-font/font",
        },
        {
          from: require.resolve("ifml-font/dist/font/ifml-font.svg"),
          to: assetPath + "ifml-font/font",
        },
        {
          from: require.resolve("ifml-font/dist/font/ifml-font.ttf"),
          to: assetPath + "ifml-font/font",
        },
        {
          from: require.resolve("ifml-font/dist/font/ifml-font.woff"),
          to: assetPath + "ifml-font/font",
        },
        {
          from: require.resolve("ifml-font/dist/font/ifml-font.woff2"),
          to: assetPath + "ifml-font/font",
        },
        
      ]
    })
  ],
  mode: 'development',
  devtool: 'source-map'
};